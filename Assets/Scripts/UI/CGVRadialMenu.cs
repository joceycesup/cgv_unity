﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using CGV.Player;

#pragma warning disable 649
namespace CGV.UI {

	[Serializable]
	public class CGVRadialMenuOption {
		public string description;
		public UnityAction selectEvent;

		public CGVRadialMenuOption (string desc, UnityAction select) {
			description = desc;
			selectEvent = select;
		}
	}

	[RequireComponent (typeof (CanvasGroup))]
	public class CGVRadialMenu : MonoBehaviour {

		private CanvasGroup Group;
		[SerializeField] private Image Background;
		[SerializeField] private Image HighlightSection;
		[SerializeField] private Transform Outline;
		[SerializeField] private Color DefaultColor = Color.gray;
		[SerializeField] private Color HighlightColor = Color.red;

		[SerializeField]
		private TMPro.TextMeshProUGUI DefaultOptionText;
		private Image DefaultOptionTextParent;
		[SerializeField]
		private List<TMPro.TextMeshProUGUI> OptionsText;
		private List<Image> OptionsTextParent;

		private CGVPlayerController Player;

		[Header ("WWise Events")]
		[SerializeField] protected AK.Wwise.Event OpenMenuEvent;
		[SerializeField] protected AK.Wwise.Event CloseMenuEvent;

		private int SectionCount = 8;
		private float SectionAngle = 45f;
		private float SectionAngleHalf = 22.5f;

		private int LastSection = 0;

		[SerializeField]
		[CGVSquareValue]
		private float MinSqrMagnitude = 0.1f;

		[Tooltip ("Time between the moment the stick is released and the menu is hidden. Prevents unexpected behaviour when closing the menu at the same time the stick is released.")]
		[SerializeField]
		[Min (0f)]
		private float DeadzoneDelay = 0.05f;

		private float LastSqrMagnitude;
		private int LastSectionBeforeDeadzone = -1;
		private float LastSectionTime;

		private List<CGVRadialMenuOption> Options = new List<CGVRadialMenuOption> (8);
		private CGVRadialMenuOption DeadzoneOption;

		protected bool _Hidden = true;
		public bool Hidden { get { return _Hidden; } protected set { _Hidden = value; } }

		void Awake () {
			//Debug.Log ("Radial menu awake");
			Player = GetComponentInParent<CGVPlayerController> ();
			Group = GetComponent<CanvasGroup> ();
			Background.color = DefaultColor;
			HighlightSection.color = HighlightColor;

			DefaultOptionTextParent = DefaultOptionText.transform.parent.GetComponent<Image> ();
			DefaultOptionTextParent.color = DefaultColor;
			OptionsTextParent = new List<Image> (OptionsText.Count);
			for (int i = 0; i < OptionsText.Count; ++i) {
				OptionsTextParent.Add (OptionsText[i].transform.parent.GetComponent<Image> ());
				OptionsTextParent[i].color = DefaultColor;
			}

			Hide ();
		}

		void Update () {
			if (Hidden)
				return;
			//SetHighlightVector (new Vector2 (-Input.GetAxisRaw ("Horizontal"), Input.GetAxisRaw ("Vertical")));
			SetHighlightVector (Player.PlayerActions.Move, true);
		}

		public bool Show () {
			if (SectionCount <= 0)
				return false;
			if (!Hidden)
				return false;

			SetCurrentSection (-1);
			LastSectionBeforeDeadzone = -1;

			Hidden = false;
			Group.alpha = 1f;
			Group.interactable = true;
			Group.blocksRaycasts = true;

			OpenMenuEvent?.Post (gameObject);

			return true;
		}

		public void Hide () {
			if (Hidden)
				return;
			/*
			if (LastSqrMagnitude < MinSqrMagnitude && LastSectionBeforeDeadzone >= 0) {
				Debug.Log ("---------------------------------");
				Debug.Log (LastSectionBeforeDeadzone);
				Debug.Log (LastSqrMagnitude);
				Debug.Log ((Time.unscaledTime - LastSectionTime).ToString ("0.00000"));
			}//*/
			if (LastSqrMagnitude < MinSqrMagnitude && LastSectionBeforeDeadzone >= 0 && (Time.unscaledTime - LastSectionTime) < DeadzoneDelay)
				LastSection = LastSectionBeforeDeadzone;

			LastSectionBeforeDeadzone = -1;
			Hidden = true;
			Group.alpha = 0f;
			Group.interactable = false;
			Group.blocksRaycasts = false;

			CloseMenuEvent?.Post (gameObject);
			/*
			if (lastSqrMagnitude >= 0.1 && _Options != null && lastSection < _Options.Count)
				_Options[lastSection].selectEvent.Invoke ();/*/
			if (LastSection >= 0 && Options != null && LastSection < Options.Count)
				Options[LastSection].selectEvent.Invoke ();
			else if (DeadzoneOption != null)
				DeadzoneOption.selectEvent.Invoke ();
			//*/
		}

		public void Toggle () {
			if (Hidden)
				Show ();
			else
				Hide ();
		}
		/*
		public void SetOptions (List<CGVRadialMenuOption> options) {
			if (options == null)
				return;
			_Options = options;
			SetSectionCount (_Options.Count);
		}//*/

		public void SetOptions (IEnumerable<CGVRadialMenuOption> options, CGVRadialMenuOption deadzoneOption = null) {
			if (options == null)
				return;
			Options.Clear ();
			Options.AddRange (options);
			SetSectionCount (Options.Count);
			//Debug.LogFormat ("Set Options ({0})", deadzoneOption);
			DeadzoneOption = deadzoneOption;
			DefaultOptionText.transform.parent.gameObject.SetActive (DeadzoneOption != null);
			if (DeadzoneOption != null)
				DefaultOptionText.SetText (DeadzoneOption.description);
		}

		protected void SetSectionCount (int count) {
			if (SectionCount == count || count <= 0)
				return;
			SectionCount = Mathf.Min (count, 8);
			SectionAngle = 360f / SectionCount;
			SectionAngleHalf = 180f / SectionCount;

			HighlightSection.fillAmount = 1f / SectionCount;
#if UNITY_EDITOR
			if (CGVScene.IsFirstLoadedScene && OptionsTextParent == null) {
				Debug.Log ("Radial Menu not initialized yet");
				DefaultOptionTextParent = DefaultOptionText.transform.parent.GetComponent<Image> ();
				DefaultOptionTextParent.color = DefaultColor;
				OptionsTextParent = new List<Image> (OptionsText.Count);
				for (int i = 0; i < OptionsText.Count; ++i) {
					OptionsTextParent.Add (OptionsText[i].transform.parent.GetComponent<Image> ());
					OptionsTextParent[i].color = DefaultColor;
				}
			}
#endif
			for (int i = 0; i < 8; ++i) {
				Transform s = Outline.GetChild (i);
				if (i < SectionCount) {
					s.gameObject.SetActive (true);
					s.localRotation = Quaternion.Euler (0f, 0f, -SectionMiddleAngle (i) - SectionAngleHalf);
					OptionsTextParent[i].gameObject.SetActive (true);
					OptionsText[i].SetText (Options[i].description);
				}
				else {
					s.gameObject.SetActive (false);
					OptionsTextParent[i].gameObject.SetActive (false);
				}
			}

			if (LastSection >= SectionCount) {
				LastSection = 0;
			}
			SetCurrentSection (-1);
		}

		protected void SetCurrentSection (int section) {
			if (LastSection < 0)
				DefaultOptionTextParent.color = DefaultColor;
			else
				OptionsTextParent[LastSection].color = DefaultColor;

			if (section < 0) {
				HighlightSection.fillAmount = 0f;
				DefaultOptionTextParent.color = HighlightColor;
				UAP_AccessibilityManager.StopSpeaking ();
			}
			else {
				HighlightSection.fillAmount = 1f / SectionCount;
				HighlightSection.rectTransform.localRotation = Quaternion.Euler (0f, 0f, -SectionMiddleAngle (section) - SectionAngleHalf);
				//Debug.Log (UAP_AccessibilityManager.IsSpeaking ());
				UAP_AccessibilityManager.StopSpeaking ();
				UAP_AccessibilityManager.SaySkippable (Options[section].description);

				OptionsTextParent[section].color = HighlightColor;
				//Debug.Log (Options[section].description);
			}
			LastSection = section;
		}

		public void SetHighlightVector (Vector2 v, bool invertX = false, bool invertY = false) {
			if (SectionCount <= 0)
				return;
			int section = -1;

			if (invertX)
				v.x = -v.x;
			if (invertY)
				v.y = -v.y;

			if (v.sqrMagnitude >= MinSqrMagnitude) {
				float angle = Vector2.SignedAngle (Vector2.up, v);
				if (angle < 0f) angle = 360f + angle;

				section = (int)((angle + SectionAngleHalf) / SectionAngle);

				if (section >= SectionCount)
					section = 0;
				else if (section != LastSection && LastSection >= 0) {
					if (JuneCodes.Maths.JCMaths.DeltaAngle (SectionMiddleAngle (LastSection), angle) <= (SectionAngleHalf + 5f))
						section = LastSection;
				}
				LastSectionTime = Time.unscaledTime;
			}
			else {
				if (LastSection >= 0)
					LastSectionBeforeDeadzone = LastSection;
				if (Mathf.Approximately (LastSqrMagnitude, v.sqrMagnitude) && Mathf.Approximately (v.sqrMagnitude, 0f))
					section = LastSection;
			}

			if (section != LastSection)
				SetCurrentSection (section);

			LastSqrMagnitude = v.sqrMagnitude;
		}

		private float SectionMiddleAngle (int section) {
			return section * SectionAngle;
		}

		public void SwitchOption (bool next) {
			if (LastSectionBeforeDeadzone < 0) {
				if (next)
					SetCurrentSection (0);
				else
					SetCurrentSection (SectionCount - 1);
			}
			else
				SetCurrentSection (LoopIndex (LastSectionBeforeDeadzone + (next ? 1 : -1), SectionCount));
		}

		int LoopIndex (int index, int count) {
			if (count <= 0)
				return 0;
			int res = index % count;
			return res < 0 ? res + count : res;
		}
	}
}
