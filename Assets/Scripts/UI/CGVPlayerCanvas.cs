﻿using System;
using UnityEngine;

#pragma warning disable 649
namespace CGV.UI {
	[RequireComponent (typeof (AccessibleUIGroupRoot))]
	public class CGVPlayerCanvas : MonoBehaviour {

		protected AccessibleUIGroupRoot _Accessibility;
		public AccessibleUIGroupRoot Accessibility {
			get {
				if (_Accessibility == null)
					_Accessibility = GetComponent<AccessibleUIGroupRoot> ();
				return _Accessibility;
			}
		}

		protected Canvas _Canvas;
		public Canvas Canvas {
			get {
				if (_Canvas == null)
					_Canvas = GetComponent<Canvas> ();
				return _Canvas;
			}
		}

		private void Awake () {
			SetPanelsVisibility ((Panel)(-1), false);
		}

		[SerializeField]
		protected RectTransform _Container;
		public RectTransform Container { get { return _Container; } }

		[SerializeField]
		protected CanvasGroup _Crosshair;

		[SerializeField]
		protected CanvasGroup _Died;

		[SerializeField]
		protected CGVRadialMenu _RadialMenu;
		public CGVRadialMenu RadialMenu { get { return _RadialMenu; } }

		[SerializeField]
		protected CGVPlayerStartGameMenu _StartGameMenu;
		public CGVPlayerStartGameMenu StartGameMenu { get { return _StartGameMenu; } }

		[SerializeField]
		protected CGVScoreBoard _ScoreBoard;
		public CGVScoreBoard ScoreBoard { get { return _ScoreBoard; } }

		[Flags]
		public enum Panel {
			Crosshair = 0x0001,
			Death = 0x0002,
			StartGame = 0x0004,
			ScoreBoard = 0x0008,
		}

		void OnEnable () {
			CGVScene.OnSceneChanged += SceneChanged;
			CGVPlayableSceneManager.OnSceneReady += HideOnlineMenu;
			if (CGVPlayableSceneManager.IsReady)
				HideOnlineMenu ();
		}

		void OnDisable () {
			CGVScene.OnSceneChanged -= SceneChanged;
			CGVPlayableSceneManager.OnSceneReady -= HideOnlineMenu;
		}

		private void SceneChanged (CGVScene scene) {
			/*
			SetPanelVisibility (Panel.OnlineMenu,
				scene.IsPlayable &&
				CGVNetworkManager.IsOnline);//*/
			SetPanelVisibility (Panel.StartGame,
				scene.IsPlayable &&
				CGVGameSettings.instance.Multiplayer != CGVGameSettings.MultiplayerMode.Solo);
		}

		public void SetAccessible (bool accessible) {
			Accessibility.enabled = accessible;
		}

		public void HideOnlineMenu () {
			//SetPanelVisibility (Panel.OnlineMenu, false);
		}

		private void SetVisibility (CanvasGroup group, bool visible) {
			group.alpha = visible ? 1f : 0f;
			group.gameObject.SetActive (visible);
		}

		private Panel _NonePanel = (Panel)0;

		public void SetPanelsVisibility (Panel panels, bool visible) {
			if (Enum.IsDefined (typeof (Panel), panels)) {
				SetPanelVisibility (panels, visible);
				return;
			}
			foreach (Panel panel in Enum.GetValues (typeof (Panel))) {
				if (panels == _NonePanel)
					break;
				if (panels.HasFlag (panel)) {
					panels = (Panel)(panels & ~panel);
					SetPanelVisibility (panel, visible);
				}
			}
		}

		private void SetPanelVisibility (Panel panel, bool visible) {
			switch (panel) {
				case Panel.Crosshair:
					SetVisibility (_Crosshair, visible);
					break;
				case Panel.Death:
					SetVisibility (_Died, visible);
					break;
				case Panel.StartGame:
					_StartGameMenu.gameObject.SetActive (visible);
					break;
				case Panel.ScoreBoard:
					_ScoreBoard.gameObject.SetActive (visible);
					break;
			}
		}
	}
}
