﻿using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 649
namespace CGV.UI {
	[RequireComponent (typeof (Button))]
	public class CGVButtonEventWrapper : MonoBehaviour {

		private Button _Button;

		[SerializeField]
		private UltEvents.UltEvent clickEvent;

		void OnEnable () {
			_Button = GetComponent<Button> ();
			_Button.onClick.AddListener (() => {
				clickEvent.Invoke ();
			});
		}
	}
}
