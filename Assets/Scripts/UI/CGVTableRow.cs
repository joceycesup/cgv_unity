﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGV.UI {
	/// <summary>
	/// Template component for a %UI table row.
	/// </summary>
	public class CGVTableRow : MonoBehaviour {

		[SerializeField] protected CGVTableCell _CellPrefab;
		[SerializeField] protected Transform _CellsContainer;

		[SerializeField] protected TMPro.TextMeshProUGUI _Description;

		protected CGVTable _Table;
		protected object _RowObject;

		protected Func<object, int, GUIContent> _CellContentCallback;

		protected List<CGVTableCell> _Cells = new List<CGVTableCell> ();

		public void Init (CGVTable table, object rowObject, string description, Func<object, int, GUIContent> cellContentCallback) {
			if (_Table != null)
				return;
			_Table = table;
			_RowObject = rowObject;
			_CellContentCallback = cellContentCallback;
			_Description.SetText (description);
			for (int c = _Table.ColumnCount - 1; c >= 0; --c) {
				UpdateCell (c);
			}
		}

		public void UpdateCell (int column) {
			SetCellContent (column, _CellContentCallback (_RowObject, column));
		}

		public void UpdateRow () {
			for (int c = 0; c < _Cells.Count; ++c) {
				UpdateCell (c);
			}
		}

		public void SetCellContent (int column, GUIContent content) {
			if (column < 0)
				return;
			for (int i = _Cells.Count; i <= column; ++i) {
				_Cells.Add (Instantiate (_CellPrefab.gameObject, _CellsContainer).GetComponent<CGVTableCell> ());
			}
			_Cells[column].SetContent (content);
		}

		public void SetRowContent (IEnumerable<GUIContent> rowContent) {
			int i = 0;
			foreach (var content in rowContent) {
				SetCellContent (i, content);
				i++;
			}
		}
	}
}
