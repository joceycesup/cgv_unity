﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using CGV.Player;

namespace CGV.UI {
	/// <summary>
	/// %UI table that automatically fills the score for every team at the end of a game.
	/// </summary>
	public class CGVScoreBoard : CGVTable {

		public override void InitTable (out List<GUIContent> headerContent) {
			headerContent = new List<GUIContent> () {
				new GUIContent("Kills"),
				new GUIContent("Deaths"),
			};
		}

		protected override IEnumerable<object> RowObjects () {
			return CGVPlayableSceneManager.Teams.Cast<object> ();
		}

		protected override string RowDescription (object rowObject) {
			int teamID = (int)rowObject;
			CGVTeamValue team = CGVTeamEnum.GetValue (teamID, true);
			return team == null ? "Unknown" : team.Name;
		}

		protected override GUIContent CellContent (object rowObject, int column) {
			int teamID = (int)rowObject;
			switch (column) {
				case 0:
					return new GUIContent (CGVPlayableSceneManager.GetTeamKills (teamID).ToString ());
				case 1:
					return new GUIContent (CGVPlayableSceneManager.GetTeamDeaths (teamID).ToString ());
			}
			return GUIContent.none;
		}
	}
}
