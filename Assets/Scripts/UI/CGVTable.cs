﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CGV.UI {
	/// <summary>
	/// Base class for a %UI table.
	/// It fills the column headers and rows using the provided prefab and reimplemented callbacks.
	/// </summary>
	public abstract class CGVTable : MonoBehaviour {

		[SerializeField] protected CGVTableRow _RowPrefab;
		[SerializeField] protected Transform _RowsContainer;

		protected CGVTableRow _HeaderRow;
		private List<GUIContent> _HeaderContent;

		protected Dictionary<object, CGVTableRow> _Rows = new Dictionary<object, CGVTableRow> ();

		public int ColumnCount { get { return _HeaderContent.Count; } }

		protected List<CGVTableCell> _Cells = new List<CGVTableCell> ();

		void Awake () {
			InitTable (out _HeaderContent);
			_HeaderRow = Instantiate (_RowPrefab.gameObject, _RowsContainer).GetComponent<CGVTableRow> ();
			_HeaderRow.Init (this, null, "", UpdateHeaderCell);
			_HeaderRow.SetRowContent (_HeaderContent);
		}

		private void OnEnable () {
			RefreshTable ();
		}

		public void RefreshTable () {
			ClearTable ();
			foreach (var rowObject in RowObjects ()) {
				UpdateRow (rowObject);
			}
		}

		private GUIContent UpdateHeaderCell (object rowObject, int column) {
			return _HeaderContent[column];
		}

		public abstract void InitTable (out List<GUIContent> headerContent);

		protected abstract IEnumerable<object> RowObjects ();
		protected abstract string RowDescription (object rowObject);
		protected abstract GUIContent CellContent (object rowObject, int column);

		protected GUIContent UpdateCellContent (object rowObject, int column) {
			GUIContent content = CellContent (rowObject, column);
			if (string.IsNullOrEmpty (content.tooltip)) {
				content.tooltip = string.Format ("{0} {1} : {2}", RowDescription (rowObject), ColumnDescription (column), content.text);
			}
			return content;
		}

		public string ColumnDescription (int column) {
			if (column < 0 || column >= _HeaderContent.Count)
				return null;
			return _HeaderContent[column].text;
		}

		public void ClearTable () {
			foreach (var row in _Rows) {
				Destroy (row.Value.gameObject);
			}
			_Rows.Clear ();
		}

		public void UpdateRows () {
			foreach (var row in _Rows.Keys) {
				UpdateRow (row);
			}
		}

		public void UpdateRow (object rowObject) {
			_Rows.TryGetValue (rowObject, out CGVTableRow row);
			if (row == null) {
				row = Instantiate (_RowPrefab.gameObject, _RowsContainer).GetComponent<CGVTableRow> ();
				row.Init (this, rowObject, RowDescription (rowObject), UpdateCellContent);

				_Rows[rowObject] = row;

				MarkLayoutForRebuild ();
			}
			row.UpdateRow ();
		}

		private bool _WillRebuildLayout = false;

		protected void MarkLayoutForRebuild () {
			if (_WillRebuildLayout)
				return;
			StartCoroutine (WillRebuildLayout ());
		}

		private IEnumerator WillRebuildLayout () {
			_WillRebuildLayout = true;
			yield return null;
			LayoutRebuilder.ForceRebuildLayoutImmediate (transform as RectTransform);
			_WillRebuildLayout = false;
		}
	}
}
