﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGV.UI {
	/// <summary>
	/// Template component for a %UI table cell.
	/// </summary>
	public class CGVTableCell : MonoBehaviour {

		[SerializeField] protected TMPro.TextMeshProUGUI _Text;

		public void SetContent (GUIContent content) {
			_Text.SetText (content.text);
			AccessibleLabel label = GetComponent<AccessibleLabel> ();
			if (label != null && !string.IsNullOrEmpty (content.tooltip)) {
				label.m_TryToReadLabel = false;
				label.m_Text = content.tooltip;
			}
		}
	}
}
