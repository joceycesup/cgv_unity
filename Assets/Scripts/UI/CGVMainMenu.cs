﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 649
namespace CGV.UI {
	public class CGVMainMenu : MonoBehaviour {

		[SerializeField] private RectTransform _RootPanel;
		[SerializeField] private RectTransform _OnlinePanel;
		[SerializeField] private RectTransform _LocalPanel;
		private List<RectTransform> _PanelHistory = new List<RectTransform> (3);
		private int _HistorySize = 0;

		void Start () {
			_PanelHistory.Add (_RootPanel);
		}

		public void GoToPreviousPanel () {
			if (_HistorySize <= 0)
				return;

			_PanelHistory[_HistorySize].gameObject.SetActive (false);
			_PanelHistory.RemoveAt (_HistorySize);
			_HistorySize--;
			_PanelHistory[_HistorySize].gameObject.SetActive (true);
		}

		public void OpenOnlinePanel () {
			CGVGameSettings.instance.Multiplayer = CGVGameSettings.MultiplayerMode.Online;
			OpenPanel (_OnlinePanel);
		}

		public void OpenLocalPanel () {
			CGVGameSettings.instance.Multiplayer = CGVGameSettings.MultiplayerMode.Local;
			OpenPanel (_LocalPanel);
		}

		public void OpenPanel (RectTransform panel) {
			_PanelHistory[_HistorySize].gameObject.SetActive (false);
			_HistorySize++;
			panel.gameObject.SetActive (true);
			_PanelHistory.Add (panel);
		}
	}
}
