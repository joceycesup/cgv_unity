﻿using System;
using System.Collections.Generic;
using UnityEngine;

using JuneCodes;

using CGV.Player;

#pragma warning disable 649
namespace CGV {
	/// <summary>
	/// This asset class references most settings used in the game.
	/// It is located at the path "Assets/Resources/GameSettings.asset"
	/// </summary>
	[CreateAssetMenu (fileName = "GameSettings.asset", menuName = "CGV/Game Settings")]
	public class CGVGameSettings : JCSingletonScriptableObject<CGVGameSettings> {

		public static Action OnSettingsUpdated;

		protected override bool KeepOldest => false;

		[Serializable]
		public enum MultiplayerMode {
			None = 0,
			Solo = 1,
			Local = 2,
			Online = 3
		}

		[Serializable]
		public enum GameMode {
			None = 0,
			TeamDeathmatch = 1,
			Deathmatch = 2
		}

		[Serializable]
		public enum TeamSelectionMode {
			Enforced = 0,
			Random = 1,
			Free = 2
		}

		[Header ("Game")]
		[SerializeField] private CGVSceneSet _SceneSet;
		public CGVSceneSet SceneSet { get { return _SceneSet; } }

		[SerializeField] private CGVPlayableCharacterSet _PlayableCharacters;
		public CGVPlayableCharacterSet PlayableCharacters { get { return _PlayableCharacters; } }

		[SerializeField]
		public MultiplayerMode Multiplayer = MultiplayerMode.None;
		[SerializeField]
		public GameMode Mode = GameMode.TeamDeathmatch;
		[Range (1, 4)]
		[SerializeField]
		private int _MaxLocalPlayers = 4;
		public int MaxLocalPlayers { get { return _MaxLocalPlayers; } set { _MaxLocalPlayers = Mathf.Max (1, value); } }
		[Min (1)]
		[SerializeField]
		private int _MaxPlayerCount = 20;
		public int MaxPlayerCount { get { return _MaxPlayerCount; } set { _MaxPlayerCount = Mathf.Max (1, value); } }
		/// <summary>
		/// Max kills during a game. If this value is negative, the game doesn't have a max kill count.
		/// </summary>
		[SerializeField]
		[NaughtyAttributes.SpecialNegativeValue ("No threshold", false)]
		private int _KillThreshold = -1;
		public int KillThreshold { get { return _KillThreshold; } set { _KillThreshold = value <= 0 ? -1 : value; } }
		/// <summary>
		/// Max duration of a game. If this value is negative, the game doesn't have a time limit.
		/// </summary>
		[SerializeField]
		[NaughtyAttributes.SpecialNegativeValue ("No max duration", false)]
		private float _GameDuration = -1f;
		public float GameDuration { get { return _GameDuration; } set { _GameDuration = value <= 0f ? -1f : value; } }
		/// <summary>
		/// Delay before a player can respawn during the game. If this value is negative, players respawn immediately.
		/// </summary>
		[SerializeField]
		[NaughtyAttributes.SpecialNegativeValue ("Immediate", "Fixed delay", false)]
		private float _RespawnDelay = -1f;
		public float RespawnDelay { get { return _RespawnDelay; } set { _RespawnDelay = value <= 0f ? -1f : value; } }
		[SerializeField]
		[NaughtyAttributes.SpecialNegativeValue ("Don't enforce team", true)]
		private int _EnforceTeamIndex = -1;
		public int EnforceTeamIndex { get { return _EnforceTeamIndex; } set { _EnforceTeamIndex = value < 0 ? -1 : value; } }

		[NaughtyAttributes.Dropdown ("_TeamSelectionChoices")]
		[NaughtyAttributes.HideIfValue (NaughtyAttributes.ComparisonOperator.GreaterThanOrEqualTo, 0, "_EnforceTeamIndex")]
		[SerializeField]
		private TeamSelectionMode _TeamSelection = TeamSelectionMode.Free;
		public TeamSelectionMode TeamSelection {
			get { return _EnforceTeamIndex >= 0 ? TeamSelectionMode.Enforced : _TeamSelection; }
			set {
				_TeamSelection = value;
				if (value == TeamSelectionMode.Free)
					EnforceTeamIndex = -1;
			}
		}

		private NaughtyAttributes.DropdownList<TeamSelectionMode> _TeamSelectionChoices = new NaughtyAttributes.DropdownList<TeamSelectionMode> {
			{ "Free to choose", TeamSelectionMode.Free },
			{ "Randomly assigned", TeamSelectionMode.Random }
		};

		public bool AllowClassSelection = false;

		/// <summary>
		/// Radii for the different detection levels.
		/// </summary>
		[SerializeField]
		[NaughtyAttributes.SteppedVector]
		private Vector3 _DetectionRadius = new Vector3 (5f, 10f, 20f);
		public ref readonly Vector3 DetectionRadius { get { return ref _DetectionRadius; } }
		public float SquareDistanceForDetectionRadius (CGVPlayerSight.DetectionRadius radius) {
			switch (radius) {
				case CGVPlayerSight.DetectionRadius.Level1:
					return _DetectionRadius.x * _DetectionRadius.x;
				case CGVPlayerSight.DetectionRadius.Level2:
					return _DetectionRadius.y * _DetectionRadius.y;
				case CGVPlayerSight.DetectionRadius.Level3:
					return _DetectionRadius.z * _DetectionRadius.z;
				case CGVPlayerSight.DetectionRadius.AlwaysHeard:
					return float.MaxValue;
			}
			return 0f;
		}
		public float DistanceForDetectionRadius (CGVPlayerSight.DetectionRadius radius) {
			switch (radius) {
				case CGVPlayerSight.DetectionRadius.Level1:
					return _DetectionRadius.x;
				case CGVPlayerSight.DetectionRadius.Level2:
					return _DetectionRadius.y;
				case CGVPlayerSight.DetectionRadius.Level3:
					return _DetectionRadius.z;
				case CGVPlayerSight.DetectionRadius.AlwaysHeard:
					return float.MaxValue;
			}
			return 0f;
		}

		public bool FriendlyFire = false;
		public bool SelfDamage = false;

		[Header ("Settings")]
		[SerializeField] public bool InvertViewYAxis = false;
		[SerializeField] private bool _MainPlayerUseController = true;
		public bool MainPlayerUseController {
			get { return _MainPlayerUseController; }
			set {
				if (_MainPlayerUseController != value) {
					_MainPlayerUseController = value;
				}
			}
		}
		[SerializeField]
		private bool _ShowMainPlayerDisplay = true;
		public bool ShowMainPlayerDisplay {
			get { return _ShowMainPlayerDisplay; }
			set {
				if (_ShowMainPlayerDisplay != value) {
					_ShowMainPlayerDisplay = value;
					OnSettingsUpdated?.Invoke ();
				}
			}
		}
		[SerializeField]
		private int _MaxDisplayCount = 3;
		public int MaxDisplayCount { get { return _MaxDisplayCount; } set { _MaxDisplayCount = Mathf.Max (1, value); } }

		protected bool Initialized = false;

		protected override bool IsInitialized () {
			return Initialized;
		}

		protected override void InitSingleton () {
			//Debug.Log ("Initializing Game Settings");
			Initialized = true;
		}
	}
}
