﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using CGV.UI;
using CGV.Equipment;

/*
 * This script is a modification of the script FirstPersonController
 * from the Standard Assets.
 * 
 * 
 * 
 *
 */

#pragma warning disable 649
namespace CGV.Player {
	[RequireComponent (typeof (CharacterController))]
	[RequireComponent (typeof (NavMeshAgent))]
	public class CGVDogMasterPlayerCharacter : CGVPlayerCharacter {

		public override bool NeedsToHaveMainController { get { return true; } }

		private static readonly CGVPlayerAbility AutomaticNavigationAbilityMask = CGVPlayerAbility.Jump | CGVPlayerAbility.Walk | CGVPlayerAbility.Run | CGVPlayerAbility.Crouch;

		// NavmeshAgent
		protected NavMeshAgent m_Agent;
		protected bool _UseNavMeshAgent = false;
		public bool UseNavMeshAgent {
			get { return _UseNavMeshAgent; }
			protected set {
				if (_UseNavMeshAgent == value)
					return;
				_UseNavMeshAgent = value;
				if (_UseNavMeshAgent)
					AddTemporaryAbilityMask (AutomaticNavigationAbilityMask);
				else
					RemoveTemporaryAbilityMask (AutomaticNavigationAbilityMask);
				_MouseLook.SetMounted (value);
			}
		}

		// Dog
		[Header ("Dog")]
		[NaughtyAttributes.Required]
		[SerializeField] protected CGVDog _Dog;
		public CGVDog Dog { get { return _Dog; } }

		// Wall Detection
		[Header ("Wall Detection")]
		[SerializeField] protected AK.Wwise.Event HitWallEvent;
		[SerializeField] protected AK.Wwise.Event StartTouchWallEvent;
		[SerializeField] protected AK.Wwise.Event StopTouchWallEvent;
		[SerializeField] protected AK.Wwise.RTPC TouchWallForceRTPC;

		// POI
		[Header ("POI")]
		[NaughtyAttributes.Required]
		[SerializeField] protected CGVBeacon Beacon;
		[SerializeField] protected float NextBeaconDistance = 10f;
		[SerializeField] protected float BeaconRadius = 2f;
		[SerializeField] protected float MaxDistanceFromPath = 2f;

		protected bool PlaceBeacon = false;
		protected bool IsInsideBeaconArea = false;

		protected int CurrentPOIIndex = -1;
		protected CGVPointOfInterest CurrentPOI = null;
		protected NavMeshPath CurrentPOIPath;
		protected NavMeshQueryFilter _AgentFilter;

		protected Vector3 NextPathCorner;
		protected int CurrentPathSegment = 0;
		protected Vector3 ClosestPointOnPath;

		protected override void Start () {
			base.Start ();

			m_Agent = GetComponent<NavMeshAgent> ();
			m_Agent.speed = m_WalkSpeed;
			m_Agent.acceleration = m_Acceleration;

			CurrentPOIPath = new NavMeshPath ();
			_AgentFilter = new NavMeshQueryFilter { agentTypeID = m_Agent.agentTypeID, areaMask = m_Agent.areaMask };

			if (Beacon != null)
				Beacon.transform.SetParent (null);
		}

		public override void ResetCharacter () {
			base.ResetCharacter ();
			Dog?.ResetDog ();
		}

		protected override void RadialMenuOpenCharacter () { }
		protected override void RadialMenuClosedCharacter () { }

		protected override void QuickAction1Character (bool secondary = false) {
			Dog.FollowMaster ();
		}
		protected override void QuickAction2Character (bool secondary = false) {
			if (secondary)
				Dog.FetchAmmunition (); // Fetch any ammo type
			else {
				int ammoType = -1;
				CGVFirearmAction weapon = ActiveEquipment.PrimaryAction as CGVFirearmAction;
				if (weapon != null)
					ammoType = weapon.AmmoType;
				Dog.FetchAmmunition (ammoType); // Fetch current ammo type
			}
		}
		protected override void QuickAction3Character (bool secondary = false) {
			if (CurrentPOI != null) {
				if (UseNavMeshAgent) {
					Dog.FollowMaster ();
					CurrentPathSegment = 0;
					UseNavMeshAgent = false;
					CurrentPOIPath = m_Agent.path;
					ClosestPointOnPath = CurrentPOIPath.corners[0];
					m_Agent.enabled = false;
				}
				else {
					if (Dog.Guide ()) {
						CurrentPathSegment = 0;
						UseNavMeshAgent = true;
						m_Agent.enabled = true;
						m_Agent.SetPath (CurrentPOIPath);
					}
				}

				//Debug.LogFormat ("Activate navigation : {0} ({1})", UseNavMeshAgent, m_Agent.path);
			}
		}
		protected override void QuickAction4Character (bool secondary = false) {
			Dog.Attack (Sight.CurrentTarget as CGVPlayerCharacter);
		}

		private bool ReachedBeacon () {
			if (PlaceBeacon)
				return false;
			else // beacon is outside a 75 deg cone in front of the player relative to the rest of the path
				return PlaceBeacon =
					(Vector3.Dot (NextPathCorner - transform.position, Beacon.TargetPosition - transform.position) <= 0.25f) ||
					Vector3.Distance (transform.position, Beacon.TargetPosition) < BeaconRadius;
		}

		protected override void PossessedBy (CGVPlayerController player) {
			var POIs = CGVPointOfInterest.GetSortedPOIs ();
			List<CGVRadialMenuOption> poiOptionsList = new List<CGVRadialMenuOption> (POIs.Length);
			for (int i = 0; i < POIs.Length; ++i) {
				CGVPointOfInterest poi = POIs[i];
				poiOptionsList.Add (new CGVRadialMenuOption (
					poi.Description,
					() => {
						if (NavMesh.CalculatePath (transform.position, poi.transform.position, _AgentFilter, CurrentPOIPath)) {
							CurrentPOI = poi;
							ClosestPointOnPath = CurrentPOIPath.corners[0];
							PlaceBeacon = true;
							if (UseNavMeshAgent) {
								CurrentPathSegment = 0;
								m_Agent.SetPath (CurrentPOIPath);
							}
							Debug.LogFormat ("Select {0}", poi.Description);
							UAP_AccessibilityManager.Say (string.Format ("Select {0}", poi.Description));
						}
						else {
							Debug.LogWarning ("Couldn't calculate path to " + poi.Description);
							Debug.Break ();
						}
					}));
			}
			CGVRadialMenuOption poiOptionCancel = new CGVRadialMenuOption (
					"Cancel",
					() => {
						if (CurrentPOI != null) {
							CurrentPOI = null;
							Beacon?.gameObject.SetActive (false);
							if (UseNavMeshAgent) {
								m_Agent.enabled = false;
								UseNavMeshAgent = false;
							}
						}
					});
			player.RadialMenu.SetOptions (poiOptionsList, poiOptionCancel);
		}

		private void PlaceNextBeacon (NavMeshPath path, Vector3 fromPosition, int cornerOffset = 0) {
			//*
			cornerOffset++;
			if (path.corners.Length <= cornerOffset)
				return;
			NextPathCorner = path.corners[cornerOffset];
			Vector3 startPos = fromPosition;
			Vector3 beaconPos = NextPathCorner;
			float distance = 0f;
			while ((distance += Vector3.Distance (startPos, beaconPos)) < BeaconRadius && path.corners.Length > cornerOffset + 1) {
				startPos = beaconPos;
				cornerOffset++;
				beaconPos = path.corners[cornerOffset];
			}

			float distanceToBeacon = Vector3.Distance (fromPosition, beaconPos);
			if (distanceToBeacon > NextBeaconDistance) {
				float distanceToCorner = Vector3.Distance (fromPosition, startPos);
				//Debug.Log ((NextBeaconDistance - distanceToCorner) / (distanceToBeacon - distanceToCorner));
				beaconPos = Vector3.Lerp (startPos, beaconPos, (NextBeaconDistance - distanceToCorner) / (distanceToBeacon - distanceToCorner));
			}

			//Vector3 newBeaconPosition = (NextPathCorner - fromPosition).normalized * Mathf.Min (NextBeaconDistance, Vector3.Distance (fromPosition, beaconPos)) + fromPosition;

			if (Physics.Raycast (beaconPos, Vector3.down, out RaycastHit hit, 10f, 1 << 11)) { // Ground
				beaconPos = hit.point;
			}
			//Debug.Log (Vector3.Distance (fromPosition, beaconPos) <= BeaconRadius);
			Beacon.SetPosition (beaconPos);
			PlaceBeacon = false;

			/*/

			if (path.corners.Length > cornerOffset) {
				NextPathCorner = path.corners[cornerOffset];
				//Beacon.gameObject.SetActive (true);
				Vector3 newBeaconPosition = (NextPathCorner - fromPosition).normalized *
					Mathf.Min (NextBeaconDistance, Vector3.Distance (fromPosition, NextPathCorner)) + fromPosition;
				if (Physics.Raycast (newBeaconPosition, Vector3.down, out RaycastHit hit, 10f, 1 << 11)) { // Ground
					newBeaconPosition = hit.point;
				}
				Debug.Log (Vector3.Distance (fromPosition, NextPathCorner) <= BeaconRadius);
				Beacon.SetPosition (newBeaconPosition);
				PlaceBeacon = false;
			}//*/
		}

		protected override void FixedUpdate () {
			base.FixedUpdate ();

			//if (PlayerController != null && !PlayerController.RadialMenu.Hidden) return;

			if (CurrentPOI != null) {
				if (NavMesh.FindClosestEdge (transform.position, out NavMeshHit navHit, _AgentFilter)) {
					Debug.DrawLine (transform.position, navHit.position, Color.cyan);
				}

				NavMeshPath path = UseNavMeshAgent ? m_Agent.path : CurrentPOIPath;
				if (path.corners.Length > 1 + CurrentPathSegment) {
					NextPathCorner = path.corners[1 + CurrentPathSegment];

					RecalculatePath ();
					if (ReachedBeacon ()) {
						Beacon.Reached (false, !IsInsideBeaconArea);
						IsInsideBeaconArea = true;
					}
					else
						IsInsideBeaconArea = false;
					if (PlaceBeacon)
						PlaceNextBeacon (path, UseNavMeshAgent ? transform.position : ClosestPointOnPath, CurrentPathSegment);

#if UNITY_EDITOR
					if (!UseNavMeshAgent) Debug.DrawLine (transform.position, ClosestPointOnPath, Color.green);
					DrawPath (CurrentPOIPath);
#endif

					if (UseNavMeshAgent) {
						LastSpeed = m_Agent.velocity.magnitude;
						_FootSteps.SetSpeed (LastSpeed);
					}
				}
				else if (UseNavMeshAgent && !m_Agent.pathPending) {
					// The player reached the final destination
					_FootSteps.SetSpeed (0f);
					UseNavMeshAgent = m_Agent.enabled = false;
					CurrentPOI = null;
					EndNavigation ();
					//Beacon.Reached (true);
				}//*/
			}
		}

#if UNITY_EDITOR
		private void DrawPath (NavMeshPath path) {
			Debug.DrawLine (transform.position, Beacon.transform.position, Color.blue);
			Vector3 pos = path.corners[0];
			for (int i = 1; i < path.corners.Length; ++i) {
				Debug.DrawLine (pos, path.corners[i], Color.red);
				pos = path.corners[i];
			}
		}
#endif

		protected bool RecalculatePath () {
			if (CurrentPOI == null)
				return false;
			if (UseNavMeshAgent)
				return false;
			CGVDebugUtils.DrawCube (CurrentPOIPath.corners[Mathf.Min (CurrentPathSegment + 1, CurrentPOIPath.corners.Length - 1)], Color.magenta);
			if (Time.frameCount % 10 != 0) return false; // once every x frames
			if (CurrentPOIPath.corners.Length < 2 + CurrentPathSegment) {
				Debug.LogWarning (CurrentPOIPath.corners.Length - CurrentPathSegment);
				Debug.Break ();
				return false;
			}
			NextPathCorner = CurrentPOIPath.corners[CurrentPathSegment + 1];
			if (Vector3.Distance (transform.position, CurrentPOIPath.corners[CurrentPathSegment + 1]) < MaxDistanceFromPath) {
				CurrentPathSegment++;
				//Debug.Log (CurrentPathSegment);
				if (CurrentPOIPath.corners.Length < 2 + CurrentPathSegment) {
					//Debug.LogWarning (CurrentPOIPath.corners.Length - CurrentPathSegment);
					//Debug.Break ();
					EndNavigation ();
					//Beacon.Reached (true);
					return false;
				}
			}
			float distance = JuneCodes.Maths.JCMaths.HorizontalDistanceToSegment (
				CurrentPOIPath.corners[CurrentPathSegment],
				CurrentPOIPath.corners[CurrentPathSegment + 1],
				transform.position, out float closest);
			ClosestPointOnPath = Vector3.Lerp (CurrentPOIPath.corners[CurrentPathSegment], CurrentPOIPath.corners[CurrentPathSegment + 1], closest);
			if (distance < MaxDistanceFromPath) {
				return false;
			}

			NavMesh.CalculatePath (transform.position, CurrentPOI.transform.position, _AgentFilter, CurrentPOIPath);
			CurrentPathSegment = 0;
			if (CurrentPOIPath.corners.Length > 0)
				ClosestPointOnPath = CurrentPOIPath.corners[0];
			else
				ClosestPointOnPath = transform.position;
			PlaceBeacon = true;

			return true;
		}

		protected void EndNavigation () {
			Dog.FollowMaster ();
			Beacon.Reached (true);
		}

		protected struct WallHit {
			public int TouchFrame;
			public Collider Collider;
			public Vector3 TouchPoint;
			public Sound.CGVWallTexture Texture;

			public bool Equals (WallHit other) {
				return Collider.Equals (other.Collider);
			}
		}

		protected List<WallHit> _WallHits = new List<WallHit> (3);

		protected IEnumerator WallTouchCoroutine;
		protected int WallTouchMaxFrames = 3;
		protected Collider WallTouchSelectedCollider;
		protected GameObject WallTouchGameobject;

		private IEnumerator WallTouch () {
			StartTouchWallEvent?.Post (WallTouchGameobject);

			while (_WallHits.Count > 0) {
				yield return null;
				for (int i = _WallHits.Count - 1; i >= 0; --i) {
					if (Time.frameCount >= _WallHits[i].TouchFrame + WallTouchMaxFrames) {
						//Debug.Log ("Removing hit " + _WallHits[i].Collider);
						_WallHits.RemoveAt (i);
					}
				}

				if (_WallHits.Count > 0) {
					WallHit hit = _WallHits[0];
					if (_WallHits.Count > 1) {
						Vector3 forward = transform.forward;
						for (int i = 1; i < _WallHits.Count; ++i) {
							if (Vector3.Dot (forward, Vector3.Normalize (_WallHits[i].TouchPoint - transform.position)) > Vector3.Dot (forward, Vector3.Normalize (hit.TouchPoint - transform.position))) {
								hit = _WallHits[i];
							}
						}
					}
					if (!hit.Collider.Equals (WallTouchSelectedCollider)) {
						WallTouchSelectedCollider = hit.Collider;
						hit.Texture?.GetTextureSwitch ().SetValue (WallTouchGameobject);
					}
					WallTouchGameobject.transform.position = hit.TouchPoint;

					//foreach (WallHit wallHit in _WallHits) CGVDebugUtils.DrawCube (wallHit.TouchPoint, 0.25f, wallHit.Equals (hit) ? Color.blue : Color.cyan);
				}
			}

			StopTouchWallEvent?.Post (WallTouchGameobject);
			WallTouchCoroutine = null;
		}

		protected override void OnControllerColliderHit (ControllerColliderHit hit) {
			base.OnControllerColliderHit (hit);

			if (CharacterCollisionFlags.HasFlag (CollisionFlags.Sides)) {
				if (hit.gameObject.layer == CGVPersistentInfo.LayerWall) {

					WallHit wallHit = new WallHit () {
						Collider = hit.collider,
						TouchFrame = Time.frameCount,
						TouchPoint = hit.point
					};

					int hitIndex = _WallHits.FindIndex ((h) => { return h.Collider.Equals (hit.collider); });
					if (hitIndex < 0) {
						wallHit.Texture = hit.collider.GetComponent<Sound.CGVWallTexture> ();
						_WallHits.Add (wallHit);
						Sound.CGVSingleShotSoundEvent.PlaySingleShotAtLocation (HitWallEvent, wallHit.TouchPoint, wallHit.Texture);
						//Sound.CGVWallSoundMaterialValue wallType = Sound.CGVWallSoundMaterialEnum.GetValue (wallHit.Texture?.WallType);
						//Debug.LogFormat ("Hit wall {0} ({1}, {2})", wallType.Name, hit.point, _CharacterController.velocity.normalized);
					}
					else {
						wallHit.Texture = _WallHits[hitIndex].Texture;
						_WallHits[hitIndex] = wallHit;
					}

					if (WallTouchCoroutine == null) {
						if (WallTouchGameobject == null) {
							WallTouchGameobject = new GameObject ();
						}
						WallTouchGameobject.transform.position = hit.point;
						StartCoroutine (WallTouchCoroutine = WallTouch ());
					}
				}
			}
		}
	}
}
