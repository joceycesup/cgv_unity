﻿using System;
using UnityEngine;

#pragma warning disable 649
namespace CGV.Equipment {
	[Serializable]
	public enum ConsumableType {
		None = 0,
		Ammunition,
		Health,
		Item,
	}

	[Serializable]
	public struct CGVConsumableProperties {
		public ConsumableType Type;
		public int Id;
		public int Quantity;
		public UnityEngine.Object ConsumableObject;
		public Type ConsumableObjectType;

		public CGVConsumableProperties (ConsumableType type, int id, int quantity = 1) {
			Type = type;
			Id = id;
			Quantity = quantity;
			ConsumableObject = null;
			ConsumableObjectType = null;
		}

		public CGVConsumableProperties (ConsumableType type, int id, int quantity, UnityEngine.Object consumableObject, Type consumableObjectType)
			: this (type, id, quantity) {
			if (consumableObjectType != null) {
				ConsumableObjectType = consumableObjectType;
				ConsumableObject = consumableObject;
			}
		}
	}

	public interface ICGVConsumable {
		CGVConsumableProperties CheckContent ();
		int PickUp (int quantity);
		int Replenish (int quantity);
	}
}
