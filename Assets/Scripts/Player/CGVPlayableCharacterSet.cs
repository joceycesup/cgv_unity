﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif // UNITY_EDITOR 

#if CGV_NETWORK
using Photon.Pun;
using Photon.Realtime;
#endif // CGV_NETWORK

namespace CGV.Player {
	/// <summary>
	/// Asset class that defines which characters are available in the game.
	/// It can be created using the contextual menu "Create/CGV/Playable Character Set".
	/// See CGVPlayerCharacter for more information about the characters.
	/// </summary>
	[CreateAssetMenu (fileName = "PlayableCharactersSet.asset", menuName = "CGV/Playable Character Set")]
	public class CGVPlayableCharacterSet : ScriptableObject {

		[Serializable]
		private struct PlayableCharacter {
			public bool UseCharacter;
			public CGVPlayerCharacter Character;

			public PlayableCharacter (CGVPlayerCharacter character) {
				UseCharacter = true;
				Character = character;
			}

			public PlayableCharacter (bool useCharacter = true, CGVPlayerCharacter character = null) {
				UseCharacter = useCharacter;
				Character = character;
			}
		}

#if UNITY_EDITOR
		[CustomPropertyDrawer (typeof (PlayableCharacter))]
		public class EditorArgumentPropertyDrawer : PropertyDrawer {

			public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
				EditorGUI.BeginProperty (position, GUIContent.none, property);
				EditorGUI.BeginChangeCheck ();

				SerializedProperty useProperty = property.FindPropertyRelative ("UseCharacter");
				useProperty.boolValue = EditorGUI.ToggleLeft (new Rect (position.x, position.position.y, 20f, position.height), "", useProperty.boolValue);

				SerializedProperty charProperty = property.FindPropertyRelative ("Character");
				if (!useProperty.boolValue && charProperty.objectReferenceValue != null)
					GUI.color = new Color (1f, 1f, 1f, 0.6f);
				//EditorGUI.BeginDisabledGroup (!useProperty.boolValue && charProperty.objectReferenceValue != null);
				position.width -= 20f;
				position.x += 20f;
				float labelWidth = EditorGUIUtility.labelWidth;
				EditorGUIUtility.labelWidth = 100f;
				EditorGUI.ObjectField (position, charProperty, new GUIContent ((charProperty.objectReferenceValue as CGVPlayerCharacter)?.Name));
				EditorGUIUtility.labelWidth = labelWidth;
				GUI.color = Color.white;
				//EditorGUI.EndDisabledGroup ();

				if (EditorGUI.EndChangeCheck ()) {
					property.serializedObject.ApplyModifiedProperties ();
				}

				EditorGUI.EndProperty ();
			}

			public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
				return EditorGUIUtility.singleLineHeight;
			}
		}

		private void CheckDuplicates () {
			SerializedProperty list = new SerializedObject (this).FindProperty ("_PlayableCharacters");
			//Debug.LogWarning ("Checking duplicates");
			for (int i = 0; i < _PlayableCharacters.Count - 1; ++i) {
				if (_PlayableCharacters[i].Character == null)
					continue;
				for (int j = i + 1; j < _PlayableCharacters.Count; ++j) {
					if (_PlayableCharacters[j].Character == null)
						continue;
					if (_PlayableCharacters[i].Character.Name.Equals (_PlayableCharacters[j].Character.Name, StringComparison.InvariantCultureIgnoreCase)) {
						if (_PlayableCharacters[i].Character.Equals (_PlayableCharacters[j].Character)) {
							_PlayableCharacters[j] = new PlayableCharacter (_PlayableCharacters[j].UseCharacter);
							//Debug.LogWarningFormat ("Elements {0} and {1} in the Playable Characters Set are identical", i, j);
						}
						else {
							Debug.LogWarningFormat ("Elements {0} and {1} in the Playable Characters Set have identical names! ({2} = {3})", i, j, _PlayableCharacters[i].Character.Name, _PlayableCharacters[j].Character.Name);
						}
					}
				}
			}
		}

		[NaughtyAttributes.OnValueChanged ("CheckDuplicates")]
#endif
		[SerializeField]
		[NaughtyAttributes.ReorderableList]
		//private List<CGVPlayerCharacter> _PlayableCharacters = new List<CGVPlayerCharacter> ();
		private List<PlayableCharacter> _PlayableCharacters = new List<PlayableCharacter> ();

		public CGVPlayerCharacter MainPlayerCharacter { get; private set; }

		public ReadOnlyCollection<CGVPlayerCharacter> GetAvailableCharactersForPlayer (CGVPlayerController player) {
			List<CGVPlayerCharacter> available = new List<CGVPlayerCharacter> (_PlayableCharacters.Count);
			foreach (PlayableCharacter playable in _PlayableCharacters) {
				if (!playable.UseCharacter || playable.Character == null)
					continue;
				if (player.IsMainLocalPlayer || !playable.Character.NeedsToHaveMainController)
					available.Add (playable.Character);
			}
			return available.AsReadOnly ();
		}

		public CGVPlayerCharacter CreateDefaultCharacterForPlayer (CGVPlayerController player) {
			CGVPlayerCharacter res = null;
			if (player.IsMainLocalPlayer && MainPlayerCharacter != null) {
				res = MainPlayerCharacter;
			}
			else {
				foreach (PlayableCharacter playable in _PlayableCharacters) {
					if (player.IsMainLocalPlayer) {
						res = playable.Character;
						if (playable.Character.NeedsToHaveMainController)
							break;
					}
					else if (!playable.Character.NeedsToHaveMainController) {
						res = playable.Character;
						break;
					}
				}
			}
			if (res != null) {
#if CGV_NETWORK
				if (CGVNetworkManager.IsOnline)
					res = PhotonNetwork.Instantiate (res.gameObject.name, Vector3.zero, Quaternion.identity).GetComponent<CGVPlayerCharacter> ();
				else
#endif // CGV_NETWORK
				res = GameObject.Instantiate (res.gameObject).GetComponent<CGVPlayerCharacter> ();
			}
			return res;
		}

		public CGVPlayerCharacter CreateCharacterForPlayer (CGVPlayerController player, CGVPlayerCharacter character) {
			if (!_PlayableCharacters.Exists ((p) => { return p.UseCharacter && p.Character.Equals (character); }))
				return null;
#if CGV_NETWORK
			return PhotonNetwork.Instantiate (character.gameObject.name, Vector3.zero, Quaternion.identity).GetComponent<CGVPlayerCharacter> ();
#else
			return GameObject.Instantiate (character.gameObject).GetComponent<CGVPlayerCharacter> ();
#endif // CGV_NETWORK
		}

		public CGVPlayerCharacter GetCharacterByName (string name) {
			if (string.IsNullOrEmpty (name))
				return null;
			PlayableCharacter character = _PlayableCharacters.Find ((p) => { return p.UseCharacter && p.Character != null && p.Character.Name.Equals (name, StringComparison.InvariantCultureIgnoreCase); });
			return character.Character;
		}

		public void SetMainPlayerCharacter (string name) {
			if (string.IsNullOrEmpty (name))
				return;
			PlayableCharacter character = _PlayableCharacters.Find ((p) => { return p.UseCharacter && p.Character != null && p.Character.Name.Equals (name, StringComparison.InvariantCultureIgnoreCase); });
			MainPlayerCharacter = character.Character;
		}
	}
}
