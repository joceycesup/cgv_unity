﻿using System;
using UnityEngine;

using JuneCodes.FlexibleEnum;

#pragma warning disable 649
namespace CGV.Player {
	/// <summary>
	/// Team that can be used in the game.
	/// </summary>
	[Serializable]
	public class CGVTeamValue : JCEnumValue {
		[SerializeField] [ColorUsage (false)] protected Color _Color = Color.cyan;
		public Color Color { get { return _Color; } }
	}

	/// <summary>
	/// Container class for Teams.
	/// Used by CGVTeamEnum
	/// </summary>
	[Serializable]
	public class CGVTeamList : JCEnumValueContainer<CGVTeamValue> { }

	/// <summary>
	/// Singleton asset class to access the different Teams available in the game.
	/// It is located at the path "Assets/Resources/Teams.asset".
	/// See CGVTeamValue for more information about the values themselves.
	/// </summary>
	[CreateAssetMenu (fileName = "Teams.asset", menuName = "CGV/Teams")]
	public class CGVTeamEnum : JCFlexibleEnumSet<CGVTeamEnum, CGVTeamList, CGVTeamValue> {
	}
}
