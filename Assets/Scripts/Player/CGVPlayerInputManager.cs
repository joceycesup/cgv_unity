﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JuneCodes;
using InControl;

namespace CGV.Player {
	[JCCreateSingletonOnStart]
	public class CGVPlayerInputManager : JCSingletonMonoBehaviour<CGVPlayerInputManager> {
		public delegate void PlayerInputEvent ();
		public static PlayerInputEvent OnFreeDeviceAttached;

		private void Start () {
			if (InputManager.Devices == null)
				return;
			foreach (InputDevice device in InputManager.Devices)
				OnDeviceAttached (device);
		}

		void OnEnable () {
			InputManager.OnDeviceDetached += OnDeviceDetached;
			InputManager.OnDeviceAttached += OnDeviceAttached;
		}

		void OnDisable () {
			InputManager.OnDeviceDetached -= OnDeviceDetached;
			InputManager.OnDeviceAttached -= OnDeviceAttached;
		}

		void OnDeviceDetached (InputDevice inputDevice) {
			var player = FindPlayerUsingJoystick (inputDevice);
			if (player != null) {
				player.PlayerActions.Device = null;
			}
		}

		void OnDeviceAttached (InputDevice inputDevice) {
			//Debug.Log ("Device attached");
			if (FindPlayerUsingJoystick (inputDevice) != null)
				return;

			foreach (var player in CGVPlayerController.LocalPlayers) {
				if (player.IsMainLocalPlayer && !CGVGameSettings.instance.MainPlayerUseController)
					continue;
				//Debug.LogFormat ("Checking Devices for player {0}", player.LocalPlayerIndex);
				if (player.PlayerActions.Device == null) {
					//Debug.LogFormat ("Assigning Device {0} to player {1}", inputDevice.Name, player.LocalPlayerIndex);
					player.PlayerActions.Device = inputDevice;
					return;
				}
			}
			//Debug.Log ("Free Device attached");
			OnFreeDeviceAttached?.Invoke ();
		}

		CGVPlayerController FindPlayerUsingJoystick (InputDevice inputDevice) {
			var playerCount = CGVPlayerController.LocalPlayers.Count;
			for (var i = 0; i < playerCount; i++) {
				var player = CGVPlayerController.LocalPlayers[i];
				if (player.PlayerActions?.Device == inputDevice) {
					return player;
				}
			}

			return null;
		}

		bool ThereIsNoPlayerUsingJoystick (InputDevice inputDevice) {
			return FindPlayerUsingJoystick (inputDevice) == null;
		}

		CGVPlayerController FindPlayerUsingKeyboard () {
			var playerCount = CGVPlayerController.LocalPlayers.Count;
			for (var i = 0; i < playerCount; i++) {
				var player = CGVPlayerController.LocalPlayers[i];
				if (player.PlayerActions == null)
					continue;
				if (player.PlayerActions.ListenOptions.IncludeKeys)
					return player;
			}

			return null;
		}

		bool ThereIsNoPlayerUsingKeyboard () {
			return FindPlayerUsingKeyboard () == null;
		}

		public void CreatePlayerActions (CGVPlayerController player, out CGVPlayerActions playerActions) {
			bool useKeyboard = false;
			if (player.IsMainLocalPlayer)
				useKeyboard = ThereIsNoPlayerUsingKeyboard ();
			playerActions = CGVPlayerActions.CreateWithDefaultBindings (useKeyboard, !player.IsMainLocalPlayer || CGVGameSettings.instance.MainPlayerUseController);
			playerActions.Device = null;
		}

		public bool GetInputDevices (CGVPlayerController player, out InputDevice playerDevice) {
			playerDevice = null;
			if (player.PlayerActions == null)
				return false;
			if (player.PlayerActions.Device != null)
				return false;//*
			if (player.IsMainLocalPlayer && !CGVGameSettings.instance.MainPlayerUseController) {
				return false;
			}//*/

			//Debug.LogFormat ("Get Devices for player {0}", player.LocalPlayerIndex);

			bool hasController = false;
			//playerActions.IncludeDevices.Clear ();
			if (InputManager.Devices != null) {
				foreach (InputDevice device in InputManager.Devices) {
					if (device.DeviceClass == InputDeviceClass.Controller) {
						if (FindPlayerUsingJoystick (device) != null)
							continue;
						hasController = true;
						playerDevice = device;
						//Debug.LogFormat ("Assigning Device {0} to player {1}", device.Name, player.LocalPlayerIndex);
						break;
					}
				}
			}

			return hasController;
		}

		public void RemovePlayer (CGVPlayerController player) {
			if (player.PlayerActions == null)
				return;
			//Debug.LogFormat ("Removing player {0}'s Device", player.LocalPlayerIndex);
			player.PlayerActions.Device = null;
		}
	}
}
