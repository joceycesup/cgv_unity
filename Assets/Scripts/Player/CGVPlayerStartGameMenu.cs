﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using TMPro;

using CGV.Player;

using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

namespace CGV.UI {
	public class CGVPlayerStartGameMenu : MonoBehaviourPunCallbacks {

		private CGVPlayerController _PlayerController;
		private static CGVPlayerStartGameMenu _MainPlayerMenu;

		private bool _IsReady = false;
		private bool IsReady {
			get { return _IsReady; }
			set {
				_IsReady = value;
				_PlayerReadyButton.interactable = !_IsReady;
				if (_IsReady) {
					CurrentSelectable = null;
					_CurrentSelectableRoot = null;
				}
			}
		}

		[SerializeField]
		private RectTransform _TeamSelection;
		private TMP_Dropdown _TeamDropdown;
		private List<int> _TeamOptions;
		public int CurrentTeamId { get; private set; } = JuneCodes.FlexibleEnum.JCEnumKey.InvalidId;

		[SerializeField]
		private Button _PlayerReadyButton;

		[SerializeField]
		private Button _StartGameButton;

		private Selectable _CurrentSelectable;
		private Transform _CurrentSelectableRoot;
		private Selectable CurrentSelectable {
			get { return _CurrentSelectable; }
			set {
				if (value == _CurrentSelectable)
					return;
				if (_CurrentSelectable != null) {
					_CurrentSelectable.OnPointerExit (new PointerEventData (EventSystem.current));
					TMP_Dropdown dropdown = CurrentSelectable as TMP_Dropdown;
					if (dropdown != null)
						dropdown.Hide ();
				}
				_CurrentSelectable = value;
				if (_CurrentSelectable != null)
					_CurrentSelectable.OnPointerEnter (new PointerEventData (EventSystem.current));
			}
		}

		[SerializeField]
		private CGVPlayableSceneManager _SceneManagerPrefab;

		private List<int> _Players = new List<int> ();
		private List<bool> _PlayersReady = new List<bool> ();

		private void Awake () {
			_PlayerController = GetComponentInParent<CGVPlayerController> ();

			_TeamDropdown = _TeamSelection.GetComponentInChildren<TMP_Dropdown> ();
			_TeamDropdown.onValueChanged.AddListener (TeamChanged);

			_StartGameButton.interactable = false;
		}

		private void UpdateUI () {
			_TeamSelection.gameObject.SetActive (CGVGameSettings.instance.TeamSelection == CGVGameSettings.TeamSelectionMode.Free);
		}

		public override void OnEnable () {
			base.OnEnable ();

			IsReady = false;

			CGVPlayableSceneManager.OnSceneReady += SceneIsReady;
			CGVPlayableSceneManager.OnSpawnsUpdated += UpdateTeams;
			UpdateTeams ();
#if CGV_NETWORK
			PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
#endif // CGV_NETWORK
			if (CGVPlayableSceneManager.IsReady)
				SceneIsReady ();

#if CGV_NETWORK
			if (CGVNetworkManager.IsOnline) {
				_StartGameButton.gameObject.SetActive (PhotonNetwork.IsMasterClient);
			}
			else 
#endif // CGV_NETWORK
			//if (_PlayerController.IsMainLocalPlayer) Debug.LogWarningFormat ("&& {0} &&", CGVGameSettings.instance.Multiplayer);
			if (CGVGameSettings.instance.Multiplayer == CGVGameSettings.MultiplayerMode.Local) {
				if (_PlayerController.IsMainLocalPlayer)
					_MainPlayerMenu = this;
				else
					SelectReadyButton ();
				_MainPlayerMenu._Players.Add (_PlayerController.LocalPlayerIndex);
				_MainPlayerMenu._PlayersReady.Add (false);
				_MainPlayerMenu._StartGameButton.interactable = false;
				_StartGameButton.gameObject.SetActive (_PlayerController.IsMainLocalPlayer);
			}
			else {
				_StartGameButton.gameObject.SetActive (false);
			}
		}

		public override void OnDisable () {
			base.OnDisable ();
			CGVPlayableSceneManager.OnSceneReady -= SceneIsReady;
			CGVPlayableSceneManager.OnSpawnsUpdated -= UpdateTeams;
#if CGV_NETWORK
			PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
#endif // CGV_NETWORK

			_Players.Clear ();
			_PlayersReady.Clear ();
		}

		void UpdateTeams () {
			if (IsReady)
				return;
			var options = new List<TMP_Dropdown.OptionData> (CGVPlayableSceneManager.Teams.Count);
			_TeamOptions = new List<int> (CGVPlayableSceneManager.Teams.Count);
			foreach (int teamID in CGVPlayableSceneManager.Teams) {
				CGVTeamValue team = CGVTeamEnum.GetValue (teamID);
				options.Add (new TMP_Dropdown.OptionData (team.Name));
				_TeamOptions.Add (teamID);
			}
			_TeamDropdown.options = options;
			//_TeamDropdown.value = 0;
			_TeamDropdown.value = LoopIndex (_TeamDropdown.value, options.Count);
			if (!JuneCodes.FlexibleEnum.JCEnumKey.IdIsValid (CurrentTeamId))
				TeamChanged (0);
		}

		void SceneIsReady () {
			UpdateTeams ();
		}

		private void TeamChanged (int index) {
			if (index >= _TeamOptions.Count)
				return;
			CurrentTeamId = _TeamOptions[index];
			//Debug.Log ("Current team : " + CurrentTeamId);
		}

		void StartGame () {
#if CGV_NETWORK
			if (PhotonNetwork.IsMasterClient) {
				PhotonNetwork.CurrentRoom.IsOpen = false;
				PhotonNetwork.Instantiate (_SceneManagerPrefab.gameObject.name, Vector3.zero, Quaternion.identity);
			}
			else
#endif // CGV_NETWORK
			if (CGVGameSettings.instance.Multiplayer == CGVGameSettings.MultiplayerMode.Local) {
				CGVPlayableSceneManager.CreateSceneManager ();
			}
		}

		public override void OnJoinedRoom () {
			base.OnJoinedRoom ();
			if (CGVNetworkManager.TryGetCurrentRoomAllowClassSelection (out bool allowClassSelection))
				CGVGameSettings.instance.AllowClassSelection = allowClassSelection;
			if (CGVNetworkManager.TryGetCurrentRoomAllowTeamSelection (out bool allowTeamSelection))
				CGVGameSettings.instance.TeamSelection = allowTeamSelection ? CGVGameSettings.TeamSelectionMode.Free : CGVGameSettings.TeamSelectionMode.Random;
			UpdateUI ();
		}

#if CGV_NETWORK
		public override void OnCreatedRoom () {
			base.OnCreatedRoom ();

			_Players.Add (PhotonNetwork.LocalPlayer.ActorNumber);
			_PlayersReady.Add (false);
			_StartGameButton.interactable = false;
		}

		public override void OnPlayerEnteredRoom (Player newPlayer) {
			base.OnPlayerEnteredRoom (newPlayer);
			if (PhotonNetwork.IsMasterClient) {
				_Players.Add (newPlayer.ActorNumber);
				_PlayersReady.Add (false);
				_StartGameButton.interactable = false;
			}
		}

		public override void OnPlayerLeftRoom (Player otherPlayer) {
			base.OnPlayerLeftRoom (otherPlayer);
			if (PhotonNetwork.IsMasterClient) {
				_StartGameButton.interactable = false;
				for (int i = 0; i < _Players.Count; ++i) {
					if (_Players[i].ActorNumber == otherPlayer.ActorNumber) {
						_Players.RemoveAt (i);
						_PlayersReady.RemoveAt (i);
						break;
					}
				}
				CheckIfEveryPlayerIsReady ();
			}
		}

		public void OnEvent (EventData photonEvent) {
			byte eventCode = photonEvent.Code;

			if (eventCode == CGVNetworkManager.PlayerIsReadyEvent) {
				PlayerIsReady ((int)photonEvent.CustomData);
			}
		}
#endif // CGV_NETWORK

		[NaughtyAttributes.Button ("Player Is Ready")]
		void PlayerIsReady () {
			IsReady = true;
#if CGV_NETWORK
			if (CGVNetworkManager.IsOnline) {
				if (PhotonNetwork.IsMasterClient) {
					PlayerIsReady (PhotonNetwork.LocalPlayer.ActorNumber);
				}
				else {
					//photonView.RPC ("PlayerIsReadyRPC", RpcTarget.MasterClient, new object[] { PhotonNetwork.LocalPlayer.ActorNumber });

					object content = PhotonNetwork.LocalPlayer.ActorNumber;
					RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.MasterClient };
					SendOptions sendOptions = new SendOptions { Reliability = true };
					PhotonNetwork.RaiseEvent (CGVNetworkManager.PlayerIsReadyEvent, content, raiseEventOptions, sendOptions);
				}
			}
			else
#endif // CGV_NETWORK
			if (CGVGameSettings.instance.Multiplayer == CGVGameSettings.MultiplayerMode.Local) {
				_MainPlayerMenu.PlayerIsReady (_PlayerController.LocalPlayerIndex);
			}
		}

		void CheckIfEveryPlayerIsReady () {
			bool everyPlayerIsReady = true;
			foreach (bool ready in _PlayersReady) {
				if (!ready) {
					everyPlayerIsReady = false;
					break;
				}
			}
			_StartGameButton.interactable = everyPlayerIsReady;
		}

		void PlayerIsReady (int actorNumber) {
			if (
#if CGV_NETWORK
				(CGVNetworkManager.IsOnline && PhotonNetwork.IsMasterClient) ||
#endif // CGV_NETWORK
				CGVGameSettings.instance.Multiplayer == CGVGameSettings.MultiplayerMode.Local) {
				for (int i = 0; i < _Players.Count; ++i) {
					if (_Players[i] == actorNumber) {
						_PlayersReady[i] = true;
						break;
					}
				}
				CheckIfEveryPlayerIsReady ();
			}
		}

		#region Multiplayer UI
		[NaughtyAttributes.Button ("Select Ready Button")]
		void SelectReadyButton () {
			_CurrentSelectableRoot = _PlayerReadyButton.transform;
			CurrentSelectable = _PlayerReadyButton;
		}

		[NaughtyAttributes.Button ("Select Team Dropdown")]
		void SelectTeamDropdown () {
			_CurrentSelectableRoot = _TeamDropdown.transform;
			CurrentSelectable = _TeamDropdown;
			_TeamDropdown.Show ();
		}

		[NaughtyAttributes.Button ("Go Through Options")]
		void GoThroughOptions () {
			GoThroughOptions (true);
		}

		void GoThroughOptions (bool next) {
			TMP_Dropdown dropdown = CurrentSelectable as TMP_Dropdown;
			if (dropdown != null) {
				dropdown.value = LoopIndex (dropdown.value + (next ? 1 : -1), dropdown.options.Count);
				if (dropdown.IsExpanded) {
					dropdown.RefreshShownValue ();
					dropdown.Show ();
				}
			}
		}

		[NaughtyAttributes.Button ("Select Next Selectable")]
		void SelectNextSelectable () {
			SelectElement (true);
		}

		[NaughtyAttributes.Button ("Select Previous Selectable")]
		void SelectPreviousSelectable () {
			SelectElement (false);
		}

		void SelectElement (bool down) {
			Transform previousTransform = _CurrentSelectableRoot;
			Selectable previousSelectable = null;

			do {
				previousTransform = _CurrentSelectableRoot.parent.GetChild (LoopIndex (previousTransform.GetSiblingIndex () + (down ? 1 : -1), _CurrentSelectableRoot.parent.childCount));
				if (!previousTransform.gameObject.activeInHierarchy)
					continue;
				previousSelectable = previousTransform.GetComponentInChildren<Selectable> ();
				if (previousSelectable != null && !previousSelectable.IsInteractable ()) {
					previousSelectable = null;
					continue;
				}
			} while (previousSelectable == null && previousTransform != CurrentSelectable.transform);

			if (previousSelectable != null) {
				_CurrentSelectableRoot = previousTransform;
				CurrentSelectable = previousSelectable;
			}
		}

		int LoopIndex (int index, int count) {
			if (count <= 0)
				return 0;
			int res = index % count;
			return res < 0 ? res + count : res;
		}

		public void SendInput (Vector2 input) {
			if (IsReady)
				return;
			if (!Mathf.Approximately (input.y, 0f)) {
				if (Mathf.Abs (input.x) > Mathf.Abs (input.y)) {
					GoThroughOptions (input.x > 0);
				}
				else {
					SelectElement (input.y > 0);
				}
			}
			else if (!Mathf.Approximately (input.x, 0f)) {
				GoThroughOptions (input.x > 0);
			}
		}

		public void Submit () {
			if (IsReady || CurrentSelectable == null)
				return;
			//Debug.LogFormat ("Player {0} submits {1}", _PlayerController.LocalPlayerIndex, _CurrentSelectable.gameObject);
			//_CurrentSelectable.OnPointerDown (new PointerEventData (EventSystem.current));
			//_CurrentSelectable.OnPointerUp (new PointerEventData (EventSystem.current));
			//ExecuteEvents.pointerClickHandler (_CurrentSelectable as IPointerClickHandler, new PointerEventData (EventSystem.current));
			ExecuteEvents.Execute (CurrentSelectable.gameObject, new PointerEventData (EventSystem.current), ExecuteEvents.submitHandler);
		}
		#endregion Multiplayer UI
	}
}
