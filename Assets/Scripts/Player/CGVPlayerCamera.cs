﻿using UnityEngine;

namespace CGV.Player {
	[RequireComponent (typeof (Camera))]
	public class CGVPlayerCamera : MonoBehaviour {

		protected Camera _Camera;
		public Camera Camera {
			get {
				if (_Camera == null)
					_Camera = GetComponent<Camera> ();
				return _Camera;
			}
		}

		[SerializeField]
		protected GameObject _AudioListener;

		protected static CGVPlayerCamera _CurrentAudioListener;

		protected static Material _HideDisplayMaterial;

		public void Awake () {
			_AudioListener.SetActive (false);
			SetCameraEnabled (false);
		}

		public void SetAsMainListener () {
			if (_CurrentAudioListener != null && _CurrentAudioListener != this)
				_CurrentAudioListener._AudioListener.SetActive (false);
			_CurrentAudioListener = this;
			_AudioListener.SetActive (true);
		}

		public void SetCameraEnabled (bool enable) {
			Camera.enabled = enable;
			_WillDisableAndClear = false;
		}

		private bool _WillDisableAndClear = false;
		private bool _CameraCleared = false;

		public void DisableAndClearCamera () {
			_CameraCleared = false;
			_WillDisableAndClear = true;
		}

		private void OnPostRender () {
			if (!_WillDisableAndClear)
				return;
			if (_HideDisplayMaterial == null) {
				Debug.LogWarning ("Shader : " + Shader.Find ("Custom/HideDisplay"));
				_HideDisplayMaterial = new Material (Shader.Find ("Custom/HideDisplay"));
			}
			GL.PushMatrix ();
			_HideDisplayMaterial.SetPass (0);
			GL.LoadOrtho ();
			GL.Begin (GL.QUADS);
			GL.Color (Color.black);
			GL.Vertex3 (0f, 0f, 0f);
			GL.Vertex3 (0f, 1f, 0f);
			GL.Vertex3 (1f, 1f, 0f);
			GL.Vertex3 (1f, 0f, 0f);
			GL.End ();
			GL.PopMatrix ();
			_CameraCleared = true;
		}

		private void OnPreCull () {
			if (_WillDisableAndClear && _CameraCleared) {
				_WillDisableAndClear = false;
				Camera.enabled = false;
			}
		}
	}
}
