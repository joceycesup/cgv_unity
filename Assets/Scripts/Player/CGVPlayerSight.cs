﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGV.Player {
	/// <summary>
	/// %Player component that stores the targets acquired by a character during the game.
	/// It is notified when a character emits a sound and adds it to the list accordingly.
	/// </summary>
	[RequireComponent (typeof (CGVPlayerCharacter))]
	public class CGVPlayerSight : MonoBehaviour {

		public delegate void PlayerDetectionRadiusEvent (CGVPlayerCharacter character, DetectionRadius radius, float minDelay = 0.01f);
		public static PlayerDetectionRadiusEvent OnPlayerSoundEmitted;
		public delegate void DetectionRadiusEvent (CGVTarget target, DetectionRadius radius, float minDelay = 0.01f);
		public static DetectionRadiusEvent OnSoundEmitted;

		public delegate void TargetAcquisitionEvent (CGVTarget target);
		public TargetAcquisitionEvent OnTargetAcquired;
		public TargetAcquisitionEvent OnTargetLost;
		public TargetAcquisitionEvent OnCurrentTargetLost;

		public static readonly float InfiniteDelay = -1f;

		[Serializable]
		public enum DetectionRadius {
			AlwaysHeard = -1,
			NoSoundEmitted = 0,
			Level1 = 1,
			Level2 = 2,
			Level3 = 3,
		}

#if CGV_DEBUG
		[Serializable]
#endif
		private class AcquiredTarget {
			public CGVTarget Target;
			public float TimeSinceUpdated = 0f;
			public float LoseTargetDelay = 1f;
			public IEnumerator LoseTargetCoroutine;

			public AcquiredTarget (CGVTarget target, float delay) {
				Target = target;
				LoseTargetDelay = delay;
			}
		}

#if CGV_DEBUG
		[SerializeField]
		[NaughtyAttributes.ReorderableList]
#endif
		private List<AcquiredTarget> AcquiredTargets = new List<AcquiredTarget> ();
		private bool TargetIsAcquired (CGVTarget target, out int index) {
			index = -1;
			if (target == null) {
				return false;
			}
			for (int i = 0; i < AcquiredTargets.Count; ++i) {
				if (ReferenceEquals (AcquiredTargets[i].Target, target)) {
					index = i;
					break;
				}
			}
			return (index >= 0);
		}

		private static readonly CGVPlayerAbility AimMask = CGVPlayerAbility.SwitchEquipment;

		private CGVPlayerCharacter _Player;
		private CGVTarget _CurrentTarget = null;
		public CGVTarget CurrentTarget {
			get { return _CurrentTarget; }
			set {
				if (_CurrentTarget == value)
					return;
				if (value == null) {
					_CurrentTargetIndex = -1;
					Debug.LogFormat ("Current target ({0}) set to null.", _CurrentTarget.Name);
				}
				else if (TargetIsAcquired (value, out int index)) {
					_CurrentTargetIndex = index;
				}
				else {
					AddToAcquiredTargets (value);
				}
				_CurrentTarget = value;
			}
		}
		private int _CurrentTargetIndex = -1;
		public int CurrentTargetIndex {
			get { return _CurrentTargetIndex; }
			set {
				if (_CurrentTargetIndex == value)
					return;
				if (value < 0 || value >= AcquiredTargets.Count) {
					_CurrentTargetIndex = -1;
					_CurrentTarget = null;
					return;
				}
				_CurrentTargetIndex = value;
				_CurrentTarget = AcquiredTargets[_CurrentTargetIndex].Target;
			}
		}
		//private List<CGVTarget> AcquiredTargets;
		public bool TargetIsLocked { get; set; } = false;

		[SerializeField] protected AK.Wwise.Event _TargetAcquiredEvent;
		public AK.Wwise.Event TargetAcquiredEvent { get { return _TargetAcquiredEvent; } }
		[SerializeField] protected AK.Wwise.RTPC _TargetAcquiredDistRTPC;
		public AK.Wwise.RTPC TargetAcquiredDistRTPC { get { return _TargetAcquiredDistRTPC; } }
		[NaughtyAttributes.MinValue (0f)]
		[SerializeField] protected float _TargetAcquiredPingDelay = 0.1f;
		public float TargetAcquiredPingDelay { get { return _TargetAcquiredPingDelay; } }
		[NaughtyAttributes.MinValue (0f)]
		[SerializeField] protected float _LoseTargetMinDelay = 15f;
		public float LoseTargetMinDelay { get { return _LoseTargetMinDelay; } }

		private bool _IsAiming = false;
		public bool IsAiming {
			get { return _IsAiming; }
			set {
				if (_IsAiming == value)
					return;
				_IsAiming = value;
				if (_IsAiming) {
					_Player.AddTemporaryAbilityMask (AimMask);
					_Player.AddAbility (CGVPlayerAbility.SwitchTarget);
				}
				else {
					_Player.RemoveTemporaryAbilityMask (AimMask);
					_Player.RemoveAbility (CGVPlayerAbility.SwitchTarget);
				}
			}
		}

		void OnEnable () {
			OnPlayerSoundEmitted += PlayerEmittedSound;
			OnSoundEmitted += TargetEmittedSound;
		}

		void OnDisable () {
			OnPlayerSoundEmitted -= PlayerEmittedSound;
			OnSoundEmitted -= TargetEmittedSound;
		}

		private void Start () {
			_Player = GetComponent<CGVPlayerCharacter> ();
			_Player.OnGotKilled += (x, y) => { RemoveEveryAcquiredTarget (); };
		}

		private void AddToAcquiredTargets (CGVTarget target, float minAcquiredTime = 0.01f, bool checkIfAlreadyAcquired = true) {
			if (checkIfAlreadyAcquired) {
				if (TargetIsAcquired (target, out int index)) {
					AcquiredTargets[index].TimeSinceUpdated = 0f;
					if (AcquiredTargets[index].LoseTargetDelay > 0f) {
						if (minAcquiredTime < 0f)
							AcquiredTargets[index].LoseTargetDelay = -1f;
						else
							AcquiredTargets[index].LoseTargetDelay = Mathf.Max (minAcquiredTime, AcquiredTargets[index].LoseTargetDelay);
					}
					return;
				}
			}
			if (minAcquiredTime >= 0f)
				minAcquiredTime = Mathf.Max (minAcquiredTime, LoseTargetMinDelay);
			Debug.LogFormat ("Target {0} acquired for{1}.", target, (minAcquiredTime < 0f ? "ever" : " " + minAcquiredTime + " seconds"));
			AcquiredTarget acquired = new AcquiredTarget (target, minAcquiredTime);
			if (minAcquiredTime > 0f) {
				acquired.LoseTargetCoroutine = WillLoseTarget (acquired);
				StartCoroutine (acquired.LoseTargetCoroutine);
			}
			AcquiredTargets.Add (acquired);
			OnTargetAcquired?.Invoke (target);
			target.OnTargetStateChanged += TargetStateChanged;
		}

		private IEnumerator WillLoseTarget (AcquiredTarget acquired) {
			while (acquired.TimeSinceUpdated < acquired.LoseTargetDelay) {
				acquired.TimeSinceUpdated += Time.deltaTime;
				yield return null;
			}
			acquired.LoseTargetCoroutine = null;
			LoseTarget (acquired.Target);
		}

		private bool RemoveFromAcquiredTargets (CGVTarget target) {
			bool res = TargetIsAcquired (target, out int index);
			if (res) {
				Debug.LogFormat ("Target {0} is being removed.", target);
				AcquiredTarget acquired = AcquiredTargets[index];
				if (acquired.LoseTargetCoroutine != null)
					StopCoroutine (acquired.LoseTargetCoroutine);
				target.OnTargetStateChanged -= TargetStateChanged;
				AcquiredTargets.RemoveAt (index);
				OnTargetLost?.Invoke (target);
			}
			return res;
		}

		private void RemoveEveryAcquiredTarget () {
			//Debug.Log ("Every target removed.");
			foreach (AcquiredTarget acquired in AcquiredTargets) {
				if (acquired.LoseTargetCoroutine != null)
					StopCoroutine (acquired.LoseTargetCoroutine);
				acquired.Target.OnTargetStateChanged -= TargetStateChanged;
				OnTargetLost?.Invoke (acquired.Target);
			}
			AcquiredTargets.Clear ();
		}

		private void PlayerEmittedSound (CGVPlayerCharacter character, DetectionRadius radius, float minDelay) {
			if (_Player == null || character == null || _Player == character)
				return;
			if (!CGVPlayerCharacter.DifferentTeams (_Player, character))
				return;

			if (CheckTargetDetection (character, radius)) {
				AddToAcquiredTargets (character, minDelay);
			}
		}

		private void TargetEmittedSound (CGVTarget target, DetectionRadius radius, float minDelay) {
			if (CheckTargetDetection (target, radius)) {
				AddToAcquiredTargets (target, minDelay);
			}
		}

		private bool CheckTargetDetection (CGVTarget target, DetectionRadius radius) {
			bool isWithinRadius = radius == DetectionRadius.AlwaysHeard;
			if (!isWithinRadius) {
				isWithinRadius = Vector3.SqrMagnitude (target.transform.position - _Player.transform.position) <= CGVGameSettings.instance.SquareDistanceForDetectionRadius (radius);
			}
			return isWithinRadius;
		}

		public void AcquireEveryTarget () {
			var newTargets = FindObjectsOfType<CGVTarget> ();
			if (AcquiredTargets == null)
				AcquiredTargets = new List<AcquiredTarget> (newTargets.Length);
			else {
				RemoveEveryAcquiredTarget ();
				if (AcquiredTargets.Capacity < newTargets.Length)
					AcquiredTargets.Capacity = newTargets.Length;
			}
			foreach (CGVTarget target in newTargets) {
				if (target != _Player)
					AddToAcquiredTargets (target, 0f, false);
				//AcquiredTargets.Add (target);
			}
			StartCoroutine (PingAcquiredTargets (newTargets));
		}

		private IEnumerator PingAcquiredTargets (IEnumerable<CGVTarget> targets) {
			if (TargetAcquiredEvent == null)
				yield break;
			foreach (CGVTarget target in targets) {
				TargetAcquiredDistRTPC.SetValue (target.gameObject, Vector3.Distance (transform.position, target.Bounds.GetColliderBounds ().center));
				TargetAcquiredEvent?.Post (target.gameObject);
				yield return new WaitForSeconds (TargetAcquiredPingDelay);
			}
		}

		public void AcquireTarget (CGVTarget target, float minAcquiredTime = 0.01f) {
			AddToAcquiredTargets (target, minAcquiredTime);
		}

		public void LoseTarget (CGVTarget target) {
			if (RemoveFromAcquiredTargets (target)) {
				Debug.LogFormat ("Target {0} lost", target);
				if (CurrentTarget == target) {
					CurrentTarget = null;
					OnCurrentTargetLost?.Invoke (target);
					//FindClosestTarget ();
				}
			}
			else {
				Debug.LogFormat ("Couldn't lose target {0}", target);
			}
		}

		private void TargetStateChanged (CGVTarget target, CGVTarget.TargetState newState, CGVTarget.TargetState oldState) {
			Debug.LogFormat ("Target {0} changed state to {1}", target, newState);
			switch (newState) {
				case CGVTarget.TargetState.Dead:
					LoseTarget (target);
					break;
			}
		}

		public bool FindClosestTarget () {
			float closestTargetAngle = float.MaxValue;
			CGVTarget closestTarget = null;
			if (AcquiredTargets == null) {
				AcquireEveryTarget ();
			}
			if (AcquiredTargets.Count <= 0)
				return false;

			CurrentTargetIndex = -1;
			for (int i = 0; i < AcquiredTargets.Count; ++i) {
				CGVTarget target = AcquiredTargets[i].Target;
				if (!target.IsVisible)
					continue;
				float angle = Vector3.Angle (_Player.Camera.transform.forward, target.Bounds.GetColliderBounds ().center - _Player.Camera.transform.position);
				if (angle < closestTargetAngle) {
					closestTargetAngle = angle;
					closestTarget = target;
					//CurrentTargetIndex = i;
				}
			}
			CurrentTarget = closestTarget;
			//Debug.LogFormat ("Closest Target : {0}", CurrentTarget);
			return closestTarget != null;
		}

		public bool SwitchTarget (bool next) {
			if (AcquiredTargets == null)
				AcquireEveryTarget ();
			if (AcquiredTargets.Count <= 1)
				return false;
			if (CurrentTargetIndex < 0 || CurrentTarget == null)
				return false;
			int lastIndex = CurrentTargetIndex;
			int newIndex = lastIndex;
			do {
				if (next) {
					newIndex++;
					if (newIndex >= AcquiredTargets.Count)
						newIndex = 0;
				}
				else {
					newIndex--;
					if (newIndex < 0)
						newIndex = AcquiredTargets.Count - 1;
				}
				if (AcquiredTargets[newIndex].Target == CurrentTarget)
					return false;
				if (lastIndex == newIndex)
					return false;
			} while (!AcquiredTargets[newIndex].Target.IsVisible);
			CurrentTargetIndex = newIndex;
			//CurrentTarget = AcquiredTargets[CurrentTargetIndex].Target;
			return true;
		}
	}
}
