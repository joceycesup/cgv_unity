﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

using JuneCodes.FlexibleEnum;

using CGV.Sound;
using CGV.Equipment;

using Photon.Pun;

/*
 * This script is a modification of the script FirstPersonController
 * from the Standard Assets.
 * 
 * 
 * 
 *
 */

#pragma warning disable 649
namespace CGV.Player {

	[Flags]
	[Serializable]
	public enum CGVPlayerAbility : int {
		None = 0x000000,
		Walk = 0x000001,
		Run = 0x000002,
		Jump = 0x000004,
		Crouch = 0x000008,
		LookAround = 0x000010,
		_06 = 0x000020,
		_07 = 0x000040,
		SwitchEquipment = 0x000080,
		PrimaryAction = 0x000100,
		SecondaryAction = 0x000200,
		SpecialAction = 0x000400,
		OptionalAction = 0x000800,
		_13 = 0x001000,
		SwitchTarget = 0x002000,
		_15 = 0x004000,
		OpenRadialMenu = 0x008000,
		QuickAction1 = 0x010000,
		QuickAction2 = 0x020000,
		QuickAction3 = 0x040000,
		QuickAction4 = 0x080000,
	}

	public enum CGVPlayerAbilityCategories : int {
		None = 0x000000,
		Movement = CGVPlayerAbility.Walk | CGVPlayerAbility.Run | CGVPlayerAbility.Jump | CGVPlayerAbility.Crouch,
		Actions = CGVPlayerAbility.PrimaryAction | CGVPlayerAbility.SecondaryAction | CGVPlayerAbility.SpecialAction | CGVPlayerAbility.OptionalAction,
		QuickActions = CGVPlayerAbility.QuickAction1 | CGVPlayerAbility.QuickAction2 | CGVPlayerAbility.QuickAction3 | CGVPlayerAbility.QuickAction4,
	}

	[RequireComponent (typeof (CharacterController))]
	[RequireComponent (typeof (NavMeshAgent))]
	[RequireComponent (typeof (CGVPlayerSight))]
	public class CGVPlayerCharacter : CGVTarget, IPunObservable {

		protected readonly CGVPlayerAbility _DeathAbilityMask = (CGVPlayerAbility)0x01ffffff;
		protected readonly CGVPlayerAbility _InactiveAbilityMask = (CGVPlayerAbility)0x02ffffff;

		public delegate void CharacterPossessEvent (CGVPlayerCharacter character, int teamID);
		public static CharacterPossessEvent OnCharacterPossessed;
		public static CharacterPossessEvent OnCharacterUnpossessed;

		public virtual bool NeedsToHaveMainController { get { return false; } }

		public static CGVPlayerCharacter MainPlayer { get; protected set; }
		[Header ("Player")]

		[SerializeField] protected int _Team = JCEnumKey.InvalidId;
		public int Team {
			get { return _Team; }
			protected set {
				if (_Team == value)
					return;
				_Team = value;
#if CGV_DEBUG
				Color teamColor = CGVTeamEnum.GetValue (Team).Color;
				foreach (Renderer r in gameObject.GetComponentsInChildren<Renderer> ())
					r.material.color = teamColor;
#endif
			}
		}
		public CGVPlayerController PlayerController { get; protected set; }

		// Controls
		[Header ("Controls")]
		[SerializeField] protected float m_WalkSpeed = 5f;
		[SerializeField] protected float m_RunSpeed = 10f;
		[SerializeField] protected float m_Acceleration = 5f;
		[SerializeField] protected float m_Deceleration = 15f;
		[SerializeField] private AnimationCurve m_RunSpeedDotProductFactor = AnimationCurve.Constant (-1, 1, 1);
		[SerializeField] private AnimationCurve m_SpeedDotProductFactor = AnimationCurve.Constant (-1, 1, 1);
		[SerializeField] [Range (0f, 1f)] protected float m_RunstepLenghten;
		[SerializeField] protected float m_JumpSpeed;
		[SerializeField] protected float m_StickToGroundForce;
		[SerializeField] protected float m_GravityMultiplier;

		protected bool _IsWalking;

		protected bool _Jump;
		protected Vector3 _Movement = Vector3.zero;
		protected CharacterController _CharacterController;
		protected CGVFootsteps _FootSteps;
		protected CollisionFlags CharacterCollisionFlags;
		protected bool PreviouslyGrounded;
		protected bool IsJumping;

		protected float LastSpeed;

		// View
		[Header ("View")]
		[SerializeField] protected CGVMouseLook _MouseLook;
		public CGVMouseLook MouseLook { get { return _MouseLook; } }
		[SerializeField] protected Transform Neck;
		[SerializeField] protected Transform _Feet;
		public ref readonly Transform Feet { get { return ref _Feet; } }

		public CGVPlayerCamera Camera { get; protected set; }
		public CGVPlayerSight Sight { get; protected set; }
#if CGV_TEMP_CODE
		int CurrentTargetIndex = 0;
#endif

		protected CGVGroundTextureDetector _GroundTextureDetector = new CGVGroundTextureDetector ();
		public ref readonly CGVGroundTextureDetector GroundTextureDetector { get { return ref _GroundTextureDetector; } }

		[Header ("Misc")]
		[SerializeField] protected CGVProjectileTrajectoryRenderer _ProjectileTrajectoryRenderer;
		public CGVProjectileTrajectoryRenderer ProjectileTrajectoryRenderer { get { return _ProjectileTrajectoryRenderer; } }
		#region Equipment
		// Equipment

		[SerializeField] protected CGVEquipmentInventory _EquipmentInventory;
		public CGVEquipmentInventory EquipmentInventory { get { return _EquipmentInventory; } }

		[HideInInspector]
		[SerializeField] [NaughtyAttributes.ReorderableList] protected List<CGVEquipment> Equipment;

		protected int _ActiveEquipmentIndex = -1;
		protected int ActiveEquipmentIndex {
			get { return _ActiveEquipmentIndex; }
			set {
				if (_ActiveEquipmentIndex == value)
					return;
				CGVEquipment lastEquipment = ActiveEquipment;
				if (Equipment.Count <= 0)
					_ActiveEquipmentIndex = -1;
				else {
					if (value < 0)
						_ActiveEquipmentIndex = Equipment.Count - 1;
					else if (value >= Equipment.Count)
						_ActiveEquipmentIndex = 0;
					else
						_ActiveEquipmentIndex = value;
				}
				if (ActiveEquipment != lastEquipment) {
					if (lastEquipment != null)
						lastEquipment.Select (false);
					if (ActiveEquipment != null)
						ActiveEquipment.Select (true);
				}
				Debug.Log ("Selected " + ActiveEquipment);
			}
		}
		protected CGVEquipment ActiveEquipment {
			/*
			get { return ActiveEquipmentIndex < 0 ? null : Equipment[ActiveEquipmentIndex]; }
			set {
				if (value == null)
					ActiveEquipmentIndex = -1;
				else
					ActiveEquipmentIndex = Equipment.IndexOf (value);
			}/*/
			get { return EquipmentInventory.ActiveEquipment; }//*/
		}

		public void SwitchEquipment (bool next) {
			if (HasAbility (CGVPlayerAbility.SwitchEquipment)) {
				if (next)
					ActiveEquipmentIndex++;
				else
					ActiveEquipmentIndex--;
			}
		}
		/*
		protected bool CarryEquipment (CGVEquipment equipment) {
			if (equipment == null)
				return false;
			if (Equipment.Contains (equipment))
				return false;
			bool res = equipment.Carry (this);
			if (res) {
				if (Equipment.Count <= 0)
					_ActiveEquipmentIndex = 0;
				Equipment.Add (equipment);
			}
			return res;
		}

		protected bool DropEquipment (CGVEquipment equipment) {
			if (equipment == null)
				return false;
			bool res = Equipment.Remove (equipment);
			if (res) {
				if (ActiveEquipmentIndex >= Equipment.Count)
					ActiveEquipmentIndex--;
			}
			return res;
		}//*/

		[NaughtyAttributes.Required]
		[SerializeField]
		protected CGVAmmunitionInventory _Ammunitions;
		public CGVAmmunitionInventory Ammunitions { get { return _Ammunitions; } }
		#endregion Equipment

		#region Abilities
		// Abilities
		[JuneCodes.JCEnumFlags]
		[SerializeField]
		protected CGVPlayerAbility _PlayerAbilities;
		public CGVPlayerAbility PlayerAbilities { get { return _PlayerAbilities; } }

		protected static readonly CGVPlayerAbility EveryAbilityMask = (CGVPlayerAbility)(-1);

		protected List<CGVPlayerAbility> TemporaryAbilityMasks = new List<CGVPlayerAbility> ();
		protected CGVPlayerAbility TemporaryAbilityMask = CGVPlayerAbility.None;

		public void AddTemporaryAbilityMask (CGVPlayerAbility mask) {
			if (mask == CGVPlayerAbility.None)
				return;
			TemporaryAbilityMask |= mask;
			TemporaryAbilityMasks.Add (mask);
		}

		public bool RemoveTemporaryAbilityMask (CGVPlayerAbility mask) {
			if (mask == CGVPlayerAbility.None)
				return false;

			bool res = TemporaryAbilityMasks.Remove (mask);
			if (res)
				UpdateTemporaryAbilityMask ();
			return res;
		}

		protected void UpdateTemporaryAbilityMask () {
			TemporaryAbilityMask = CGVPlayerAbility.None;
			foreach (var m in TemporaryAbilityMasks)
				TemporaryAbilityMask |= m;
		}

		public bool HasAbility (CGVPlayerAbility ability) {
			if (TemporaryAbilityMasks.Count <= 0)
				return PlayerAbilities.HasFlag (ability);
			return PlayerAbilities.HasFlag (ability) && !TemporaryAbilityMask.HasFlag (ability);
		}

		public void AddAbility (CGVPlayerAbility ability) {
			_PlayerAbilities |= ability;
		}

		public void RemoveAbility (CGVPlayerAbility ability) {
			_PlayerAbilities &= ~ability;
		}
		#endregion Abilities

		public bool IsMainListener { get; protected set; } = false;
		protected AkSpatialAudioListener SpatialAudioListener;
		protected AkAudioListener AudioListener;

		void OnTriggerEnter (Collider other) {
			ICGVConsumable consumable = other.GetComponent<ICGVConsumable> ();
			if (consumable != null)
				consumable.PickUp (TakeConsumable (consumable.CheckContent ()));
		}

		public int TakeConsumable (CGVConsumableProperties consumable) {
			int res = -1;
			switch (consumable.Type) {
				case ConsumableType.Ammunition:
					int bullets = 0;
					if ((bullets = Ammunitions.AddAmmunition (consumable.Id, consumable.Quantity)) > 0) {
						if (ActiveEquipment != null)
							ActiveEquipment.UpdateEquipment ();
						Debug.Log (bullets + " ammunition added to inventory");
						res = bullets;
					}
					break;
			}
			return res;
		}

		protected override void Awake () {
			base.Awake ();
			Camera = GetComponentInChildren<CGVPlayerCamera> ();
			Sight = GetComponent<CGVPlayerSight> ();
			_CharacterController = GetComponent<CharacterController> ();
			_FootSteps = GetComponent<CGVFootsteps> ();
			_FootSteps.SetSpeeds (m_WalkSpeed, m_RunSpeed);
		}

		protected override void OnEnable () {
			base.OnEnable ();
			CGVPlayableSceneManager.OnSessionEnded += SessionEnded;
			/*
			if (!CGVPlayerManager.instance.GetInputDevices (this, out PlayerActions)) {
				Debug.LogWarning ("Could not find a controller for " + this);
				//Debug.Break ();
			}//*/
			//PlayerActions = CGVPlayerActions.CreateWithDefaultBindings ();
		}

		private void OnDisable () {
			CGVPlayableSceneManager.OnSessionEnded -= SessionEnded;
			/*
			if (!JCApplicationUtils.Quitting) {
				CGVPlayerManager.instance.RemovePlayer (this);
			}
			PlayerActions.Destroy ();//*/
		}

		protected virtual void Start () {
			IsJumping = false;
			LastSpeed = 0f;
			_MouseLook.Init (transform, Neck, Camera.transform);
			//Sight.Init (this);

			_EquipmentInventory = _EquipmentInventory.GetClone ();
			EquipmentInventory.InitForPlayer (this);
			_Ammunitions = _Ammunitions.GetClone ();
			/*
			for (int i = 0; i < Equipment.Count; ++i) {
				CGVEquipment equipment = Equipment[i].GetClone ();
				if (!equipment.Carry (this, true))
					DropEquipment (Equipment[i]);
				else
					Equipment[i] = equipment;
			}
			ActiveEquipmentIndex = 0;//*/

			AudioListener = GetComponentInChildren<AkAudioListener> ();
			SpatialAudioListener = GetComponentInChildren<AkSpatialAudioListener> ();
			if (AudioListener != null && AudioListener == AkSpatialAudioListener.TheSpatialAudioListener) {
				IsMainListener = true;
			}
		}

		protected override void Update () {
			base.Update ();

			if (!PreviouslyGrounded && _CharacterController.isGrounded) {
				_Movement.y = 0f;
				IsJumping = false;
			}
			if (!_CharacterController.isGrounded && !IsJumping && PreviouslyGrounded) {
				_Movement.y = 0f;
			}

			PreviouslyGrounded = _CharacterController.isGrounded;
		}

		public virtual void HoldAction (CGVPlayerAbility action) {
			if (((int)action & (int)CGVPlayerAbilityCategories.Actions) == 0)
				return;
			if (ActiveEquipment == null)
				return;

			switch (action) {
				case CGVPlayerAbility.PrimaryAction: if (HasAbility (CGVPlayerAbility.PrimaryAction)) ActiveEquipment.HoldPrimaryAction (); break;
				case CGVPlayerAbility.SecondaryAction: if (HasAbility (CGVPlayerAbility.SecondaryAction)) ActiveEquipment.HoldSecondaryAction (); break;
				case CGVPlayerAbility.SpecialAction: if (HasAbility (CGVPlayerAbility.SpecialAction)) ActiveEquipment.HoldSpecialAction (); break;
				case CGVPlayerAbility.OptionalAction: if (HasAbility (CGVPlayerAbility.OptionalAction)) ActiveEquipment.HoldOptionalAction (); break;
			}
		}

		public virtual void ReleaseAction (CGVPlayerAbility action) {
			if (((int)action & (int)CGVPlayerAbilityCategories.Actions) == 0)
				return;
			if (ActiveEquipment == null)
				return;

			switch (action) {
				case CGVPlayerAbility.PrimaryAction: ActiveEquipment.ReleasePrimaryAction (); break;
				case CGVPlayerAbility.SecondaryAction: ActiveEquipment.ReleaseSecondaryAction (); break;
				case CGVPlayerAbility.SpecialAction: ActiveEquipment.ReleaseSpecialAction (); break;
				case CGVPlayerAbility.OptionalAction: ActiveEquipment.ReleaseOptionalAction (); break;
			}
		}

		public void RadialMenuOpen () {
			AddTemporaryAbilityMask (EveryAbilityMask);
			RadialMenuOpenCharacter ();
		}
		public void RadialMenuClosed () {
			RemoveTemporaryAbilityMask (EveryAbilityMask);
			RadialMenuClosedCharacter ();
		}

		protected virtual void RadialMenuOpenCharacter () { }
		protected virtual void RadialMenuClosedCharacter () { }

		public void QuickAction1 (bool secondary = false) { if (HasAbility (CGVPlayerAbility.QuickAction1)) QuickAction1Character (secondary); }
		public void QuickAction2 (bool secondary = false) { if (HasAbility (CGVPlayerAbility.QuickAction2)) QuickAction2Character (secondary); }
		public void QuickAction3 (bool secondary = false) { if (HasAbility (CGVPlayerAbility.QuickAction3)) QuickAction3Character (secondary); }
		public void QuickAction4 (bool secondary = false) { if (HasAbility (CGVPlayerAbility.QuickAction4)) QuickAction4Character (secondary); }

		protected virtual void QuickAction1Character (bool secondary = false) { }
		protected virtual void QuickAction2Character (bool secondary = false) { }
		protected virtual void QuickAction3Character (bool secondary = false) { }
		protected virtual void QuickAction4Character (bool secondary = false) { }

		public void Jump () {
			if (HasAbility (CGVPlayerAbility.Jump)) {
				_Jump = !IsJumping;
			}
		}

		protected virtual void FixedUpdate () {
			_MouseLook.UpdateCursorLock ();

			RaycastHit hit;
			if (_GroundTextureDetector.CheckGroundTexture (transform.position, Vector3.down, _CharacterController.height, out hit)) {
				Debug.Log ("Human is on surface " + _GroundTextureDetector.TextureName);
				_GroundTextureDetector.SwitchGroundTexture (_FootSteps.SoundSource);
			}
			if (transform.position.y < CGVPlayableSceneManager.OOBHeight)
				Die (null);
		}

		public virtual void Move (Vector2 input) {
			if (!HasAbility (CGVPlayerAbility.Walk))
				return;

			// always move along the camera forward as it is the direction that it being aimed at
			Vector3 desiredMove = transform.forward * input.y + transform.right * input.x;

			float speed = 0f;
			if (input.sqrMagnitude > float.Epsilon) {
				// set the desired speed to be walking or running
				speed = m_SpeedDotProductFactor.Evaluate (Vector3.Dot (desiredMove.normalized, transform.forward)) * (_IsWalking ? m_WalkSpeed : m_RunSpeed);
				//speed = m_IsWalking ?
				//	m_WalkSpeed :
				//	m_RunSpeedDotProductFactor.Evaluate (Vector3.Dot (desiredMove.normalized, transform.forward)) * m_RunSpeed;
			}
			//Debug.Log(lastSpeed);
			speed = Mathf.Lerp (LastSpeed, speed, ((speed > LastSpeed ? m_Acceleration : m_Deceleration) * Time.fixedDeltaTime) / Mathf.Abs (speed - LastSpeed));

			//FootSteps.SetDirection (m_Input);
			//Debug.Log("Input Y : " + m_Input.y.ToString("0.000") + " ; Speed : " + speed.ToString("0.000") + " ; Direction : " + m_Input.x.ToString("0.000"));
			//FootSteps.SetSpeed (m_CharacterController.isGrounded ? m_CharacterController.velocity.magnitude : 0f);
			_FootSteps.SetSpeedAndDirection (_CharacterController.isGrounded ? _CharacterController.velocity.magnitude : 0f, input);

			// get a normal for the surface that is being touched to move along it
			RaycastHit hitInfo;
			Physics.SphereCast (transform.position, _CharacterController.radius, Vector3.down, out hitInfo,
							   _CharacterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
			desiredMove = Vector3.ProjectOnPlane (desiredMove, hitInfo.normal).normalized;

			_Movement.x = desiredMove.x * speed; _Movement.z = desiredMove.z * speed;
			//m_MoveDir = desiredMove * speed;

			//Debug.Log(m_MoveDir.magnitude);

			if (_CharacterController.isGrounded) {
				_Movement.y = -m_StickToGroundForce;

				if (_Jump) {
					_Movement.y = m_JumpSpeed;
					_Jump = false;
					IsJumping = true;
				}
			}
			else {
				_Movement += Physics.gravity * m_GravityMultiplier * Time.fixedDeltaTime;
			}

			CharacterCollisionFlags = _CharacterController.Move (_Movement * Time.fixedDeltaTime);

			_MouseLook.UpdateCursorLock ();

			LastSpeed = speed;
		}

		protected virtual void OnControllerColliderHit (ControllerColliderHit hit) {
			Rigidbody body = hit.collider.attachedRigidbody;
			//dont move the rigidbody if the character is on top of it
			if (CharacterCollisionFlags.HasFlag (CollisionFlags.Below)) {
				if (hit.gameObject.layer == CGVPersistentInfo.LayerGround) {
					_FootSteps.SetGroundPos (hit.point);
					return;
				}
			}

			if (body == null || body.isKinematic) {
				return;
			}
			Debug.Log ("ControllerColliderHit " + body);
			body.AddForceAtPosition (_CharacterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
		}

		public static bool DifferentTeams (CGVPlayerCharacter p1, CGVPlayerCharacter p2) {
			return p1.Team != p2.Team || !JCEnumKey.IdIsValid (p2.Team);
		}

		public override void TakeDamage (float damage, CGVPlayerCharacter fromPlayer, RaycastHit hit, ICGVWeapon weapon) {
			bool gotHit = weapon.TargetsEveryone;
			if (!gotHit) {
				if (fromPlayer == this)
					gotHit = CGVGameSettings.instance.SelfDamage;
				else
					gotHit = CGVGameSettings.instance.FriendlyFire || DifferentTeams (this, fromPlayer);
			}
			if (gotHit)
				base.TakeDamage (damage, fromPlayer, hit, weapon);
		}

		protected override void TriggerHitEvent (float damage, CGVPlayerCharacter fromPlayer, RaycastHit hit, ICGVWeapon weapon) {
			base.TriggerHitEvent (damage, fromPlayer, hit, weapon);
		}
		/*
		protected override bool Die (CGVPlayerCharacter player) {
			bool res = base.Die (player);
			if (res) {
			}
			return res;
		}//*/

		public override void ResetCharacter () {
			base.ResetCharacter ();
			State = CGVPlayableSceneManager.SessionIsOver ? TargetState.Inactive : TargetState.None;
		}

		protected override void StateChanged (TargetState newState, TargetState oldState) {
			switch (oldState) {
				case TargetState.Inactive: {
						RemoveTemporaryAbilityMask (_InactiveAbilityMask);
						break;
					}
				case TargetState.Dead: {
						RemoveTemporaryAbilityMask (_DeathAbilityMask);
						break;
					}
			}
			switch (newState) {
				case TargetState.Inactive: {
						AddTemporaryAbilityMask (_InactiveAbilityMask);
						break;
					}
				case TargetState.Dead: {
						AddTemporaryAbilityMask (_DeathAbilityMask);
						break;
					}
			}
		}

		void SessionEnded () {
			State = TargetState.Inactive;
		}

		public bool PossessBy (CGVPlayerController player) {
			if (PlayerController != null)
				return false;
			if (player == null)
				return false;

			PlayerController = player;
			Team = player.Team;

			OnCharacterPossessed?.Invoke (this, Team);
			PossessedBy (player);

			return true;
		}
		protected virtual void PossessedBy (CGVPlayerController player) { }

		public bool Unpossess () {
			if (PlayerController != null)
				return false;

			OnCharacterUnpossessed?.Invoke (this, Team);
			PlayerController = null;
			Team = JCEnumKey.InvalidId;

			return true;
		}

		public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info) {
			if (stream.IsWriting) {
				stream.SendNext (_Team);
			}
			else {
				Team = (int)stream.ReceiveNext ();
			}
		}
	}
}
