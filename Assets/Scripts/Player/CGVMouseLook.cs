﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

using JuneCodes.Maths;

namespace CGV {
	[Serializable]
	public class CGVMouseLook {
		public float XSensitivity = 2f;
		public float YSensitivity = 2f;

		[SerializeField]
		protected bool ClampVerticalRotation = true;
		[SerializeField]
		[NaughtyAttributes.MinMaxSlider (-90f, 90f)]
		protected Vector2 MinMaxVertical = new Vector2 (-90f, 90f);
		public Vector2 MinMaxVerticalAngle { get { return MinMaxVertical; } }

		[SerializeField]
		[NaughtyAttributes.MinMaxSlider (-45f, 45f)]
		protected Vector2 MinMaxNeck = new Vector2 (-45f, 45f);

		[SerializeField]
		protected AK.Wwise.Event CenterCameraEvent;
		[Min (0f)]
		[SerializeField]
		protected float CenterCameraDelay = 0.2f;

		public bool LockCursor = true;

		private bool _SnapToTarget = false;
		public bool SnapToTarget {
			get { return _SnapToTarget; }
			set {
				if (value) {
					CenteringCamera = false;
				}
				_SnapToTarget = value;
			}
		}
		public bool SmoothSnap = true;
		[Min (0.1f)]
		[SerializeField]
		protected float SmoothSpeed = 90f;
		[HideInInspector]
		public Vector3 Target;

		private Quaternion HorizontalRot;
		private Quaternion VerticalRot;
		private bool CursorIsLocked = true;

		private bool Mounted = false;
		private bool CenteringCamera = false;
		private float CenteringCameraEndTime;
		private float CenteringCameraSpeed;
		[SerializeField]
		protected GameObject CenteringCameraSoundSource;

		private Transform Character, Neck, Camera;

		public void SetMounted (bool mounted) {
			if (mounted == Mounted)
				return;
			Mounted = mounted;
			if (!Mounted) {
				HorizontalRot = Quaternion.identity;
				Character.localRotation *= Neck.localRotation;
				Neck.localRotation = Quaternion.identity;
			}
		}

		public void Init (Transform character, Transform neck, Transform camera) {
			Character = character;
			Neck = neck;
			Camera = camera;

			HorizontalRot = (Mounted ? neck : character).localRotation;
			VerticalRot = camera.localRotation;
		}

		public void CenterCamera () {
			if (SnapToTarget || CenteringCamera)
				return;

			//*
			CenteringCamera = true;
			CenteringCameraSpeed = Quaternion.Angle (Quaternion.identity, Mounted ? (Camera.localRotation * Neck.localRotation) : Camera.localRotation) / CenterCameraDelay;
			if (CenteringCameraSpeed <= float.Epsilon)
				CenteringCameraEndTime = 0f;
			else
				CenteringCameraEndTime = Time.time + CenterCameraDelay;
			/*/
			if (Mounted)
				Neck.localRotation = Quaternion.identity;
			Camera.localRotation = Quaternion.identity;//*/
			CenterCameraEvent?.Post (CenteringCameraSoundSource);
		}

		public void RotateView (Vector2 input) {
			//Debug.Log(CGVAngleUtils.SignedHorizontalAngle(Vector3.forward, camera.forward));
			Transform horizontalTransform = (Mounted ? Neck : Character);
			if (SnapToTarget) {
				HorizontalRot = Quaternion.Euler (0f, JCMaths.SignedHorizontalAngle (Vector3.forward, Target - horizontalTransform.position), 0f);
				if (Mounted) {
					HorizontalRot *= Quaternion.Inverse (Character.localRotation);
				}
				VerticalRot = Quaternion.Euler (-JCMaths.VerticalAngle (Vector3.forward, Target - Camera.position), 0f, 0f);
			}
			else if (CenteringCamera) {
				HorizontalRot = Mounted ? Quaternion.identity : horizontalTransform.localRotation;
				VerticalRot = Quaternion.identity;
			}
			else {
				HorizontalRot = horizontalTransform.localRotation;
				VerticalRot = Camera.localRotation;
			}

			//float yRot = CrossPlatformInputManager.GetAxis ("Mouse X") * XSensitivity;
			//float xRot = CrossPlatformInputManager.GetAxis ("Mouse Y") * YSensitivity;
			float yRot = input.x * XSensitivity;
			float xRot = input.y * YSensitivity;

			HorizontalRot *= Quaternion.Euler (0f, yRot, 0f);
			VerticalRot *= Quaternion.Euler (-xRot, 0f, 0f);

			if (ClampVerticalRotation)
				VerticalRot = ClampRotationAroundAxis (VerticalRot, 0, MinMaxVertical);
			if (Mounted)
				HorizontalRot = ClampRotationAroundAxis (HorizontalRot, 1, MinMaxNeck);

			if (SmoothSnap && (SnapToTarget || CenteringCamera)) {
				if (SnapToTarget) {
					float angle = Vector3.Angle (Camera.forward, Target - Camera.position);
					horizontalTransform.localRotation = Quaternion.Slerp (horizontalTransform.localRotation, HorizontalRot,
						Time.deltaTime * (SmoothSpeed / angle));
					Camera.localRotation = Quaternion.Slerp (Camera.localRotation, VerticalRot,
						Time.deltaTime * (SmoothSpeed / angle));
				}
				else {
					if (Time.time >= CenteringCameraEndTime) {
						CenteringCamera = false;
						if (Mounted)
							Neck.localRotation = Quaternion.identity;
						Camera.localRotation = Quaternion.identity;
					}
					else {
						float angle = Quaternion.Angle (Quaternion.identity, Mounted ? (Camera.localRotation * Neck.localRotation) : Camera.localRotation);
						if (Mounted)
							horizontalTransform.localRotation = Quaternion.Slerp (horizontalTransform.localRotation, HorizontalRot,
								Time.deltaTime * (CenteringCameraSpeed / angle));
						else
							horizontalTransform.localRotation = HorizontalRot;
						Camera.localRotation = Quaternion.Slerp (Camera.localRotation, VerticalRot,
							Time.deltaTime * (CenteringCameraSpeed / angle));
					}
				}
			}
			else {
				horizontalTransform.localRotation = HorizontalRot;
				Camera.localRotation = VerticalRot;
			}

			UpdateCursorLock ();
		}

		public void SetCursorLock (bool value) {
			LockCursor = value;
			if (!LockCursor) {//we force unlock the cursor if the user disable the cursor locking helper
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}
		}

		public void UpdateCursorLock () {
			//if the user set "lockCursor" we check & properly lock the cursor
			if (LockCursor)
				InternalLockUpdate ();
		}

		private void InternalLockUpdate () {
			if (Input.GetKeyUp (KeyCode.Escape)) {
				CursorIsLocked = false;
			}
			else if (Input.GetMouseButtonUp (0)) {
				CursorIsLocked = true;
			}

			if (CursorIsLocked) {
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
			else if (!CursorIsLocked) {
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}
		}

		Quaternion ClampRotationAroundAxis (Quaternion q, int axis, Vector2 minMax) {
			q.x /= q.w;
			q.y /= q.w;
			q.z /= q.w;
			q.w = 1.0f;

			float angle = 2.0f * Mathf.Rad2Deg * Mathf.Atan (q[axis]);

			angle = Mathf.Clamp (angle, minMax.x, minMax.y);

			q[axis] = Mathf.Tan (0.5f * Mathf.Deg2Rad * angle);

			return q;
		}
	}
}
