﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JuneCodes.FlexibleEnum;

namespace CGV.Player {
	[SelectionBase]
	public class CGVSpawn : MonoBehaviour {

		//public CGVPlayerCharacter PlayerToSpawn;

		[SerializeField]
		[JCEnumType (typeof (CGVTeamEnum))]
		protected JCEnumKey _Team;
		public int Team { get { return _Team.Id; } }

		[SerializeField]
		protected Transform _SpawnTransform;

		private void Awake () {
			if (_SpawnTransform == null)
				_SpawnTransform = transform;
		}

		protected HashSet<CGVPlayerCharacter> _Players = new HashSet<CGVPlayerCharacter> ();

		private void OnTriggerEnter (Collider other) {
			AddCharacter (other.GetComponent<CGVPlayerCharacter> ());
		}

		private void OnTriggerExit (Collider other) {
			CGVPlayerCharacter player = other.GetComponent<CGVPlayerCharacter> ();
			if (player != null) {
				if (_Players.Remove (player)) {
					player.OnTargetStateChanged -= CharacterStateChanged;
				}
			}
		}

		private void AddCharacter (CGVPlayerCharacter character) {
			if (character == null)
				return;
			if (_Players.Add (character)) {
				character.OnTargetStateChanged += CharacterStateChanged;
			}
		}

		private void CharacterStateChanged (CGVTarget target, CGVTarget.TargetState newState, CGVTarget.TargetState oldState) {
			if (newState == CGVTarget.TargetState.Dead || newState == CGVTarget.TargetState.Inactive) {
				//Debug.LogFormat ("Character {0} killed on spawn", target.Name);
				_Players.Remove (target as CGVPlayerCharacter);
			}
		}

		/// <summary>
		/// Checks wether or not a character can be spawned here.
		/// This spawn is free if no character is within its bounds.
		/// </summary>
		public bool IsFree () {
			return _Players.Count <= 0;
		}

		public bool Spawn (CGVPlayerCharacter character) {
			if (character == null)
				return false;
			if (!IsFree ())
				return false;
			if (!character.gameObject.scene.IsValid ())
				return false;

			CharacterController cc = character.GetComponent<CharacterController> ();
			if (cc != null) cc.enabled = false;
			character.transform.SetPositionAndRotation (_SpawnTransform.position, _SpawnTransform.rotation);
			character.GetComponent<Rigidbody> ().velocity = Vector3.zero;
			if (cc != null) cc.enabled = true;
			character.gameObject.SetActive (true);

			character.ResetCharacter ();
			AddCharacter (character);

			return true;
		}
	}
}
