﻿using UnityEngine;

#pragma warning disable 649
namespace CGV {
	[RequireComponent(typeof(Collider))]
	public abstract class CGVInteractable : MonoBehaviour {

		void OnEnable() {
			CanInteract = false;
		}

		void OnDisable() {
			CanInteract = false;
		}

		[Header("Interaction Events")]
		[SerializeField] private AK.Wwise.Event StartInteractEvent;
		[SerializeField] private AK.Wwise.Event StopInteractEvent;

		protected bool _CanInteract = false;
		public bool CanInteract {
			get { return _CanInteract; }
			protected set {
				if (_CanInteract == value)
					return;
				(value ? StartInteractEvent : StopInteractEvent)?.Post(gameObject);
				_CanInteract = value;
			}
		}

		void OnTriggerEnter(Collider other) {
			InteractableTriggerEnter(other);
		}

		protected virtual void InteractableTriggerEnter(Collider other) {
			if (CanInteract)
				return;
			if (other.CompareTag("Player")) {
				CanInteract = true;
				Debug.Log("Can Interact");
			}
		}

		void OnTriggerExit(Collider other) {
			InteractableTriggerExit(other);
		}

		protected virtual void InteractableTriggerExit(Collider other) {
			if (!CanInteract)
				return;
			if (other.CompareTag("Player"))
				CanInteract = false;
			Debug.Log("Cannot Interact");
		}

		public abstract bool Interact();
	}
}
