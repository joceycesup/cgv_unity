﻿using System.Collections;
using UnityEngine;

using JuneCodes.FlexibleEnum;
using JuneCodes;
using System;

using CGV.Player;

#pragma warning disable 649
namespace CGV {
	[RequireComponent (typeof (Rigidbody))]
	public class CGVProjectile : MonoBehaviour {

		public class Trajectory {
			public delegate void TrajectoryUpdatedEvent ();
			public TrajectoryUpdatedEvent OnTrajectoryUpdated;

			private Vector3[] _Positions;
			public Vector3[] Positions { get { return _Positions; } }
			public int MaxCount { get { return Positions.Length; } }
			private int _Count;
			public int Count {
				get { return _Count; }
				set { _Count = Mathf.Clamp (value, 1, MaxCount); }
			}
			public Vector3 Start { get { return Positions[0]; } }
			public Vector3 End { get { return Positions[Count - 1]; } }
			public bool Invalid = true;

			public void SetMaxCount (int count) {
				if (count == MaxCount)
					return;
				if (count <= 0)
					count = 1;
				Array.Resize<Vector3> (ref _Positions, count);
			}

			public Trajectory (int maxPositions) {
				if (maxPositions <= 0)
					maxPositions = 1;
				_Positions = new Vector3[maxPositions];
			}
		}

		/*
		[Header("Projectile Type")]
		[JCEnumType (typeof (CGVAmmunitionEnum), true)]
		[SerializeField] protected JCEnumKey _AmmoType;
		public int AmmoType {
			get { return _AmmoType.Id; }
		}//*/

		[Header ("WWise Events")]
		[SerializeField] protected AK.Wwise.Event SwooshStartEvent;
		[SerializeField] protected AK.Wwise.Event SwooshStopEvent;
		[SerializeField] protected AK.Wwise.Event HitEvent;

		// Misc
		protected GameObject _OriginalProjectile;

		protected Rigidbody _Rigidbody;

		public CGVPlayerCharacter Owner { get; protected set; }
		protected Vector3 _InitialPos;
		protected Vector3 _InitialVelocity;
		protected int _UpdateCount;

		protected bool _Launched = false;

		protected bool _LeftOwner = false;

		protected IEnumerator LaunchCoroutine;

		private void Awake () {
			_Rigidbody = GetComponent<Rigidbody> ();
			_Rigidbody.isKinematic = true;
		}

		public void Dispose () {
			JCPrefabPool.Store (_OriginalProjectile, gameObject);
		}

		public static CGVProjectile CreateProjectile (CGVProjectile original) {
			if (original == null)
				return null;
			CGVProjectile projectile = JCPrefabPool.Extract (original.gameObject).GetComponent<CGVProjectile> ();
			projectile._OriginalProjectile = original.gameObject;
			return projectile;
		}

		public virtual void ShootFromBarrel (CGVPlayerCharacter owner, GameObject barrel, float speed) {/*
			transform.SetPositionAndRotation (barrel.transform.position, barrel.transform.rotation);
			_Rigidbody.velocity = Vector3.zero;
			_Rigidbody.AddForce (barrel.transform.forward * speed, ForceMode.VelocityChange);
			_Launched = true;
			SwooshStartEvent?.Post (gameObject);/*/
			Owner = owner;
			_LeftOwner = false;
			_InitialPos = barrel.transform.position;
			_InitialVelocity = barrel.transform.forward * speed;
			StopLaunch ();
			StartCoroutine (LaunchCoroutine = LaunchProjectile ());
			//*/
		}

		protected void StopLaunch () {
			_Launched = false;
			if (LaunchCoroutine != null) {
				StopCoroutine (LaunchCoroutine);
				LaunchCoroutine = null;
			}
		}

		protected IEnumerator LaunchProjectile () {
			_UpdateCount = 0;
			_Launched = true;
			Vector3 newPos;
			while (_UpdateCount < 1024) {
				newPos = GetPosition (_UpdateCount);
				Vector3 velocity = GetVelocity (_InitialVelocity, _UpdateCount);
				if (_Rigidbody.SweepTest (newPos - transform.position, out RaycastHit hit, velocity.magnitude * Time.fixedDeltaTime)) {
					if (_LeftOwner && hit.collider.gameObject != Owner.gameObject) {
						Debug.Log (hit.collider.gameObject);
						//CGVDebugUtils.DrawCube (transform.position, 0.1f, Color.magenta, 3f);
						MoveToClosestDestination (newPos - transform.position);
						OnCollision (hit.collider.gameObject, hit.point, velocity);
						break;
					}
				}
				else {
					_LeftOwner = true;
					transform.position = newPos;
				}
				yield return new WaitForFixedUpdate ();
				_UpdateCount++;
			}
			//Debug.Log (_UpdateCount);
			_Launched = false;
			LaunchCoroutine = null;
		}

		protected float MoveToClosestDestination (Vector3 direction, int maxIterations = 4) {
			float distance = direction.magnitude;
			float division = 0.5f;
			float factor = 0f;
			Vector3 newDir = direction;
			RaycastHit hit;
			for (int i = 0; i < maxIterations; ++i, division /= 2f) {
				newDir = direction * (factor + division);
				if (!_Rigidbody.SweepTest (newDir, out hit, distance * (factor + division))) {
					transform.position += (direction * division);
					factor += division;
				}
			}
			//CGVDebugUtils.DrawCube (transform.position, 0.1f, Color.red, 3f);
			return factor;
		}

		protected virtual void OnCollision (GameObject other, Vector3 impactPoint, Vector3 velocity) {
			SwooshStopEvent?.Post (gameObject);
			HitEvent?.Post (gameObject);
		}

		private void OnCollisionEnter (Collision collision) {
			OnCollision (collision.gameObject, collision.GetContact (0).point, -collision.relativeVelocity);
		}

		public static Vector3 GetTrajectory (Vector3 initialPos, Vector3 direction, float speed, int frameSkip = 10) {
			return GetTrajectory (initialPos, direction * speed, false, frameSkip);
		}

		public static Vector3 GetTrajectory (Vector3 initialPos, Vector3 direction, float speed, bool testHit, int frameSkip = 10) {
			return GetTrajectory (initialPos, direction * speed, testHit, frameSkip);
		}

		public static Vector3 GetTrajectory (Vector3 initialPos, Vector3 initialVelocity, int frameSkip = 10) {
			return GetTrajectory (initialPos, initialVelocity, false, frameSkip);
		}

		public static Vector3 GetTrajectory (Vector3 initialPos, Vector3 initialVelocity, bool testHit, int frameSkip = 10) {/*
			int layer = (1 << CGVPersistentInfo.LayerCharacter) | (1 << CGVPersistentInfo.LayerGround) | (1 << CGVPersistentInfo.LayerWall);
			if (Physics.Raycast (initialPos, initialVelocity.normalized, out RaycastHit hit, float.MaxValue, layer)) {
				return hit.point;
			}
			return initialPos + initialVelocity;
			/*/

			Vector3 p1 = initialPos;
			Vector3 p2 = p1;
			if (frameSkip < 1)
				frameSkip = 1;
			else
				frameSkip++;
			int layer = (1 << CGVPersistentInfo.LayerCharacter) | (1 << CGVPersistentInfo.LayerGround) | (1 << CGVPersistentInfo.LayerWall);
			RaycastHit hit;
			for (int i = 1; i < 1024; i += frameSkip) {
				p2 = GetPosition (initialPos, initialVelocity, i);
				if (Physics.Raycast (p1, p2 - p1, out hit, GetVelocity (initialVelocity, i).magnitude * frameSkip * Time.fixedDeltaTime, layer)) {
					//Debug.Log (hit.collider.gameObject);
					p2 = hit.point;
					//Debug.DrawLine (p1, p2, Color.green);
					//CGVDebugUtils.DrawCube (p2, 0.25f, Color.red);
					break;
				}
				//Debug.DrawLine (p1, p2, Color.green);
				p1 = p2;
			}
			return p2;//*/
		}

		public static void GetTrajectory (Vector3 initialPos, Vector3 initialVelocity, Trajectory trajectory, bool testHit, int frameSkip = 10) {
			if (trajectory == null)
				throw new NullReferenceException ();

			Vector3 p1 = initialPos;
			Vector3 p2 = p1;
			if (frameSkip < 1)
				frameSkip = 1;
			else
				frameSkip++;
			int layer = (1 << CGVPersistentInfo.LayerCharacter) | (1 << CGVPersistentInfo.LayerGround) | (1 << CGVPersistentInfo.LayerWall);
			RaycastHit hit;
			trajectory.Positions[0] = initialPos;
			int i = 1;
			for (; i < trajectory.MaxCount; ++i) {
				int iteration = i * frameSkip;
				p2 = GetPosition (initialPos, initialVelocity, iteration);
				if (Physics.Raycast (p1, p2 - p1, out hit, GetVelocity (initialVelocity, iteration).magnitude * frameSkip * Time.fixedDeltaTime, layer)) {
					//Debug.Log (hit.collider.gameObject);
					p2 = hit.point;
					trajectory.Positions[i] = p2;
					Debug.DrawLine (p1, p2, Color.green);
					//CGVDebugUtils.DrawCube (p2, 0.1f, Color.red);
					break;
				}
				Debug.DrawLine (p1, p2, Color.green);
				Debug.DrawLine (p2, p2 + Vector3.up * 0.1f, Color.red);
				trajectory.Positions[i] = p2;
				p1 = p2;
			}
			trajectory.Count = Mathf.Min (i + 1, trajectory.MaxCount);
			trajectory.Invalid = (i >= trajectory.MaxCount);
			trajectory.OnTrajectoryUpdated?.Invoke ();
		}

		public static Vector3 GetLocalTrajectory (Vector3 initialPos, Vector3 initialVelocity, Trajectory trajectory, Transform trajectoryParent, bool testHit, int frameSkip = 10) {
			if (trajectory == null)
				throw new NullReferenceException ();

			Vector3 p1 = Vector3.zero;
			Vector3 p2 = p1;
			if (frameSkip < 1)
				frameSkip = 1;
			else
				frameSkip++;
			int layer = (1 << CGVPersistentInfo.LayerCharacter) | (1 << CGVPersistentInfo.LayerGround) | (1 << CGVPersistentInfo.LayerWall);
			RaycastHit hit;
			trajectory.Positions[0] = Vector3.zero;
			Vector3 res = initialPos;
			int i = 1;
			for (; i < trajectory.MaxCount; ++i) {
				int iteration = i * frameSkip;
				p2 = GetPosition (initialVelocity, iteration);
				if (Physics.Raycast (initialPos + p1, p2 - p1, out hit, GetVelocity (initialVelocity, iteration).magnitude * frameSkip * Time.fixedDeltaTime, layer)) {
					//Debug.Log (hit.collider.gameObject);
					res = hit.point;
					p2 = hit.point - initialPos;
					trajectory.Positions[i] = p2;
					Debug.DrawLine (initialPos + p1, initialPos + p2, Color.green);
					//CGVDebugUtils.DrawCube (p2, 0.1f, Color.red);
					break;
				}
				Debug.DrawLine (initialPos + p1, initialPos + p2, Color.green);
				Debug.DrawRay (initialPos + p2, Vector3.up * 0.1f, Color.red);
				trajectory.Positions[i] = p2;
				p1 = p2;
			}
			trajectory.Count = Mathf.Min (i + 1, trajectory.MaxCount);
			trajectory.Invalid = (i >= trajectory.MaxCount);
			if (!trajectory.Invalid) {
				float max = i;
				for (i = 1; i < trajectory.Count; ++i) {
					trajectory.Positions[i] = trajectoryParent.InverseTransformPoint (Vector3.Lerp (trajectoryParent.position, initialPos, i / max) + trajectory.Positions[i]);
				}
			}
			trajectory.OnTrajectoryUpdated?.Invoke ();
			return res;
		}

		protected Vector3 GetPosition (int iteration) {
			return GetPosition (_InitialPos, _InitialVelocity, iteration);
		}

		protected static Vector3 GetPosition (Vector3 initialVelocity, int iteration) {
			float t = iteration * Time.fixedDeltaTime;
			float v = initialVelocity.y + 0.5f * Physics.gravity.y * t;

			return new Vector3 (initialVelocity.x, v, initialVelocity.z) * t;
		}

		protected static Vector3 GetPosition (Vector3 initialPos, Vector3 initialVelocity, int iteration) {
			return initialPos + GetPosition (initialVelocity, iteration);
		}

		protected static Vector3 GetVelocity (Vector3 initialVelocity, int iteration) {
			return new Vector3 (initialVelocity.x, (Physics.gravity.y * iteration * Time.fixedDeltaTime) + initialVelocity.y, initialVelocity.z);
		}
	}
}
