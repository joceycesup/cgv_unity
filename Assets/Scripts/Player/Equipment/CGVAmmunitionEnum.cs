﻿using System;
using UnityEngine;

using JuneCodes.FlexibleEnum;

#pragma warning disable 649
namespace CGV.Equipment {
	/// <summary>
	/// Type of ammunition that can be used in the game.
	/// </summary>
	[Serializable]
	public class CGVAmmunitionType : JCEnumValue {
		[SerializeField] protected int _MaxInInventory = 100;
		public int MaxInInventory { get { return _MaxInInventory; } }
		[SerializeField] protected AK.Wwise.Event _PlayerPickUpEvent;
		public AK.Wwise.Event PlayerPickUpEvent { get { return _PlayerPickUpEvent; } }
		[SerializeField] protected AK.Wwise.Event _DogPickUpEvent;
		public AK.Wwise.Event DogPickUpEvent { get { return _DogPickUpEvent; } }
	}

	/// <summary>
	/// Container class for Ammunition types.
	/// Used by CGVAmmunitionEnum
	/// </summary>
	[Serializable]
	public class CGVAmmunitionTypeList : JCEnumValueContainer<CGVAmmunitionType> { }

	/// <summary>
	/// Singleton asset class to access the different types of Ammunitions available in the game.
	/// It is located at the path "Assets/Resources/AmmunitionTypes.asset".
	/// See CGVAmmunitionType for more information about the values themselves.
	/// </summary>
	[CreateAssetMenu (fileName = "AmmunitionTypes.asset", menuName = "CGV/Equipment/Ammunition Types")]
	public class CGVAmmunitionEnum : JCFlexibleEnumSet<CGVAmmunitionEnum, CGVAmmunitionTypeList, CGVAmmunitionType> {
	}
}
