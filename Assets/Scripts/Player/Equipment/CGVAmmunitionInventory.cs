﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JuneCodes.FlexibleEnum;

namespace CGV.Equipment {
	/// <summary>
	/// Asset class that represents the ammunitions possessed by a character.
	/// It can be created using the contextual menu "Create/CGV/Equipment/Ammunition Inventory".
	/// See CGVScene and CGVAmmunitionType for more information about ammunitions.
	/// </summary>
	[CreateAssetMenu (fileName = "AmmunitionInventory", menuName = "CGV/Equipment/Ammunition Inventory")]
	public class CGVAmmunitionInventory : CGVClonableScriptableObject<CGVAmmunitionInventory>, ISerializationCallbackReceiver {
		private Dictionary<int, int> Ammunitions = new Dictionary<int, int> ();
		[SerializeField, HideInInspector]
		private List<Vector2Int> AmmunitionsSerialized = new List<Vector2Int> ();

		public bool AmmoTypeIsInInvetory (int ammoType) {
			return Ammunitions.ContainsKey (ammoType);
		}

		public bool RemoveAmmoType (int ammoType) {
			return Ammunitions.Remove (ammoType);
		}

		public int[] GetAmmoTypes () {
			int[] res = new int[Ammunitions.Count];
			Ammunitions.Keys.CopyTo (res, 0);
			return res;
		}

		public int GetAmmunitionCount (int ammoType) {
			int available = 0;
			Ammunitions.TryGetValue (ammoType, out available);
			return available;
		}

		public int SetAmmunitionCount (int ammoType, int bullets) {
			CGVAmmunitionType ammoTypeInfo = CGVAmmunitionEnum.GetValue (ammoType);
			// this type of ammo is unknown
			// should never happen?
			if (ammoTypeInfo == null)
				return 0;
			bullets = Mathf.Clamp (bullets, 0, ammoTypeInfo.MaxInInventory);
			Ammunitions[ammoType] = bullets;
			return bullets;
		}

		public int GetAmmunition (int ammoType, int bullets) {
			int available = 0;
			if (Ammunitions.TryGetValue (ammoType, out available)) {
				if (available > bullets) {
					Ammunitions[ammoType] = available - bullets;
					available = bullets;
				}
				else if (available > 0) {
					Ammunitions[ammoType] = 0;
				}
			}
			return available;
		}

		public int AddAmmunition (int ammoType, int bullets) {
			if (bullets <= 0)
				return 0;

			CGVAmmunitionType ammoTypeInfo = CGVAmmunitionEnum.GetValue (ammoType);
			// this type of ammo is unknown
			// should never happen?
			if (ammoTypeInfo == null)
				return 0;

			if (Ammunitions.TryGetValue (ammoType, out int available)) {
				available += bullets;
				if (available > ammoTypeInfo.MaxInInventory) {
					bullets -= available - ammoTypeInfo.MaxInInventory;
					available = ammoTypeInfo.MaxInInventory;
				}
				if (bullets > 0) {
					Ammunitions[ammoType] = available;
				}
			}
			else {
				bullets = Mathf.Min (bullets, ammoTypeInfo.MaxInInventory);
				Ammunitions[ammoType] = bullets;
			}
			return bullets;
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize () {
			if (Ammunitions == null)
				Ammunitions = new Dictionary<int, int> ();
			else
				Ammunitions.Clear ();
			foreach (Vector2Int pair in AmmunitionsSerialized) {
				Ammunitions[pair.x] = pair.y;
			}
#if !UNITY_EDITOR
			AmmunitionsSerialized = null;
#endif
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize () {/*
			Debug.Log ("Serializing Inventory");
			AmmunitionsSerialized.Clear ();
			foreach (var pair in Ammunitions) {
				AmmunitionsSerialized.Add (new Vector2Int (pair.Key, pair.Value));
				//Debug.LogFormat ("\t{0} : {1}", pair.Key, pair.Value);
			}//*/
		}

		protected override void InitClone () { }
	}
}
