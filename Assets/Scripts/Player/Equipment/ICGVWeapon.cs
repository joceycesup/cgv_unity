﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGV.Equipment {
	public interface ICGVWeapon {

		bool TargetsEveryone { get; }
	}
}
