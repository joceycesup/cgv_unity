﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using JuneCodes.Editor;
using JuneCodes.FlexibleEnum.Editor;

using CGV.Equipment;

namespace CGV.Editor {
	[CustomEditor (typeof (CGVAmmunitionInventory))]
	public class CGVAmmunitionInventoryInspector : UnityEditor.Editor {

		CGVAmmunitionInventory Inventory;

		private ReorderableList InventoryList;

		SerializedProperty InventoryProperty;

		int CurrentAmmoType = -1;
		int CurrentAmmoCount = 0;

		private void OnEnable () {
			Inventory = (CGVAmmunitionInventory)target;
			InventoryProperty = serializedObject.FindProperty ("AmmunitionsSerialized");

			InventoryList = new ReorderableList (serializedObject, InventoryProperty, true, false, false, true) {
				drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
					Vector2Int value = InventoryProperty.GetArrayElementAtIndex (index).vector2IntValue;

					CGVAmmunitionType ammoTypeInfo = CGVAmmunitionEnum.GetValue (value.x);
					int newCount = EditorGUI.IntField (rect, ammoTypeInfo?.Name, value.y);

					if (newCount != value.y) {
						InventoryProperty.GetArrayElementAtIndex (index).vector2IntValue = new Vector2Int (value.x, Inventory.SetAmmunitionCount (value.x, newCount));
						serializedObject.ApplyModifiedProperties ();
					}
				},
				onRemoveCallback = (ReorderableList list) => {
					SerializedProperty element = InventoryProperty.GetArrayElementAtIndex (list.index);
					InventoryProperty.DeleteArrayElementAtIndex (list.index);
					list.serializedProperty.serializedObject.ApplyModifiedProperties ();
				}
			};
		}

		public override void OnInspectorGUI () {
			InventoryList.DoLayoutList ();

			Rect enumFieldRect = EditorGUILayout.GetControlRect (true);
			enumFieldRect.width /= 2f;
			MessageType fieldState = MessageType.None;
			if (Inventory.AmmoTypeIsInInvetory (CurrentAmmoType))
				fieldState = MessageType.Warning;
			//CurrentAmmoType = EditorGUI.IntField (enumFieldRect, CurrentAmmoType);
			CurrentAmmoType = JCEnumGUI.EnumKeyField (enumFieldRect, GUIContent.none, CurrentAmmoType, typeof (CGVAmmunitionEnum), true, fieldState);

			enumFieldRect.x += enumFieldRect.width;
			CGVAmmunitionType ammoTypeInfo = CGVAmmunitionEnum.GetValue (CurrentAmmoType);
			int maxAmmoCount = ammoTypeInfo != null ? ammoTypeInfo.MaxInInventory : int.MaxValue;
			float labelWidth = EditorGUIUtility.labelWidth;
			EditorGUIUtility.labelWidth = 5f;
			CurrentAmmoCount = Mathf.Clamp (EditorGUI.IntField (enumFieldRect, " ", CurrentAmmoCount), 0, maxAmmoCount);
			EditorGUIUtility.labelWidth = labelWidth;

			EditorGUI.BeginDisabledGroup (ammoTypeInfo == null || Inventory.AmmoTypeIsInInvetory (CurrentAmmoType));
			if (GUILayout.Button ("Add Ammo type")) {
				InventoryProperty.arraySize++;
				InventoryProperty.GetArrayElementAtIndex (InventoryProperty.arraySize - 1).vector2IntValue = new Vector2Int (CurrentAmmoType, CurrentAmmoCount);
				//Inventory.AddAmmunition (CurrentAmmoType, CurrentAmmoCount);
				CurrentAmmoType = -1;
				CurrentAmmoCount = 0;
				serializedObject.ApplyModifiedProperties ();
			}
			EditorGUI.EndDisabledGroup ();
		}
	}
}
