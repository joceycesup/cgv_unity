﻿using UnityEngine;

using JuneCodes;

namespace CGV.Equipment {
	[CreateAssetMenu (fileName = "ProjectileLauncherAction", menuName = "CGV/Equipment/Action/ProjectileLauncher")]
	public class CGVProjectileLauncherAction : CGVFirearmAction {

		[NaughtyAttributes.Required]
		[SerializeField] protected CGVProjectile _ProjectilePrefab;
		public CGVProjectile ProjectilePrefab { get { return _ProjectilePrefab; } }

		protected override void ShootBullet (GameObject barrel = null) {
			//CGVProjectile projectile = Instantiate (_ProjectilePrefab);

			CGVProjectile projectile = CGVProjectile.CreateProjectile (_ProjectilePrefab);
			base.ShootBullet (barrel);
			//projectile.OnDisappear += (p) => { JCPrefabPool.Store (_ProjectilePrefab.gameObject, p.gameObject); };
			projectile.gameObject.SetActive (true);
			projectile.ShootFromBarrel (_Owner.Carrier, barrel, MuzzleVelocity);
		}
	}
}
