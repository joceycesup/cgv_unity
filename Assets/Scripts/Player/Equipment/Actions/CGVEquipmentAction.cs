﻿using UnityEngine;

using CGV.Player;

namespace CGV.Equipment {
	/// <summary>
	/// Base class for an equipment action.
	/// It handles the initialization, the start and stop of the action and how loud it is (see CGVPlayerSight for more information).
	/// </summary>
	public abstract class CGVEquipmentAction : CGVClonableScriptableObject<CGVEquipmentAction> {

		protected CGVEquipment _Owner;

		[Tooltip ("Should it be triggered when owning equipment is selected?")]
		[SerializeField] protected bool TriggerActionOnSelected = false;
		[Tooltip ("Is the action executed as long as the player holds the corresponding button?")]
		[SerializeField] protected bool ContinuousAction = true;

		protected bool _Pressed = false;
		protected bool _FirstFrame { get { return !_Pressed; } }

		[SerializeField] protected CGVPlayerSight.DetectionRadius _SoundRadius = CGVPlayerSight.DetectionRadius.NoSoundEmitted;

		protected abstract void InitAction ();

		public void Init (CGVEquipment owner) {
			_Owner = owner;
			//Debug.LogFormat ("{0} Init", this);
			InitAction ();
		}

		public abstract void EquipmentCarried (CGVPlayerCharacter player, bool baseEquipment);
		public abstract void EquipmentDropped (CGVPlayerCharacter player);
		public abstract void EquipmentUpdated ();

		public void EquipmentSelected (bool selected) {
			if (TriggerActionOnSelected && selected)
				HoldAction ();
			EquipmentSelectedAction (selected);
		}
		protected abstract void EquipmentSelectedAction (bool selected);

		protected abstract bool HoldAction ();
		protected abstract bool ReleaseAction ();

		public bool Hold () {
#if CGV_DEBUG
			if (_SoundRadius != CGVPlayerSight.DetectionRadius.AlwaysHeard && _SoundRadius != CGVPlayerSight.DetectionRadius.NoSoundEmitted) {
				Color soundColor = _SoundRadius == CGVPlayerSight.DetectionRadius.Level1 ? Color.yellow : (_SoundRadius == CGVPlayerSight.DetectionRadius.Level2 ? Color.magenta : Color.red);
				CGVDebugUtils.DrawCircle (_Owner.Carrier.transform.position, Vector3.up, CGVGameSettings.instance.DistanceForDetectionRadius (_SoundRadius), soundColor, 16);
			}
#endif
			if (!ContinuousAction && _Pressed)
				return false;
			bool res = HoldAction ();
			_Pressed = true;
			if (res && _SoundRadius != CGVPlayerSight.DetectionRadius.NoSoundEmitted) {
				CGVPlayerSight.OnPlayerSoundEmitted?.Invoke (_Owner.Carrier, _SoundRadius);
			}
			return res;
		}

		public bool Release () {
			if (!_Pressed)
				return false;
#if CGV_DEBUG
			if (_SoundRadius != CGVPlayerSight.DetectionRadius.AlwaysHeard && _SoundRadius != CGVPlayerSight.DetectionRadius.NoSoundEmitted) {
				Color soundColor = _SoundRadius == CGVPlayerSight.DetectionRadius.Level1 ? Color.yellow : (_SoundRadius == CGVPlayerSight.DetectionRadius.Level2 ? Color.magenta : Color.red);
				CGVDebugUtils.DrawCircle (_Owner.Carrier.transform.position, Vector3.up, CGVGameSettings.instance.DistanceForDetectionRadius (_SoundRadius), soundColor, 16, 0.5f);
			}
#endif
			_Pressed = false;
			return ReleaseAction ();
		}
	}
}
