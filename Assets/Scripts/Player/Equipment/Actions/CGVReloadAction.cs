﻿using UnityEngine;

using CGV.Player;

namespace CGV.Equipment {
	[CreateAssetMenu (fileName = "ReloadAction", menuName = "CGV/Equipment/Action/Reload")]
	public class CGVReloadAction : CGVEquipmentAction {

		protected CGVFirearmAction Weapon;

		protected override void InitAction () {
			for (int i = 0; i < CGVEquipment.NbActions; ++i) {
				Weapon = _Owner[i] as CGVFirearmAction;
				if (Weapon != null) {
					break;
				}
			}
			if (Weapon == null) {
				Debug.LogErrorFormat ("{0} doesn't have a WeaponAction to reload!", _Owner);
			}
		}

		protected override void InitClone () { }

		public override void EquipmentCarried (CGVPlayerCharacter player, bool baseEquipment) { }
		public override void EquipmentDropped (CGVPlayerCharacter player) { }
		public override void EquipmentUpdated () { }

		protected override void EquipmentSelectedAction (bool selected) { }

		protected override bool HoldAction () {
			return Weapon.StartReload ();
		}

		protected override bool ReleaseAction () {
			return true;
		}
	}
}
