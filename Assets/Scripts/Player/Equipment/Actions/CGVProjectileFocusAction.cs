﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CGV.Sound;
using CGV.Player;

namespace CGV.Equipment {
	[CreateAssetMenu (fileName = "ProjectileFocusAction", menuName = "CGV/Equipment/Action/ProjectileFocus")]
	public class CGVProjectileFocusAction : CGVEquipmentAction {

		[Header ("Sonification")]
		[SerializeField] protected AK.Wwise.Event _StaticLocationEvent;
		[Tooltip ("Event played when the new predicted location is too far from the last one")]
		[SerializeField] protected AK.Wwise.Event _DynamicLocationEvent;
		[SerializeField] protected AK.Wwise.Event _StopLocationEvent;
		[SerializeField] protected AK.Wwise.Event _StopFocusEvent;
		[Min (0f)]
		[SerializeField] protected float _DelayBetweenStaticSounds = 0.5f;
		[Min (0f)]
		[SerializeField] protected float _DelayBetweenDynamicSounds = 0.2f;
		[Min (0f)]
		[SerializeField] protected float _MaxDistanceBetweenStaticSounds = 5f;

		[Min (0f)]
		[SerializeField] private float _DelayBetweenPredictions = 0.05f;
		protected float _NextPredictionTime = 0f;

		protected CGVFirearmAction _ProjectileLauncher;
		protected Vector3 _LastPredictedLocation = Vector3.zero;
		protected Vector3 _PredictedLocation = Vector3.zero;
		protected float _LastPredictionSoundTime;
		protected CGVSingleShotSoundEvent _LastPredictionSingleShot;

		protected CGVProjectile.Trajectory _BackupTrajectory = null; // present in case the carrier doesn't have a renderer

		protected override void InitAction () { }

		protected override void InitClone () { }

		public override void EquipmentCarried (CGVPlayerCharacter player, bool baseEquipment) {
			_ProjectileLauncher = _Owner.PrimaryAction as CGVFirearmAction;
		}
		public override void EquipmentDropped (CGVPlayerCharacter player) {
		}
		public override void EquipmentUpdated () { }

		protected override void EquipmentSelectedAction (bool selected) {
		}

		protected override bool HoldAction () {
			bool dynamicDelayElapsed = Time.time > (_LastPredictionSoundTime + _DelayBetweenDynamicSounds);
			//if (dynamicDelayElapsed && Time.time >= _NextPredictionTime)
			CGVProjectile.Trajectory trajectory;
			if (_Owner.Carrier.ProjectileTrajectoryRenderer == null) {
				if (_BackupTrajectory == null) {
					_BackupTrajectory = new CGVProjectile.Trajectory (128);
				}
				trajectory = _BackupTrajectory;
				CGVProjectile.GetTrajectory (_ProjectileLauncher.Barrel.transform.position, _ProjectileLauncher.Barrel.transform.forward * _ProjectileLauncher.MuzzleVelocity, trajectory, true, 5);
				_PredictedLocation = trajectory.End;
			}
			else {
				if (_Owner.Carrier.ProjectileTrajectoryRenderer.PredictedTrajectory == null) {
					_Owner.Carrier.ProjectileTrajectoryRenderer.PredictedTrajectory = new CGVProjectile.Trajectory (128);
				}
				trajectory = _Owner.Carrier.ProjectileTrajectoryRenderer.PredictedTrajectory;
				// higher (than 5) frameskip value will result in a low-res trajectory prediction
				_PredictedLocation = CGVProjectile.GetLocalTrajectory (_ProjectileLauncher.Barrel.transform.position, _ProjectileLauncher.Barrel.transform.forward * _ProjectileLauncher.MuzzleVelocity, trajectory, _Owner.Carrier.ProjectileTrajectoryRenderer.transform, true, 5);
			}

			if (_FirstFrame) {
				_Owner.Carrier.ProjectileTrajectoryRenderer?.gameObject.SetActive (true);
				PlayPredictionSound (_StaticLocationEvent);
			}
			else if (dynamicDelayElapsed) {
				if (Vector3.Distance (_PredictedLocation, _LastPredictedLocation) > _MaxDistanceBetweenStaticSounds) {
					PlayPredictionSound (_DynamicLocationEvent);
				}
				else if (Time.time > (_LastPredictionSoundTime + _DelayBetweenStaticSounds)) {
					PlayPredictionSound (_StaticLocationEvent);
				}
			}

			return true;
		}

		protected void PlayPredictionSound (AK.Wwise.Event sound) {
			_LastPredictionSoundTime = Time.time;
			_LastPredictionSingleShot = CGVSingleShotSoundEvent.PlaySingleShotAtLocation (sound, _PredictedLocation);
			_LastPredictedLocation = _PredictedLocation;
		}

		protected override bool ReleaseAction () {
			_Owner.Carrier.ProjectileTrajectoryRenderer?.gameObject.SetActive (false);
			_LastPredictionSoundTime = 0f;
			_StopFocusEvent?.Post (null);
			return true;
		}
	}
}
