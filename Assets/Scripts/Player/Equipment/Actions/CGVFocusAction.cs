﻿using UnityEngine;

using JuneCodes.Maths;

using CGV.Player;

namespace CGV.Equipment {
	[CreateAssetMenu (fileName = "FocusAction", menuName = "CGV/Equipment/Action/Focus")]
	public class CGVFocusAction : CGVEquipmentAction {

		[Header ("Focus Mode")]
		[Tooltip ("Leave empty if you don't want this weapon to be able to focus")]
		[SerializeField] protected CGVFocusModePreset FocusMode;

		private bool CanStartFocus = true;

		protected override void InitAction () { }

		protected override void InitClone () { }

		public override void EquipmentCarried (CGVPlayerCharacter player, bool baseEquipment) {
			AkSoundEngine.SetSwitch ("Waveform_Simple", "Sine", player.Camera.gameObject);
		}
		public override void EquipmentDropped (CGVPlayerCharacter player) {
			if (_Pressed)
				_Owner.Carrier.Sight.IsAiming = false;
		}
		public override void EquipmentUpdated () { }

		protected override void EquipmentSelectedAction (bool selected) {
			if (selected) {
				_Owner.Carrier.Sight.OnCurrentTargetLost += LostCurrentTarget;
				_Owner.Carrier.Sight.OnTargetAcquired += RefreshCurrentTarget;
			}
			else {
				_Owner.Carrier.Sight.OnCurrentTargetLost -= LostCurrentTarget;
				_Owner.Carrier.Sight.OnTargetAcquired -= RefreshCurrentTarget;
				DisableFocus ();
			}
		}

		protected void LostCurrentTarget (CGVTarget target) {
			//Owner.Carrier.Sight.FindClosestTarget ();
			if (_Owner.Carrier.Sight.FindClosestTarget ()) {
				Debug.Log ("Lost current target, found closest");
			}
			else {
				Debug.Log ("Couldn't find closest target");
			}
		}

		protected void RefreshCurrentTarget (CGVTarget target) {
			if (_Owner.Carrier.Sight.CurrentTarget == null) {
				Debug.Log ("Setting current target to newly spawned");
				_Owner.Carrier.Sight.CurrentTarget = target;
			}
		}

		protected override bool HoldAction () {//*
			if (FocusMode == null)
				return false;

			if (_FirstFrame) {
				CanStartFocus = true;
				_Owner.Carrier.AddAbility (CGVPlayerAbility.SwitchTarget);
				if (!FocusMode.RememberLastTarget)
					_Owner.Carrier.Sight.FindClosestTarget ();
				//TODO: RememberLastTarget
				//else
				//Owner.Carrier.Sight.CurrentTarget = 
			}

			if (_Owner.Carrier.Sight.CurrentTarget == null || !_Owner.Carrier.Sight.CurrentTarget.IsVisible) {
				_Owner.Carrier.MouseLook.SnapToTarget = false;
				return false;
			}

			if (_FirstFrame) {
				_Owner.Carrier.Sight.TargetIsLocked = false;
				_Owner.Carrier.Sight.IsAiming = true;
			}

			Transform transform = _Owner.Carrier.Camera.transform;
			Vector3 forward = transform.forward;

			bool wasOnTarget = _Owner.Carrier.Sight.TargetIsLocked;

			Bounds targetBounds = _Owner.Carrier.Sight.CurrentTarget.Bounds.GetColliderBounds ();

			Vector3 vectorToTarget = targetBounds.center - transform.position;

			bool isUnderTarget = targetBounds.center.y >= transform.position.y;

			float hMinAngle = Mathf.Max (
				JCMaths.HorizontalAngle (vectorToTarget, (targetBounds.center + targetBounds.extents) - transform.position),
				JCMaths.HorizontalAngle (vectorToTarget, (targetBounds.center + new Vector3 (targetBounds.extents.x, 0f, -targetBounds.extents.z)) - transform.position));
			float hAngle = JCMaths.HorizontalAngle (forward, vectorToTarget) - hMinAngle;
			float vMinAngle = Vector3.Angle (vectorToTarget, (targetBounds.center + new Vector3 (0f, isUnderTarget ? targetBounds.extents.y : -targetBounds.extents.y)) - transform.position);
			float vAngle = Mathf.Abs (JCMaths.VerticalAngle (forward, vectorToTarget)) - vMinAngle;

			if (_Owner.Carrier.IsMainListener) {
				//AkSoundEngine.SetRTPCValue (FocusMode.DistFocusX.Id, Mathf.Max (0f, hAngle / FocusMode.MaxAimAngle.x));
				FocusMode.DistFocusX?.SetGlobalValue (Mathf.Max (0f, hAngle / FocusMode.MaxAimAngle.x));

				//AkSoundEngine.SetRTPCValue (FocusMode.DistFocusY.Id, Mathf.Max (0f, vAngle / FocusMode.MaxAimAngle.y));
				//AkSoundEngine.SetRTPCValue (FocusMode.DistFocusYSigned.Id, Mathf.Max (0f, vAngle / FocusMode.MaxAimAngle.y) * (forward.y >= vectorToTarget.normalized.y ? 1f : -1f));
				//AkSoundEngine.SetRTPCValue (FocusMode.SumAxes.Id, Mathf.Max (0f, vAngle / FocusMode.MaxAimAngle.y) + Mathf.Max (0f, hAngle / FocusMode.MaxAimAngle.x));
				FocusMode.DistFocusY?.SetGlobalValue (Mathf.Max (0f, vAngle / FocusMode.MaxAimAngle.y));
				FocusMode.DistFocusYSigned?.SetGlobalValue (Mathf.Max (0f, vAngle / FocusMode.MaxAimAngle.y) * (forward.y >= vectorToTarget.normalized.y ? 1f : -1f));
				FocusMode.SumAxes?.SetGlobalValue (Mathf.Max (0f, vAngle / FocusMode.MaxAimAngle.y) + Mathf.Max (0f, hAngle / FocusMode.MaxAimAngle.x));
			}

#if CGV_DEBUG
			Vector3 right = Vector3.Cross (Vector3.up, vectorToTarget.normalized);
			Debug.DrawRay (transform.position, right, Color.magenta);
			Debug.DrawLine (transform.position, targetBounds.center, Color.black);
			Debug.DrawLine (transform.position, targetBounds.center + new Vector3 (0, targetBounds.extents.y), Color.grey);
			Debug.DrawLine (transform.position, targetBounds.center - new Vector3 (0, targetBounds.extents.y), Color.grey);
			Debug.DrawLine (transform.position, targetBounds.center + right * Mathf.Min (targetBounds.extents.x, targetBounds.extents.z), Color.grey);
			Debug.DrawLine (transform.position, targetBounds.center - right * Mathf.Min (targetBounds.extents.x, targetBounds.extents.z), Color.grey);
			Debug.DrawLine (transform.position, transform.position + forward * vectorToTarget.magnitude, _Owner.Carrier.Sight.TargetIsLocked ? Color.red : Color.green);

			//VerticalLine.color = hAngle >= 0f ? Color.red : Color.green;
			//HorizontalLine.color = vAngle >= 0f ? Color.red : Color.green;
			//VerticalLine.rectTransform.localScale = HorizontalLine.rectTransform.localScale = new Vector3 (1f, IsOnTarget ? 0.5f : 1f, 1f);
#endif

			bool vAligned = vAngle < 0f, hAligned = hAngle < 0f;
			if (!vAligned || !hAligned) {
				if (_Owner.Carrier.IsMainListener) {
					if (_Owner.Carrier.Sight.TargetIsLocked) {
						CanStartFocus = false;
						FocusMode.LostTargetEvent?.Post (_Owner.Carrier.Camera.gameObject, (uint)AkCallbackType.AK_EndOfEvent, StartFocusCallback);
					}
					if (CanStartFocus)
						FocusMode.StartFocusEvent?.Post (_Owner.Carrier.Camera.gameObject);
				}
				_Owner.Carrier.Sight.TargetIsLocked = false;
			}
			if (vAligned && hAligned) {
				if (!_Owner.Carrier.Sight.TargetIsLocked) {
					//  AkSoundEngine.PostEvent(OnTarget.Id, gameObject);
					FocusMode.OnTargetEvent?.Post (_Owner.Carrier.Camera.gameObject);
					_Owner.Carrier.Sight.TargetIsLocked = true;
					//OnTarget?.Invoke ();
				}
			}
			//Debug.Log(hAngle / MaxAimAngle);

			_Owner.Carrier.MouseLook.SnapToTarget = _Owner.Carrier.Sight.TargetIsLocked;
			if (_Owner.Carrier.Sight.TargetIsLocked) {
				_Owner.Carrier.MouseLook.Target = targetBounds.center;
			}
			//else if (wasOnTarget) LostTarget?.Invoke ();
			//*/
			return true;
		}

		protected override bool ReleaseAction () {//*
			if (FocusMode == null)
				return false;

#if CGV_DEBUG1
			VerticalLine.color = Color.red;
			HorizontalLine.color = Color.red;
			VerticalLine.rectTransform.localScale = HorizontalLine.rectTransform.localScale = Vector3.one;
#endif
			_Owner.Carrier.RemoveAbility (CGVPlayerAbility.SwitchTarget);
			DisableFocus ();
			//*/
			return true;
		}

		private void DisableFocus () {
			if (_Owner.Carrier.IsMainListener)
				FocusMode.StopFocusEvent?.Post (_Owner.Carrier.Camera.gameObject);
			_Owner.Carrier.Sight.IsAiming = false;
			_Owner.Carrier.Sight.TargetIsLocked = false;
			_Owner.Carrier.MouseLook.SnapToTarget = false;
		}

		void StartFocusCallback (object in_cookie, AkCallbackType in_type, object in_info) {
			CanStartFocus = true;
		}
	}
}
