﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGV {
	[RequireComponent (typeof (LineRenderer))]
	public class CGVProjectileTrajectoryRenderer : MonoBehaviour {

		private CGVProjectile.Trajectory _PredictedTrajectory;
		public CGVProjectile.Trajectory PredictedTrajectory {
			get { return _PredictedTrajectory; }
			set {
				if (value == _PredictedTrajectory)
					return;
				if (_PredictedTrajectory != null)
					_PredictedTrajectory.OnTrajectoryUpdated -= UpdateRenderer;
				_PredictedTrajectory = value;
				if (_PredictedTrajectory != null)
					_PredictedTrajectory.OnTrajectoryUpdated += UpdateRenderer;
			}
		}
		[SerializeField] private LineRenderer _LineRenderer;

		public void UpdateRenderer () {
			if (PredictedTrajectory == null)
				return;

			if (_PredictedTrajectory.Invalid) {
				_LineRenderer.enabled = false;
			}
			else {
				_LineRenderer.enabled = true;
				_LineRenderer.positionCount = PredictedTrajectory.Count;
				_LineRenderer.SetPositions (PredictedTrajectory.Positions);
			}
		}
	}
}
