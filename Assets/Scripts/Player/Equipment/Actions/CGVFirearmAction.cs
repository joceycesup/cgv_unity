﻿using System.Collections;
using UnityEngine;

using JuneCodes.FlexibleEnum;

using CGV.Sound;
using CGV.Player;

namespace CGV.Equipment {
	[CreateAssetMenu (fileName = "FirearmAction", menuName = "CGV/Equipment/Action/Firearm")]
	public class CGVFirearmAction : CGVEquipmentAction, ICGVWeapon {

		public bool TargetsEveryone => false;

		protected int HitLayers = 0xe01; // (7 << 9) | 1
		protected static RaycastHit[] Hits = new RaycastHit[16];

		[Header ("Weapon General Parameters")]
		[JCEnumType (typeof (CGVAmmunitionEnum), false)]
		[SerializeField] protected JCEnumKey _AmmoType;
		public int AmmoType {
			get { return _AmmoType.Id; }
		}
		[NaughtyAttributes.SpecialNegativeValue ("Infinite Ammo", "Fixed Ammo Count", false)]
		[SerializeField] protected int _MagSize = 12; // -1 is infinite bullets
		public int MagSize {
			get { return _MagSize; }
			protected set { _MagSize = value; }
		}
		[NaughtyAttributes.Duplicate ("ContinuousAction")]
		[SerializeField] protected bool IsAutomatic;
		[NaughtyAttributes.MinValue (0.01f)]
		[SerializeField] protected float FiringRate = 4f;
		[NaughtyAttributes.SpecialNegativeValue ("InstaKill", "Fixed Value", true)]
		[SerializeField] protected float DamagePerHit = 1f;
		[NaughtyAttributes.SpecialNegativeValue ("Infinite", "Fixed Value", false)]
		[SerializeField] protected float _MuzzleVelocity = 400f;
		public float MuzzleVelocity { get { return _MuzzleVelocity; } }

		[NaughtyAttributes.SpecialNegativeValue ("Use Reload Sound Duration", true)]
		[SerializeField] protected float ReloadDelay = 4f;
		[SerializeField] protected bool AutoReload = false;
		[Tooltip ("Reload round per round like a shotgun")]
		[SerializeField] protected bool RoundPerRoundReload = false;

		[Header ("Ballistic")]
		[SerializeField] protected bool _IsBallistic = false;
		public bool IsBallistic {
			get { return _IsBallistic; }
		}/*
		[Range (-90f, 90f)]
		[NaughtyAttributes.ShowIf ("_IsBallistic")]
		[SerializeField] protected float BallisticAngle = 0;
		[NaughtyAttributes.ShowIf ("_IsBallistic")]
		[NaughtyAttributes.SpecialNegativeValue ("Infinite", "Finite Distance", false)]
		[SerializeField] protected float BallisticMaxRange = -1f;//*/

		[Header ("Cone")]
		[NaughtyAttributes.HideIf ("_IsBallistic")]
		[NaughtyAttributes.SpecialNegativeValue ("Straight Line", "Angle", false)]
		[SerializeField] protected float ConeAngle = -1f;

		[Header ("Bursts")]
		[NaughtyAttributes.SpecialNegativeValue ("No Bursts", "Fixed Value", false)]
		[SerializeField] protected int MaxBurstSize = -1;
		[Min (float.Epsilon)]
		[NaughtyAttributes.HideIfValue (NaughtyAttributes.ComparisonOperator.LessThanOrEqualTo, 0, "MaxBurstSize")]
		[SerializeField] protected float DelayBetweenBursts = 1f;
		protected int CurrentBurstCount = 0;

		[Header ("Range")]
		[NaughtyAttributes.SpecialNegativeValue ("Infinite", "Finite Distance", false)]
		[SerializeField] protected float ShortRangeDistance = -1f;
		[NaughtyAttributes.HideIfValue (NaughtyAttributes.ComparisonOperator.LessThanOrEqualTo, 0f, "ShortRangeDistance")]
		[NaughtyAttributes.SpecialNegativeValue ("Infinite", "Finite Distance", false)]
		[SerializeField] protected float MediumRangeDistance = -1f;
		[NaughtyAttributes.HideIfValue (NaughtyAttributes.ComparisonOperator.LessThanOrEqualTo, 0f, "ShortRangeDistance", "MediumRangeDistance")]
		[NaughtyAttributes.SpecialNegativeValue ("Infinite", "Finite Distance", false)]
		[SerializeField] protected float LongRangeDistance = -1f;

		protected float GetMaxDistance () {
			return (LongRangeDistance <= 0) ? float.MaxValue : LongRangeDistance;
		}

		[Range (0f, 1f)]
		[SerializeField] protected float ShortRangeMultiplier = 1f;
		[Range (0f, 1f)]
		[NaughtyAttributes.HideIfValue (NaughtyAttributes.ComparisonOperator.LessThanOrEqualTo, 0f, "ShortRangeDistance")]
		[SerializeField] protected float MediumRangeMultiplier = 1f;
		[Range (0f, 1f)]
		[NaughtyAttributes.HideIfValue (NaughtyAttributes.ComparisonOperator.LessThanOrEqualTo, 0f, "ShortRangeDistance", "MediumRangeDistance")]
		[SerializeField] protected float LongRangeMultiplier = 1f;

		protected float GetDamageMultiplier (float distance) {
			if (ShortRangeDistance < 0f || distance <= ShortRangeDistance)
				return ShortRangeMultiplier;
			else if (MediumRangeDistance < 0f || distance <= MediumRangeDistance)
				return MediumRangeMultiplier;
			else if (LongRangeDistance < 0f || distance <= LongRangeDistance)
				return LongRangeMultiplier;
			return -1f;
		}

		protected int _Bullets = -1;
		public int Bullets {
			get { return _Bullets >= 0 ? _Bullets : MagSize; }
			protected set { _Bullets = value; }
		}
		public bool CanReload { get { return !Reloading && (MagSize > 0) && (Bullets < MagSize); } }
		public bool IsLoaded { get { return Bullets != 0; } }

		protected bool StopReloadAndShoot = false;
		protected bool Reloading = false;
		protected float ReloadingEndTime = 0f;
		protected IEnumerator ReloadingRoundCoroutine;
		protected float NextShot = 0f;

		[Header ("WWise Events")]
		[SerializeField] protected AK.Wwise.Event ShootEvent;
		[SerializeField] protected AK.Wwise.Event ReloadEvent;
		[SerializeField] protected AK.Wwise.RTPC AmmoCountRTPC;
		[SerializeField] protected AK.Wwise.Event EmptyMagEvent;
		[SerializeField] protected AK.Wwise.Event ShellEvent;
		[SerializeField] protected AK.Wwise.Event _HitEvent;
		public AK.Wwise.Event HitEvent { get { return _HitEvent; } }

		public GameObject Barrel { get { return _Owner.Carrier.Camera.gameObject; } }

		protected override void InitAction () {
			HitLayers = (1 << CGVPersistentInfo.LayerCharacter) | (1 << CGVPersistentInfo.LayerGround) | (1 << CGVPersistentInfo.LayerWall) | 1;
			if (ShortRangeDistance < 0f)
				MediumRangeDistance = -1f;
			if (MediumRangeDistance < 0f)
				LongRangeDistance = -1f;
		}

		protected override void InitClone () { }

		public override void EquipmentCarried (CGVPlayerCharacter player, bool baseEquipment) { }
		public override void EquipmentDropped (CGVPlayerCharacter player) {
			CancelReload ();
		}
		public override void EquipmentUpdated () {
			ReloadIfEmpty ();
		}

		protected override void EquipmentSelectedAction (bool selected) {
			if (selected)
				ReloadIfEmpty ();
			else
				CancelReload ();
		}

		protected void CancelReload () {
			Reloading = false;
			if (ReloadingRoundCoroutine != null)
				_Owner?.Carrier?.StopCoroutine (ReloadingRoundCoroutine);
			ReloadingRoundCoroutine = null;
		}

		protected void ReloadIfEmpty () {
			if (AutoReload && Bullets == 0)
				StartReload ();
		}
		protected void PlanNextShot () {
			if (FiringRate > 0f)
				NextShot = Time.time + (1f / FiringRate);
		}
		protected virtual void ShootBullet (GameObject barrel) {
			if (Bullets > 0)
				Bullets--;
		}

		public virtual bool StartReload () {
			if (!CanReload) { // no need to reload
							  //Debug.LogFormat ("Can't Reload ({0}, {1})", Reloading, ReloadingEndTime);
				return false;
			}
			//if (Pressed) return false;

			int bullets = Mathf.Min (_Owner.Carrier.Ammunitions.GetAmmunitionCount (AmmoType), MagSize - Bullets);
			//Debug.LogFormat ("Start Reload ({0})", bullets);
			bool res = bullets > 0;
			if (res) {
				Reloading = true;
				if (ReloadDelay >= 0f) { // We use a fixed delay, otherwise we use the sound duration
					if (RoundPerRoundReload) {
						_Owner.Carrier.StartCoroutine (ReloadingRoundCoroutine = ReloadRound ());
					}
					else {
						ReloadingEndTime = Time.time + ReloadDelay;
						ReloadEvent?.Post (Barrel);
					}
				}
				else if (ReloadEvent == null) // No sound, reloading is immediate
					Reload ();
				else {
					ReloadingEndTime = float.MaxValue; // The callback will reset this value
					ReloadEvent?.Post (Barrel, (uint)AkCallbackType.AK_EndOfEvent, ReloadAfterSound);
				}
				//Debug.LogFormat ("Reloading {0} with {1} bullets", name, bullets);
			}
			return res;
		}

		private IEnumerator ReloadRound () {
			ReloadingEndTime = float.MaxValue;
			ReloadEvent?.Post (Barrel);
			yield return new WaitForSeconds (ReloadDelay);
			ReloadingEndTime = 0f;
			ReloadingRoundCoroutine = null;
			Reload ();
		}

		void ReloadAfterSound (object in_cookie, AkCallbackType in_type, object in_info) {
			//Debug.LogFormat ("Reload After Sound ({0})", MagSize - Bullets);
			ReloadingEndTime = 0f;
			Reload ();
		}

		void Reload () {
			Reloading = false; // Will not be able to reload unless we set it to false
			if (!CanReload)
				return;

			if (RoundPerRoundReload) {
				int bullets = _Owner.Carrier.Ammunitions.GetAmmunition (AmmoType, 1);
				//Debug.LogFormat ("Reload Round ({0})", bullets);
				Bullets += bullets;

				if (StopReloadAndShoot) {
					NextShot = 0f;
					StopReloadAndShoot = false;
					HoldAction ();
				}
				else { // If player wants to shoot, reloading is interrupted
					   // Debug.LogFormat ("Continue to reload");
					StartReload (); // We start reloading the next round
				}
			}
			else {
				Bullets += _Owner.Carrier.Ammunitions.GetAmmunition (AmmoType, MagSize - Bullets);
			}
		}

		// Shoot
		protected override bool HoldAction () {
#if CGV_DEBUG
			if (ConeAngle > 0f)
				CGVDebugUtils.DrawCone (Barrel.transform.position, Barrel.transform.forward, ConeAngle, GetMaxDistance (), new Color (1f, 0f, 1f, 0.25f), 16);
#endif

			bool res = Time.time >= NextShot;
			if (Reloading) {
				if (_FirstFrame)
					StopReloadAndShoot = true;
				if (Time.time >= ReloadingEndTime) {
					Reload ();
				}
				else
					res = false;
			}
			if (res) {
				StopReloadAndShoot = false;
				if (IsLoaded) {
					AmmoCountRTPC?.SetValue (Barrel, Bullets - 1);
					ShootEvent?.Post (Barrel);
					CGVSingleShotSoundEvent.PlaySingleShotAtLocation (ShellEvent, _Owner.Carrier.Feet, _Owner.Carrier.GroundTextureDetector);
					//ShellEvent?.Post (Owner.Carrier.Feet.gameObject);
					ShootBullet (Barrel);

					if (Bullets == 0 && AutoReload)
						StartReload ();
					else {
						PlanNextShot ();
						if (MaxBurstSize > 0) {
							CurrentBurstCount++;
							if (CurrentBurstCount >= MaxBurstSize) {
								CurrentBurstCount = 0;
								NextShot = Mathf.Max (NextShot, Time.time + DelayBetweenBursts);
							}
						}
					}

					if (!IsBallistic) {
						CGVTarget tmpTarget = null;
						float damageMultiplier = -1f;
						if (ConeAngle > 0f) {
							ShootInCone ();
						}
						else {
							if (_Owner.Carrier.Sight.CurrentTarget != null && _Owner.Carrier.Sight.TargetIsLocked) {
								tmpTarget = _Owner.Carrier.Sight.CurrentTarget;
								damageMultiplier = GetDamageMultiplier (Vector3.Distance (Barrel.transform.position, tmpTarget.Bounds.GetColliderBounds ().center));
								Hits[0] = new RaycastHit ();
							}
							else {
								if (Physics.Raycast (Barrel.transform.position, Barrel.transform.forward, out Hits[0], GetMaxDistance (), HitLayers, QueryTriggerInteraction.Ignore)) {
									tmpTarget = Hits[0].transform.GetComponentInParent<CGVTarget> ();
									if (tmpTarget == null) {
										//Debug.LogFormat ("Hit environment at {0} ({1})", hit.point, hit.transform.gameObject.layer);
										//Debug.DrawLine (Barrel.transform.position, Hits[0].point, Color.red, 1f);
										//AK.Wwise.Switch textureSwitch = CGVSoundUtils.GetTextureSwitch (hit.transform.GetComponentInParent<ICGVTextureSwitchProvider> ());
										//CGVSingleShotSoundEvent.PlaySingleShotAtLocation (HitEvent, hit.point, CGVSoundUtils.BulletDelay (MuzzleVelocity, hit), textureSwitch);
										CGVSoundUtils.PlayBulletSoundEnvironment (_Owner.Carrier, this, Hits[0]);
									}
									else
										damageMultiplier = GetDamageMultiplier (Vector3.Distance (Barrel.transform.position, Hits[0].point));
								}
								else
									CGVSoundUtils.PlayFlyingBulletSound (_Owner.Carrier, this);
							}
						}

						if (tmpTarget != null && damageMultiplier > 0f)
							tmpTarget.TakeDamage (DamagePerHit <= 0f ? float.MaxValue : DamagePerHit * damageMultiplier, _Owner.Carrier, Hits[0], this);
					}
				}
				else if (_FirstFrame || NextShot > 0f) {
					//Debug.Log ("Mag is empty");
					EmptyMagEvent?.Post (Barrel);
					NextShot = -1f;
					//PlanNextShot (); // prevent the sound effect from happening every frame but instead at the normal firing rate
					res = false;
				}
			}
			return res;
		}

		protected override bool ReleaseAction () {
			if (MaxBurstSize > 0 && CurrentBurstCount > 0) {
				CurrentBurstCount = 0;
				NextShot = Mathf.Max (NextShot, Time.time + DelayBetweenBursts);
			}
			//StopReloadAndShoot = false;
			return true;
		}

		protected int ShootInCone () {
			float maxDistance = GetMaxDistance ();
			float maxDistanceSqr = maxDistance * maxDistance;
			if (maxDistance < 0f)
				return 0;
			float angle = (ConeAngle * Mathf.Deg2Rad) / 2f;
			float radius = Mathf.Sin (angle) * maxDistance;
			float minDot = Mathf.Cos (angle);
			//Debug.LogFormat ("ShootInCone : {0} / {1}", radius, minDot);
			int hitCount = Physics.SphereCastNonAlloc (Barrel.transform.position - Barrel.transform.forward * radius, radius, Barrel.transform.forward, Hits, maxDistance + radius * 2f, HitLayers);
			//Debug.DrawLine (Barrel.transform.position - Barrel.transform.forward * radius, Barrel.transform.position + Barrel.transform.forward * (maxDistance + radius), Color.cyan, 2f);

			int res = hitCount;

			if (hitCount == Hits.Length)
				hitCount--;

			CGVTarget tmpTarget = null;
			float damageMultiplier = -1f;
			bool adjustedHitIsInCone;
			for (int i = 0; i < hitCount; ++i) {
				tmpTarget = null;
				damageMultiplier = -1f;

				//Debug.LogFormat ("Hit {0} : {1}", Hits[i].transform, Hits[i].point);
				Vector3 hitForward = Vector3.Normalize (Hits[i].point - Barrel.transform.position);
				if (Vector3.Dot (Barrel.transform.forward, hitForward) < minDot) {
					if (adjustedHitIsInCone = Physics.Raycast (Barrel.transform.position, Vector3.RotateTowards (Barrel.transform.forward, hitForward, angle, 0f), out Hits[Hits.Length - 1], maxDistance, HitLayers, QueryTriggerInteraction.Ignore))
						adjustedHitIsInCone = Hits[i].collider == Hits[Hits.Length - 1].collider;

					if (!adjustedHitIsInCone) {
						res--;
						continue;
					}
					else {
						Debug.DrawLine (Hits[i].point, Hits[Hits.Length - 1].point, Color.blue, 1f);
						//CGVDebugUtils.DrawCube (Hits[Hits.Length - 1].point, .25f, Color.blue, 2f);
						CGVDebugUtils.DrawCube (Hits[i].point, .25f, Color.blue, 1f);
					}
					Hits[i] = Hits[Hits.Length - 1];
				}
				else {
					bool hitIsInvalid = false;
					if ((Hits[i].point - Barrel.transform.position).sqrMagnitude > maxDistanceSqr)
						hitIsInvalid = true;
					else if (!Physics.Raycast (Barrel.transform.position, hitForward, out Hits[Hits.Length - 1], maxDistance, HitLayers, QueryTriggerInteraction.Ignore))
						hitIsInvalid = true;
					else if (Hits[i].collider != Hits[Hits.Length - 1].collider)
						hitIsInvalid = true;

					if (hitIsInvalid) {
						res--;
						continue;
					}

					//Debug.LogFormat ("Dot : {0}/{1}", Vector3.Dot (Barrel.transform.forward, Vector3.Normalize (Hits[i].point - Barrel.transform.position)), minDot);
					//CGVDebugUtils.DrawCube (Hits[i].point, .25f, Color.cyan, 1f);
				}

				//*
				tmpTarget = Hits[i].transform.GetComponentInParent<CGVTarget> ();
				if (tmpTarget == null) {
					Debug.DrawLine (Barrel.transform.position, Hits[i].point, Color.red, 1f);
					CGVSoundUtils.PlayBulletSoundEnvironment (_Owner.Carrier, this, Hits[i]);
				}
				else
					damageMultiplier = GetDamageMultiplier (Vector3.Distance (Barrel.transform.position, Hits[i].point));

				if (tmpTarget != null && damageMultiplier > 0f)
					tmpTarget.TakeDamage (DamagePerHit <= 0f ? float.MaxValue : DamagePerHit * damageMultiplier, _Owner.Carrier, Hits[i], this);//*/
			}
			//Debug.LogFormat ("Cone : {0} hits", res);

			return res;
		}
	}
}
