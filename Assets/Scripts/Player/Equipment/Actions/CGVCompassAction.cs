﻿using UnityEngine;

using JuneCodes.Maths;

using CGV.Player;

#pragma warning disable 649
namespace CGV.Equipment {
	[CreateAssetMenu (fileName = "CompassAction", menuName = "CGV/Equipment/Action/Compass")]
	public class CGVCompassAction : CGVEquipmentAction {

		private enum CardinalPoint {
			None,
			North = 0x01,
			East = 0x02,
			South = 0x03,
			West = 0x04,
			Invalid = 0xff
		}

		[Header ("Cardinal Points")]
		[SerializeField] private AK.Wwise.Event NorthEvent;
		[SerializeField] private AK.Wwise.Event EastEvent;
		[SerializeField] private AK.Wwise.Event SouthEvent;
		[SerializeField] private AK.Wwise.Event WestEvent;

		[SerializeField] private float ExitCardinalPointAngle = 15.0f;
		private CardinalPoint LastCardinalPoint = CardinalPoint.Invalid;

		private AngleInterval[] CardinalPointsExitZone;
		private AngleInterval TraveledAngle;
		private static readonly float cos45 = 0.70710678118654752440084436210485f;

		[Header ("Vertical Alignment")]
		[SerializeField] private AK.Wwise.Event StartVerticalAlignmentEvent;
		[SerializeField] private AK.Wwise.Event StopVerticalAlignmentEvent;
		[SerializeField] private AK.Wwise.RTPC VerticalAlignmentRTPC;

		private Vector3 CurrentForward;
		private Vector3 LastForward;

		protected override void InitAction () { }

		protected override void InitClone () { }

		public override void EquipmentCarried (CGVPlayerCharacter player, bool baseEquipment) {
			CameraTransform = player.Camera.transform;
		}
		public override void EquipmentDropped (CGVPlayerCharacter player) { }
		public override void EquipmentUpdated () { }

		protected override void EquipmentSelectedAction (bool selected) {
			if (!selected) {
				DisableCompass ();
			}
		}

		private Transform CameraTransform;

		protected override bool HoldAction () {
			StartVerticalAlignmentEvent?.Post (CameraTransform.gameObject);

			CurrentForward = CameraTransform.forward;

			float vAngle = -JCMaths.VerticalAngle (Vector3.forward, CurrentForward);
			if (vAngle < 0f) {
				VerticalAlignmentRTPC?.SetGlobalValue (Mathf.Abs (vAngle / _Owner.Carrier.MouseLook.MinMaxVerticalAngle.x));
			}
			else {
				VerticalAlignmentRTPC?.SetGlobalValue (-Mathf.Abs (vAngle / _Owner.Carrier.MouseLook.MinMaxVerticalAngle.y));
			}

			CurrentForward = _Owner.Carrier.transform.forward;
			CurrentForward.y = 0f;

			if (LastCardinalPoint == CardinalPoint.Invalid) {
				LastForward = CurrentForward;
				LastCardinalPoint = CardinalPoint.None;
			}

			if (LastForward == CurrentForward) // .Equals tests for strict equality
				return true;
			if (CardinalPointsExitZone == null || CardinalPointsExitZone.Length <= 0) {
				CardinalPointsExitZone = new AngleInterval[4] {
					new AngleInterval(-ExitCardinalPointAngle, ExitCardinalPointAngle),
					new AngleInterval(90f - ExitCardinalPointAngle, 90f + ExitCardinalPointAngle),
					new AngleInterval(180f - ExitCardinalPointAngle, ExitCardinalPointAngle - 180f),
					new AngleInterval(-90f - ExitCardinalPointAngle, -90f + ExitCardinalPointAngle)
				};
			}
			bool toTheRight = Vector3.Cross (LastForward, CurrentForward).y > 0;
			TraveledAngle = new AngleInterval (
				JCMaths.SignedHorizontalAngle (Vector3.forward, toTheRight ? LastForward : CurrentForward),
				JCMaths.SignedHorizontalAngle (Vector3.forward, toTheRight ? CurrentForward : LastForward));
			//*
			float dotProduct = Vector3.Dot (CurrentForward, Vector3.forward);
			CardinalPoint cardinalPoint = CardinalPoint.North;
			float cardinalAngle = 0f;
			if (dotProduct <= cos45) {
				if (dotProduct < -cos45) {
					cardinalPoint = CardinalPoint.South;
					cardinalAngle = 180f;
				}
				else {
					cardinalPoint = CurrentForward.x < 0f ? CardinalPoint.West : CardinalPoint.East;
					cardinalAngle = CurrentForward.x < 0f ? -90f : 90f;
				}
			}
			if (LastCardinalPoint != CardinalPoint.None) {
				if (!TraveledAngle.Intersects (CardinalPointsExitZone[(int)LastCardinalPoint - 1])) {
					//Debug.Log ("Exited " + LastCardinalPoint);
					LastCardinalPoint = CardinalPoint.None;
				}
			}
			else if (TraveledAngle.Contains (cardinalAngle)) {
				LastCardinalPoint = cardinalPoint;
				//Debug.Log ("Entered " + cardinalPoint);
				switch (LastCardinalPoint) {
					case CardinalPoint.North:
						NorthEvent?.Post (CameraTransform.gameObject);
						break;
					case CardinalPoint.East:
						EastEvent?.Post (CameraTransform.gameObject);
						break;
					case CardinalPoint.South:
						SouthEvent?.Post (CameraTransform.gameObject);
						break;
					case CardinalPoint.West:
						WestEvent?.Post (CameraTransform.gameObject);
						break;
				}
			}

			LastForward = CurrentForward;
			return true;
		}

		protected override bool ReleaseAction () {
			DisableCompass ();
			return true;
		}

		private void DisableCompass () {
			LastCardinalPoint = CardinalPoint.Invalid;
			StopVerticalAlignmentEvent?.Post (CameraTransform.gameObject);
		}
	}
}
