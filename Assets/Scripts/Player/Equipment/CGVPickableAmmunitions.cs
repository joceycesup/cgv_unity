﻿using UnityEngine;
using UnityEngine.Events;

using JuneCodes.FlexibleEnum;

#pragma warning disable 649
namespace CGV.Equipment {
	public class CGVPickableAmmunitions : MonoBehaviour, ICGVConsumable {

		[SerializeField]
		protected bool _DestroyWhenPickedUp = true;

		[JCEnumType (typeof (CGVAmmunitionEnum), false)]
		[SerializeField] public JCEnumKey AmmoType;

		[NaughtyAttributes.SpecialNegativeValue ("Infinite", false)]
		public int Quantity = 1;

		[SerializeField]
		//[NaughtyAttributes.InfoBox ("If empty, the object will destroy itself when picked up.")]
		protected UltEvents.UltEvent _OnPickedUp;
		/*
		[NaughtyAttributes.HideIfValue (NaughtyAttributes.ComparisonOperator.LessThanOrEqualTo, 0, "Quantity")]
		public int PickCount = 1;//*/

		//private static CGVObjectPool<CGVPickableAmmunitions> Pool;

		CGVConsumableProperties ICGVConsumable.CheckContent () {
			CGVConsumableProperties res = new CGVConsumableProperties (ConsumableType.Ammunition, AmmoType.Id, Quantity);
			return res;
		}

		int ICGVConsumable.Replenish (int quantity) {
			if (Quantity < 0)
				return 0;
			Quantity += quantity;
			return quantity;
		}

		int ICGVConsumable.PickUp (int quantity) {
			if (quantity == 0)
				return 0;
			if (!this.isActiveAndEnabled)
				return 0;

			int res = 0;

			if (Quantity != 0) {
				CGVAmmunitionType ammoTypeInfo = CGVAmmunitionEnum.GetValue (AmmoType, true);
				if (ammoTypeInfo != null)
					ammoTypeInfo.PlayerPickUpEvent?.Post (gameObject);

				if (Quantity > 0) {
					if (quantity < 0 || quantity >= Quantity) {
						res = Quantity;
						Quantity = 0;/*
						if (Pool == null)
							Pool = CGVObjectPool<CGVPickableAmmunitions>.GetPool ();
						Pool.Store (this);/*/
						_OnPickedUp?.Invoke ();
						if (_DestroyWhenPickedUp)
							Destroy (gameObject);
						//*/
					}
					else {
						res = quantity;
						Quantity -= quantity;
					}
				}
				else {
					if (quantity < 0) {
						res = ammoTypeInfo.MaxInInventory;
					}
					else {
						res = quantity;
					}
				}
			}
			return res;
		}
	}
}
