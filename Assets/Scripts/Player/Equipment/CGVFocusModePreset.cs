﻿using System;
using System.Collections.Generic;
using UnityEngine;

using AK.Wwise;
using Event = AK.Wwise.Event;

#pragma warning disable 649
namespace CGV {
	[CreateAssetMenu (fileName = "FocusMode", menuName = "CGV/Equipment/Focus Mode Preset")]
	public class CGVFocusModePreset : ScriptableObject {

		[Header ("Targets")]
		[SerializeField] private bool _RememberLastTarget = false;
		public bool RememberLastTarget { get { return _RememberLastTarget; } }

		[Header ("Sonification")]
		[SerializeField] private bool _SonifyOtherTargets = true;
		public bool SonifyOtherTargets { get { return _SonifyOtherTargets; } }

		[Header ("WWise Events")]
		[SerializeField] private Event _StartFocusEvent;
		[SerializeField] private Event _StopFocusEvent;
		[SerializeField] private Event _OnTargetEvent;
		[SerializeField] private Event _LostTargetEvent;
		public Event StartFocusEvent { get { return _StartFocusEvent; } }
		public Event StopFocusEvent { get { return _StopFocusEvent; } }
		public Event OnTargetEvent { get { return _OnTargetEvent; } }
		public Event LostTargetEvent { get { return _LostTargetEvent; } }

		[Header ("WWise Parameters")]
		[SerializeField] private RTPC _DistFocusX;
		[SerializeField] private RTPC _DistFocusY;
		[SerializeField] private RTPC _DistFocusYSigned;
		[SerializeField] private RTPC _SumAxes;
		public RTPC DistFocusX { get { return _DistFocusX; } }
		public RTPC DistFocusY { get { return _DistFocusY; } }
		public RTPC DistFocusYSigned { get { return _DistFocusYSigned; } }
		public RTPC SumAxes { get { return _SumAxes; } }

		[Header ("Focus Angle Range")]
		[SerializeField] private Vector2 _MaxAimAngle = new Vector2 (30f, 30f);
		public Vector2 MaxAimAngle { get { return _MaxAimAngle; } }


		[SerializeField] private float _SnapMargin = 0f;
		public float SnapMargin { get { return _SnapMargin; } }
		[SerializeField] private float _SnapStrength = 0f;
		public float SnapStrength { get { return _SnapStrength; } }
		[SerializeField] private bool _SnapTargetOnActivation = false;
		public bool SnapTargetOnActivation { get { return _SnapTargetOnActivation; } }
		[SerializeField] private bool _SnapTargetOnChange = false;
		public bool SnapTargetOnChange { get { return _SnapTargetOnChange; } }

		[SerializeField] private bool _StopIfRunning = false;
		public bool StopIfRunning { get { return _StopIfRunning; } }
		[Range (0f, 1f)]
		[SerializeField] private float _SpeedModifier = 1f;
		public float SpeedModifier { get { return _SpeedModifier; } }
		[Range (0f, 1f)]
		[SerializeField] private float _StrafeSpeedModifier = 1f;
		public float StrafeSpeedModifier { get { return _StrafeSpeedModifier; } }
		[Range (0f, 1f)]
		[SerializeField] private float _AimSpeedModifier = 1f;
		public float AimSpeedModifier { get { return _AimSpeedModifier; } }
	}
}
