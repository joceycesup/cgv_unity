﻿using UnityEngine;

#pragma warning disable 649
namespace CGV {
	public class CGVBall : CGVProjectile {


		/*
		protected override void OnCollision (Collision collision) {
			if (_Launched && collision.gameObject.layer == CGVPersistentInfo.LayerGround) {
				Debug.Log ("Hit Ground");
				base.OnCollision (collision);
			}
		}/*/
		protected override void OnCollision (GameObject other, Vector3 impactPoint, Vector3 velocity) {
			base.OnCollision (other, impactPoint, velocity);
			if (other.layer == CGVPersistentInfo.LayerGround) {
				Debug.Log ("Hit Ground");
				_Rigidbody.isKinematic = true;
			}
			else {
				_Rigidbody.isKinematic = false;
				if (_Launched) {
					Debug.Log (velocity.y);
					_Rigidbody.velocity = new Vector3 (0f, velocity.y, 0f);
				}
			}
		}
	}
}
