﻿using System.Collections;
using UnityEngine;

using CGV.Sound;
using CGV.Equipment;
using CGV.Player;

namespace CGV {
	public class CGVGrenade : CGVProjectile, ICGVWeapon {

		public bool TargetsEveryone => true;

		[Header ("Detonation")]
		[SerializeField] protected float _ExplodeDelay = 3f;
		public float ExplodeDelay { get { return _ExplodeDelay; } }

		[Min (1)]
		[SerializeField] protected int _BeepCount = 3;
		public float BeepCount { get { return _BeepCount; } }

		[Header ("Damage")]
		[SerializeField] protected float _ExplosionRadius = 5f;
		public float ExplosionRadius { get { return _ExplosionRadius; } }

		[NaughtyAttributes.SpecialNegativeValue ("InstaKill", "Fixed Value", false)]
		[SerializeField] protected float _Damage = 1f;
		public float Damage { get { return _Damage; } }

		[SerializeField] protected AnimationCurve _DamageCurve = AnimationCurve.EaseInOut (0f, 1f, 1f, 0f);

		[Header ("WWise Events")]
		[SerializeField] protected AK.Wwise.Event _BeepEvent;
		[SerializeField] protected AK.Wwise.Event _StickEvent;
		[SerializeField] protected AK.Wwise.Event _ExplodeEvent;

		protected IEnumerator ExplodeCoroutine;
		protected int _CurrentBeepCount = 0;

		public override void ShootFromBarrel (CGVPlayerCharacter owner, GameObject barrel, float speed) {
			//StartCoroutine (ExplodeCoroutine = ExplodeWithDelay ());
			base.ShootFromBarrel (owner, barrel, speed);
		}

		protected override void OnCollision (GameObject other, Vector3 impactPoint, Vector3 velocity) {
			base.OnCollision (other, impactPoint, velocity);//*
			if (!_Launched)
				return;
			StopLaunch ();
			_CurrentBeepCount = 0;
			if (_StickEvent?.Post (gameObject, (uint)AkCallbackType.AK_EndOfEvent, NextBeepCallback) == 0) {
				Explode ();
			}//*/
		}

		private void NextBeepCallback (object in_cookie, AkCallbackType in_type, AkCallbackInfo in_info) {
			_CurrentBeepCount++;
			if (_CurrentBeepCount > _BeepCount) {
				Explode ();
			}
			else if (_BeepEvent?.Post (gameObject, (uint)AkCallbackType.AK_EndOfEvent, NextBeepCallback) == 0) {
				Explode ();
			}
		}

		protected IEnumerator ExplodeWithDelay () {
			yield return new WaitForSeconds (ExplodeDelay);
			StopLaunch ();
			ExplodeCoroutine = null;
			Explode ();
		}

		protected void Explode () {
			Debug.Log ("EXPLOSION!!!");
			CGVSingleShotSoundEvent.PlaySingleShotAtLocation (_ExplodeEvent, transform.position);
			//gameObject.SetActive (false);

			//CGVDebugUtils.DrawDodecahedron (transform.position, ExplosionRadius, Color.red, 1f);
			CGVDebugUtils.DrawIcosahedron (transform.position, ExplosionRadius, Color.red, 1f);

			foreach (Collider collider in Physics.OverlapSphere (transform.position, _ExplosionRadius, (1 << CGVPersistentInfo.LayerCharacter))) {
				CGVTarget target = collider.GetComponentInParent<CGVTarget> ();
				if (target != null) {/*
					CGVPlayerCharacter player = collider.GetComponentInParent<CGVPlayerCharacter> ();
					if (player != null) {

					}
					else {
					}//*/
					DealDamage (target);
				}
			}

			Dispose ();
		}

		private void DealDamage (CGVTarget target) {
			float distance = Vector3.Distance (transform.position, target.Bounds.GetColliderBounds ().center);
			if (distance > _ExplosionRadius)
				return;
			float damage = _Damage * _DamageCurve.Evaluate (distance / _ExplosionRadius);
			target.TakeDamage (damage, Owner, this);
		}

		private void OnDisable () {
			StopLaunch ();
			if (ExplodeCoroutine != null) {
				StopCoroutine (ExplodeCoroutine);
				ExplodeCoroutine = null;
			}
		}

		/*
		protected override void OnCollision (GameObject other, Vector3 impactPoint, Vector3 velocity) {
			base.OnCollision (other, impactPoint, velocity);
			if (other.layer == CGVPersistentInfo.LayerGround) {
				Debug.Log ("Hit Ground");
				_Rigidbody.isKinematic = true;
			}
			else {
				_Rigidbody.velocity = Vector3.zero;
				_Rigidbody.isKinematic = false;
			}
		}//*/
	}
}
