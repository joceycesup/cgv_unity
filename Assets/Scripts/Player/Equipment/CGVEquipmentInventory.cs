﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CGV.Player;

namespace CGV.Equipment {
	[CreateAssetMenu (fileName = "EquipmentInventory", menuName = "CGV/Equipment/Equipment Inventory")]
	public class CGVEquipmentInventory : CGVClonableScriptableObject<CGVEquipmentInventory> {

		protected override void InitClone () { }

		[NaughtyAttributes.ReorderableList]
		[SerializeField] protected List<CGVEquipment> _Equipment;

		protected int _ActiveEquipmentIndex = -1;
		protected int ActiveEquipmentIndex {
			get { return _ActiveEquipmentIndex; }
			set {
				if (_ActiveEquipmentIndex == value)
					return;
				CGVEquipment lastEquipment = ActiveEquipment;
				if (_Equipment.Count <= 0)
					_ActiveEquipmentIndex = -1;
				else {
					if (value < 0)
						_ActiveEquipmentIndex = _Equipment.Count - 1;
					else if (value >= _Equipment.Count)
						_ActiveEquipmentIndex = 0;
					else
						_ActiveEquipmentIndex = value;
				}
				if (ActiveEquipment != lastEquipment) {
					if (lastEquipment != null)
						lastEquipment.Select (false);
					if (ActiveEquipment != null)
						ActiveEquipment.Select (true);
				}
				Debug.Log ("Selected " + ActiveEquipment);
			}
		}

		public void InitForPlayer (CGVPlayerCharacter player) {
			for (int i = 0; i < _Equipment.Count; ++i) {
				CGVEquipment equipment = _Equipment[i].GetClone ();
				if (!equipment.Carry (player, true))
					Drop (_Equipment[i]);
				else
					_Equipment[i] = equipment;
			}
			SelectFirst ();
		}

		public void SelectFirst () {
			ActiveEquipmentIndex = 0;
		}

		public CGVEquipment ActiveEquipment {
			get { return ActiveEquipmentIndex < 0 ? null : _Equipment[ActiveEquipmentIndex]; }
			set {
				if (value == null)
					ActiveEquipmentIndex = -1;
				else
					ActiveEquipmentIndex = _Equipment.IndexOf (value);
			}
		}

		public void SwitchEquipment (bool next) {
			if (next)
				ActiveEquipmentIndex++;
			else
				ActiveEquipmentIndex--;
		}

		public bool Carry (CGVEquipment equipment, CGVPlayerCharacter player) {
			if (equipment == null)
				return false;
			if (_Equipment.Contains (equipment))
				return false;
			bool res = equipment.Carry (player);
			if (res) {
				if (_Equipment.Count <= 0)
					_ActiveEquipmentIndex = 0;
				_Equipment.Add (equipment);
			}
			return res;
		}

		public bool Drop (CGVEquipment equipment) {
			if (equipment == null)
				return false;
			bool res = _Equipment.Remove (equipment);
			if (res) {
				if (ActiveEquipmentIndex >= _Equipment.Count)
					ActiveEquipmentIndex--;
			}
			return res;
		}
	}
}
