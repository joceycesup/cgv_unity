﻿using System;
using UnityEngine;

using CGV.Player;

namespace CGV.Equipment {
	/// <summary>
	/// Asset class that represents an equipment and its actions.
	/// It can be created using the contextual menu "Create/CGV/Equipment/New Equipment".
	/// </summary>
	[CreateAssetMenu (fileName = "Equipment", menuName = "CGV/Equipment/New Equipment")]
	public class CGVEquipment : CGVClonableScriptableObject<CGVEquipment> {

		public CGVPlayerCharacter Carrier { get; protected set; }

		[SerializeField]
		protected CGVEquipmentAction _PrimaryAction;
		public CGVEquipmentAction PrimaryAction { get { return _PrimaryAction; } }
		[SerializeField]
		protected CGVEquipmentAction _SecondaryAction;
		public CGVEquipmentAction SecondaryAction { get { return _SecondaryAction; } }
		[SerializeField]
		protected CGVEquipmentAction _SpecialAction;
		public CGVEquipmentAction SpecialAction { get { return _SpecialAction; } }
		[SerializeField]
		protected CGVEquipmentAction _OptionalAction;
		public CGVEquipmentAction OptionalAction { get { return _OptionalAction; } }

		[Header ("WWise Events")]
		[SerializeField] protected AK.Wwise.Event SelectEvent;
		[SerializeField] protected AK.Wwise.Event CarryEvent;

		public static readonly int NbActions = 4;

		public CGVEquipmentAction this[int actionIndex] {
			get {
				switch (actionIndex) {
					case 0: return _PrimaryAction;
					case 1: return _SecondaryAction;
					case 2: return _SpecialAction;
					case 3: return _OptionalAction;
					default:
						throw new IndexOutOfRangeException ("Invalid action index!");
				}
			}
		}

		protected override void InitClone () {
			if (_PrimaryAction) (_PrimaryAction = _PrimaryAction.GetClone ()).Init (this);
			if (_SecondaryAction) (_SecondaryAction = _SecondaryAction.GetClone ()).Init (this);
			if (_SpecialAction) (_SpecialAction = _SpecialAction.GetClone ()).Init (this);
			if (_OptionalAction) (_OptionalAction = _OptionalAction.GetClone ()).Init (this);
		}

		public virtual bool Carry (CGVPlayerCharacter player, bool baseEquipment = false) {
			if (Carrier != null)
				return false;
			if (player == null)
				return false;
			Carrier = player;
			if (!baseEquipment)
				CarryEvent?.Post (Carrier.gameObject);
			PrimaryAction?.EquipmentCarried (player, baseEquipment);
			SecondaryAction?.EquipmentCarried (player, baseEquipment);
			SpecialAction?.EquipmentCarried (player, baseEquipment);
			OptionalAction?.EquipmentCarried (player, baseEquipment);
			return true;
		}

		public virtual bool Drop (CGVPlayerCharacter player) {
			if (Carrier != player)
				return false;
			if (player == null)
				return false;
			Carrier = null;
			PrimaryAction?.EquipmentDropped (player);
			SecondaryAction?.EquipmentDropped (player);
			SpecialAction?.EquipmentDropped (player);
			OptionalAction?.EquipmentDropped (player);
			return true;
		}

		protected bool Selected = false;

		public virtual void Select (bool selected) {
			Selected = selected;
			if (selected)
				SelectEvent?.Post (Carrier.gameObject);
			PrimaryAction?.EquipmentSelected (selected);
			SecondaryAction?.EquipmentSelected (selected);
			SpecialAction?.EquipmentSelected (selected);
			OptionalAction?.EquipmentSelected (selected);
		}

		// Call when the carrier equipment has been updated
		public virtual void UpdateEquipment () {
			PrimaryAction?.EquipmentUpdated ();
			SecondaryAction?.EquipmentUpdated ();
			SpecialAction?.EquipmentUpdated ();
			OptionalAction?.EquipmentUpdated ();
		}

		public virtual bool HoldPrimaryAction () { return PrimaryAction ? PrimaryAction.Hold () : false; }
		public virtual bool ReleasePrimaryAction () { return PrimaryAction ? PrimaryAction.Release () : false; }

		public virtual bool HoldSecondaryAction () { return SecondaryAction ? SecondaryAction.Hold () : false; }
		public virtual bool ReleaseSecondaryAction () { return SecondaryAction ? SecondaryAction.Release () : false; }

		public virtual bool HoldSpecialAction () { return SpecialAction ? SpecialAction.Hold () : false; }
		public virtual bool ReleaseSpecialAction () { return SpecialAction ? SpecialAction.Release () : false; }

		public virtual bool HoldOptionalAction () { return OptionalAction ? OptionalAction.Hold () : false; }
		public virtual bool ReleaseOptionalAction () { return OptionalAction ? OptionalAction.Release () : false; }
	}
}
