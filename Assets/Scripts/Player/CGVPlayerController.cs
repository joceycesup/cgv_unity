﻿using System.Collections.Generic;
using UnityEngine;

using JuneCodes;
using JuneCodes.FlexibleEnum;

using CGV.UI;

using Photon.Pun;
using Photon.Realtime;

namespace CGV.Player {
	public class CGVPlayerController : MonoBehaviourPunCallbacks {

		[SerializeField]
		protected string _PlayerName = "Player";
		public ref readonly string PlayerName { get { return ref _PlayerName; } }

		public int LocalPlayerIndex { get; protected set; }
		public bool IsMainLocalPlayer { get { return LocalPlayerIndex == 0; } }

		public int Team { get; protected set; } = JCEnumKey.InvalidId;

		public CGVPlayerActions PlayerActions;
		//public ref readonly CGVPlayerActions PlayerActions { get { return ref _PlayerActions; } }

		public CGVPlayerCharacter ControlledCharacter { get; protected set; }

		protected bool _NeedToPressMove = false;

		protected static List<CGVPlayerController> _LocalPlayers = new List<CGVPlayerController> ();
		public static ref readonly List<CGVPlayerController> LocalPlayers { get { return ref _LocalPlayers; } }

		[SerializeField]
		protected CGVPlayerCamera _ControllerCamera;
		public Camera Camera {
			get { return ControlledCharacter != null ? ControlledCharacter.Camera.Camera : _ControllerCamera.Camera; }
		}
		public CGVPlayerCanvas Canvas { get; protected set; }

		#region Radial Menu
		public CGVRadialMenu RadialMenu { get; private set; }
		protected bool RadialMenuIsOpen { get; private set; } = false;
		#endregion Radial Menu

		[RuntimeInitializeOnLoadMethod]
		static void RunOnStart () {
			JuneCodes.JCTypePool<CGVPlayerController> pool = JCTypePool<CGVPlayerController>.GetPool (1);
			CGVPlayerController mainPlayer = pool.Extract ();
			mainPlayer.gameObject.SetActive (true);
			//*
			CGVPlayerInputManager.OnFreeDeviceAttached += () => {
				pool.Extract ().gameObject.SetActive (true);
			};//*/
		}

		private void Awake () {
			int index = 0;
			foreach (CGVPlayerController player in _LocalPlayers) {
				if (player.LocalPlayerIndex == index)
					index++;
			}
			Canvas = GetComponentInChildren<CGVPlayerCanvas> ();
			RadialMenu = GetComponentInChildren<CGVRadialMenu> ();
			//Camera.rect = Canvas.Container.rect;
			LocalPlayerIndex = index;
			Canvas.SetAccessible (IsMainLocalPlayer);
			CGVPlayerInputManager.instance.CreatePlayerActions (this, out PlayerActions);
			if (IsMainLocalPlayer)
				_ControllerCamera.SetAsMainListener ();
			else {
				foreach (var accessible in GetComponentsInChildren<UAP_BaseElement> ()) {
					Destroy (accessible);
				}
			}
			DontDestroyOnLoad (gameObject);
		}

		public override void OnEnable () {
			base.OnEnable ();
			CGVGameSettings.OnSettingsUpdated += SettingsUpdated;

			CGVScene.OnSceneWillChange += OnSceneWillChange;
			CGVScene.OnSceneChanged += OnSceneChanged;

			CGVPlayableSceneManager.OnSceneReady += GetReady;
			CGVPlayableSceneManager.OnSessionEnded += SessionEnded;
			if (CGVPlayableSceneManager.IsReady)
				GetReady ();

			_LocalPlayers.Add (this);
			CGVSplitScreen.instance.AddPlayerToSplitScreen (this);
			//*
			if (CGVPlayerInputManager.instance.GetInputDevices (this, out InControl.InputDevice device)) {
				PlayerActions.Device = device;
				Debug.LogFormat ("Player {0} found a Device", LocalPlayerIndex);
			}
			else
				Debug.LogFormat ("Could not find Devices for player {0}", LocalPlayerIndex);
			//*/
		}

		public override void OnDisable () {
			base.OnDisable ();
			CGVGameSettings.OnSettingsUpdated -= SettingsUpdated;

			CGVScene.OnSceneWillChange -= OnSceneWillChange;
			CGVScene.OnSceneChanged -= OnSceneChanged;

			CGVPlayableSceneManager.OnSceneReady -= GetReady;
			CGVPlayableSceneManager.OnSessionEnded -= SessionEnded;

			CGVSplitScreen.instance.RemovePlayerFromSplitScreen (this);
			if (_LocalPlayers.Contains (this)) {
				CGVPlayerInputManager.instance.RemovePlayer (this);
				_LocalPlayers.Remove (this);
			}
		}

		private void SettingsUpdated () {
			if (IsMainLocalPlayer) {
				if (ControlledCharacter != null) {
					if (CGVGameSettings.instance.ShowMainPlayerDisplay)
						ControlledCharacter.Camera.SetCameraEnabled (true);
					else
						ControlledCharacter.Camera.DisableAndClearCamera ();
				}
			}
		}
		/*
#if CGV_NETWORK
		public override void OnPlayerEnteredRoom (Player newPlayer) {
			base.OnPlayerEnteredRoom (newPlayer);
			Vector3 position = Vector3.zero;
			Quaternion rotation = Quaternion.identity;
			if (ControlledCharacter != null) {
				position = ControlledCharacter.transform.position;
				rotation = ControlledCharacter.transform.rotation;
				PhotonNetwork.Destroy (ControlledCharacter.gameObject);
			}

			player = PhotonNetwork.Instantiate (Prefab.gameObject.name, position, rotation).GetComponent<Player> ();
		}
#endif // CGV_NETWORK
//*/
		void OnSceneChanged (CGVScene scene) {
			Debug.LogFormat ("Player {0} is changing scene (playable : {1})", LocalPlayerIndex, scene.IsPlayable);
			_ControllerCamera.SetCameraEnabled (scene.IsPlayable && ControlledCharacter == null);
			if (!scene.IsPlayable) {
				Canvas.SetPanelsVisibility (
					CGVPlayerCanvas.Panel.Crosshair |
					CGVPlayerCanvas.Panel.Death |
					CGVPlayerCanvas.Panel.StartGame |
					CGVPlayerCanvas.Panel.ScoreBoard, false);
			}
			else if (CGVGameSettings.instance.TeamSelection == CGVGameSettings.TeamSelectionMode.Free || CGVGameSettings.instance.AllowClassSelection) {
				Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.StartGame, true);
			}
			else {
				CGVPlayableSceneManager.CreateSceneManager ();
			}
		}

		void OnSceneWillChange (CGVScene scene) {
			Debug.LogFormat ("Player {0} will change scene (playable : {1})", LocalPlayerIndex, scene.IsPlayable);
			Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.Crosshair, false);
			Team = JCEnumKey.InvalidId;
			Possess (null);
		}

		void GetReady () {
			Debug.LogFormat ("Player {0} is getting ready!", LocalPlayerIndex);
			//if (false) {
			if (CGVGameSettings.instance.AllowClassSelection || CGVGameSettings.instance.TeamSelection == CGVGameSettings.TeamSelectionMode.Free) {
				if (CGVGameSettings.instance.TeamSelection == CGVGameSettings.TeamSelectionMode.Free && JCEnumKey.IdIsValid (Canvas.StartGameMenu.CurrentTeamId)) {
					Team = Canvas.StartGameMenu.CurrentTeamId;
					Debug.LogFormat ("Player {0} selected team {1}", LocalPlayerIndex, Team);
				}
				else
					Team = CGVPlayableSceneManager.instance.GetNextTeam ();

				// permettre selection character
				/*
				CGVPlayerCharacter character = CGVGameSettings.instance.PlayableCharacters.CreateDefaultCharacterForPlayer (this);
				SpawnCharacter (character);/*/
				SpawnAndPossess ();
				//*/
			}
			else {
				Team = CGVPlayableSceneManager.instance.GetNextTeam ();/*
				CGVPlayerCharacter character = CGVGameSettings.instance.PlayableCharacters.CreateDefaultCharacterForPlayer (this);
				SpawnCharacter (character);/*/
				SpawnAndPossess ();
				//*/
			}
		}

		void SessionEnded () {
			Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.Crosshair | CGVPlayerCanvas.Panel.Death | CGVPlayerCanvas.Panel.StartGame, false);
			Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.ScoreBoard, true);
		}

		private void SpawnAndPossess () {
			CGVSpawn spawn = CGVPlayableSceneManager.instance.GetSpawnForPlayer (this);
			if (spawn != null) {
				CGVPlayerCharacter character = CGVGameSettings.instance.PlayableCharacters.CreateDefaultCharacterForPlayer (this);
				if (spawn.Spawn (character)) {
					Debug.LogFormat ("Spawning a character! (player {0}, {1})", LocalPlayerIndex, character);
					Possess (character);
				}
			}
		}

		private void SpawnCharacter (CGVPlayerCharacter character) {
			if (character == null)
				return;
			if (CGVPlayableSceneManager.instance.SpawnCharacter (character, this)) {
				Debug.LogFormat ("Spawning a character! (player {0}, {1})", LocalPlayerIndex, character);
				Possess (character);
			}
		}

		public void Possess (CGVPlayerCharacter character) {
			if (character == null) {
				if (ControlledCharacter == null)
					return;
				ControlledCharacter.Unpossess ();
				ControlledCharacter.Camera.SetCameraEnabled (false);
				ControlledCharacter.OnTargetStateChanged -= CharacterStateChanged;
				ControlledCharacter.OnGotKilled -= CharacterGotKilled;
				ControlledCharacter = null;
				_ControllerCamera.SetCameraEnabled (true);
				if (IsMainLocalPlayer)
					_ControllerCamera.SetAsMainListener ();
				CGVSplitScreen.instance.UpdateDisplay ();

				Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.Crosshair, false);
				Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.StartGame, true);
			}
			else {
				if (character.NeedsToHaveMainController && !IsMainLocalPlayer)
					return;
				if (character == ControlledCharacter)
					return;
				else
					Possess (null);
				if (!character.PossessBy (this))
					return;
				ControlledCharacter = character;
				_ControllerCamera.SetCameraEnabled (false);
				ControlledCharacter.Camera.SetCameraEnabled (true);
				if (IsMainLocalPlayer && !CGVGameSettings.instance.ShowMainPlayerDisplay)
					ControlledCharacter.Camera.DisableAndClearCamera ();
				CGVSplitScreen.instance.UpdateDisplay ();
				if (IsMainLocalPlayer) {
					ControlledCharacter.Camera.SetAsMainListener ();
				}
				Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.StartGame, false);
				Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.Crosshair, true);

				CGVSplitScreen.instance.UpdateCameraRect (this);
				ControlledCharacter.OnTargetStateChanged += CharacterStateChanged;
				ControlledCharacter.OnGotKilled += CharacterGotKilled;
				Debug.Log ("Possessed a character! (player " + LocalPlayerIndex + ")");
			}
		}

		private void CharacterStateChanged (CGVTarget target, CGVTarget.TargetState newState, CGVTarget.TargetState oldState) {
			if (oldState == CGVTarget.TargetState.Dead || oldState == CGVTarget.TargetState.Inactive) {
				Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.Death, false);
				Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.Crosshair, true);
			}
			switch (newState) {
				case CGVTarget.TargetState.Dead: {
						Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.Crosshair, false);
						Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.Death, true);
						break;
					}
				case CGVTarget.TargetState.Inactive: {
						Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.Death, false);
						Canvas.SetPanelsVisibility (CGVPlayerCanvas.Panel.Crosshair, false);
						break;
					}
			}
		}

		private void CharacterGotKilled (CGVTarget target, CGVPlayerCharacter player) {
			//Debug.LogFormat ("----- {0} controlled by player {1} got killed by {2}", target.Name, LocalPlayerIndex, player.gameObject);
		}

		void Update () {
			if (IsMainLocalPlayer && Input.GetKeyDown (KeyCode.B)) {
				CGVGameSettings.instance.ShowMainPlayerDisplay = !CGVGameSettings.instance.ShowMainPlayerDisplay;
				Debug.Log ("ShowMainPlayerDisplay set to " + CGVGameSettings.instance.ShowMainPlayerDisplay);
			}
			if (ControlledCharacter == null) {
				if (!IsMainLocalPlayer) {
					Canvas.StartGameMenu.SendInput (PlayerActions.MenuMoveValue);
					if (PlayerActions.MenuSubmit.WasPressed)
						Canvas.StartGameMenu.Submit ();
				}
				return;
			}
			if (PlayerActions.HasControl) {

				if (ControlledCharacter.HasAbility (CGVPlayerAbility.SwitchEquipment)) {/*
					if (PlayerActions.NextEquipment.WasPressed)
						ControlledCharacter.SwitchEquipment (true);
					else if (PlayerActions.PreviousEquipment.WasPressed)
						ControlledCharacter.SwitchEquipment (false);/*/
					if (PlayerActions.NextEquipment.WasPressed)
						ControlledCharacter.EquipmentInventory.SwitchEquipment (true);
					else if (PlayerActions.PreviousEquipment.WasPressed)
						ControlledCharacter.EquipmentInventory.SwitchEquipment (false);//*/
				}

				if (Input.GetKeyDown (KeyCode.End))
					ControlledCharacter.Sight.AcquireEveryTarget ();

				if (ControlledCharacter.HasAbility (CGVPlayerAbility.SwitchTarget)) {
#if CGV_TEMP_CODE
					if (Input.GetKeyDown (KeyCode.KeypadPlus))
						ControlledCharacter.Sight.SwitchTarget (true);
					else if (Input.GetKeyDown (KeyCode.KeypadMinus))
						ControlledCharacter.Sight.SwitchTarget (false);
#endif
					if (PlayerActions.NextTarget.WasPressed)
						ControlledCharacter.Sight.SwitchTarget (true);
					else if (PlayerActions.PreviousTarget.WasPressed)
						ControlledCharacter.Sight.SwitchTarget (false);
				}

				if (PlayerActions.EquipmentAction1.IsPressed)
					ControlledCharacter.HoldAction (CGVPlayerAbility.PrimaryAction);
				else if (PlayerActions.EquipmentAction1.WasReleased)
					ControlledCharacter.ReleaseAction (CGVPlayerAbility.PrimaryAction);
				if (PlayerActions.EquipmentAction2.IsPressed)
					ControlledCharacter.HoldAction (CGVPlayerAbility.SecondaryAction);
				else if (PlayerActions.EquipmentAction2.WasReleased)
					ControlledCharacter.ReleaseAction (CGVPlayerAbility.SecondaryAction);
				if (PlayerActions.EquipmentAction3.IsPressed)
					ControlledCharacter.HoldAction (CGVPlayerAbility.SpecialAction);
				else if (PlayerActions.EquipmentAction3.WasReleased)
					ControlledCharacter.ReleaseAction (CGVPlayerAbility.SpecialAction);
				if (PlayerActions.EquipmentAction4.IsPressed)
					ControlledCharacter.HoldAction (CGVPlayerAbility.OptionalAction);
				else if (PlayerActions.EquipmentAction4.WasReleased)
					ControlledCharacter.ReleaseAction (CGVPlayerAbility.OptionalAction);

				if (PlayerActions.QuickAction1.WasPressed)
					ControlledCharacter.QuickAction1 ();
				if (PlayerActions.QuickAction2.WasLongPressed)
					ControlledCharacter.QuickAction2 (true);
				else if (PlayerActions.QuickAction2.WasReleased && !PlayerActions.QuickAction2.WasLongReleased)
					ControlledCharacter.QuickAction2 ();
				if (PlayerActions.QuickAction3.WasPressed)
					ControlledCharacter.QuickAction3 ();
				if (PlayerActions.QuickAction4.WasPressed)
					ControlledCharacter.QuickAction4 ();

				/*
				CGVPlayerAbility c = CGVPlayerAbility.OptionalAction;
				if (((CGVPlayerAbility)CGVPlayerAbilityCategories.Actions).HasFlag (c))
					Debug.Log ("coucou");//*/
				if (PlayerActions.CenterCamera.WasPressed) {
					ControlledCharacter.MouseLook.CenterCamera ();
				}

				if (PlayerActions.OpenRadialMenu.WasPressed)
					OpenRadialMenu ();
				else if (PlayerActions.OpenRadialMenu.WasReleased) {
					_NeedToPressMove = true;
					CloseRadialMenu ();
				}
				if (RadialMenuIsOpen && RadialMenu != null) {
					if (PlayerActions.NextRadialMenuOption.WasPressed)
						RadialMenu.SwitchOption (true);
					else if (PlayerActions.PreviousRadialMenuOption.WasPressed)
						RadialMenu.SwitchOption (false);
				}

				if (ControlledCharacter.HasAbility (CGVPlayerAbility.LookAround)) {
					Vector2 value = PlayerActions.View.Value;
					if (CGVGameSettings.instance.InvertViewYAxis && PlayerActions.View.LastInputType != InControl.BindingSourceType.MouseBindingSource)
						value.y = -value.y;
					ControlledCharacter.MouseLook.RotateView (value);
				}

				if (PlayerActions.Jump.WasPressed)
					ControlledCharacter.Jump ();
			}
		}

		public void OpenRadialMenu () {
			if (!ControlledCharacter.HasAbility (CGVPlayerAbility.OpenRadialMenu))
				return;
			if (RadialMenuIsOpen || RadialMenu == null)
				return;
			if (RadialMenu.Show ()) {
				RadialMenuIsOpen = true;
				ControlledCharacter.RadialMenuOpen ();
			}
		}
		public void CloseRadialMenu () {
			if (!RadialMenuIsOpen || RadialMenu == null)
				return;
			RadialMenuIsOpen = false;
			RadialMenu.Hide ();
			ControlledCharacter.RadialMenuClosed ();
		}

		private void FixedUpdate () {
			if (ControlledCharacter == null)
				return;
			if (PlayerActions.HasControl) {
				if (_NeedToPressMove && PlayerActions.Move.WasPressed)
					_NeedToPressMove = false;
				if (!_NeedToPressMove)
					ControlledCharacter.Move (PlayerActions.Move.Value);
			}
		}
	}
}
