﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JuneCodes;

namespace CGV.Player {
	[JCCreateSingletonIfNull]
	public class CGVSplitScreen : JCSingletonMonoBehaviour<CGVSplitScreen> {

		//[SerializeField]
		private int _TargetDisplay = 1;
		private List<CGVPlayerController> _Players = new List<CGVPlayerController> (3);

		private Texture2D _SplitTexture;

		private void Awake () {
			Display.onDisplaysUpdated += Display_onDisplaysUpdated;
			_SplitTexture = new Texture2D (10, 10, TextureFormat.ARGB32, false);
			for (int y = 0; y < _SplitTexture.height; ++y)
				for (int x = 0; x < _SplitTexture.width; ++x)
					_SplitTexture.SetPixel (x, y, Color.magenta);
			_SplitTexture.Apply ();
		}

		private void Display_onDisplaysUpdated () {
			Debug.Log ("DisplaysUpdated");
		}

		public bool AddPlayerToSplitScreen (CGVPlayerController player) {
			if (player.IsMainLocalPlayer)
				return false;
			if (_Players.Contains (player))
				return false;
			_Players.Add (player);
			if (Display.displays.Length > _TargetDisplay) {/*
				Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
				Screen.fullScreen = true;
				Display.displays[TargetDisplay].Activate (800, 600, 60);//*/
				Display.displays[_TargetDisplay].Activate ();
			}
			//Debug.LogFormat ("{0} Displays Detected", Display.displays.Length);
			player.Canvas.Canvas.targetDisplay = _TargetDisplay;
			UpdateSplitScreen ();
			UpdateDisplay ();
			return true;
		}

		public bool RemovePlayerFromSplitScreen (CGVPlayerController player) {
			if (JCApplicationUtils.Quitting)
				return true;
			if (player.IsMainLocalPlayer)
				return false;
			if (!_Players.Contains (player))
				return false;
			_Players.Remove (player);
			UpdateSplitScreen ();
			UpdateDisplay ();
			return true;
		}

		public bool UpdateCameraRect (CGVPlayerController player) {
			if (player.IsMainLocalPlayer)
				return true;
			int index = _Players.IndexOf (player);
			if (index < 0)
				return false;/*
			float width = 1f / _Players.Count;
			Rect rect = new Rect (index * width, 0f, width, 1f);
			_Players[index].Canvas.Container.anchorMin = rect.min;
			_Players[index].Canvas.Container.anchorMax = rect.max;
			return true;//*/
			player.Camera.rect = new Rect (player.Canvas.Container.anchorMin, player.Canvas.Container.anchorMax);
			return true;
		}//*/

		private void UpdateSplitScreen () {/*
			if (_Players.Count == 4) {
				_Players[0].Camera.rect = new Rect (0f, 0f, 0.5f, 0.5f);
				_Players[1].Camera.rect = new Rect (0.5f, 0f, 0.5f, 0.5f);
				_Players[2].Camera.rect = new Rect (0f, 0.5f, 0.5f, 0.5f);
				_Players[3].Camera.rect = new Rect (0.5f, 0.5f, 0.5f, 0.5f);
			}
			else {
			}//*/
			float width = 1f / _Players.Count;
			for (int i = 0; i < _Players.Count; ++i) {
				Rect rect = new Rect (i * width, 0f, width, 1f);
				_Players[i].Canvas.Container.anchorMin = rect.min;
				_Players[i].Canvas.Container.anchorMax = rect.max;
				_Players[i].Camera.rect = _Players[i].Canvas.Container.rect;
			}
		}

		public void UpdateDisplay () {
			for (int i = 0; i < _Players.Count; ++i) {
				_Players[i].Camera.targetDisplay = _Players[i].Canvas.Canvas.targetDisplay;
			}
		}

		public void OnGUI () {
			//GUI.DrawTexture ();
		}
	}
}
