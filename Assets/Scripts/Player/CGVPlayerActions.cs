﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using InControl;

namespace CGV.Player {
	public class CGVPlayerActions : PlayerActionSet {

		public bool HasKeyboardControl { get; protected set; } = false;
		public bool HasControl {
			get {
				return HasKeyboardControl || (Device != null);
			}
		}

		public PlayerAction Run;
		public PlayerAction Jump;

		public PlayerAction Left;
		public PlayerAction Right;
		public PlayerAction Down;
		public PlayerAction Up;
		public PlayerTwoAxisAction Move;

		public PlayerAction MenuLeft;
		public PlayerAction MenuRight;
		public PlayerAction MenuDown;
		public PlayerAction MenuUp;
		//public PlayerTwoAxisAction MenuMove;
		public Vector2 MenuMoveValue {
			get {
				return new Vector2 (
					(MenuLeft.WasRepeated || MenuLeft.WasPressed ? -MenuLeft.Value : (MenuRight.WasRepeated || MenuRight.WasPressed ? MenuRight.Value : 0)),
					(MenuDown.WasRepeated || MenuDown.WasPressed ? MenuDown.Value : (MenuUp.WasRepeated || MenuUp.WasPressed ? -MenuUp.Value : 0)));
			}
		}
		public PlayerAction MenuSubmit;

		public PlayerAction ViewLeft;
		public PlayerAction ViewRight;
		public PlayerAction ViewDown;
		public PlayerAction ViewUp;
		public PlayerTwoAxisAction View;

		public PlayerAction OpenRadialMenu;
		public PlayerAction PreviousRadialMenuOption;
		public PlayerAction NextRadialMenuOption;

		public PlayerAction QuickAction1;
		public PlayerAction QuickAction2;
		public PlayerAction QuickAction3;
		public PlayerAction QuickAction4;

		public PlayerAction EquipmentAction1;
		public PlayerAction EquipmentAction2;
		public PlayerAction EquipmentAction3;
		public PlayerAction EquipmentAction4;

		public PlayerAction NextEquipment;
		public PlayerAction PreviousEquipment;
		public PlayerOneAxisAction SwitchEquipment;

		public PlayerAction NextTarget;
		public PlayerAction PreviousTarget;

		public PlayerAction CenterCamera;

		public CGVPlayerActions () {
			Run = CreatePlayerAction ("Run");
			Jump = CreatePlayerAction ("Jump");

			Left = CreatePlayerAction ("Move Left");
			Right = CreatePlayerAction ("Move Right");
			Down = CreatePlayerAction ("Move Down");
			Up = CreatePlayerAction ("Move Up");
			Move = CreateTwoAxisPlayerAction (Left, Right, Down, Up);

			MenuLeft = CreatePlayerAction ("Menu Move Left");
			MenuRight = CreatePlayerAction ("Menu Move Right");
			MenuDown = CreatePlayerAction ("Menu Move Down");
			MenuUp = CreatePlayerAction ("Menu Move Up");
			MenuLeft.StateThreshold = MenuRight.StateThreshold = MenuDown.StateThreshold = MenuUp.StateThreshold = 0.5f;
			//MenuMove = CreateTwoAxisPlayerAction (MenuLeft, MenuRight, MenuDown, MenuUp);
			MenuSubmit = CreatePlayerAction ("Menu Submit");

			ViewLeft = CreatePlayerAction ("View Left");
			ViewRight = CreatePlayerAction ("View Right");
			ViewUp = CreatePlayerAction ("View Up");
			ViewDown = CreatePlayerAction ("View Down");
			View = CreateTwoAxisPlayerAction (ViewLeft, ViewRight, ViewDown, ViewUp);

			OpenRadialMenu = CreatePlayerAction ("Open Radial Menu");
			PreviousRadialMenuOption = CreatePlayerAction ("Previous Radial Menu Option");
			NextRadialMenuOption = CreatePlayerAction ("Next Radial Menu Option");

			QuickAction1 = CreatePlayerAction ("Quick Action 1");
			QuickAction2 = CreatePlayerAction ("Quick Action 2");
			QuickAction3 = CreatePlayerAction ("Quick Action 3");
			QuickAction4 = CreatePlayerAction ("Quick Action 4");

			EquipmentAction1 = CreatePlayerAction ("Equipment Action 1");
			EquipmentAction2 = CreatePlayerAction ("Equipment Action 2");
			EquipmentAction3 = CreatePlayerAction ("Equipment Action 3");
			EquipmentAction4 = CreatePlayerAction ("Equipment Action 4");

			PreviousEquipment = CreatePlayerAction ("Previous Equipment");
			NextEquipment = CreatePlayerAction ("Next Equipment");
			SwitchEquipment = CreateOneAxisPlayerAction (PreviousEquipment, NextEquipment);
			PreviousTarget = CreatePlayerAction ("Previous Target");
			NextTarget = CreatePlayerAction ("Next Target");

			CenterCamera = CreatePlayerAction ("Center Camera");
		}

		public static CGVPlayerActions CreateWithDefaultBindings (bool useKeyboard = false, bool useController = true) {
			var playerActions = new CGVPlayerActions ();

			playerActions.SetIncludeKeyboardAndMouse (useKeyboard);

			// How to set up mutually exclusive keyboard bindings with a modifier key.
			// playerActions.Back.AddDefaultBinding( Key.Shift, Key.Tab );
			// playerActions.Next.AddDefaultBinding( KeyCombo.With( Key.Tab ).AndNot( Key.Shift ) );
			playerActions.HasKeyboardControl = useKeyboard;

			if (useKeyboard) {
				playerActions.Run.AddDefaultBinding (Key.Shift);
				//playerActions.Run.AddDefaultBinding (InputControlType.Action1);
				//playerActions.Run.AddDefaultBinding (Mouse.LeftButton);

				playerActions.Jump.AddDefaultBinding (Key.Space);
				//playerActions.Jump.AddDefaultBinding (InputControlType.Action3);

				// Movement
				playerActions.Up.AddDefaultBinding (Key.UpArrow);
				playerActions.Down.AddDefaultBinding (Key.DownArrow);
				playerActions.Left.AddDefaultBinding (Key.LeftArrow);
				playerActions.Right.AddDefaultBinding (Key.RightArrow);

				// View
				playerActions.ViewUp.AddDefaultBinding (Mouse.PositiveY);
				playerActions.ViewDown.AddDefaultBinding (Mouse.NegativeY);
				playerActions.ViewLeft.AddDefaultBinding (Mouse.NegativeX);
				playerActions.ViewRight.AddDefaultBinding (Mouse.PositiveX);

				playerActions.CenterCamera.AddDefaultBinding (Key.F);

				// Radial Menu
				playerActions.OpenRadialMenu.AddDefaultBinding (Key.Tab);

				// Quick Actions
				playerActions.QuickAction1.AddDefaultBinding (Key.A);
				playerActions.QuickAction2.AddDefaultBinding (Key.E);
				playerActions.QuickAction3.AddDefaultBinding (Key.G);
				playerActions.QuickAction4.AddDefaultBinding (Key.T);

				// Equipment
				playerActions.EquipmentAction1.AddDefaultBinding (Mouse.LeftButton);
				playerActions.EquipmentAction2.AddDefaultBinding (Mouse.RightButton);
				playerActions.EquipmentAction3.AddDefaultBinding (Key.Space);
				playerActions.EquipmentAction4.AddDefaultBinding (Key.R);

				playerActions.PreviousEquipment.AddDefaultBinding (Mouse.NegativeScrollWheel);
				playerActions.NextEquipment.AddDefaultBinding (Mouse.PositiveScrollWheel);
			}

			if (useController) {
				// Menu
				playerActions.MenuUp.AddDefaultBinding (InputControlType.DPadUp);
				playerActions.MenuDown.AddDefaultBinding (InputControlType.DPadDown);
				playerActions.MenuLeft.AddDefaultBinding (InputControlType.DPadLeft);
				playerActions.MenuRight.AddDefaultBinding (InputControlType.DPadRight);
				playerActions.MenuUp.AddDefaultBinding (InputControlType.LeftStickUp);
				playerActions.MenuDown.AddDefaultBinding (InputControlType.LeftStickDown);
				playerActions.MenuLeft.AddDefaultBinding (InputControlType.LeftStickLeft);
				playerActions.MenuRight.AddDefaultBinding (InputControlType.LeftStickRight);
				playerActions.MenuSubmit.AddDefaultBinding (InputControlType.Action1);

				// Movement
				playerActions.Left.AddDefaultBinding (InputControlType.LeftStickLeft);
				playerActions.Right.AddDefaultBinding (InputControlType.LeftStickRight);
				playerActions.Up.AddDefaultBinding (InputControlType.LeftStickUp);
				playerActions.Down.AddDefaultBinding (InputControlType.LeftStickDown);

				// View
				playerActions.ViewLeft.AddDefaultBinding (InputControlType.RightStickLeft);
				playerActions.ViewRight.AddDefaultBinding (InputControlType.RightStickRight);
				playerActions.ViewUp.AddDefaultBinding (InputControlType.RightStickUp);
				playerActions.ViewDown.AddDefaultBinding (InputControlType.RightStickDown);

				playerActions.CenterCamera.AddDefaultBinding (InputControlType.RightStickButton);

				playerActions.PreviousTarget.AddDefaultBinding (InputControlType.LeftBumper);
				playerActions.NextTarget.AddDefaultBinding (InputControlType.RightBumper);

				// Radial Menu
				playerActions.OpenRadialMenu.AddDefaultBinding (InputControlType.Action3);
				playerActions.PreviousRadialMenuOption.AddDefaultBinding (InputControlType.LeftBumper);
				playerActions.NextRadialMenuOption.AddDefaultBinding (InputControlType.RightBumper);

				// Quick Actions
				playerActions.QuickAction1.AddDefaultBinding (InputControlType.DPadDown);
				playerActions.QuickAction2.AddDefaultBinding (InputControlType.DPadRight);
				playerActions.QuickAction3.AddDefaultBinding (InputControlType.DPadLeft);
				playerActions.QuickAction4.AddDefaultBinding (InputControlType.DPadUp);

				// Equipment
				playerActions.EquipmentAction1.AddDefaultBinding (InputControlType.RightTrigger);

				playerActions.EquipmentAction2.AddDefaultBinding (InputControlType.LeftTrigger);

				playerActions.EquipmentAction3.AddDefaultBinding (InputControlType.Action1);
				playerActions.EquipmentAction3.AddDefaultBinding (InputControlType.LeftStickButton);

				playerActions.EquipmentAction4.AddDefaultBinding (InputControlType.Action2);

				playerActions.PreviousEquipment.AddDefaultBinding (InputControlType.LeftBumper);
				playerActions.NextEquipment.AddDefaultBinding (InputControlType.RightBumper);

				// PlayerActions settings
				playerActions.ListenOptions.IncludeUnknownControllers = true;
				playerActions.ListenOptions.MaxAllowedBindings = 4;
				//playerActions.ListenOptions.MaxAllowedBindingsPerType = 1;
				//playerActions.ListenOptions.AllowDuplicateBindingsPerSet = true;
				playerActions.ListenOptions.UnsetDuplicateBindingsOnSet = true;
				//playerActions.ListenOptions.IncludeMouseButtons = true;
				//playerActions.ListenOptions.IncludeModifiersAsFirstClassKeys = true;
				//playerActions.ListenOptions.IncludeMouseButtons = true;
				//playerActions.ListenOptions.IncludeMouseScrollWheel = true;
			}

			playerActions.ListenOptions.OnBindingFound = (action, binding) => {
				if (binding == new KeyBindingSource (Key.Escape)) {
					action.StopListeningForBinding ();
					return false;
				}
				return true;
			};

			playerActions.ListenOptions.OnBindingAdded += (action, binding) => {
				Debug.Log ("Binding added... " + binding.DeviceName + ": " + binding.Name);
			};

			playerActions.ListenOptions.OnBindingRejected += (action, binding, reason) => {
				Debug.Log ("Binding rejected... " + reason);
			};

			return playerActions;
		}

		void SetIncludeKeyboardAndMouse (bool value) {
			ListenOptions.IncludeKeys = value;
			ListenOptions.IncludeMouseButtons = value;
			ListenOptions.IncludeMouseScrollWheel = value;
		}
	}
}
