﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

using CGV.Sound;
using CGV.Equipment;

#pragma warning disable 649
namespace CGV.Player {

	[RequireComponent (typeof (NavMeshAgent))]
	public class CGVDog : MonoBehaviour {

		private CGVGroundTextureDetector _GroundTextureDetector = new CGVGroundTextureDetector ();

		private CGVDogMasterPlayerCharacter _Master;

		private Matrix4x4 _InitMatrix;

		// Controls
		[Header ("Controls")]
		[SerializeField] protected float _MaxWalkSpeed = 10f;
		[SerializeField] protected float _MaxRunSpeed = 20f;
		[SerializeField] protected float _ReconSpeed = 15f;
		[SerializeField] protected float _ReconDistance = 20f;
		[SerializeField] protected float _Acceleration = 40f;

		[Min (0f)]
		[SerializeField] protected float _LeadMasterDistance = 4f;

		[SerializeField] protected float _AttackPlayerSpeedMultiplier = 0.5f;

		[SerializeField] protected float _ThreatGrowlDanceDelay = 15f;
		[SerializeField] protected float _ShutUpDuration = 45f;
		[SerializeField] protected float _IdleDelay = 30f;
		[SerializeField] protected float _PetDuration = 10f;

		[SerializeField] protected float _SoundThreatRadius = 10f;
		[SerializeField] protected float _VisualThreatDistance = 10f;
		[SerializeField] protected float _VisualThreatAngle = 10f;

		[Header ("Sound Events")]
		[SerializeField] private AK.Wwise.Event PetEvent;
		[SerializeField] private AK.Wwise.Event StartRunningEvent;
		[SerializeField] private AK.Wwise.Event GrabBallEvent;
		[SerializeField] private AK.Wwise.Event GiveBallEvent;

		[Header ("Events")]
		[SerializeField] public UltEvents.UltEvent Bark;
		[SerializeField] public UltEvents.UltEvent GrabBall;
		[SerializeField] public UltEvents.UltEvent GiveBall;

		[Header ("Reactions")]
		[SerializeField] private AK.Wwise.Event BarkEvent;
		[SerializeField] private AK.Wwise.Event ConfirmEvent;
		[SerializeField] private AK.Wwise.Event WhineEvent;
		[SerializeField] private AK.Wwise.Event GiveItemEvent;

		private NavMeshAgent _Agent;
		private NavMeshQueryFilter _AgentFilter;
		//private CGVBall _FetchingBall;
		//private CGVBall _FetchedBall;

		private ICGVConsumable _FetchingConsumable;
		private CGVConsumableProperties _FetchedConsumable;

		private bool _ReachedMaster = true;

		private Vector3 _StartForward;
		private Vector3 _StartLocation;

		[Serializable]
		public enum DogState {
			None = 0x0001,
			FollowPlayer = 0x0002,
			Idle = 0x0004,
			Pet = 0x0008,
			Fetch = 0x0010,
			Guide = 0x0020,
			Attack = 0x0040,
			WarnThreat = 0x0080,
			ProtectFromOtherDogs = 0x0100,
		}

		// This dictionary contains a mask of every possible next state for each state.
		private readonly Dictionary<DogState, DogState> _NextStates = new Dictionary<DogState, DogState> () {
			{ DogState.None, (DogState) (-1) },
			{ DogState.FollowPlayer, (DogState) (-1) },
			{ DogState.Idle, (DogState) (-1) },
			{ DogState.Pet, (DogState) (-1) },
			{ DogState.Fetch, (DogState) (-1) },
			{ DogState.Guide, (DogState) (-1) },
			{ DogState.Attack, (DogState) (-1) },
			{ DogState.WarnThreat, (DogState) (-1) },
			{ DogState.ProtectFromOtherDogs, (DogState) (-1) }
		};

		private DogState _State = DogState.None;
		public DogState State {
			get { return _State; }
			protected set {
				if (_State == value)
					return;
				if (!_NextStates.TryGetValue (value, out DogState states) || !states.HasFlag (value))
					return;

				if (value == DogState.FollowPlayer) {
					_Agent.stoppingDistance = 2f;
					_ReachedMaster = false;
					_Agent.SetDestination (_Master.transform.position);
				}
				else
					_Agent.stoppingDistance = 0.1f;
				_State = value;
			}
		}

		[Serializable]
		public enum DogReaction {
			None,
			Bark,
			Confirm,
			Whine,
			GiveItem,
		}

		public void TriggerReaction (DogReaction reaction) {
			switch (reaction) {
				case DogReaction.Bark:
					BarkEvent?.Post (gameObject);
					Bark.Invoke ();
					break;
				case DogReaction.Confirm:
					ConfirmEvent?.Post (gameObject);
					break;
				case DogReaction.GiveItem:
					GiveItemEvent?.Post (gameObject);
					break;
			}
		}

		void Start () {
			_Master = GetComponentInParent<CGVDogMasterPlayerCharacter> ();
			transform.SetParent (null);

			//_StartLocation = transform.position - _Master.transform.position;
			_StartLocation = _Master.transform.worldToLocalMatrix.MultiplyPoint (transform.position);
			_StartForward = _Master.transform.worldToLocalMatrix.MultiplyVector (transform.forward);

			_Agent = GetComponent<NavMeshAgent> ();
			_Agent.speed = _MaxRunSpeed;
			_Agent.acceleration = _Acceleration;
			_AgentFilter = new NavMeshQueryFilter { agentTypeID = _Agent.agentTypeID, areaMask = _Agent.areaMask };
			//_FetchingBall = _FetchedBall = null;
			_Master.Bounds.Refresh ();
			State = DogState.FollowPlayer;
		}

		public void ResetDog () {
			if (_Master == null)
				return;
			State = DogState.FollowPlayer;
			transform.position = _Master.transform.localToWorldMatrix.MultiplyPoint (_StartLocation);
			transform.forward = _Master.transform.localToWorldMatrix.MultiplyVector (_StartForward);
		}

		void Update () {
			switch (State) {
				case DogState.FollowPlayer: {
						if (!_ReachedMaster && _Agent.remainingDistance <= _Agent.stoppingDistance && !_Agent.pathPending) { // Check if "reached" method is viable
							Debug.Log ("Reached Master");/*
							if (_FetchedBall != null) {
								Master.TakeConsumable (new CGVConsumableProperties (ConsumableType.Ammunition, _FetchedBall.AmmoType, 1, _FetchedBall, typeof (CGVBall)));
								GiveBall.Invoke ();
								_FetchedBall = null;
								GiveBallEvent?.Post (gameObject);
							}//*/
							if (_FetchedConsumable.Type != ConsumableType.None) {
								_Master.TakeConsumable (_FetchedConsumable);
								TriggerReaction (DogReaction.GiveItem);
								_FetchedConsumable.Type = ConsumableType.None;
							}
							_ReachedMaster = true;
						}
						_Agent.SetDestination (_Master.transform.position);
						break;
					}
				case DogState.Guide: { // Check if consumable is still valid
						_Agent.SetDestination (_Master.transform.position + _Master.transform.forward * _LeadMasterDistance);
						break;
					}//*
				case DogState.Fetch: { // Check if consumable is still valid
									   //_Agent.SetDestination (Master.transform.position);
						break;
					}//*/
			}

			/*
			if (_FetchedBall != null)
				_Agent.SetDestination (Master.transform.position);
			else if (_FetchingBall != null && _FetchingBall.isActiveAndEnabled)
				_Agent.SetDestination (_FetchingBall.transform.position);//*/
		}

		private void FixedUpdate () {
			RaycastHit hit;
			if (_GroundTextureDetector.CheckGroundTexture (transform.position, Vector3.down, _Agent.height, out hit)) {
				Debug.Log ("Dog is on surface " + _GroundTextureDetector.TextureName);
			}
		}

		/// <summary>
		/// The dog tries to change its state to Guide.
		/// </summary>
		public bool Guide () {
			Debug.LogFormat ("Guide {0} , {1}", _State, State = DogState.Guide);
			return true;
		}


		/// <summary>
		/// The dog tries to change its state to Follow.
		/// </summary>
		public bool FollowMaster () {
			Debug.Log ("Follow Master");
			State = DogState.FollowPlayer;
			return false;
		}

		/// <summary>
		/// Not implemented yet
		/// </summary>
		public bool Attack (CGVPlayerCharacter character) {
			Debug.LogFormat ("Attack {0}", character);
			return false;
		}

		public static float GetPathLength (NavMeshPath path) {
			float lng = 0.0f;

			if ((path.status != NavMeshPathStatus.PathInvalid) && (path.corners.Length > 1)) {
				for (int i = 1; i < path.corners.Length; ++i) {
					lng += Vector3.Distance (path.corners[i - 1], path.corners[i]);
				}
			}

			return lng;
		}

		/// <summary>
		/// The dog tries to fetch closest ammo of type ammoType.
		/// </summary>
		/// <param name="ammoType">If negative, the dog will try to fetch any type of ammo that is the closest.</param>
		/// <returns>Whether or not it could find ammo for that type.</returns>
		public bool FetchAmmunition (int ammoType = -1) {
			//if (_FetchingConsumable != null)
			//return false;
			CGVAmmunitionType ammoTypeValue = CGVAmmunitionEnum.GetValue (ammoType, true);
			Debug.LogFormat ("Fetch ammunition for type {0}", ammoTypeValue == null ? "any" : ammoTypeValue.Name);
			float distance = float.MaxValue;
			CGVPickableAmmunitions closestAmmo = null;
			NavMeshPath closestPath = null;
			NavMeshPath path = new NavMeshPath ();
			foreach (CGVPickableAmmunitions ammo in FindObjectsOfType<CGVPickableAmmunitions> ()) {
				if (ammoTypeValue != null && !ammoTypeValue.Matches (ammo.AmmoType)) {
					continue;
				}
				path.ClearCorners ();
				if (NavMesh.CalculatePath (transform.position, ammo.transform.position, _AgentFilter, path)) {
					float tmpDist = GetPathLength (path);
					if (tmpDist < distance) {
						NavMeshPath tmpPath = (closestPath == null) ? new NavMeshPath () : closestPath;
						distance = tmpDist;
						closestPath = path;
						path = tmpPath;
						closestAmmo = ammo;
					}
				}
			}

			if (closestAmmo != null) {
				_FetchingConsumable = closestAmmo;
				_Agent.SetPath (closestPath);
				State = DogState.Fetch;
				return true;
			}
			Debug.LogFormat ("No ammunition found for type {0}", ammoTypeValue == null ? "any" : ammoTypeValue.Name);
			return false;
		}

		public bool Fetch (ICGVConsumable consumable) {
			Debug.LogFormat ("Fetch {0}", consumable == null ? "any" : consumable.ToString ());
			if (_FetchingConsumable != null)
				return false;
			if (consumable != null) {
				_FetchingConsumable = consumable;
				State = DogState.Fetch;
				return true;
			}
			return false;
		}
		/*
		public bool FetchBall (CGVBall ball) {
			if (_FetchedBall != null || ball == null)
				return false;
			StartRunningEvent?.Post (gameObject);
			_FetchingBall = ball;
			return true;
		}//*/

		void OnTriggerEnter (Collider other) {/*
			//Debug.Log ("Trigger " + other.gameObject);
			if (_FetchedBall == null && other.CompareTag ("Ball")) {
				Debug.Log ("Reached Ball");
				CGVBall ball = other.gameObject.GetComponent<CGVBall> ();
				if (ball == _FetchingBall) {
					GrabBallEvent?.Post (gameObject);
					GrabBall.Invoke ();
					ball.gameObject.SetActive (false);
					_FetchingBall = null;
					_FetchedBall = ball;
				}
			}
			else //*/
			if (_FetchingConsumable != null && other.GetComponentInParent<ICGVConsumable> () == _FetchingConsumable) {
				_FetchedConsumable = _FetchingConsumable.CheckContent ();
				_FetchedConsumable.Quantity = _FetchingConsumable.PickUp (-1);
				_FetchingConsumable = null;
				State = DogState.FollowPlayer;
			}
		}/*

		public override ConsumableProperties CheckContent () {
			if (fetchedBall == null)
				return new ConsumableProperties ();
			ConsumableProperties res = new ConsumableProperties (ConsumableType.Ammunition, fetchedBall.AmmoType, 1, fetchedBall, typeof (CGVBall));
			return res;
		}

		public override bool PickUp () {
			bool res = fetchedBall != null;
			if (res)
				GiveBall.Invoke ();
			fetchedBall = null;
			GiveBallEvent?.Post (gameObject);
			return res;
		}//*/
	}
}
