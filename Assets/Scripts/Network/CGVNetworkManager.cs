﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

using JuneCodes;
using ExitGames.Client.Photon;

namespace CGV {
#if CGV_NETWORK
	[JCCreateSingletonOnStart]
#else
	[JCCreateSingletonIfNull]
#endif
	public class CGVNetworkManager : JCSingletonMonoBehaviour<CGVNetworkManager>, IConnectionCallbacks, IInRoomCallbacks, ILobbyCallbacks, IMatchmakingCallbacks {/*
		IConnectionCallbacks: connection related callbacks.
		IInRoomCallbacks: callbacks that happen inside the room.
		ILobbyCallbacks: lobby related callbacks.
		IMatchmakingCallbacks: matchmaking related callbacks.
		IOnEventCallback: a single callback for any received event. This is equivalent to the C# event OnEventReceived.
		IWebRpcCallback: a single callback for receiving WebRPC operation response.
		IPunInstantiateMagicCallback: a single callback for instantiated PUN prefabs.
		IPunObservable: PhotonView serialization callbacks.
		IPunOwnershipCallbacks: PUN ownership transfer callbacks.
		//*/

		// Custom event codes
		public static readonly byte PlayerIsReadyEvent = 30;

		// Custom names and properties
		public static readonly string LobbyName = "CGV_Lobby";
		public static readonly string MapNameProperty = "CGV_MapName";
		public static readonly string GameModeProperty = "CGV_GameMode";
		public static readonly string AllowClassSelectionProperty = "CGV_AllowClassSelection";
		public static readonly string AllowTeamSelectionProperty = "CGV_AllowTeamSelection";

		public static bool IsOnline { get { return PhotonNetwork.IsConnectedAndReady && CGVGameSettings.instance.Multiplayer == CGVGameSettings.MultiplayerMode.Online; } }

		void Start () {
			//Debug.Log ("Starting Network Manager");
			//Connect ();
		}

		void OnEnable () {
			PhotonNetwork.AddCallbackTarget (this);
		}

		void OnDisable () {
			PhotonNetwork.RemoveCallbackTarget (this);
		}

		void OnDestroy () {
			if (PhotonNetwork.IsConnected) {
				PhotonNetwork.Disconnect ();
			}
		}

		public static void Connect () {
			// we check if we are connected or not, we join if we are , else we initiate the connection to the server.
			if (PhotonNetwork.IsConnected) {
				Debug.Log ("PUN -> Already Connected");
			}
			else {
				// #Critical, we must first and foremost connect to Photon Online Server.
				//PhotonNetwork.GameVersion = Application.version;
				//PhotonNetwork.NickName = "DefaultNickname";
				Debug.Log ("PUN -> Connect using Settings");
				PhotonNetwork.ConnectUsingSettings ();
			}
		}

		public static bool JoinLobby () {
			TypedLobby sqlLobby = new TypedLobby (LobbyName, LobbyType.SqlLobby);
			return PhotonNetwork.JoinLobby (sqlLobby);
		}

		public static bool PeekRoomList (CGVGameSettings.GameMode mode = CGVGameSettings.GameMode.None) {
			TypedLobby sqlLobby = new TypedLobby (LobbyName, LobbyType.SqlLobby);
			string sqlLobbyFilter = "";
			if (mode != CGVGameSettings.GameMode.None)
				sqlLobbyFilter = GameModeProperty + " = " + ((int)mode);   // request games with current GameMode
			return PhotonNetwork.GetCustomRoomList (sqlLobby, sqlLobbyFilter);
		}
		//*
		public static bool CreateRoom (string hostname, CGVPlayableScene scene) {
			if (string.IsNullOrEmpty (hostname))
				return false;
			if (scene == null)
				return false;

			RoomOptions roomOptions = new RoomOptions ();
			roomOptions.MaxPlayers = (byte)CGVGameSettings.instance.MaxPlayerCount;
			// in this example, C0 might be 0 or 1 for the two (fictional) game modes
			roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable () {
				{ GameModeProperty, (int)CGVGameSettings.instance.Mode },
				{ MapNameProperty, scene.Name },
				{ AllowClassSelectionProperty, CGVGameSettings.instance.AllowClassSelection },
				{ AllowTeamSelectionProperty, CGVGameSettings.instance.TeamSelection == CGVGameSettings.TeamSelectionMode.Free }
			};
			// this makes these properties available in the lobby
			roomOptions.CustomRoomPropertiesForLobby = new string[] { GameModeProperty, MapNameProperty, AllowClassSelectionProperty, AllowTeamSelectionProperty };
			// let's create this room in SqlLobby "CGVLobby" explicitly
			TypedLobby sqlLobby = new TypedLobby (LobbyName, LobbyType.SqlLobby);
			//lbClient.OpCreateRoom (roomName, roomOptions, sqlLobby);

			Debug.Log ("PUN -> Creating Room " + hostname);
			ListProperties (roomOptions.CustomRoomProperties);
			return PhotonNetwork.CreateRoom (hostname, roomOptions, sqlLobby);
		}//*/

		public static bool JoinRoom (string hostname) {
			if (string.IsNullOrEmpty (hostname))
				return false;

			Debug.Log ("PUN -> Joining Room " + hostname);
			return PhotonNetwork.JoinRoom (hostname);
		}

		private static object GetCurrentRoomProperty (string property) {
			if (PhotonNetwork.CurrentRoom == null)
				return null;
			return PhotonNetwork.CurrentRoom.CustomProperties[property];
		}

		private static bool TryGetCurrentRoomProperty (string propertyName, out object value) {
			if (PhotonNetwork.CurrentRoom == null) {
				value = null;
				return false;
			}
			object property = PhotonNetwork.CurrentRoom.CustomProperties[propertyName];
			if (property == null) {
				value = null;
				return false;
			}
			value = property;
			return true;
		}

		public static string GetCurrentRoomMapName () {
			return GetCurrentRoomProperty (MapNameProperty) as string;
		}

		public static bool TryGetCurrentRoomGameMode (out CGVGameSettings.GameMode mode) {
			if (TryGetCurrentRoomProperty (GameModeProperty, out object value)) {
				mode = (CGVGameSettings.GameMode)value;
				return true;
			}
			mode = CGVGameSettings.GameMode.None;
			return false;
		}

		public static bool TryGetCurrentRoomAllowTeamSelection (out bool allowed) {
			if (TryGetCurrentRoomProperty (AllowTeamSelectionProperty, out object value)) {
				allowed = (bool)value;
				return true;
			}
			allowed = false;
			return false;
		}

		public static bool TryGetCurrentRoomAllowClassSelection (out bool allowed) {
			if (TryGetCurrentRoomProperty (AllowClassSelectionProperty, out object value)) {
				allowed = (bool)value;
				return true;
			}
			allowed = false;
			return false;
		}

		private static void ListProperties (ExitGames.Client.Photon.Hashtable properties) {
			Debug.Log ("-------- Properties --------");
			foreach (var prop in properties) {
				Debug.LogFormat ("{0}={1}", prop.Key, prop.Value);
			}
			Debug.Log ("-----------------------------");
		}
		/*
		public static bool JoinOrCreateRoom (string hostname, CGVPlayableScene scene) {
			if (string.IsNullOrEmpty (hostname))
				return false;

			if (scene != null) {
				RoomOptions roomOptions = new RoomOptions ();
				roomOptions.MaxPlayers = (byte)CGVGameSettings.instance.MaxPlayerCount;
				// in this example, C0 might be 0 or 1 for the two (fictional) game modes
				roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable () {
					{ "C0", (int)CGVGameSettings.instance.Mode },
					{ "C1", scene.Name }
				};
				roomOptions.CustomRoomPropertiesForLobby = new string[] { "C0", "C1" }; // this makes "C0" and "C1" available in the lobby
																						// let's create this room in SqlLobby "CGVLobby" explicitly
				TypedLobby sqlLobby = new TypedLobby (LobbyName, LobbyType.SqlLobby);
				//lbClient.OpCreateRoom (roomName, roomOptions, sqlLobby);
				Debug.Log ("PUN -> Joining or creating Room " + hostname);
				return PhotonNetwork.JoinOrCreateRoom (hostname, roomOptions, sqlLobby);
			}
			else {
				Debug.Log ("PUN -> Joining Room " + hostname);
				return PhotonNetwork.JoinRoom (hostname);
			}
		}//*/

		#region IConnectionCallbacks
		public void OnConnected () {
			Debug.Log ("PUN -> Connected");
		}

		public void OnConnectedToMaster () {
			Debug.Log ("PUN -> Connected To Master");
		}

		public void OnDisconnected (DisconnectCause cause) {
			Debug.Log ("PUN -> Disconnected : " + cause);
		}

		public void OnRegionListReceived (RegionHandler regionHandler) {
			Debug.Log ("PUN -> ----- Region List -----");
			foreach (var region in regionHandler.EnabledRegions) {
				Debug.Log ("Region Code : " + region.Code);
			}
			Debug.Log ("------------------------------");
		}

		public void OnCustomAuthenticationResponse (Dictionary<string, object> data) {
			throw new System.NotImplementedException ();
		}

		public void OnCustomAuthenticationFailed (string debugMessage) {
			throw new System.NotImplementedException ();
		}
		#endregion IConnectionCallbacks

		#region IInRoomCallbacks
		public void OnPlayerEnteredRoom (Photon.Realtime.Player newPlayer) {
			Debug.LogFormat ("PUN -> Player {0} entered room", newPlayer.NickName);
		}

		public void OnPlayerLeftRoom (Photon.Realtime.Player otherPlayer) {
			Debug.LogFormat ("PUN -> Player {0} left room", otherPlayer.NickName);
		}

		public void OnRoomPropertiesUpdate (ExitGames.Client.Photon.Hashtable propertiesThatChanged) {
			Debug.Log ("PUN -> Room Properties Changed");
			ListProperties (propertiesThatChanged);
		}

		public void OnPlayerPropertiesUpdate (Photon.Realtime.Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps) {
			throw new System.NotImplementedException ();
		}

		public void OnMasterClientSwitched (Photon.Realtime.Player newMasterClient) {
			throw new System.NotImplementedException ();
		}
		#endregion IInRoomCallbacks

		#region ILobbyCallbacks
		public void OnJoinedLobby () {
			Debug.Log ("PUN -> Joined Lobby");
		}

		public void OnLeftLobby () {
			Debug.Log ("PUN -> Left Lobby");
		}

		public void OnRoomListUpdate (List<RoomInfo> roomList) {
			Debug.Log ("PUN -> Room List Updated");
			Debug.Log ("-------- Room Infos --------");
			foreach (var info in roomList) {
				Debug.Log (info.Name);
			}
			Debug.Log ("----------------------------");
		}

		public void OnLobbyStatisticsUpdate (List<TypedLobbyInfo> lobbyStatistics) {
			throw new System.NotImplementedException ();
		}
		#endregion ILobbyCallbacks

		#region IMatchmakingCallbacks
		public void OnFriendListUpdate (List<FriendInfo> friendList) {
			throw new System.NotImplementedException ();
		}

		public void OnCreatedRoom () {
			Debug.Log ("PUN -> Created Room");
		}

		public void OnCreateRoomFailed (short returnCode, string message) {
			Debug.Log ("PUN -> Create Room failed : " + message);
		}

		public void OnJoinedRoom () {
			Debug.Log ("PUN -> Joined Room");
			ListProperties (PhotonNetwork.CurrentRoom.CustomProperties);
		}

		public void OnJoinRoomFailed (short returnCode, string message) {
			Debug.Log ("PUN -> Joined Room failed : " + message);
		}

		public void OnJoinRandomFailed (short returnCode, string message) {
			throw new System.NotImplementedException ();
		}

		public void OnLeftRoom () {
			Debug.Log ("PUN -> Left Room");
		}
		#endregion IMatchmakingCallbacks
	}
}
