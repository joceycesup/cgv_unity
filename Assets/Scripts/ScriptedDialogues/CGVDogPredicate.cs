﻿using System;
using UnityEngine;

using CGV.Player;

namespace CGV {
	public class CGVDogPredicate : CGVPredicate {
		[Serializable]
		public enum DogAction {
			Bark,
			GrabBall,
			GiveBall,
		}

		[SerializeField] private CGVDog Dog;
		[SerializeField] private DogAction Action;

		private UltEvents.UltEvent Event;

		protected override void Init () {
			switch (Action) {
				case DogAction.Bark:
					Event = Dog.Bark;
					break;
				case DogAction.GrabBall:
					Event = Dog.GrabBall;
					break;
				case DogAction.GiveBall:
					Event = Dog.GiveBall;
					break;
			}
			if (Event != null)
				Event.DynamicCalls += MeetCondition;
			else
				MeetCondition ();
		}

		protected override void StopCheck () {
			if (Event != null) {
				Event.DynamicCalls -= MeetCondition;
				Event = null;
			}
		}

		protected override bool CheckCondition () {
			if (Dog == null)
				return true;

			return ConditionIsMet;
		}
	}
}
