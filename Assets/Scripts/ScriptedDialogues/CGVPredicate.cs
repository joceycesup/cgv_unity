﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGV {
	public abstract class CGVPredicate : MonoBehaviour {

		public bool ConditionIsMet { get; protected set; } = false;
		private bool IsChecking = false;

		public WaitUntil BeginCheck() {
			ConditionIsMet = false;
			IsChecking = true;
			Init ();
			return new WaitUntil (() => { return ConditionIsMet; });
		}

		void Update () {
			if (!IsChecking)
				return;
			if (CheckCondition ()) {
				ConditionIsMet = true;
				IsChecking = false;
				StopCheck ();
			}
		}

		protected void MeetCondition() {
			ConditionIsMet = true;
		}

		protected abstract void Init ();
		protected abstract void StopCheck ();
		protected abstract bool CheckCondition ();
	}
}
