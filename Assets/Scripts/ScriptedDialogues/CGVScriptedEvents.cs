﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 649
namespace CGV {
	public class CGVScriptedEvents : MonoBehaviour {

		[Serializable]
		public struct ScriptedEvent {
			[SerializeField] private string Description;
			[SerializeField] private CGVPredicate _Predicate;
			public CGVPredicate Predicate { get { return _Predicate; } }
			[SerializeField] public float _Delay;
			public float Delay { get { return _Delay; } }
			[SerializeField] UltEvents.UltEvent _Actions;
			public UltEvents.UltEvent Actions { get { return _Actions; } }
			[SerializeField] AK.Wwise.Event _DialogueEvent;
			public AK.Wwise.Event DialogueEvent { get { return _DialogueEvent; } }
			[SerializeField] private GameObject _SoundSource;
			public GameObject SoundSource { get { return _SoundSource; } }

			public bool Play (GameObject gameObject, AkCallbackManager.EventCallback callback) {
				Actions.Invoke ();
				bool res = DialogueEvent?.Post (gameObject, (uint)AkCallbackType.AK_EndOfEvent, callback) != 0;
				if (res)
					Debug.Log ("Event \"" + Description + "\" playing!");
				else
					Debug.LogWarning ("Event \"" + Description + "\" has an invalid Wwise Event.");
				return res;
			}
		}

		private int CurrentEventIndex;
		[SerializeField]
		List<ScriptedEvent> Events;

		[SerializeField] UltEvents.UltEvent _Actions;

		void Start () {
			PlayCurrentEvent ();
		}

		void PlayCurrentEvent () {
			if (CurrentEventIndex >= Events.Count)
				return;
			StartCoroutine (DelayAction ());
		}

		void PlayNextEvent () {
			CurrentEventIndex++;
			PlayCurrentEvent ();
		}

		private IEnumerator DelayAction () {
			ScriptedEvent Event = Events[CurrentEventIndex];
			if (Event.Delay > 0) {
				yield return new WaitForSeconds (Event.Delay);
			}
			if (Event.Predicate != null && !Event.Predicate.ConditionIsMet) {
				yield return Event.Predicate.BeginCheck ();
			}
			if (!Event.Play (Event.SoundSource != null ? Event.SoundSource : gameObject, CallbackFunction)) {
				PlayNextEvent ();
			}
		}

		void CallbackFunction (object in_cookie, AkCallbackType in_type, object in_info) {
			PlayNextEvent ();
		}
	}
}
