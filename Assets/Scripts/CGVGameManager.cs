﻿using UnityEngine;

using JuneCodes;

using CGV.Player;

namespace CGV {
	[JCCreateSingletonOnStart]
	public class CGVGameManager : JCSingletonMonoBehaviour<CGVGameManager> {

#if UNITY_EDITOR
		private void Awake () {
			CGVSpawn[] spawns = FindObjectsOfType<CGVSpawn> ();
			if (spawns.Length > 0) {
				int teamID = JuneCodes.FlexibleEnum.JCEnumKey.InvalidId;

				foreach (var spawn in spawns) {
					if (JuneCodes.FlexibleEnum.JCEnumKey.IdIsValid (spawn.Team)) {
						teamID = spawn.Team;
						break;
					}
				}

				CGVGameSettings.instance.Multiplayer = CGVGameSettings.MultiplayerMode.Solo;
				CGVGameSettings.instance.Mode = CGVGameSettings.GameMode.None;
				CGVGameSettings.instance.AllowClassSelection = false;
				CGVGameSettings.instance.EnforceTeamIndex = teamID;
				CGVGameSettings.instance.TeamSelection = CGVGameSettings.TeamSelectionMode.Enforced;
				if (FindObjectOfType<CGVPlayableSceneManager> () == null) {
					Debug.Log ("No SceneManager found, creating one");
					CGVPlayableSceneManager.CreateSceneManager ();
				}
			}
		}
#endif
	}
}
