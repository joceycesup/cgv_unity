﻿using System.Collections;
using UnityEngine;

using JuneCodes;

#pragma warning disable 649
namespace CGV.Sound {
	/// <summary>
	/// Component that fires a singleshot sound at a specific location.
	/// It is automatically pooled.
	/// </summary>
	public class CGVSingleShotSoundEvent : MonoBehaviour {

		private static readonly uint CallbackFlags = (uint)AkCallbackType.AK_EndOfEvent | (uint)AkCallbackType.AK_Duration;

		//private static CGVObjectPool<CGVSingleShotSoundEvent> Pool;
		private static JCTypePool<CGVSingleShotSoundEvent> Pool;

		private AK.Wwise.Event _SoundEvent;

		private ICGVTextureSwitchProvider _Texture;

		private Transform _AttachedTransform;
		private bool _DetachOnPlay = false;

		public bool IsPlaying { get; private set; } = false;

		private GameObject _SoundSource;

		private void InitSoundSource () {
			if (_SoundSource == null)
				_SoundSource = transform.GetChild (0).gameObject;
		}

#if CGV_DEBUG
		private void Start () {
			_SoundSource.name = _SoundSource.name + gameObject.name.Substring (gameObject.name.IndexOf ("_"));
			//Debug.Log (gameObject.name + " spawned and started");
		}
#endif

		private static CGVSingleShotSoundEvent PlaySingleShot (AK.Wwise.Event singleShotEvent, Transform transform, bool detachOnPlay, Vector3 location, float delay, AK.Wwise.Switch singleShotSwitch, ICGVTextureSwitchProvider texture) {
			if (singleShotEvent == null) {
				Debug.Log ("Trying to play null event as singleShot");
				return null;
			}
			if (!singleShotEvent.IsValid ())
				return null;
			if (Pool == null)
				//Pool = CGVObjectPool<CGVSingleShotSoundEvent>.GetPool ();
				Pool = JCTypePool<CGVSingleShotSoundEvent>.GetPool ();
			if (Pool == null)
				return null;
			CGVSingleShotSoundEvent ssse = Pool.Extract ();
			if (ssse == null)
				return null;

			ssse.IsPlaying = false;
			ssse._AttachedTransform = transform;
			ssse.InitSoundSource ();
			if (transform != null)
				ssse._SoundSource.transform.localPosition = location;
			else
				ssse.transform.position = location;
			ssse._DetachOnPlay = detachOnPlay;

			ssse._SoundEvent = singleShotEvent;
			ssse._Texture = texture;
			singleShotSwitch?.SetValue (ssse._SoundSource);
			ssse.gameObject.SetActive (true);
			ssse.StartCoroutine (ssse.PlaySoundWithDelay (delay));
			return ssse;
		}

		public static CGVSingleShotSoundEvent PlaySingleShotOnTransform (AK.Wwise.Event singleShotEvent, Transform transform, Vector3 localPosition, float delay, AK.Wwise.Switch singleShotSwitch = null) {
			return PlaySingleShot (singleShotEvent, transform, false, localPosition, delay, singleShotSwitch, null);
		}

		public static CGVSingleShotSoundEvent PlaySingleShotOnTransform (AK.Wwise.Event singleShotEvent, Transform transform, Vector3 localPosition, float delay, ICGVTextureSwitchProvider texture) {
			return PlaySingleShot (singleShotEvent, transform, false, localPosition, delay, null, texture);
		}

		public static CGVSingleShotSoundEvent PlaySingleShotOnTransform (AK.Wwise.Event singleShotEvent, Transform transform, Vector3 localPosition, AK.Wwise.Switch singleShotSwitch = null) {
			return PlaySingleShot (singleShotEvent, transform, false, localPosition, -1f, singleShotSwitch, null);
		}

		public static CGVSingleShotSoundEvent PlaySingleShotOnTransform (AK.Wwise.Event singleShotEvent, Transform transform, Vector3 localPosition, ICGVTextureSwitchProvider texture) {
			return PlaySingleShot (singleShotEvent, transform, false, localPosition, -1f, null, texture);
		}

		public static CGVSingleShotSoundEvent PlaySingleShotAtLocation (AK.Wwise.Event singleShotEvent, Transform transform, float delay, AK.Wwise.Switch singleShotSwitch = null) {
			return PlaySingleShot (singleShotEvent, transform, true, Vector3.zero, delay, singleShotSwitch, null);
		}

		public static CGVSingleShotSoundEvent PlaySingleShotAtLocation (AK.Wwise.Event singleShotEvent, Transform transform, float delay, ICGVTextureSwitchProvider texture) {
			return PlaySingleShot (singleShotEvent, transform, true, Vector3.zero, delay, null, texture);
		}

		public static CGVSingleShotSoundEvent PlaySingleShotAtLocation (AK.Wwise.Event singleShotEvent, Transform transform, AK.Wwise.Switch singleShotSwitch = null) {
			return PlaySingleShot (singleShotEvent, transform, true, Vector3.zero, -1f, singleShotSwitch, null);
		}

		public static CGVSingleShotSoundEvent PlaySingleShotAtLocation (AK.Wwise.Event singleShotEvent, Transform transform, ICGVTextureSwitchProvider texture) {
			return PlaySingleShot (singleShotEvent, transform, true, Vector3.zero, -1f, null, texture);
		}

		public static CGVSingleShotSoundEvent PlaySingleShotAtLocation (AK.Wwise.Event singleShotEvent, Vector3 location, float delay, AK.Wwise.Switch singleShotSwitch = null) {
			return PlaySingleShot (singleShotEvent, null, true, location, delay, singleShotSwitch, null);
		}

		public static CGVSingleShotSoundEvent PlaySingleShotAtLocation (AK.Wwise.Event singleShotEvent, Vector3 location, float delay, ICGVTextureSwitchProvider texture) {
			return PlaySingleShot (singleShotEvent, null, true, location, delay, null, texture);
		}

		public static CGVSingleShotSoundEvent PlaySingleShotAtLocation (AK.Wwise.Event singleShotEvent, Vector3 location, AK.Wwise.Switch singleShotSwitch = null) {
			return PlaySingleShot (singleShotEvent, null, true, location, -1f, singleShotSwitch, null);
		}

		public static CGVSingleShotSoundEvent PlaySingleShotAtLocation (AK.Wwise.Event singleShotEvent, Vector3 location, ICGVTextureSwitchProvider texture) {
			return PlaySingleShot (singleShotEvent, null, true, location, -1f, null, texture);
		}

		private IEnumerator PlaySoundWithDelay (float delay) {
			if (delay <= 0f)
				yield return null;
			else
				yield return new WaitForSeconds (delay);
			//if (SoundSwitch != null) Debug.Log (SoundSwitch);
			CopyAttachedTransform (false);
			AkSoundEngine.SetObjectPosition (_SoundSource, _SoundSource.transform);
			if (_SoundEvent?.Post (_SoundSource, CallbackFlags, CallbackFunction) == 0)
				Pool.Store (this);
		}

		private void Update () {
			if (!IsPlaying)
				_Texture?.GetTextureSwitch ().SetValue (_SoundSource);
			else {
				CopyAttachedTransform ();
				//AkSoundEngine.SetObjectPosition (_SoundSource, _SoundSource.transform);
#if CGV_DEBUG
				//Debug.Log (SoundSource.transform.position);
				CGVDebugUtils.DrawCube (_SoundSource.transform.position, .25f, Color.yellow);
#endif
			}
		}

		void CallbackFunction (object in_cookie, AkCallbackType in_type, object in_info) {
			//Debug.Log("I am the best porgrammer ever // coucou le chat // rekt the game ");
			//CGVDebugUtils.DrawCube(transform.position, Color.red, .5f);
			//if (SoundSwitch != null)
			//AkSoundEngine.SetSwitch()
			//Debug.Log ("Storing");
			if (in_type == AkCallbackType.AK_EndOfEvent) {
				IsPlaying = false;
				if (_AttachedTransform != null)
					_SoundSource.transform.localPosition = Vector3.zero;
				//_SoundEvent = null;
				//transform.localPosition = Vector3.zero;
				Pool.Store (this);
			}
			else { // AK_Duration : used to mark the beginning of the sound, useful when there is a delay before the sound
				   //Debug.Log (in_type);
				   //Debug.Log (Texture?.GetTextureSwitch ());
#if CGV_DEBUG1
				Debug.LogFormat ("Playing event {0} as singleShot on {1}", _SoundEvent.Name, gameObject.name);
				if (_SoundSource.GetComponent<AkSpatialAudioEmitter> () == null)
					Debug.LogWarningFormat ("This singleShot ({0}) doesn't have an emitter.", gameObject.name);
#endif
				CopyAttachedTransform ();
				IsPlaying = true;
			}
		}

		private void CopyAttachedTransform (bool canDetach = true) {
			if (_AttachedTransform == null) return;
			transform.position = _AttachedTransform.position;
			transform.rotation = _AttachedTransform.rotation;
			//transform.SetPositionAndRotation (AttachedTransform.position, AttachedTransform.rotation);
			if (_DetachOnPlay && canDetach)
				_AttachedTransform = null;
		}
	}
}
