﻿using UnityEngine;

using CGV.Player;

namespace CGV.Sound {
	public static class CGVSoundUtils {

		public static readonly AK.Wwise.Switch DefaultTextureSwitch;

		static CGVSoundUtils () {
			DefaultTextureSwitch = CGVWallSoundMaterialEnum.instance.DefaultValue.TextureSwitch;
		}

		public static readonly float SpeedOfSound = 340.34838f; // at 15 Celsius

		/// <summary>
		/// This method presumes the listener is at the same location as the weapon.
		/// </summary>
		/// <param name="muzzleVelocity">Muzzle velocity of the weapon</param>
		/// <param name="distance">Distance from the weapoin to the impact point</param>
		/// <returns>The sound delay induced by the distance from the weapon to the impact point, and from the impact point to the weapon.</returns>
		public static float BulletDelay (float muzzleVelocity, float distance) {
			float delay = distance / SpeedOfSound;
			if (muzzleVelocity > 0f)
				delay += distance / muzzleVelocity;
			return delay;
		}

		/// <summary>
		/// This method finds the correct delay relative to the main spatial audio listener.
		/// </summary>
		/// <param name="muzzleVelocity">Muzzle velocity of the weapon</param>
		/// <param name="hit">RaycastHit cast by the weapon</param>
		/// <returns>The sound delay induced by the distance from the weapon to the impact point, and from the impact point to the listener.</returns>
		public static float BulletDelay (float muzzleVelocity, RaycastHit hit, CGVPlayerCharacter shooter) {
			float delay = shooter.IsMainListener ? SoundDelay (hit.point) : 0f;
			//Debug.LogFormat ("Hit delay : {0} + {1}", delay.ToString ("0.000"), Mathf.Max (hit.distance / muzzleVelocity, 0f).ToString ("0.000"));
			if (muzzleVelocity > 0f)
				delay += hit.distance / muzzleVelocity;
			return delay;
		}

		/// <summary>
		/// This method finds the correct delay relative to the main spatial audio listener.
		/// </summary>
		/// <param name="location">Location of the sound</param>
		/// <returns>The sound delay induced by the distance from the given location to the listener.</returns>
		public static float SoundDelay (Vector3 location) {
			return Vector3.Distance (AkSpatialAudioListener.TheSpatialAudioListener.transform.position, location) / SpeedOfSound;
		}

		public static CGVSingleShotSoundEvent PlayBulletSoundEnvironment (CGVPlayerCharacter shooter, Equipment.CGVFirearmAction weapon, RaycastHit hit) {
			AK.Wwise.Switch textureSwitch = GetTextureSwitch (hit.transform.GetComponentInParent<ICGVTextureSwitchProvider> ());
			//Debug.Log (textureSwitch);
			//Debug.DrawLine (weapon.Barrel.transform.position, hit.point, Color.red, 1f);
			float bulletDelay = BulletDelay (weapon.MuzzleVelocity, hit, shooter);
			/*
			if (!shooter.IsMainListener && JCMaths.DistanceToSegment (weapon.Barrel.transform.position, hit.point, AkSpatialAudioListener.TheSpatialAudioListener.transform.position, out float closestT) <= CGVSoundSettings.FlyingBulletsRadius) {
#if UNITY_EDITOR
				if (UnityEditor.SceneView.lastActiveSceneView != null) {
					CGVDebugUtils.DrawCircle (AkSpatialAudioListener.TheSpatialAudioListener.transform.position, UnityEditor.SceneView.lastActiveSceneView.camera.transform.forward, CGVSoundSettings.FlyingBulletsRadius, Color.red, 16, 1f);
				}
				Debug.Log ("Flying bullet at " + Vector3.Lerp (weapon.Barrel.transform.position, hit.point, closestT));
				CGVDebugUtils.DrawCube (Vector3.Lerp (weapon.Barrel.transform.position, hit.point, closestT), 0.25f, true, Color.red, 1f);
#endif
				CGVFlyingBullet.Shoot (weapon.Barrel.transform.position, hit.point, Vector3.Lerp (weapon.Barrel.transform.position, hit.point, closestT), weapon.MuzzleVelocity, SoundDelay (hit.point));
			}/*/
			if (!shooter.IsMainListener) {
#if UNITY_EDITOR
				if (UnityEditor.SceneView.lastActiveSceneView != null) {
					CGVDebugUtils.DrawCircle (AkSpatialAudioListener.TheSpatialAudioListener.transform.position, UnityEditor.SceneView.lastActiveSceneView.camera.transform.forward, CGVSoundSettings.FlyingBulletsRadius, Color.red, 16, 1f);
				}
#endif
				CGVFlyingBullet.Shoot (weapon.Barrel.transform.position, hit.point);
			}
			//*/

			return CGVSingleShotSoundEvent.PlaySingleShotAtLocation (weapon.HitEvent, hit.point, bulletDelay, textureSwitch);
		}

		public static void PlayFlyingBulletSound (CGVPlayerCharacter shooter, Equipment.CGVFirearmAction weapon) {
			if (!shooter.IsMainListener) {
#if UNITY_EDITOR
				if (UnityEditor.SceneView.lastActiveSceneView != null) {
					CGVDebugUtils.DrawCircle (AkSpatialAudioListener.TheSpatialAudioListener.transform.position, UnityEditor.SceneView.lastActiveSceneView.camera.transform.forward, CGVSoundSettings.FlyingBulletsRadius, Color.red, 16, 1f);
				}
#endif
				//CGVDebugUtils.DrawCube (weapon.Barrel.transform.position + weapon.Barrel.transform.forward * Vector3.Dot (AkSpatialAudioListener.TheSpatialAudioListener.transform.position - weapon.Barrel.transform.position, weapon.Barrel.transform.forward), 0.25f, true, Color.red, 1f);
				CGVFlyingBullet.ShootWithoutHit (weapon.Barrel.transform.position, weapon.Barrel.transform.forward);
			}
		}

		/// <summary>
		/// Retrieves the switch associated with this texture or returns the Default Wall Texture if texture is null;
		/// </summary>
		/// <param name="texture"></param>
		/// <returns></returns>
		public static AK.Wwise.Switch GetTextureSwitch (ICGVTextureSwitchProvider texture) {
			if (texture != null)
				return texture.GetTextureSwitch ();
			return DefaultTextureSwitch;
		}
	}
}
