﻿using System;
using UnityEngine;

using JuneCodes.FlexibleEnum;

#pragma warning disable 649
namespace CGV.Sound {
	/// <summary>
	/// Type of Wall that can be used in the game.
	/// </summary>
	[Serializable]
	public class CGVWallSoundMaterialValue : JCEnumValue {
		[SerializeField] [ColorUsage (false)] protected Color _Color = Color.green;
		public Color Color { get { return _Color; } }

		[Range (0f, 100f)]
		[SerializeField] private float _MinOcclusion = 20f;
		public float MinOcclusion { get { return _MinOcclusion; } }

		[Range (0f, 100f)]
		[SerializeField] private float _MaxOcclusion = 50f;
		public float MaxOcclusion { get { return _MaxOcclusion; } }

		[Min (0f)]
		[SerializeField] private float _MaxThickness = 1f;
		public float MaxThickness { get { return _MaxThickness; } }

		[SerializeField] protected AK.Wwise.Switch _TextureSwitch;
		public AK.Wwise.Switch TextureSwitch { get { return _TextureSwitch; } }
	}

	/// <summary>
	/// Container class for Wall types.
	/// Used by CGVWallSoundMaterialEnum
	/// </summary>
	[Serializable]
	public class CGVWallSoundMaterialList : JCEnumValueContainer<CGVWallSoundMaterialValue> { }

	/// <summary>
	/// Singleton asset class to access the different types of Walls available in the game.
	/// It is located at the path "Assets/Resources/Sound/WallMaterials.asset".
	/// See CGVWallSoundMaterialValue for more information about the values themselves.
	/// </summary>
	[CreateAssetMenu (fileName = "Sound/WallMaterials.asset", menuName = "CGV/Sound/Wall Materials")]
	public class CGVWallSoundMaterialEnum : JCFlexibleEnumSet<CGVWallSoundMaterialEnum, CGVWallSoundMaterialList, CGVWallSoundMaterialValue> {
	}
}
