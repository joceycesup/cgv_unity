﻿using UnityEngine;
using AK.Wwise;

#if UNITY_EDITOR
using UnityEditor;
#endif

using JuneCodes.FlexibleEnum;

#pragma warning disable 649
namespace CGV.Sound {
	/// <summary>
	/// Component that assigns a wall texture to a collider.
	/// See CGVWallSoundMaterialValue for more information about wall textures.
	/// </summary>
	[RequireComponent (typeof (Collider))]
	public class CGVWallTexture : MonoBehaviour, ICGVTextureSwitchProvider {

		[JCEnumType (typeof (CGVWallSoundMaterialEnum))]
		[SerializeField] public JCEnumKey WallType;

		public Switch GetTextureSwitch () {
			return CGVWallSoundMaterialEnum.GetValue (WallType).TextureSwitch;
		}

#if UNITY_EDITOR
		private MeshCollider mc;
		private BoxCollider bc;

		private bool initialized = false;

		private void GetCollider () {
			if (initialized)
				return;
			Collider coll = GetComponent<Collider> ();
			mc = coll as MeshCollider;
			if (mc == null) {
				bc = coll as BoxCollider;
			}
			initialized = true;
		}

		//public static bool ShowGroundTextures = true;
		//public static bool ShowInvalidOnly = false;

		private Color errorColor = Color.red;

		private void OnDrawGizmos () {
			if (Application.isPlaying)
				return;
			ShowGroundTexture ();
		}

		private void OnDrawGizmosSelected () {
			ShowGroundTexture ();
		}

		private static readonly float sizeOffset = .1f;

		private void ShowGroundTexture () {
			if (!EditorPrefs.GetBool ("ShowGroundTextures", false))
				return;

			bool textureIsInvalid = true;

			Color c = errorColor;
			CGVWallSoundMaterialValue type = CGVWallSoundMaterialEnum.GetValue (WallType, true);
			if (type != null) {
				c = type.Color;
				textureIsInvalid = false;
			}

			if (!EditorPrefs.GetBool ("ShowInvalidOnly", false) || textureIsInvalid) {
				GetCollider ();
				if (mc != null) {
					Gizmos.color = c;
					Gizmos.DrawMesh (mc.sharedMesh, transform.position, transform.rotation, transform.lossyScale);
				}
				else if (bc != null) {
					Matrix4x4 tmpM = Gizmos.matrix;
					Gizmos.color = c;
					Vector3 boxOffset = transform.lossyScale;
					boxOffset = new Vector3 (sizeOffset / boxOffset.x, sizeOffset / boxOffset.y, sizeOffset / boxOffset.z);
					Gizmos.matrix = this.transform.localToWorldMatrix;
					Gizmos.DrawCube (bc.center, bc.size + boxOffset);
					Gizmos.matrix = tmpM;
				}
			}
		}
#endif
	}
}
