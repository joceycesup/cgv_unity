﻿namespace CGV.Sound {
	/// <summary>
	/// Provides a sound switch.
	/// Intended originally for wall and ground textures but can be implemented by anything.
	/// </summary>
	public interface ICGVTextureSwitchProvider {
		AK.Wwise.Switch GetTextureSwitch ();
	}
}
