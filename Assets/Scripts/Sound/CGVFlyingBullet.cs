﻿using System.Collections;
using UnityEngine;

using JuneCodes;
using JuneCodes.Maths;

namespace CGV.Sound {
	/// <summary>
	/// Component that simulates the sound of a flying bullet around the main player.
	/// It is automatically pooled.
	/// </summary>
	public class CGVFlyingBullet : MonoBehaviour {

		private static readonly uint CallbackFlags = (uint)AkCallbackType.AK_EndOfEvent | (uint)AkCallbackType.AK_Duration;

		private static JCTypePool<CGVFlyingBullet> Pool;

		[SerializeField]
		protected AK.Wwise.Event _StartFlyingEvent;
		[SerializeField]
		protected AK.Wwise.RTPC _BulletDistanceRTPC;

		//public static CGVFlyingBullet Shoot (Vector3 startPos, Vector3 endPos, Vector3 closest, float bulletSpeed, float extraTime = 0f) {
		public static CGVFlyingBullet Shoot (Vector3 startPos, Vector3 endPos) {
			if (Pool == null)
				//Pool = CGVObjectPool<CGVSingleShotSoundEvent>.GetPool ();
				Pool = JCTypePool<CGVFlyingBullet>.GetPool ();
			if (Pool == null)
				return null;

			Vector3 playerPos = AkSpatialAudioListener.TheSpatialAudioListener.transform.position;
			if (Vector3.Distance (playerPos, startPos) <= CGVSoundSettings.FlyingBulletsRadius) {
				return null;
			}

			if (JCMaths.DistanceToSegment (startPos, endPos, playerPos, out float closestT) <= CGVSoundSettings.FlyingBulletsRadius) {
				Vector3 closestPos = Vector3.Lerp (startPos, endPos, closestT);

				CGVFlyingBullet fb = Pool.Extract ();
				if (fb == null)
					return null;

				fb.gameObject.SetActive (true);
				//fb.StartCoroutine (fb.Fly (startPos, endPos, closest, bulletSpeed, extraTime));
				fb.StartFlight (startPos, endPos, closestPos);
				return fb;
			}
			return null;
		}

		//*
		public static CGVFlyingBullet ShootWithoutHit (Vector3 startPos, Vector3 direction) {
			if (Pool == null)
				//Pool = CGVObjectPool<CGVSingleShotSoundEvent>.GetPool ();
				Pool = JCTypePool<CGVFlyingBullet>.GetPool ();
			if (Pool == null)
				return null;

			Vector3 playerPos = AkSpatialAudioListener.TheSpatialAudioListener.transform.position;
			if (Vector3.Distance (playerPos, startPos) <= CGVSoundSettings.FlyingBulletsRadius) {
				return null;
			}

			Vector3 closestPos = startPos + direction * Vector3.Dot (playerPos - startPos, direction);
			if (Vector3.Distance (playerPos, closestPos) <= CGVSoundSettings.FlyingBulletsRadius) {
				if (Vector3.Dot (startPos - playerPos, direction) >= 0f)
					return null;

				CGVFlyingBullet fb = Pool.Extract ();
				if (fb == null)
					return null;

				fb.gameObject.SetActive (true);
				//fb.StartCoroutine (fb.Fly (startPos, endPos, closest, bulletSpeed, extraTime));
				fb.StartFlight (startPos, closestPos);
				return fb;
			}
			return null;
		}

		private void StartFlight (Vector3 startPos, Vector3 closestPos) {
			_Direction = Vector3.Normalize (closestPos - startPos);

			CGVDebugUtils.DrawCube (closestPos, 0.25f, true, Color.red, 1f);

			Vector3 playerPos = AkSpatialAudioListener.TheSpatialAudioListener.transform.position;
			_BulletDistanceRTPC?.SetValue (gameObject, Vector3.Distance (closestPos, playerPos) / CGVSoundSettings.FlyingBulletsRadius);

			{
				float dist = Vector3.Distance (closestPos, startPos);
				Vector3 tmpPos = closestPos - _Direction * dist;
				if (Vector3.SqrMagnitude (closestPos - tmpPos) < Vector3.SqrMagnitude (closestPos - startPos)) {
					startPos = tmpPos;
				}
				_Distance = dist * 2f;
			}

			transform.position = startPos;
			//Debug.Log (Time.time);
			if (_StartFlyingEvent?.Post (gameObject, CallbackFlags, CallbackFunction) == 0)
				Pool.Store (this);
		}//*/

		private void StartFlight (Vector3 startPos, Vector3 endPos, Vector3 closestPos) {
			_Direction = Vector3.Normalize (endPos - startPos);

			CGVDebugUtils.DrawCube (closestPos, 0.25f, true, Color.red, 1f);

			Vector3 playerPos = AkSpatialAudioListener.TheSpatialAudioListener.transform.position;
			_BulletDistanceRTPC?.SetValue (gameObject, Vector3.Distance (closestPos, playerPos) / CGVSoundSettings.FlyingBulletsRadius);

			{
				float minDist = Mathf.Sqrt (Mathf.Min (Vector3.SqrMagnitude (closestPos - startPos), Vector3.SqrMagnitude (endPos - closestPos)));
				Vector3 tmpPos = closestPos - _Direction * minDist;
				if (Vector3.SqrMagnitude (closestPos - tmpPos) < Vector3.SqrMagnitude (closestPos - startPos)) {
					startPos = tmpPos;
				}
				minDist = Mathf.Min (minDist, CGVSoundSettings.FlyingBulletsRadius);
				_Distance = minDist * 2f;
			}

			transform.position = startPos;
			//Debug.Log (Time.time);
			if (_StartFlyingEvent?.Post (gameObject, CallbackFlags, CallbackFunction) == 0)
				Pool.Store (this);
		}

		void CallbackFunction (object in_cookie, AkCallbackType in_type, object in_info) {
			if (in_type == AkCallbackType.AK_EndOfEvent) {
				Pool.Store (this);
			}
			else { // AK_Duration : used to mark the beginning of the sound, useful when there is a delay before the sound
				AkDurationCallbackInfo durationInfo = in_info as AkDurationCallbackInfo;
				//Debug.LogFormat ("Bullet sound duration = {0} (estimated = {1})", durationInfo.fDuration / 1000f, durationInfo.fEstimatedDuration / 1000f);
				StartCoroutine (Fly (durationInfo.fDuration / 1000f));
			}
		}

		private Vector3 _Direction;
		private float _Distance;

		private IEnumerator Fly (float totalTime) {
			CGVDebugUtils.DrawCube (transform.position, 1f, true, Color.cyan, totalTime);
			Vector3 endPos = transform.position + _Direction * _Distance;
			Debug.DrawLine (transform.position, endPos, Color.cyan, totalTime);
			CGVDebugUtils.DrawCube (endPos, 1f, true, Color.cyan, totalTime);

			//Debug.LogFormat ("Bullet speed = {0}", (_Distance / totalTime));
			Vector3 deltaPos = _Direction * (_Distance / totalTime) * Time.fixedDeltaTime;
			while (true) {
				//Debug.Log (Time.time);
				//Debug.Log (Vector3.Distance (transform.position, transform.position + deltaPos));
				transform.position += deltaPos;
				CGVDebugUtils.DrawCube (transform.position, 0.25f, Color.magenta);
				yield return new WaitForFixedUpdate ();
				//yield return null;
			}
		}

		/*
		[SerializeField]
		protected AK.Wwise.Event _StopFlyingEvent;
		[SerializeField]
		protected AK.Wwise.RTPC _DopplerRTPC;

		private static float _BulletAttenuation = -1f;
		float FlightTime = -1f;
		AK.Wwise.RTPC BulletFlightTimeRTPC;

		private IEnumerator Fly (Vector3 startPos, Vector3 endPos, Vector3 closestPos, float bulletSpeed, float extraTime) {
			_StartFlyingEvent?.Post (gameObject);
			transform.position = startPos;

			Vector3 playerPos = AkSpatialAudioListener.TheSpatialAudioListener.transform.position;
			Vector3 closestToPlayer = playerPos - closestPos;
			Vector3 direction = Vector3.Normalize (endPos - startPos);
			float delay = Mathf.Min ((Vector3.Distance (startPos, endPos) / bulletSpeed) + extraTime, CGVSoundSettings.FlyingBulletsMaxTime);

			if (_BulletAttenuation < 0f) {
				yield return null;
				_BulletAttenuation = Mathf.Max (_BulletAttenuation, AkSoundEngine.GetMaxRadius (gameObject));
				Debug.LogWarning ("MaxRadius = " + _BulletAttenuation);
			}
			if (FlightTime < 0f) {
				FlightTime = BulletFlightTimeRTPC.GetGlobalValue ();
			}

			if (_BulletAttenuation > 0f) {
				Vector3 closestToAttenuation = direction * Mathf.Sqrt (_BulletAttenuation * _BulletAttenuation - Vector3.SqrMagnitude (transform.position - closestPos));
				Vector3 tmpPos = closestPos - closestToAttenuation;
				if (Vector3.SqrMagnitude (playerPos - tmpPos) < Vector3.SqrMagnitude (playerPos - startPos)) {
					startPos = tmpPos;
				}
				tmpPos = closestPos + closestToAttenuation;
				if (Vector3.SqrMagnitude (playerPos - tmpPos) < Vector3.SqrMagnitude (playerPos - endPos)) {
					endPos = tmpPos;
				}
			}

			float speed = 400f;
			{
				float dopplerStartValue = -1f;
				float startToClosestSqrDist = Vector3.SqrMagnitude (closestPos - startPos);
				float endToClosestSqrDist = Vector3.SqrMagnitude (closestPos - endPos);
				if (endToClosestSqrDist > startToClosestSqrDist) {
					dopplerStartValue = -startToClosestSqrDist / endToClosestSqrDist;
				}
				_DopplerRTPC?.SetValue (gameObject, dopplerStartValue);
				speed = Vector3.Distance (startPos, endPos) / FlightTime;
			}
			_DopplerRTPC?.SetValue (gameObject, 1f);

			float currentBulletSqrDistToEnd = 1f;
			while (true) {
				transform.position += direction * speed * Time.fixedDeltaTime;
				if (currentBulletSqrDistToEnd < (currentBulletSqrDistToEnd = Vector3.SqrMagnitude (endPos - transform.position))) {
					break;
				}
				yield return new WaitForFixedUpdate ();
			}
			_StopFlyingEvent?.Post (gameObject);
			Pool.Store (this);
		}

		private IEnumerator Fly2 (Vector3 startPos, Vector3 endPos, Vector3 closest, float bulletSpeed, float extraTime) {
			_DopplerRTPC?.SetValue (gameObject, -1f);
			_StartFlyingEvent?.Post (gameObject);
			Vector3 playerPos = AkSpatialAudioListener.TheSpatialAudioListener.transform.position;
			Vector3 closestToPlayer = playerPos - closest;
			Vector3 direction = Vector3.Normalize (endPos - startPos);
			float delay = Mathf.Min ((Vector3.Distance (startPos, endPos) / bulletSpeed) + extraTime, CGVSoundSettings.FlyingBulletsMaxTime);

			yield return null;
			if (_BulletAttenuation < 0f) {
				_BulletAttenuation = Mathf.Max (_BulletAttenuation, AkSoundEngine.GetMaxRadius (gameObject));
				Debug.LogWarning ("MaxRadius = " + _BulletAttenuation);
			}
			if (_BulletAttenuation > 0f) {
				float d = Mathf.Sqrt (_BulletAttenuation * _BulletAttenuation - closestToPlayer.sqrMagnitude);
				Vector3 tmpPos = closest - direction * d;
				if (Vector3.SqrMagnitude (playerPos - tmpPos) < Vector3.SqrMagnitude (playerPos - startPos))
					startPos = tmpPos;
				tmpPos = closest + direction * d;
				if (Vector3.SqrMagnitude (playerPos - tmpPos) < Vector3.SqrMagnitude (playerPos - endPos))
					endPos = tmpPos;
			}
			bulletSpeed = Vector3.Distance (startPos, endPos) / delay;

			transform.position = startPos;

			float closestSqrMagnitudeToPlayer = closestToPlayer.sqrMagnitude;
			float maxSqrMagnitudeToPlayer = Mathf.Max (Vector3.SqrMagnitude (playerPos - startPos), Vector3.SqrMagnitude (playerPos - endPos)) - closestSqrMagnitudeToPlayer;
			closestToPlayer.Normalize ();
			float totalSqrMagnitude = Vector3.SqrMagnitude (endPos - startPos);
			float currentSqrMagnitude = totalSqrMagnitude;
			float dopplerValue = float.MaxValue;
			bool dopplerPositive = false;

			Debug.LogFormat ("Start flight at ({0}) for {1}s", transform.position, delay);
			CGVDebugUtils.DrawCube (startPos, 1f, true, Color.cyan, delay);
			Debug.DrawLine (startPos, endPos, Color.cyan, delay);
			CGVDebugUtils.DrawCube (endPos, 1f, true, Color.cyan, delay);

			while (true) {
				transform.position += direction * bulletSpeed * Time.deltaTime;
				//Debug.LogFormat ("{0} : {1}", currentSqrMagnitude, currentSqrMagnitude = Vector3.SqrMagnitude (endPos - transform.position));
				if (currentSqrMagnitude < (currentSqrMagnitude = Vector3.SqrMagnitude (endPos - transform.position))) {
					break;
				}
				CGVDebugUtils.DrawCube (transform.position, 0.25f, Color.magenta);

				Vector3 vectorToPlayer = AkSpatialAudioListener.TheSpatialAudioListener.transform.position - transform.position;
				float tmpDoppler = (vectorToPlayer.sqrMagnitude - closestSqrMagnitudeToPlayer) / maxSqrMagnitudeToPlayer;
				if (!dopplerPositive && tmpDoppler > dopplerValue) {
					dopplerPositive = true;
				}
				tmpDoppler = Vector3.SqrMagnitude (transform.position - closest) / Mathf.Max (Vector3.SqrMagnitude (endPos - closest), Vector3.SqrMagnitude (startPos - closest));
				dopplerValue = Mathf.Clamp01 (tmpDoppler);
				Debug.LogFormat ("{0} : Doppler {1}", gameObject.name, (dopplerPositive ? dopplerValue : -dopplerValue));
				_DopplerRTPC?.SetValue (gameObject, dopplerPositive ? dopplerValue : -dopplerValue);
				yield return null;
			}
			//Debug.Log ("End flight " + transform.position);
			_StopFlyingEvent?.Post (gameObject);
			Pool.Store (this);
		}//*/
	}
}
