﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
#endif

namespace CGV.Sound {
	/// <summary>
	/// This component automatically sets the collider on this object to encapsulate the entire level.
	/// This prevents issues on the Reverb Volume of the level.
	/// </summary>
	[RequireComponent (typeof (AkRoom))]
	[RequireComponent (typeof (BoxCollider))]
	public class CGVSoundRoomResizer : MonoBehaviour {

#if UNITY_EDITOR
		[PostProcessScene (0)]
		private static void SetAllRoomsSize () {
			CGVSoundRoomResizer[] editorOnlyCleanings = GetScripts<CGVSoundRoomResizer> ();
			for (int i = 0; i < editorOnlyCleanings.Length; i++) {
				editorOnlyCleanings[i].SetRoomSize ();
			}
		}

		[NaughtyAttributes.Button]
		private void SetRoomSize () {
			BoxCollider collider = GetComponent<BoxCollider> ();
			Matrix4x4 matrix = transform.worldToLocalMatrix;

			Bounds bounds = new Bounds ();
			foreach (var surface in FindObjectsOfType<NavMeshSurface> ()) {
				if (surface.navMeshData == null)
					continue;
				Bounds navMeshBounds = surface.navMeshData.sourceBounds;
				//Debug.LogFormat ("Surface (center = {0}, size = {1})", navMeshBounds.center, navMeshBounds.size);
				//bounds.Encapsulate (new Bounds (surface.center, surface.size));
				bounds.Encapsulate (navMeshBounds);
				bounds.Encapsulate (new Vector3 (0f, navMeshBounds.center.y + navMeshBounds.size.y / 2f + 5f, 0f));
			}
			if (bounds.size.sqrMagnitude < Vector3.one.sqrMagnitude)
				return;
			SerializedObject colliderSO = new SerializedObject (collider);
			colliderSO.FindProperty ("m_Center").vector3Value = matrix.MultiplyPoint (bounds.center);
			colliderSO.FindProperty ("m_Size").vector3Value = matrix.MultiplyVector (bounds.size);
			colliderSO.ApplyModifiedProperties ();
			//collider.center = matrix.MultiplyPoint (bounds.center);
			//collider.size = matrix.MultiplyVector (bounds.size);
		}

		private static T[] GetScripts<T> () {
			object[] obj = UnityEngine.Object.FindObjectsOfType (typeof (T));
			T[] objType = new T[obj.Length];


			if (obj != null) {
				for (int i = 0; i < obj.Length; i++) {
					objType[i] = (T)obj[i];
				}
			}

			return (objType);
		}
#endif
	}
}
