﻿using UnityEngine;
using System.Collections;

namespace CGV.Sound {
	[RequireComponent (typeof (AkSpatialAudioEmitter))]
	public class CGVOccludedSoundObject : MonoBehaviour {

		private static Vector3 ObjectPoint;
		private static Vector3 PlayerPoint;
		private static Vector3 TargetDirection;
		private static float DistToPlayer;

		protected static RaycastHit[] HitsObject = new RaycastHit[3];
		protected static RaycastHit[] HitsPlayer = new RaycastHit[3];

		[SerializeField]
		protected AK.Wwise.RTPC OcclusionRTPC;

		float LastOcclusion;

		protected Collider[] Walls = new Collider[2];
		protected CGVWallTexture[] Textures = new CGVWallTexture[2];
		protected CGVWallSoundMaterialValue[] SoundMaterials = new CGVWallSoundMaterialValue[2];

		void Update () {
			ObjectPoint = transform.position;
			if (AkSpatialAudioListener.TheSpatialAudioListener == null)
				return;
			PlayerPoint = AkSpatialAudioListener.TheSpatialAudioListener.transform.position;
			//PlayerPoint = PlayerPoint;

			TargetDirection = PlayerPoint - ObjectPoint;

			DistToPlayer = Vector3.Distance (PlayerPoint, ObjectPoint);

			if (DistToPlayer <= AkSoundEngine.GetMaxRadius (gameObject)) {
				//*
				int hitMaxO = Physics.RaycastNonAlloc (ObjectPoint, TargetDirection, HitsObject, DistToPlayer, 1 << 10) - 1;
				if (hitMaxO < 0) {
					//Debug.Log ("No hit");
					SetOcclusion (0f);
#if CGV_DEBUG
					Debug.DrawRay (ObjectPoint, TargetDirection, Color.blue);
#endif
				}
				else if (hitMaxO < 2) {
					int hitMaxP = Physics.RaycastNonAlloc (PlayerPoint, -TargetDirection, HitsPlayer, DistToPlayer, 1 << 10) - 1;
					if (hitMaxO != hitMaxP) {
						SetOcclusion (100f);
#if CGV_DEBUG
						Debug.DrawRay (ObjectPoint, TargetDirection, Color.red);
#endif
					}
					else {
						RefreshTextures (0);
						//Debug.Log (HitsObject[0].collider == HitsPlayer[hitMaxO].collider);

#if CGV_DEBUG
						Debug.DrawLine (ObjectPoint, HitsObject[0].point, Color.cyan);
						Debug.DrawLine (PlayerPoint, HitsPlayer[0].point, Color.cyan);
#endif

						float occlusion = Mathf.Lerp (
							SoundMaterials[0].MinOcclusion,
							SoundMaterials[0].MaxOcclusion,
							Vector3.Distance (HitsObject[0].point, HitsPlayer[hitMaxO].point) / SoundMaterials[0].MaxThickness);

						if (hitMaxO == 1) {
							RefreshTextures (1);
							occlusion += Mathf.Lerp (
								SoundMaterials[1].MinOcclusion,
								SoundMaterials[1].MaxOcclusion,
								Vector3.Distance (HitsObject[hitMaxO].point, HitsPlayer[0].point) / SoundMaterials[1].MaxThickness);
#if CGV_DEBUG
							Debug.DrawLine (HitsPlayer[hitMaxO].point, HitsObject[hitMaxO].point, Color.cyan);
#endif
							//Debug.Log (HitsObject[hitMaxO].collider == HitsPlayer[0].collider);
						}
						SetOcclusion (occlusion);
						//Debug.LogFormat ("Hits : {0}/{1}", hitMaxO, hitMaxP);
					}
				}
				else {
					//Debug.Log ("Too many hits");
					SetOcclusion (100f);
#if CGV_DEBUG
					Debug.DrawRay (ObjectPoint, TargetDirection, Color.red);
#endif
				}
			}
		}

		private void RefreshTextures (int index) {
			if (HitsObject[index].collider != Walls[index]) {
				Walls[index] = HitsObject[index].collider;
				Textures[index] = HitsObject[index].collider.GetComponent<CGVWallTexture> ();
				SoundMaterials[index] = CGVWallSoundMaterialEnum.GetValue (Textures[index]?.WallType);
				//Debug.Log ("Refreshing textures");
			}
		}

		private void SetOcclusion (float value) {
			if (Mathf.Approximately (LastOcclusion, value))
				return;
			LastOcclusion = value;
			//Debug.LogFormat ("Set Occlusion : {0}", value);
			OcclusionRTPC.SetValue (gameObject, value);
			//AkSoundEngine.SetObjectObstructionAndOcclusion (this.gameObject, AkSpatialAudioListener.TheSpatialAudioListener.gameObject, 0f, value);
		}
	}
}
