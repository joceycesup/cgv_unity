﻿using UnityEngine;

using JuneCodes;

#pragma warning disable 649
namespace CGV.Sound {
	[CreateAssetMenu (fileName = "Sound/SoundSettings.asset", menuName = "CGV/Sound/Settings")]
	public class CGVSoundSettings : JCSingletonScriptableObject<CGVSoundSettings> {
		[SerializeField] protected float _FlyingBulletsRadius = 5f;
		public static float FlyingBulletsRadius { get { return instance._FlyingBulletsRadius; } }
		[SerializeField] protected float _FlyingBulletsMaxTime = 0.2f;
		public static float FlyingBulletsMaxTime { get { return instance._FlyingBulletsMaxTime; } }

	}
}
