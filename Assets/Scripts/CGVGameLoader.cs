﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

using Photon.Pun;
using Photon.Realtime;
using System.Globalization;

namespace CGV {
	/// <summary>
	/// This behaviour loads the game and checks the arguments passed with the executable.
	/// </summary>
	public class CGVGameLoader : MonoBehaviour
#if CGV_NETWORK
		, IConnectionCallbacks
		, IMatchmakingCallbacks
		, ILobbyCallbacks
#endif // CGV_NETWORK
		{

		[SerializeField] private CGVGameSettings _Settings;
		//[SerializeField] private CGVSceneSet _SceneSet;

		private string _MapToLoad = string.Empty;
		private CGVPlayableScene _SceneToLoad;

		private string _Hostname = string.Empty;

		private bool _ListenToNetworkCallbacks = true;

#if UNITY_EDITOR
		[SerializeField]
		private bool _UseEditorArguments = true;
		[SerializeField]
		[NaughtyAttributes.ShowIf ("_UseEditorArguments")]
		[NaughtyAttributes.ReorderableList]
		private List<EditorArgument> _EditorArguments = new List<EditorArgument> (0);

		[Serializable]
		private struct EditorArgument {
			public bool UseArgument;
			public string Argument;
		}

		[CustomPropertyDrawer (typeof (EditorArgument))]
		public class EditorArgumentPropertyDrawer : PropertyDrawer {

			public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
				EditorGUI.BeginProperty (position, GUIContent.none, property);
				EditorGUI.BeginChangeCheck ();

				SerializedProperty useProperty = property.FindPropertyRelative ("UseArgument");
				useProperty.boolValue = EditorGUI.ToggleLeft (new Rect (position.x, position.position.y, 20f, position.height), "", useProperty.boolValue);

				if (!useProperty.boolValue)
					GUI.color = new Color (1f, 1f, 1f, 0.6f);
				//EditorGUI.BeginDisabledGroup (!useProperty.boolValue);
				position.width -= 20f;
				position.x += 20f;
				SerializedProperty argProperty = property.FindPropertyRelative ("Argument");
				argProperty.stringValue = EditorGUI.TextField (position, argProperty.stringValue);
				//EditorGUI.EndDisabledGroup ();
				GUI.color = Color.white;

				if (EditorGUI.EndChangeCheck ()) {
					property.serializedObject.ApplyModifiedProperties ();
				}

				EditorGUI.EndProperty ();
			}

			public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
				return EditorGUIUtility.singleLineHeight;
			}
		}
#endif

		private void Awake () {
			LoadSettings ();
		}

		void OnEnable () {
			PhotonNetwork.AddCallbackTarget (this);
		}

		void OnDisable () {
			PhotonNetwork.RemoveCallbackTarget (this);
		}

		void Start () {
			StartCoroutine (StartGameWithDelay (0.5f));
		}

		private IEnumerator StartGameWithDelay (float delay) {
			yield return new WaitForSecondsRealtime (delay);
			StartGame ();
		}

		void LoadSettings () {
			_Settings = ScriptableObject.Instantiate<CGVGameSettings> (_Settings);
			string[] args = System.Environment.GetCommandLineArgs ();
#if UNITY_EDITOR
			//*
			if (_UseEditorArguments && _EditorArguments.Count > 0) {
				List<string> list = new List<string> (args.Length + _EditorArguments.Count * 2);
				list.AddRange (args);

				for (int i = 0; i < _EditorArguments.Count; ++i) {
					if (!_EditorArguments[i].UseArgument)
						continue;

					string[] tokens = _EditorArguments[i].Argument.Split (' ');
					for (int t = 0; t < tokens.Length; ++t) {
						list.Add (tokens[t]);
					}
				}
				/*/
			if (_UseEditorArguments && _EditorArguments.Count > 0) {
				List<string> list = new List<string> (args.Length + _EditorArguments.Count * 2);
				for (int i = _EditorArguments.Count - 1; i >= 0; --i) {
					string[] tokens = _EditorArguments[i].Split (' ');
					if (tokens.Length > 1) {
						_EditorArguments[i] = tokens[0];
						for (int t = tokens.Length - 1; t > 0; --t) {
							_EditorArguments.Insert (i + 1, tokens[t]);
						}
					}
				}
				foreach (var arg in _EditorArguments)
					Debug.Log (arg);
				list.AddRange (args);
				//*/
				//list.AddRange (_EditorArguments);
				args = list.ToArray ();
			}
#endif
			for (int i = 1; i < args.Length; i++) {
				//Debug.Log ("ARG " + i + ": " + args[i]);
				switch (args[i].ToLower (System.Globalization.CultureInfo.InvariantCulture)) {
					case "-map": {
							if (CanGetNextParam (args, i))
								_MapToLoad = args[++i];
							break;
						}
					case "-hostname": {
							if (CanGetNextParam (args, i))
								_Hostname = args[++i];
							break;
						}
					case "-duration": {
							if (CanGetNextParam (args, i))
								if (TryParseF (args[++i], out float duration))
									_Settings.GameDuration = Mathf.Max (1f, duration);
							break;
						}
					case "-team": {
							if (CanGetNextParam (args, i)) {
								string teamParam = args[++i];
								if (int.TryParse (teamParam, out int teamNumber)) {
									_Settings.TeamSelection = CGVGameSettings.TeamSelectionMode.Enforced;
									_Settings.EnforceTeamIndex = teamNumber;
								}
								else {
									if (teamParam.StartsWith ("rand", StringComparison.InvariantCultureIgnoreCase))
										_Settings.TeamSelection = CGVGameSettings.TeamSelectionMode.Random;
									else
										_Settings.TeamSelection = CGVGameSettings.TeamSelectionMode.Free;
								}
							}
							break;
						}
					case "-gamemode":
					case "-mode": {
							if (CanGetNextParam (args, i)) {
								string mode = args[++i];
								if (mode.Equals ("tdm", StringComparison.InvariantCultureIgnoreCase))
									_Settings.Mode = CGVGameSettings.GameMode.TeamDeathmatch;
								else if (mode.Equals ("dm", StringComparison.InvariantCultureIgnoreCase))
									_Settings.Mode = CGVGameSettings.GameMode.Deathmatch;
							}
							break;
						}
					case "-multi": {
							if (CanGetNextParam (args, i)) {
								string mode = args[++i];
								if (mode.Equals ("online", StringComparison.InvariantCultureIgnoreCase))
									_Settings.Multiplayer = CGVGameSettings.MultiplayerMode.Online;
								else if (mode.Equals ("local", StringComparison.InvariantCultureIgnoreCase))
									_Settings.Multiplayer = CGVGameSettings.MultiplayerMode.Local;
							}
							if (CanGetNextParam (args, i))
								if (int.TryParse (args[++i], out int maxPlayerCount))
									_Settings.MaxPlayerCount = Mathf.Max (1, maxPlayerCount);
							break;
						}
					case "-killthreshold": {
							if (CanGetNextParam (args, i))
								if (int.TryParse (args[++i], out int killThreshold))
									_Settings.KillThreshold = Mathf.Max (1, killThreshold);
							break;
						}
					case "-invertyaxis": {
							_Settings.InvertViewYAxis = true;
							break;
						}
					case "-mainplayeryieldscontroller": {
							Debug.Log ("Main player yields controller");
							_Settings.MainPlayerUseController = false;
							break;
						}
					case "-class": {
							if (CanGetNextParam (args, i))
								_Settings.PlayableCharacters.SetMainPlayerCharacter (args[++i]);
							break;
						}
					case "-hidemainplayerdisplay": {
							Debug.Log ("Hide main player display");
							_Settings.ShowMainPlayerDisplay = false;
							break;
						}
					case "-displaycount": {
							if (CanGetNextParam (args, i))
								if (int.TryParse (args[++i], out int displayCount))
									_Settings.MaxDisplayCount = Mathf.Max (1, displayCount);
							break;
						}
				}
			}
		}

		void StartGame () {
#if CGV_NETWORK
			if (!string.IsNullOrEmpty (_Hostname)) {
				// query rooms
				// check if present
				// if not, create with parameters
				CGVNetworkManager.Connect ();
			}
			else
#endif // CGV_NETWORK
			{
				_ListenToNetworkCallbacks = false;
				_Settings.SceneSet.SetAsInstance ();
				if (!string.IsNullOrEmpty (_MapToLoad)) {
					//_Settings.Initialize (_MultiplayerMode == MultiplayerMode.Online, );
					if (_Settings.Multiplayer == CGVGameSettings.MultiplayerMode.None)
						_Settings.Multiplayer = CGVGameSettings.MultiplayerMode.Local;

					if (!_Settings.SceneSet.LoadPlayableSceneOrDefault (_MapToLoad, true)) {
						CGVSceneSet.LoadMenu ();
					}
				}
				else {
					CGVSceneSet.LoadMenu ();
				}
			}
		}

		private static bool TryParseF (string s, out float result) {
			return float.TryParse (s, NumberStyles.Any, CultureInfo.InvariantCulture, out result);
		}

		private static bool CanGetNextParam (string[] args, int index) {
			return index < args.Length - 1 && !args[index + 1].StartsWith ("-");
		}

#if CGV_NETWORK

		private void CreateRoom () {
			if (_SceneToLoad == null) {
				if (string.IsNullOrEmpty (_MapToLoad))
					Debug.LogWarning ("GameLoader -> You need to specify a map with -map in order to create a room!");
				else
					Debug.LogWarning ("GameLoader -> Couldn't find a map that matches the specified name!");
				CGVGameSettings.instance.SceneSet.LoadMenu ();
				return;
			}
			if (!CGVNetworkManager.CreateRoom (_Hostname, _SceneToLoad))
				CGVGameSettings.instance.SceneSet.LoadMenu ();
		}

		#region IConnectionCallbacks
		public void OnConnected () {
		}

		public void OnConnectedToMaster () {
			if (!_ListenToNetworkCallbacks)
				return;
			_SceneToLoad = _Settings.SceneSet.GetPlayableScene (_MapToLoad);
			//Debug.Log ("Connected to Master, trying to create or join room.");
			//if (CGVNetworkManager.PeekRoomList ())
			//Debug.Log ("Connected to Master, peeking room list.");
			if (CGVNetworkManager.JoinLobby ())
				Debug.Log ("GameLoader -> Connected to Master, joining lobby to get room list.");
			else {
				Debug.Log ("GameLoader -> Connected to Master, could not join lobby.");
				//Debug.Log ("Connected to Master, could not peek room list.");
				CreateRoom ();
			}
			//CGVNetworkManager.JoinOrCreateRoom (_Hostname, _SceneToLoad);
		}

		public void OnDisconnected (DisconnectCause cause) {
		}

		public void OnRegionListReceived (RegionHandler regionHandler) {
		}

		public void OnCustomAuthenticationResponse (Dictionary<string, object> data) {
		}

		public void OnCustomAuthenticationFailed (string debugMessage) {
		}
		#endregion IConnectionCallbacks

		#region ILobbyCallbacks
		public void OnJoinedLobby () {
		}

		public void OnLeftLobby () {
		}

		public void OnRoomListUpdate (List<RoomInfo> roomList) {
			bool roomExists = false;
			foreach (var info in roomList) {
				if (info.Name.Equals (_Hostname)) {
					roomExists = true;
					break;
				}
			}
			if (roomExists) {
				Debug.Log ("GameLoader -> Room list updated, trying to join room.");
				if (!CGVNetworkManager.JoinRoom (_Hostname))
					CGVGameSettings.instance.SceneSet.LoadMenu ();
			}
			else {
				Debug.Log ("GameLoader -> Room list updated, trying to create room.");
				CreateRoom ();
			}
		}

		public void OnLobbyStatisticsUpdate (List<TypedLobbyInfo> lobbyStatistics) {
		}
		#endregion ILobbyCallbacks

		#region IMatchmakingCallbacks
		public void OnFriendListUpdate (List<FriendInfo> friendList) {
			throw new System.NotImplementedException ();
		}

		public void OnCreatedRoom () {
			if (!_ListenToNetworkCallbacks)
				return;
			Debug.LogFormat ("GameLoader -> Created Room {0}, loading map {1}", _Hostname, _MapToLoad);
			_SceneToLoad.Load (true);
		}

		public void OnCreateRoomFailed (short returnCode, string message) {
			if (!_ListenToNetworkCallbacks)
				return;
			if (returnCode == ErrorCode.GameIdAlreadyExists) {
				if (!CGVNetworkManager.JoinRoom (_Hostname))
					CGVGameSettings.instance.SceneSet.LoadMenu ();
			}
			else
				Debug.Log ("GameLoader -> Could not create Room " + _Hostname);
		}

		public void OnJoinedRoom () {
			if (!_ListenToNetworkCallbacks)
				return;
			Debug.Log ("GameLoader -> Joined Room " + _Hostname);

			string roomMapName = CGVNetworkManager.GetCurrentRoomMapName ();
			CGVPlayableScene scene = CGVGameSettings.instance.SceneSet.GetPlayableScene (roomMapName);

			if (scene == null) {
				Debug.LogWarning ("GameLoader -> The room we joined doesn't have a map!");
				PhotonNetwork.LeaveRoom ();
				CGVGameSettings.instance.SceneSet.LoadMenu ();
				return;
			}
			//PhotonNetwork.IsMessageQueueRunning = false;
			_SceneToLoad = scene;
			_SceneToLoad.Load (true, () => {
				//PhotonNetwork.IsMessageQueueRunning = true;
			});
		}

		public void OnJoinRoomFailed (short returnCode, string message) {
			if (!_ListenToNetworkCallbacks)
				return;
			Debug.Log ("GameLoader -> Could not join Room " + _Hostname);
		}

		public void OnJoinRandomFailed (short returnCode, string message) {
			throw new System.NotImplementedException ();
		}

		public void OnLeftRoom () {
			Debug.Log ("GameLoader -> Left Room");
		}
		#endregion IMatchmakingCallbacks
#endif // CGV_NETWORK
	}
}
