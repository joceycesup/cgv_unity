﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using JuneCodes.Editor;

namespace CGV {
	/// <summary>
	/// Custom inspector for the class CGVSceneSet.
	/// </summary>
	[CustomEditor (typeof (CGVSceneSet))]
	public class CGVSceneSetEditor : UnityEditor.Editor {

		CGVSceneSet _SceneSet;

		private ReorderableList _PlayableScenesList;

		SerializedProperty _PlayableScenesProperty;

		private void OnEnable () {
			_SceneSet = (CGVSceneSet)target;
			_PlayableScenesProperty = serializedObject.FindProperty ("_PlayableScenes");

			_PlayableScenesList = new ReorderableList (serializedObject, _PlayableScenesProperty, true, false, true, true) {
				drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
					serializedObject.Update ();
					rect.height -= 2f;
					GUI.Box (EditorGUI.IndentedRect (rect), GUIContent.none, EditorStyles.helpBox);
					rect = EditorStyles.helpBox.padding.Remove (rect);

					rect.height = EditorGUIUtility.singleLineHeight;
					SerializedProperty playableSceneProperty = _PlayableScenesProperty.GetArrayElementAtIndex (index);
					CGVPlayableScene value = playableSceneProperty.GetValue<CGVPlayableScene> ();

					EditorGUI.ObjectField (rect, playableSceneProperty, new GUIContent (value == null ? "Asset" : value.Name));
					serializedObject.ApplyModifiedProperties ();

					rect.y += rect.height + 2f;
					if (value != null) {
						SceneAsset scene = AssetDatabase.LoadAssetAtPath<SceneAsset> (value.Scene.ScenePath);
						SceneReferencePropertyDrawer.BuildUtils.BuildScene buildScene = SceneReferencePropertyDrawer.BuildUtils.GetBuildScene (scene);

						int sceneControlID = GUIUtility.GetControlID (FocusType.Passive);
						SceneReferencePropertyDrawer.DrawSceneInfoGUI (rect, buildScene, sceneControlID);
					}
					else {
						EditorGUI.HelpBox (rect, "You need to set the Playable Scene Asset", MessageType.Error);
					}
				},
				onRemoveCallback = (ReorderableList list) => {
					SerializedProperty element = _PlayableScenesProperty.GetArrayElementAtIndex (list.index);
					_PlayableScenesProperty.DeleteArrayElementAtIndex (list.index);
					list.serializedProperty.serializedObject.ApplyModifiedProperties ();
				},
				onAddCallback = (ReorderableList list) => {
					int index = list.index;
					if (index >= 0 && index < list.count) {
						index++;
						list.index = index;
					}
					else {
						index = list.count;
					}

					list.serializedProperty.InsertArrayElementAtIndex (index);

					list.serializedProperty.serializedObject.ApplyModifiedProperties ();
				},
				elementHeightCallback = (int index) => {
					return EditorGUIUtility.singleLineHeight * 2f + 4f + EditorStyles.helpBox.padding.vertical;
				},
				drawHeaderCallback = (Rect rect) => {
					GUI.Label (rect, "Playable Scenes");
				},
			};
		}

		public override void OnInspectorGUI () {
			DrawDefaultInspector ();
			_PlayableScenesList.DoLayoutList ();
		}
	}
}
