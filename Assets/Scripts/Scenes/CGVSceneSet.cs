﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;

using JuneCodes;

#pragma warning disable 649
namespace CGV {
	/// <summary>
	/// Asset class that defines which scenes are available in the game.
	/// It can be created using the contextual menu "Create/CGV/Scenes/Scene Set".
	/// See CGVScene and CGVPlayableScene for more information about scenes.
	/// </summary>
	[CreateAssetMenu (fileName = "SceneSet", menuName = "CGV/Scenes/Scene Set")]
	public class CGVSceneSet : JCSingletonScriptableObject<CGVSceneSet>, IEnumerable<CGVPlayableScene> {

		protected override bool KeepOldest => false;

		private static System.Random _Random = new System.Random ();

		[SerializeField]
		private SceneReference _Menu;
		private CGVScene _MenuScene;
		public CGVScene MenuScene {
			get {
				if (_MenuScene == null && !string.IsNullOrEmpty (_Menu))
					_MenuScene = CGVScene.CreateSceneReference (_Menu, "Menu");
				return _MenuScene;
			}
		}

		[SerializeField]
		private SceneReference _End;
		private CGVScene _EndScene;
		public CGVScene EndScene {
			get {
				if (_EndScene == null && !string.IsNullOrEmpty (_End))
					_EndScene = CGVScene.CreateSceneReference (_End, "End");
				return _EndScene;
			}
		}

		[SerializeField]
		[HideInInspector]
		private List<CGVPlayableScene> _PlayableScenes;

		[SerializeField]
		[NaughtyAttributes.SpecialNegativeValue ("No maximum", false)]
		private int _MaxSceneCount = -1;

		[SerializeField]
		private bool _RandomizeScenes;
		[SerializeField]
		[NaughtyAttributes.HideIfValue (NaughtyAttributes.ComparisonOperator.LessThanOrEqualTo, 0, "_MaxSceneCount")]
		private bool _RepeatScenes = true;

		private int CurrentPlayableSceneIndex = -1;
		private bool WentThroughScenes = false;

		public void Awake () {
			CurrentPlayableSceneIndex = -1;
			WentThroughScenes = false;
		}

		public static void LoadMenu () {
			if (instance.MenuScene == null)
				instance.LoadNextScene ();
			else
				instance.MenuScene.Load (true);
		}

		public static void LoadEnd () {
			if (instance.EndScene == null)
				LoadMenu ();
			else
				instance.EndScene.Load (true);
		}

		public bool LoadPlayableScene (string sceneName, bool async = false) {
			return LoadPlayableScene (sceneName, false, async);
		}

		public bool LoadPlayableSceneOrDefault (string sceneName, bool async = false) {
			return LoadPlayableScene (sceneName, true, async);
		}

		public CGVPlayableScene GetPlayableScene (string sceneName) {
			if (string.IsNullOrEmpty (sceneName))
				return null;

			CGVPlayableScene scene = null;

			foreach (CGVPlayableScene playableScene in _PlayableScenes) {
				if (string.Equals (sceneName, playableScene.Name, System.StringComparison.InvariantCultureIgnoreCase)) {
					scene = playableScene;
					break;
				}
			}
			return scene;
		}

		private bool LoadPlayableScene (string sceneName, bool useDefault = true, bool async = false) {
			if (string.IsNullOrEmpty (sceneName))
				return false;

			CGVPlayableScene scene = GetPlayableScene (sceneName);

			if (scene == null) {
				if (useDefault && _PlayableScenes.Count > 0) {
					scene = _PlayableScenes[0];
				}
			}

			if (scene == null)
				return false;
			scene.Load (async);
			return true;
		}

		public bool LoadNextScene () {
			if (WentThroughScenes)
				return false;
			if (CurrentPlayableSceneIndex < 0 && _RandomizeScenes) {
				for (int i = 0; i < _PlayableScenes.Count - 1; i++) {
					int j = _Random.Next (i, _PlayableScenes.Count);
					CGVPlayableScene temp = _PlayableScenes[i];
					_PlayableScenes[i] = _PlayableScenes[j];
					_PlayableScenes[j] = temp;
				}
			}
			CurrentPlayableSceneIndex++;
			if (CurrentPlayableSceneIndex >= (_MaxSceneCount <= 0 ? _PlayableScenes.Count : Mathf.Min (_MaxSceneCount, _PlayableScenes.Count))) {
				WentThroughScenes = true;
				return false;
			}
			_PlayableScenes[CurrentPlayableSceneIndex].Load (true);
			return true;
		}

		private CGVPlayableScene PickRandomScene (int currentIndex = -1) {
			if (_PlayableScenes.Count <= 0)
				return null;
			int index = _Random.Next (0, currentIndex < 0 ? _PlayableScenes.Count : _PlayableScenes.Count - 1);
			if (currentIndex >= 0 && index >= currentIndex) {
				index++;
			}
			return _PlayableScenes[index];
		}

		public IEnumerator<CGVPlayableScene> GetEnumerator () {
			return _PlayableScenes.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator () {
			return _PlayableScenes.GetEnumerator ();
		}
	}
}
