﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using TMPro;

#pragma warning disable 649
namespace CGV.UI {
	/// <summary>
	/// Component that handles the display of a playable scene in a menu.
	/// It makes sure accessibility components are updated when displayed.
	/// </summary>
	[RequireComponent (typeof (Button))]
	public class CGVPlayableSceneViewer : MonoBehaviour {

		[SerializeField]
		private Image _SceneThumbnail;
		[SerializeField]
		private TextMeshProUGUI _SceneName;
		[SerializeField]
		private AccessibleButton _SceneNameUAP;

		[SerializeField]
		private CGVPlayableScene _PlayableScene;
		public CGVPlayableScene PlayableScene {
			get { return _PlayableScene; }
			set {
				if (value == _PlayableScene)
					return;
				_PlayableScene = value;
				UpdateView ();
			}
		}

		private void UpdateView () {
			if (_PlayableScene != null) {
				_SceneThumbnail.sprite = _PlayableScene.Thumbnail;
				_SceneName.SetText (_PlayableScene.Name);
				_SceneNameUAP.m_Text = _PlayableScene.Name;
			}
		}

		private void Awake () {
			UpdateView ();
		}

		public void PlayScene () {
			PlayableScene.Load (true);
		}
	}
}
