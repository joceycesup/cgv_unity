﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace CGV {
	/// <summary>
	/// Scene wrapper class that simplifies scene loading and sends callbacks when scenes are loaded.
	/// </summary>
	public class CGVScene : ScriptableObject {

		public delegate void SceneChangeEvent (CGVScene scene);
		public static SceneChangeEvent OnSceneWillChange;
		public static SceneChangeEvent OnSceneChanged;

		[SerializeField]
		private string _Name;
		public string Name { get { return _Name; } }

		[SerializeField]
		private SceneReference _Scene;
		public SceneReference Scene { get { return _Scene; } }

		public virtual bool IsPlayable => false;

		public static CGVScene LastLoadedScene { get; protected set; }
		public static bool IsFirstLoadedScene { get { return LastLoadedScene == null; } }

		public static CGVScene CreateSceneReference (SceneReference scene, string name) {
			CGVScene reference = ScriptableObject.CreateInstance<CGVScene> ();
			reference._Scene = scene;
			reference._Name = name;
			return reference;
		}

		public void Load (bool async = false, System.Action asyncCallback = null) {
			if (LastLoadedScene != null)
				OnSceneWillChange?.Invoke (LastLoadedScene);
#if CGV_NETWORK_nope
			if (Photon.Pun.PhotonNetwork.IsMasterClient)
				Photon.Pun.PhotonNetwork.LoadLevel (_Scene);

			if (async)
				SceneManager.LoadSceneAsync (_Scene).completed += (a) => {
					SceneWasLoaded ();
					asyncCallback?.Invoke ();
				};
			else {
				SceneManager.LoadScene (_Scene);
				SceneWasLoaded ();
			}
#else
			if (async)
				SceneManager.LoadSceneAsync (_Scene).completed += (a) => {
					SceneWasLoaded ();
					asyncCallback?.Invoke ();
				};
			else {
				SceneManager.LoadScene (_Scene);
				SceneWasLoaded ();
			}
#endif
		}

		protected virtual void SceneWasLoaded () {
			LastLoadedScene = this;
			Debug.Log ("Scene loaded : " + this);
			OnSceneChanged?.Invoke (this);
		}

		public override string ToString () {
			return string.IsNullOrEmpty (Name) ? base.ToString () : Name;
		}
	}
}
