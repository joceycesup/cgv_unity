﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGV.UI {
	/// <summary>
	/// Component that automatically fills a %UI list of playable levels with the scenes listed in the currently active CGVSceneSet in the CGVGameSettings.
	/// </summary>
	public class CGVPlayableSceneViewList : MonoBehaviour {

		[SerializeField] private GameObject _SceneViewPrefab;

		[SerializeField] private Transform _ViewersContainer;

		private List<CGVPlayableSceneViewer> _Viewers = new List<CGVPlayableSceneViewer> ();

		private void Start () {
			foreach (CGVPlayableScene scene in CGVGameSettings.instance.SceneSet) {
				CGVPlayableSceneViewer viewer = Instantiate (_SceneViewPrefab, _ViewersContainer).GetComponent<CGVPlayableSceneViewer> ();
				viewer.PlayableScene = scene;
				_Viewers.Add (viewer);
			}
		}

		void OnEnable () {
			UpdateViews ();
		}

		private void UpdateViews () {

		}
	}
}
