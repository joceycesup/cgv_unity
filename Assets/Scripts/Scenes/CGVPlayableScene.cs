﻿using UnityEngine;

#if CGV_NETWORK
using Photon.Pun;
using Photon.Realtime;
#endif // CGV_NETWORK

#pragma warning disable 649
namespace CGV {
	/// <summary>
	/// Asset class that contains informations about a playable scene.
	/// These should be referenced by a CGVPlayableSceneSet asset.
	/// It can be created using the contextual menu "Create/CGV/Scenes/Playable Scene".
	/// </summary>
	[CreateAssetMenu (fileName = "PlayableScene", menuName = "CGV/Scenes/Playable Scene")]
	public class CGVPlayableScene : CGVScene {

		[SerializeField]
		[NaughtyAttributes.ShowAssetPreview]
		private Sprite _Thumbnail;
		public Sprite Thumbnail { get { return _Thumbnail; } }

		public override bool IsPlayable => true;
		/*
		protected override void SceneWasLoaded () {
#if CGV_NETWORK
			if (!CGVNetworkManager.IsOnline)
#endif // CGV_NETWORK
			if (FindObjectOfType<CGVPlayableSceneManager> () == null) {
				Debug.Log ("No SceneManager found, creating one");
				new GameObject ("SceneManager", typeof (CGVPlayableSceneManager));
			}
			base.SceneWasLoaded (); // Base is called after to ensure there is a SceneManager before the OnSceneChanged callback
		}//*/

		public override string ToString () {
			return string.Format ("Playable Scene {0}", Name);
		}
	}
}
