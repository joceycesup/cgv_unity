﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using JuneCodes.FlexibleEnum;

using CGV.Player;

#if CGV_NETWORK
using Photon.Pun;
#endif // CGV_NETWORK

namespace CGV {
	/// <summary>
	/// Singleton component that loads level information before the start of the game.
	/// It also handles the game flow, checks the stop conditions and keeps track of the score.
	/// </summary>
	public class CGVPlayableSceneManager : MonoBehaviour
#if CGV_NETWORK
		, IPunObservable
#endif // CGV_NETWORK
		{

		public static float OOBHeight { get; protected set; } = -1000f;

		//*
		[RuntimeInitializeOnLoadMethod]
		static void RunOnStart () {
			CGVScene.OnSceneWillChange += ClearSpawns;
			CGVScene.OnSceneChanged += UpdateSceneInfo;
		}//*/

		private static void ClearSpawns (CGVScene scene) {
			_Spawns.Clear ();
		}

		private static void UpdateSceneInfo (CGVScene scene) {
			if (scene != null && !scene.IsPlayable)
				return;
			Bounds bounds = new Bounds ();
			foreach (var surface in FindObjectsOfType<UnityEngine.AI.NavMeshSurface> ()) {
				if (surface.navMeshData == null)
					continue;
				Bounds navMeshBounds = surface.navMeshData.sourceBounds;
				bounds.Encapsulate (navMeshBounds);
			}
			OOBHeight = bounds.min.y - 20f;
			//Debug.Log ("OOBHeight = " + OOBHeight);

			ClearSpawns (scene);
			if (scene == null || scene.IsPlayable) {
				foreach (CGVSpawn spawn in FindObjectsOfType<CGVSpawn> ()) {
					RegisterSpawn (spawn);
				}
			}
			OnSpawnsUpdated?.Invoke ();
		}

		public delegate void PlayableSceneEvent ();
		public static PlayableSceneEvent OnSceneReady;
		public static PlayableSceneEvent OnSpawnsUpdated;
		public static PlayableSceneEvent OnSessionEnded;

		public static CGVPlayableSceneManager instance { get; protected set; }

		protected int _TeamIndex = 0;

		protected static List<int> _Teams = new List<int> (3);
		public static ref readonly List<int> Teams { get { return ref _Teams; } }
		protected static Dictionary<int, List<CGVSpawn>> _Spawns = new Dictionary<int, List<CGVSpawn>> ();

		protected static Dictionary<int, int> _Kills = new Dictionary<int, int> ();
		protected static Dictionary<int, int> _Deaths = new Dictionary<int, int> ();
		protected int _DeathCount = 0;

		protected static bool _IsReady = false;
		public static bool IsReady {
			get { return _IsReady; }
			protected set {
				if (value == _IsReady)
					return;
				_IsReady = value;
				if (_IsReady)
					OnSceneReady?.Invoke ();
			}
		}
		protected static bool _SessionIsOver = false;
		public static bool SessionIsOver {
			get { return _SessionIsOver; }
			protected set {
				if (value == _SessionIsOver)
					return;
				_SessionIsOver = value;
				if (_SessionIsOver) {
					instance.StopAllCoroutines ();
					OnSessionEnded?.Invoke ();
				}
			}
		}

		void Awake () {
			SessionIsOver = false;
			instance = this;
#if UNITY_EDITOR
			UpdateSceneInfo (CGVScene.LastLoadedScene);
#endif // UNITY_EDITOR
		}

		private void Start () {
			IsReady = true;
			Debug.LogFormat ("----- Beginning countdown, {0} seconds remaining", CGVGameSettings.instance.GameDuration);
			if (CGVGameSettings.instance.GameDuration > 0f)
				StartCoroutine (BeginCountdown ());
		}

		private IEnumerator BeginCountdown () {
			yield return new WaitForSeconds (CGVGameSettings.instance.GameDuration);
			SessionIsOver = true;
		}

		private void OnEnable () {
			_Kills.Clear ();
			_Deaths.Clear ();
			CGVPlayerCharacter.OnCharacterPossessed += CharacterPossessed;
			CGVPlayerCharacter.OnCharacterUnpossessed += CharacterUnpossessed;
		}

		private void OnDisable () {
			CGVPlayerCharacter.OnCharacterPossessed -= CharacterPossessed;
			CGVPlayerCharacter.OnCharacterUnpossessed -= CharacterUnpossessed;
		}

		void CharacterPossessed (CGVPlayerCharacter character, int teamID) {
			character.OnGotKilled += CharacterGotKilled;
		}

		void CharacterUnpossessed (CGVPlayerCharacter character, int teamID) {
			character.OnGotKilled -= CharacterGotKilled;
		}

		void CharacterGotKilled (CGVTarget target, CGVPlayerCharacter by) {
			CGVPlayerCharacter killed = target as CGVPlayerCharacter;
			if (killed == null)
				return;
			if (!_Deaths.ContainsKey (killed.Team))
				_Deaths[killed.Team] = 0;
			if (by != null) {
				Debug.LogFormat ("----- {0} got killed by {1}", target.Name, by.gameObject);
				if (!_Kills.ContainsKey (by.Team))
					_Kills[by.Team] = 0;
				if (killed != by) {
					_Kills[by.Team]++;
				}
				Debug.LogFormat ("----- Team {0} kill count : {1}", by.Team, _Kills[by.Team]);
			}
			else
				Debug.LogFormat ("----- {0} died alone", target.Name);
			_Deaths[killed.Team]++;
			_DeathCount++;
			if (CGVGameSettings.instance.KillThreshold > 0 && _DeathCount >= CGVGameSettings.instance.KillThreshold) {
				SessionIsOver = true;
			}
			StartCoroutine (RespawnCharacterWithDelay (killed));
			Debug.LogFormat ("----- Team {0} death count : {1}", killed.Team, _Deaths[killed.Team]);
		}

		private IEnumerator RespawnCharacterWithDelay (CGVPlayerCharacter character) {
			yield return new WaitForSeconds (CGVGameSettings.instance.RespawnDelay);
			if (!SessionIsOver)
				SpawnCharacter (character);
		}

		void OnDestroy () {
			IsReady = false;
			instance = null;
			StopAllCoroutines ();
		}

		public static void CreateSceneManager () {
			if (instance == null)
				new GameObject ("SceneManager", typeof (CGVPlayableSceneManager));
		}

		public static int GetTeamKills (int teamID) {
			if (!_Kills.ContainsKey (teamID))
				_Kills[teamID] = 0;
			return _Kills[teamID];
		}

		public static int GetTeamDeaths (int teamID) {
			if (!_Deaths.ContainsKey (teamID))
				_Deaths[teamID] = 0;
			return _Deaths[teamID];
		}

		public static void RegisterSpawn (CGVSpawn spawn) {
			if (spawn == null)
				return;
			CGVTeamValue team = CGVTeamEnum.GetValue (spawn.Team, true);
			Debug.LogFormat ("Spawn registered for team {0}, {1}", team != null ? team.Name : "None", spawn.gameObject);
			if (_Spawns.TryGetValue (spawn.Team, out List<CGVSpawn> spawns)) {
				if (!spawns.Contains (spawn))
					spawns.Add (spawn);
			}
			else {
				_Spawns[spawn.Team] = new List<CGVSpawn> (4) { spawn };
			}
			if (!_Teams.Contains (spawn.Team) && JCEnumKey.IdIsValid (spawn.Team)) {
				_Teams.Add (spawn.Team);
			}
		}

		public int GetNextTeam () {
			if (_Teams.Count <= 0)
				return JCEnumKey.InvalidId;
			if (_Teams.Count == 1)
				return _Teams[0];
			int res = _Teams[_TeamIndex];
			_TeamIndex++;
			if (_TeamIndex >= _Teams.Count)
				_TeamIndex = 0;
			return res;
		}

		public bool SpawnCharacter (CGVPlayerCharacter character, CGVPlayerController player = null) {
			if (character == null)
				return false;

			int teamID = player != null ? player.Team : character.Team;
			CGVTeamValue team = CGVTeamEnum.GetValue (teamID, true);
			Debug.LogFormat ("Trying to spawn character {0} of team {1}", character.Name, team != null ? team.Name : "None");

			if (_Spawns.TryGetValue (teamID, out List<CGVSpawn> spawns)) {
				foreach (CGVSpawn spawn in spawns) {
					if (spawn.Spawn (character)) {
						return true;
					}
					else {
						Destroy (character.gameObject);
					}
				}
			}
			return false;
		}

		public CGVSpawn GetSpawnForPlayer (CGVPlayerController player) {
			if (player == null)
				return null;

			if (_Spawns.TryGetValue (player.Team, out List<CGVSpawn> spawns)) {
				foreach (CGVSpawn spawn in spawns) {
					if (spawn.IsFree ()) {
						return spawn;
					}
				}
			}
			return null;
		}

#if CGV_NETWORK
		public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info) {
			//Debug.Log ("PlayableSceneManager ID=" + PhotonView.Get (this).ViewID);
			if (stream.IsWriting) {
				//stream.SendNext (Input.LookZ);
				//Debug.LogFormat ("Serializing PlayableSceneManager, TeamIndex={0}", _TeamIndex);
				stream.SendNext (_TeamIndex);
			}
			else {
				//Input.LookZ = (float)stream.ReceiveNext ();
				//Debug.LogFormat ("Deserializing PlayableSceneManager, TeamIndex={0}", _TeamIndex);
				_TeamIndex = (int)stream.ReceiveNext ();
				//Debug.LogFormat ("Deserialized PlayableSceneManager,  TeamIndex={0}", _TeamIndex);
			}
		}
#endif // CGV_NETWORK
	}
}
