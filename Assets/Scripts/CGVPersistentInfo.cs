﻿using UnityEngine;

using JuneCodes;

#pragma warning disable 649
namespace CGV {
	[JCCreateSingletonIfNull]
	public class CGVPersistentInfo : JCSingletonMonoBehaviour<CGVPersistentInfo> {

		// Layers
		public static int LayerCharacter { get; private set; }
		public static int LayerFamiliar { get; private set; }
		public static int LayerGround { get; private set; }
		public static int LayerWall { get; private set; }

		[RuntimeInitializeOnLoadMethod]
		static void InitializeLayers () {
			LayerCharacter = LayerMask.NameToLayer ("Character");
			LayerFamiliar = LayerMask.NameToLayer ("Familiar");
			LayerGround = LayerMask.NameToLayer ("Ground");
			LayerWall = LayerMask.NameToLayer ("Wall");
		}
	}
}
