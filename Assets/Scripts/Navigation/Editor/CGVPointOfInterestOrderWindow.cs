﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using System.Collections.Generic;

#pragma warning disable 649
namespace CGV.Editor {
	/// <summary>
	/// %Editor window that allows the level designers to manipulate and sort the Points of Interest (CGVPointOfInterest) in a level.
	/// It automatically lists every POI when a level is open in the editor.
	/// It can be open via the menu "Window/CGV/POI Window".
	/// </summary>
	public class CGVPointOfInterestOrderWindow : EditorWindow {

		ReorderableList _POIList;
		List<CGVPointOfInterest> _POIs;

		[MenuItem ("Window/CGV/POI Window")]
		static void Init () {
			CGVPointOfInterestOrderWindow window = EditorWindow.GetWindow<CGVPointOfInterestOrderWindow> ();
			window.titleContent = CGVEditorUtils.IconContent ("Points of Interest", "Points of Interest");
			window.Show ();
		}

		private void OnEnable () {
			CGVPointOfInterest.OnPOIAdded += POIAdded;
			CGVPointOfInterest.OnPOIDestroyed += POIDestroyed;
			Selection.selectionChanged += SceneSelectionChanged;
			JuneCodes.Editor.JCEditorUtility.EditorSceneChanged += EditorSceneChanged;
			if (_POIList == null) {
				_POIs = new List<CGVPointOfInterest> ();
				_POIList = new ReorderableList (_POIs, typeof (CGVPointOfInterest), true, true, false, true) {
					drawElementCallback = (rect, index, isActive, isFocused) => {
						CGVPointOfInterest poi = _POIs[index];
						EditorGUI.LabelField (rect, string.Format ("{0} ({1})", poi.Description, poi.Order));
					},
					drawHeaderCallback = (Rect rect) => {
						EditorGUI.LabelField (rect, string.Format ("{0} Points Of Interest", _POIs.Count), EditorStyles.label);
					},
					onRemoveCallback = (ReorderableList list) => {
						CGVPointOfInterest poi = _POIs[list.index];
						_POIs.RemoveAt (list.index);
						UpdateOrder ();
						if (Application.isPlaying)
							Destroy (poi.gameObject);
						else
							DestroyImmediate (poi.gameObject);
					},
					onReorderCallback = (ReorderableList list) => {
						UpdateOrder ();
					},
					onSelectCallback = (ReorderableList list) => {
						IndexChangedInList (list.index);
					},
					/*
					onRemoveCallback += (ReorderableList list) => {
						SerializedProperty element = elements.GetArrayElementAtIndex (index);
						if (element.propertyType == SerializedPropertyType.ObjectReference && element.objectReferenceValue != null)
							element.objectReferenceValue = null;
						elements.DeleteArrayElementAtIndex (list.index);
						list.serializedProperty.serializedObject.ApplyModifiedProperties ();
						//Debug.Log ("Remove " + list.index);
					},/*
					elementHeightCallback = (int index) => {
						if (index >= elements.arraySize)
							return 0f;
						SerializedProperty element = elements.GetArrayElementAtIndex (index);
						return EditorGUI.GetPropertyHeight (element, true) + 4f;
					},//*/
				};
			}
			_POIs.Clear ();
			_POIs.AddRange (CGVPointOfInterest.GetSortedPOIs ());
		}

		private void OnDisable () {
			CGVPointOfInterest.OnPOIAdded -= POIAdded;
			CGVPointOfInterest.OnPOIDestroyed -= POIDestroyed;
			Selection.selectionChanged -= SceneSelectionChanged;
			JuneCodes.Editor.JCEditorUtility.EditorSceneChanged -= EditorSceneChanged;
		}

		private void EditorSceneChanged () {
			//Debug.Log ("Scene changed");
			_POIs.Clear ();
			_POIs.AddRange (CGVPointOfInterest.GetSortedPOIs ());
		}

		void IndexChangedInList (int index) {
			if (index < 0) {
				Selection.activeGameObject = null;
			}
			else {
				CGVPointOfInterest poi = _POIs[index];
				Selection.activeGameObject = poi.gameObject;
			}
		}

		void POIAdded (CGVPointOfInterest poi) {
			//Debug.LogFormat ("POI {0} is being added", poi.Description);
			if (_POIs.Contains (poi))
				return;
			if (poi.Order < 0)
				SetPOIOrder (poi, _POIs.Count);
			else {
				// Handles duplication
				foreach (var other in _POIs) {
					if (other.Order == poi.Order)
						SetPOIOrder (poi, _POIs.Count);
				}
			}
			//poi.Order = _POIs.Count;
			_POIs.Add (poi);
			_POIs.Sort ();
			Repaint ();
		}

		void POIDestroyed (CGVPointOfInterest poi) {
			//Debug.LogFormat ("POI {0} is being destroyed", poi.Description);
			if (!_POIs.Contains (poi))
				return;
			_POIs.Remove (poi);
			Repaint ();
		}

		void UpdateOrder () {
			for (int i = 0; i < _POIs.Count; ++i) {
				SetPOIOrder (_POIs[i], i);
				//_POIs[i].Order = i;
			}
		}

		void SetPOIOrder (CGVPointOfInterest poi, int order) {
			SerializedObject poiSO = new SerializedObject (poi);
			SerializedProperty orderProperty = poiSO.FindProperty ("_Order");
			if (orderProperty.intValue != order) {
				orderProperty.intValue = order;
				poiSO.ApplyModifiedProperties ();
				Debug.LogFormat ("Setting order of {0} = {1}", poi.Description, poi.Order);
			}
		}

		void SceneSelectionChanged () {
			if (Selection.activeGameObject == null) {
				_POIList.index = -1;
				IndexChangedInList (-1);
				Repaint ();
				return;
			}
			CGVPointOfInterest poi = Selection.activeGameObject.GetComponent<CGVPointOfInterest> ();
			if (poi != null) {
				int index = _POIs.IndexOf (poi);
				if (index >= 0) {
					_POIList.index = index;
					IndexChangedInList (index);
				}
				Repaint ();
			}
		}

		void OnGUI () {
			EditorGUI.BeginChangeCheck ();
			/*
			EditorGUILayout.Space ();

			CGVPointOfInterest[] POIs = CGVPointOfInterest.GetSortedPOIs ();

			EditorGUI.BeginDisabledGroup (_HighlightedPOI == null);
			if (GUILayout.Button ("Select none")) {
				Selection.activeGameObject = null;
				_HighlightedPOI.HighlightInSceneView = false;
				_HighlightedPOI = null;
			}
			EditorGUI.EndDisabledGroup ();

			EditorGUILayout.Space ();

			CGVPointOfInterest lastPOI = null;
			for (int i = 0; i < POIs.Length; ++i) {
				CGVPointOfInterest poi = POIs[i];

				if (poi.Order < 0) {
					poi.Order = lastPOI != null ? lastPOI.Order + 1 : 0;
				}

				if (_HighlightedPOI == null) {
					if (poi.HighlightInSceneView)
						_HighlightedPOI = poi;
				}
				if (_HighlightedPOI != poi) {
					poi.HighlightInSceneView = false;
				}

				if (lastPOI != null) {
					EditorGUI.indentLevel++;
					if (lastPOI.Order == poi.Order) {
						poi.Order++;
					}
					EditorGUI.indentLevel--;
				}

				EditorGUILayout.BeginHorizontal ();

				int ud = CGVEditorUtils.UpDown (i <= 0, i >= POIs.Length - 1);
				if (ud < 0) {
					int o = lastPOI.Order;
					lastPOI.Order = poi.Order;
					poi.Order = o;
				}
				else if (ud > 0) {
					int o = POIs[i + 1].Order;
					POIs[i + 1].Order = poi.Order;
					poi.Order = o;
				}

				if (GUILayout.Button ("Select", GUILayout.Width (50f))) {
					Selection.activeGameObject = poi.gameObject;
					poi.HighlightInSceneView = true;
					_HighlightedPOI = poi;
				}

				EditorGUILayout.LabelField (string.Format ("{0} ({1})", poi.Description, poi.Order));

				EditorGUILayout.EndHorizontal ();
				lastPOI = poi;
			}
			//*/
			_POIList.DoLayoutList ();

			if (EditorGUI.EndChangeCheck ()) {
				UnityEditorInternal.InternalEditorUtility.RepaintAllViews ();
			}
		}
	}
}
