﻿using System;

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

#pragma warning disable 649
namespace CGV {
	/// <summary>
	/// Component that defines a key location in a level.
	/// </summary>
	[ExecuteInEditMode]
	public class CGVPointOfInterest : MonoBehaviour, IComparable<CGVPointOfInterest> {

#if UNITY_EDITOR
		public delegate void POIEvent (CGVPointOfInterest poi);
		public static POIEvent OnPOIAdded;
		public static POIEvent OnPOIDestroyed;
#endif

		public static CGVPointOfInterest[] GetSortedPOIs () {
			CGVPointOfInterest[] POIs = GameObject.FindObjectsOfType<CGVPointOfInterest> ();
			Array.Sort (POIs);
			return POIs;
		}

		[SerializeField]
		protected string _Description = "New POI";
		public string Description { get { return _Description; } }

		[SerializeField]
		[HideInInspector]
		protected int _Order = -1;
		public int Order { get { return _Order; } set { _Order = value; } }

		private void Awake () {
			for (int i = 0; i < transform.childCount; ++i) {
				transform.GetChild (i).gameObject.SetActive (false);
			}
		}

#if UNITY_EDITOR
		void Start () {
			OnPOIAdded?.Invoke (this);
		}

		void OnDestroy () {
			OnPOIDestroyed?.Invoke (this);
		}
#endif

#if UNITY_EDITOR
		private void OnDrawGizmos () {
			if (Application.isPlaying)
				return;
			if (Selection.activeGameObject != gameObject)
				return;

			Gizmos.color = Color.red;
			Gizmos.DrawLine (transform.position, transform.position + Vector3.up * 1000f);
			Gizmos.DrawWireSphere (transform.position, 2.5f);
		}
#endif

		public int CompareTo (CGVPointOfInterest other) {
			if (_Order < 0 || other._Order < 0) {
				return other._Order - _Order;
			}
			return _Order - other._Order;
		}
	}
}
