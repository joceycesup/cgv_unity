﻿using System.Collections;
using UnityEngine;

namespace CGV.Player {
	/// <summary>
	/// Beacons are sonified objects that are put on a path to help the player follow that path.
	/// This behaviour controls the sound cues the beacon emits.
	/// </summary>
	public class CGVBeacon : MonoBehaviour {
		[SerializeField] protected AK.Wwise.Event LastBeaconReachedEvent;

		[SerializeField] protected AK.Wwise.Event BeaconIdleEvent;
		[NaughtyAttributes.MinValue (0.1f)]
		[SerializeField] protected float DelayBetweenIdleSound = 3f;

		[NaughtyAttributes.MinValue (0.01f)]
		[SerializeField] protected float MinSetPositionSpeed = 10f;
		[NaughtyAttributes.MinValue (0f)]
		[SerializeField] protected float MaxSetPositionDelay = 1f;

		private float NextBeaconSoundTime;
		private Vector3 OriginalPosition;
		public Vector3 TargetPosition { get; private set; }

		private IEnumerator LerpPositionCoroutine;

		private void OnEnable () {
			NextBeaconSoundTime = 0f;

			TargetPosition = transform.position;
		}

		public void Reached (bool last, bool newlyReachedBeacon = true) {/*
			if (LerpPositionCoroutine != null) {
				StopCoroutine (LerpPositionCoroutine);
				LerpPositionCoroutine = null;
			}//*/

			if (last) {
				LastBeaconReachedEvent?.Post (gameObject);
				gameObject.SetActive (false);
			}
			else if (newlyReachedBeacon)
				NextBeaconSoundTime = 0f;
		}

		public void SetPosition (Vector3 newPosition) {
			if (TargetPosition == newPosition)
				//Debug.LogWarning ("Setting same position");
				return;
			if (!isActiveAndEnabled) {
				gameObject.SetActive (true);
				transform.position = newPosition;
			}
			else {
				OriginalPosition = transform.position;
				TargetPosition = newPosition;
				if (LerpPositionCoroutine == null) {
					StartCoroutine (LerpPositionCoroutine = LerpPosition ());
				}
			}
		}

		private void Update () {
			if (Time.time >= NextBeaconSoundTime) {
				BeaconIdleEvent?.Post (gameObject);
				NextBeaconSoundTime = Time.time + DelayBetweenIdleSound;
			}
		}

		private IEnumerator LerpPosition () {
			float delay = Mathf.Min (MaxSetPositionDelay, Vector3.Distance (OriginalPosition, TargetPosition) / MinSetPositionSpeed);
			//Debug.LogFormat (" > Set Position Delay : {0} ({1})", delay.ToString ("0.000"), (MaxSetPositionDelay < Vector3.Distance (OriginalPosition, _TargetPosition) / MinSetPositionSpeed) ? "fixed delay" : "speed");
			float startTime = Time.time;
			float t = 0f;
			while ((t = ((Time.time - startTime) / delay)) < 1f) {
				transform.position = Vector3.Lerp (OriginalPosition, TargetPosition, t);
				yield return null;
			}
			transform.position = TargetPosition;
			//Debug.Log (" < End Set Position");
			LerpPositionCoroutine = null;
		}
	}
}
