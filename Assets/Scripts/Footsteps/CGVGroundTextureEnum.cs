﻿using System;
using UnityEngine;

using JuneCodes.FlexibleEnum;

namespace CGV.Sound {
	/// <summary>
	/// Type of ground texture that can be used in the game.
	/// </summary>
	[Serializable]
	public class CGVGroundTextureValue : JCEnumValue {
		[SerializeField] [ColorUsage (false)] protected Color _Color = Color.green;
		public Color Color { get { return _Color; } }

		[SerializeField] protected Material _DefaultMaterial;
		public Material DefaultMaterial { get { return _DefaultMaterial; } }

		[SerializeField] protected AK.Wwise.Switch _TextureSwitch;
		public AK.Wwise.Switch TextureSwitch { get { return _TextureSwitch; } }
	}

	/// <summary>
	/// Container class for Ground types.
	/// Used by CGVGroundTextureEnum
	/// </summary>
	[Serializable]
	public class CGVGroundTextureList : JCEnumValueContainer<CGVGroundTextureValue> { }

	/// <summary>
	/// Singleton asset class to access the different types of Ground textures available in the game.
	/// It is located at the path "Assets/Resources/GroundTextures.asset".
	/// See CGVGroundTextureValue for more information about the values themselves.
	/// </summary>
	[CreateAssetMenu (fileName = "GroundTextures.asset", menuName = "CGV/Ground Textures")]
	public class CGVGroundTextureEnum : JCFlexibleEnumSet<CGVGroundTextureEnum, CGVGroundTextureList, CGVGroundTextureValue> {
	}
}
