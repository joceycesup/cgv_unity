﻿using System.Collections;
using System;
using UnityEngine;

#pragma warning disable 649
namespace CGV.Player {
	public class CGVFootsteps : MonoBehaviour {

		[Serializable]
		public struct FeetPair {
			public AK.Wwise.Event LeftStep;
			public AK.Wwise.Event RightStep;

			public AK.Wwise.Event this[bool left] {
				get { return left ? LeftStep : RightStep; }
			}
		}

		private CGVPlayerCharacter playerController;

		[SerializeField] private FeetPair ForwardSteps;
		[SerializeField] private FeetPair IdleSteps;
		[SerializeField] private FeetPair BackwardSteps;

		[SerializeField] private AK.Wwise.Event ForwardStepEvent;
		[SerializeField] private AK.Wwise.Event ForwardStepRunEvent;
		[SerializeField] private AK.Wwise.Event BackwardStepEvent;
		[SerializeField] private AK.Wwise.Event StrafeLeftStepEvent;
		[SerializeField] private AK.Wwise.Event StrafeRightStepEvent;
		private AK.Wwise.Event GetStepEvent () {
			switch (DirectionIndex) {
				case 0: return ForwardStepEvent;
				case 1: return BackwardStepEvent;
				case 2: return StrafeRightStepEvent;
				case 3: return StrafeLeftStepEvent;
				case 4: return ForwardStepRunEvent;
			}
			return null;
		}
		private int DirectionIndex = 0;
		private void UpdateDirectionIndex () {
			if (Mathf.Abs (Direction.y) < 0.001f) {
				if (Mathf.Abs (Direction.x) < 0.001f)
					DirectionIndex = -1;
				else
					DirectionIndex = 2 | (Direction.x < 0f ? 1 : 3);
			}
			else {
				DirectionIndex = Direction.y < 0 ? 1 : (Speed - WalkSpeed > 0.1f ? 4 : 0);
			}
			//Debug.Log ("Index " + DirectionIndex);
			//Debug.Log (Mathf.Approximately (Direction.y, 0f) + " : " + Direction.y);
		}

		[SerializeField] private AK.Wwise.RTPC DirectionRTPC;
		private Vector2 Direction;
		private float GroundDistance;

		[SerializeField] private AK.Wwise.RTPC RunFactorRTPC;
		private float RunFactor;

		[SerializeField] private float StepDistance = 1f;
		[SerializeField] private float StepSoundDistance = 1f;

		[SerializeField] private float StepDelay_Min = 1f;
		[SerializeField] private float StepDelay_Walking = 0.6f;
		[SerializeField] private float RandomStepDelay_Walking = 0.05f;
		[SerializeField] private float StepDelay_Running = 0.3f;
		[SerializeField] private float RandomStepDelay_Running = 0.015f;

		[SerializeField] private AnimationCurve WalkingFactor;
		[SerializeField] private AnimationCurve RunningFactor;

		[SerializeField] private GameObject _SoundSource;
		public GameObject SoundSource { get { return _SoundSource; } }

		private bool IsLeftFoot;

		private float Speed = 0f;
		private float WalkSpeed = 1f, RunSpeed = 2f;

		private float lastStepTime = -1f;
		private float nextStepTime = 0f;
		private bool takeNextStep = false;

		private float currentStepDistance = 0f;

		private void Start () {
			playerController = GetComponent<CGVPlayerCharacter> ();
		}

		private void OnEnable () {
			takeNextStep = false;
		}
		/*
		public void SetSpeed (float speed) {
			if (Mathf.Approximately (Speed, speed))
				return;
			Speed = speed;
			if (Mathf.Approximately (Direction.x, 0f)) {
			}
			//Debug.Log ("Index " + DirectionIndex);
			Debug.Log (Speed);
			AkSoundEngine.SetRTPCValue (RunFactorRTPC.Id, RunFactor = Mathf.Max (0f, (Mathf.Abs (Speed) - WalkSpeed) / (RunSpeed - WalkSpeed)));
			bool tmp = !Mathf.Approximately (Speed, 0f);
			//if (takeNextStep && !tmp)
			//AkSoundEngine.PostEvent (StopWalking.Id, SoundSource);
			takeNextStep = tmp;
			//Debug.Log(Speed);
			//Debug.Log(Mathf.Max(0f, (speed - WalkSpeed) / (RunSpeed - WalkSpeed)).ToString("0.000"));
		}

		public void SetDirection (Vector3 direction) {
			//Debug.Log ("Direction " + direction);
			//AkSoundEngine.SetRTPCValue (DirectionRTPC.Id, Direction = direction);
			if (Mathf.Approximately (Direction.x, 0f)) {
				DirectionIndex = 2 | (direction.x < 0f ? 1 : 0);
			}
			//Debug.Log ("Index " + DirectionIndex);
			SoundSource.transform.localPosition = new Vector3 (0f, -GroundDistance, StepSoundDistance * 0.5f * Mathf.Sign (Speed));
		}/*/

		public bool SetSpeed (float speed) {
			if (Mathf.Abs (Speed - speed) < 0.0001f)
				return false;
			//Debug.LogFormat ("Setting speed ({0})", Speed);
			Speed = speed;
			UpdateDirectionIndex ();
			takeNextStep = (Speed >= 0.0001f);
			return true;
		}

		public void SetSpeedAndDirection (float speed, Vector2 direction) {
			bool updateIndex = (direction != Direction);
			Direction = direction;

			updateIndex |= SetSpeed (speed);
			//SoundSource.transform.localPosition = new Vector3 (0f, -GroundDistance, StepSoundDistance * 0.5f * Direction.y);

			if (updateIndex)
				UpdateDirectionIndex ();
		}//*/

		internal void SetSpeeds (float walkSpeed, float runSpeed) {
			WalkSpeed = walkSpeed;
			RunSpeed = runSpeed;
		}

		private void TakeStep () {
			//Debug.Log("Step " + (IsLeftFoot ? "Left" : "Right"));
			//AkSoundEngine.PostEvent((Speed == 0 ? IdleSteps : (Speed < 0 ? BackwardSteps : ForwardSteps))[IsLeftFoot].Id, gameObject);
			//AkSoundEngine.PostEvent (ForwardSteps[IsLeftFoot].Id, SoundSource);
			GetStepEvent ()?.Post (SoundSource);
			lastStepTime = Time.time;
			IsLeftFoot = !IsLeftFoot;
		}

		private void FixedUpdate () {/*
			if (takeNextStep && Time.time >= nextStepTime) {
				TakeStep();
				if (Speed <= WalkSpeed) {
					nextStepTime = Time.time +
						Mathf.Lerp(StepDelay_Min, StepDelay_Walking, RunningFactor.Evaluate(Speed / WalkSpeed)) +
						UnityEngine.Random.Range(-RandomStepDelay_Walking, RandomStepDelay_Walking);
				}
				else {
					float factor = (Speed - WalkSpeed) / (RunSpeed - WalkSpeed);
					nextStepTime = Time.time +
						Mathf.Lerp(StepDelay_Walking, StepDelay_Running, RunningFactor.Evaluate(factor)) +
						UnityEngine.Random.Range(-1f, 1f) * Mathf.Lerp(RandomStepDelay_Walking, RandomStepDelay_Running, factor);
				}
			}//*/
			if (takeNextStep) {
				currentStepDistance += Speed * Time.fixedDeltaTime;
				if (currentStepDistance > StepDistance) {
					currentStepDistance -= StepDistance;
					TakeStep ();
				}
			}
		}

		public void SetGroundPos (Vector3 point) {
			//SoundSource.transform.position = point;
			//GroundDistance = (SoundSource.transform.parent.position - point).y;
			SoundSource.transform.localPosition = new Vector3 (0f, -GroundDistance, 0f);
		}
	}
}
