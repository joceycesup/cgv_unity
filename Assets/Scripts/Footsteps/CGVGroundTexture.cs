﻿using UnityEngine;
using AK.Wwise;
#if UNITY_EDITOR
using UnityEditor;
#endif

using JuneCodes.FlexibleEnum;

#pragma warning disable 649
namespace CGV.Sound {
	/// <summary>
	/// This class simplifies ground texture detection and sound switch assignment.
	/// It keeps track of the last texture that was stepped on and update the values only when needed.
	/// </summary>
	public class CGVGroundTextureDetector : ICGVTextureSwitchProvider {
		GameObject lastSurface;
		CGVGroundTexture lastTexture;
		public int TextureId {
			get { return (lastTexture != null ? lastTexture.GroundType.Id : CGVGroundTextureEnum.instance.DefaultValue.Id); }
		}
		public string TextureName {
			get { return (lastTexture != null ? CGVGroundTextureEnum.GetValue (lastTexture.GroundType).Name : CGVGroundTextureEnum.instance.DefaultValue.Name); }
		}

		public bool CheckGroundTexture (Vector3 origin, Vector3 direction, float maxDistance, out RaycastHit hit) {
			if (Physics.Raycast (origin, direction, out hit, maxDistance, 1 << 11)) {
				if (hit.collider == null) {
					lastSurface = null;
					if (lastTexture != null) {
						lastTexture = null;
						return true;
					}
					lastTexture = null;
					return false;
				}
				GameObject newSurface = hit.collider.gameObject;
				if (newSurface == lastSurface)
					return false;
				lastSurface = newSurface;

				CGVGroundTexture newTexture = newSurface.GetComponent<CGVGroundTexture> ();
				if (newTexture != null) {
					bool res = true;
					if (lastTexture != null) {
						res = newTexture.GroundType != lastTexture.GroundType;
					}
					lastTexture = newTexture;
					return res;
				}
				else if (lastTexture != null) {
					lastTexture = null;
					return true;
				}
			}
			return false;
		}

		public Switch GetTextureSwitch () {
			return CGVGroundTextureEnum.GetValue (lastTexture?.GroundType).TextureSwitch;
		}

		public bool SwitchGroundTexture (GameObject target) {
			if (target == null)
				return false;
			bool res = false;

			CGVGroundTextureValue type = CGVGroundTextureEnum.GetValue (lastTexture?.GroundType);
			if (type != null && type.TextureSwitch != null) {
				AkSoundEngine.SetSwitch (type.TextureSwitch.GroupId, type.TextureSwitch.Id, target);
				res = true;
			}
			return res;
		}
	}

	/// <summary>
	/// Component that assigns a ground texture to a collider.
	/// See CGVGroundTextureValue for more information about ground textures.
	/// </summary>
	[RequireComponent (typeof (Collider))]
	public class CGVGroundTexture : MonoBehaviour, ICGVTextureSwitchProvider {

		[NaughtyAttributes.OnValueChanged ("OverrideMaterial")]
		[JCEnumType (typeof (CGVGroundTextureEnum))]
		[SerializeField] public JCEnumKey GroundType;

		[SerializeField]
		private bool OverrideGroundMaterial = false;

		private void Start () {
			if (OverrideGroundMaterial)
				OverrideMaterial ();
		}

		[ContextMenu ("Override Material")]
		private void OverrideMaterial () {
			MeshRenderer mr = GetComponent<MeshRenderer> ();
			if (mr == null)
				return;
			CGVGroundTextureValue type = CGVGroundTextureEnum.GetValue (GroundType, true);
			if (type != null && type.DefaultMaterial != null) {
				mr.sharedMaterial = type.DefaultMaterial;
			}
		}

		public Switch GetTextureSwitch () {
			return CGVGroundTextureEnum.GetValue (GroundType).TextureSwitch;
		}

#if UNITY_EDITOR
		private MeshCollider mc;
		private BoxCollider bc;

		private bool initialized = false;

		private void GetCollider () {
			if (initialized)
				return;
			Collider coll = GetComponent<Collider> ();
			mc = coll as MeshCollider;
			if (mc == null) {
				bc = coll as BoxCollider;
			}
			initialized = true;
		}

		//public static bool ShowGroundTextures = true;
		//public static bool ShowInvalidOnly = false;

		private Color errorColor = Color.red;

		private void OnDrawGizmos () {
			if (Application.isPlaying)
				return;
			ShowGroundTexture ();
		}

		private void OnDrawGizmosSelected () {
			ShowGroundTexture ();
		}

		private static readonly float sizeOffset = .1f;

		private void ShowGroundTexture () {
			if (!EditorPrefs.GetBool ("ShowGroundTextures", false))
				return;

			bool textureIsInvalid = true;

			Color c = errorColor;
			CGVGroundTextureValue type = CGVGroundTextureEnum.GetValue (GroundType, true);
			if (type != null) {
				c = type.Color;
				textureIsInvalid = false;
			}

			if (!EditorPrefs.GetBool ("ShowInvalidOnly", false) || textureIsInvalid) {
				GetCollider ();
				if (mc != null) {
					Gizmos.color = c;
					Gizmos.DrawMesh (mc.sharedMesh, transform.position, transform.rotation, transform.lossyScale);
				}
				else if (bc != null) {
					Matrix4x4 tmpM = Gizmos.matrix;
					Gizmos.color = c;
					Vector3 boxOffset = transform.lossyScale;
					boxOffset = new Vector3 (sizeOffset / boxOffset.x, sizeOffset / boxOffset.y, sizeOffset / boxOffset.z);
					Gizmos.matrix = this.transform.localToWorldMatrix;
					Gizmos.DrawCube (bc.center, bc.size + boxOffset);
					Gizmos.matrix = tmpM;
				}
			}
		}
#endif
	}
}
