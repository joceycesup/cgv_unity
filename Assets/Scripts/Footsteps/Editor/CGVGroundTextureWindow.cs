﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using JuneCodes.FlexibleEnum;

using CGV.Sound;

#pragma warning disable 649
namespace CGV.Editor {
	public class CGVGroundTextureWindow : EditorWindow {
		static int GroundLayer;

		[MenuItem ("Window/CGV/Ground Textures Window")]
		static void Init () {
			CGVGroundTextureWindow window = EditorWindow.GetWindow<CGVGroundTextureWindow> ();
			window.titleContent = CGVEditorUtils.IconContent ("Ground Textures", "Ground Textures");
			window.Show ();
		}

		private void OnEnable () {
			GroundLayer = LayerMask.NameToLayer ("Ground");
		}

		void OnGUI () {
			EditorGUI.BeginChangeCheck ();

			EditorGUILayout.Space ();

			bool sgt = EditorGUILayout.ToggleLeft ("Show Ground Textures", EditorPrefs.GetBool ("ShowGroundTextures", false));
			EditorPrefs.SetBool ("ShowGroundTextures", sgt);
			if (sgt) {
				EditorGUI.indentLevel++;
				bool sio = EditorGUILayout.ToggleLeft ("Show Invalid Textures Only", EditorPrefs.GetBool ("ShowInvalidOnly", false));
				EditorPrefs.SetBool ("ShowInvalidOnly", sio);

				if (GUILayout.Button ("Select Invalid Textures"))
					SelectInvalidTextures ();

				//CGVGroundTexture.ShowInvalidOnly = EditorGUILayout.ToggleLeft("Show Invalid Textures Only", CGVGroundTexture.ShowInvalidOnly);
				EditorGUI.indentLevel--;
			}

			EditorGUILayout.Space ();

			if (GUILayout.Button ("Add Component to Objects on layer Ground"))
				FindGroundTextures ();

			if (EditorGUI.EndChangeCheck ()) {
				UnityEditorInternal.InternalEditorUtility.RepaintAllViews ();
			}
		}

		void SelectInvalidTextures () {
			var gts = GameObject.FindObjectsOfType<CGVGroundTexture> ();
			List<GameObject> invalidGTs = new List<GameObject> (gts.Length);
			foreach (var gt in gts) {
				CGVGroundTextureValue value = CGVGroundTextureEnum.GetValue (gt.GroundType, true);
				if (value == null) {
					invalidGTs.Add (gt.gameObject);
				}
			}
			Selection.objects = invalidGTs.ToArray ();
		}

		void FindGroundTextures () {

			var goArray = FindObjectsOfType<GameObject> ();
			for (var i = 0; i < goArray.Length; i++) {
				if (goArray[i].layer == GroundLayer) {
					Collider coll = goArray[i].GetComponent<Collider> ();
					if (coll != null) {
						CGVGroundTexture ground = goArray[i].GetComponent<CGVGroundTexture> ();
						if (ground == null)
							goArray[i].AddComponent<CGVGroundTexture> ();
					}
				}
			}
		}
	}
}
