﻿using UnityEngine;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text;

#pragma warning disable 649
namespace CGV {
	[Serializable]
	[XmlRoot("sequence_container")]
	public class CGVTargetSequenceContainer {

		[Serializable]
		public class Parameters {

			[Serializable]
			public class Mode {

				[XmlAttribute("player_can_move")]
				public bool playerCanMove;
			}

			[SerializeField]
			[XmlElement("mode")]
			public Mode mode;

		}

		[SerializeField]
		[XmlElement(ElementName = "global_parameters", IsNullable = true)]
		public Parameters global_parameters;

		[SerializeField]
		[XmlArray("sequences")]
		[XmlArrayItem("sequence")]
		public List<CGVTargetSequence> sequences = new List<CGVTargetSequence>();

		public override string ToString() {
			StringBuilder sb = new StringBuilder();

			sb.AppendLine("PlayerCanMove : " + global_parameters.mode.playerCanMove);

			foreach (var s in sequences) {
				sb.AppendLine(s.ToString());
			}

			return sb.ToString();
		}
	}
}
