﻿using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 649
namespace CGV.Player {
	/// <summary>
	/// This component is the base for a training session. It references a list of training targets that will trigger other targets when hit.
	/// When a certain number of its targets have been hit, it will be disabled and fire a callback (for example, : activating another training session, opening a door, etc.)
	/// </summary>
	public class CGVTrainingTargetSession : MonoBehaviour {
		/// <summary>
		/// List of targets that will be watched during the session.
		/// </summary>
		[Header ("Training Session")]
		[NaughtyAttributes.ReorderableList]
		[SerializeField] protected List<CGVTrainingTarget> Targets;

		/// <summary>
		/// Number of targets to hit for the session to end.
		/// </summary>
		[Min (1)]
		[SerializeField] protected int _TargetsToShoot = 1;
		protected int TargetsToShoot;

		/// <summary>
		/// Training session ended callback. Called when its targets have been hit enough times.
		/// </summary>
		[SerializeField] protected UltEvents.UltEvent OnSessionOver;

		private void OnTriggerEnter (Collider other) {
			if (other.GetComponent<CGVPlayerCharacter> ()) {
				Debug.Log ("Session start");
				Targets[0].gameObject.SetActive (true);
			}
		}

		private void Awake () {
			// Remove null targets in the list if there are any in the list
			for (int i = Targets.Count - 1; i >= 0; --i) {
				if (Targets[i] == null)
					Targets.RemoveAt (i);
			}
		}

		private void OnEnable () {
			TargetsToShoot = _TargetsToShoot;
			foreach (CGVTrainingTarget target in Targets) {
				target.OnGotKilled -= TargetGotHit;
			}
		}

		private void OnDisable () {
			foreach (CGVTrainingTarget target in Targets) {
				target.OnGotKilled -= TargetGotHit;
			}
		}

		private void TargetGotHit (CGVTarget hit, CGVPlayerCharacter player) {
			if (TargetsToShoot > 0)
				TargetsToShoot--;
			if (TargetsToShoot == 0) {
				foreach (CGVTrainingTarget target in Targets) {
					target.OnGotKilled -= TargetGotHit;
					target.KillAndSpawn (false);
				}
				OnSessionOver.Invoke ();
				gameObject.SetActive (false);
			}
		}
	}
}
