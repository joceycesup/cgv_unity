﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CGV.Sound;
using CGV.Equipment;

#if CGV_NETWORK
using Photon.Pun;
#endif

#pragma warning disable 649
namespace CGV.Player {
	[SelectionBase]
	[RequireComponent (typeof (CGVBoundsProvider))]
	public class CGVTarget :
#if CGV_NETWORK
		MonoBehaviourPun
#else
		MonoBehaviour
#endif
	{

		public enum TargetState {
			Inactive,
			None,
			Stunned,
			Drunk, // Well, you never know what the Game Designers might want to do with the project...
			Dead,

			Spawned,
			Resurected
		}

		public delegate void TargetStateChangeEvent (CGVTarget target, TargetState newState, TargetState oldState);
		public TargetStateChangeEvent OnTargetStateChanged;

		private TargetState _State = TargetState.Inactive;
		public TargetState State {
			get { return _State; }
			protected set {
				if (_State == value)
					return;
				OnTargetStateChanged?.Invoke (this, value, _State);
				StateChanged (value, _State);
				Debug.LogFormat ("{0} changes from {1} to {2}", Name, _State, value);
				_State = value;
			}
		}

		protected virtual void StateChanged (TargetState newState, TargetState oldState) { }

		public delegate void GotKilledByEvent (CGVTarget target, CGVPlayerCharacter player);

		[SerializeField]
		private string _Name = "";
		public string Name {
			get { return _Name; }
			protected set {
				if (value == null)
					_Name = string.Empty;
				else
					_Name = value;
			}
		}

		[Header ("Wwise Events")]
		[SerializeField] protected AK.Wwise.Event _HitEvent;
		public AK.Wwise.Event HitEvent { get { return _HitEvent; } }
		[SerializeField] protected AK.Wwise.Event DieEvent;
		[SerializeField] protected AK.Wwise.Event IdleEvent;

		[Header ("Health")]
		[NaughtyAttributes.SpecialNegativeValue ("Invulnerable", "Fixed value", false)]
		[SerializeField] protected float _MaxHealth = 1f;
		public float MaxHealth { get { return _MaxHealth; } }
		[NaughtyAttributes.SpecialNegativeValue ("Max Health", "Fixed value", false)]
		[SerializeField] protected float _Health = -1f;
		public float Health { get { return _Health; } protected set { _Health = value; } }

		[Header ("Auto Heal")]
		[SerializeField] protected bool ReplenishLife = true;
		[NaughtyAttributes.SpecialNegativeValue ("Immediate", "Fixed delay", false)]
		[NaughtyAttributes.ShowIf ("ReplenishLife")]
		[SerializeField] protected float ReplenishDelay = -1f;
		[Min (float.Epsilon)]
		[NaughtyAttributes.ShowIf ("ReplenishLife")]
		[SerializeField] protected float ReplenishRate = 0.1f;
		private float ReplenishLifeTime = -1f;

		public GotKilledByEvent OnGotKilled;

		public CGVBoundsProvider Bounds { get; private set; }

		protected virtual void Awake () {
			Bounds = GetComponent<CGVBoundsProvider> ();
		}

		protected virtual void Update () {
			if (ReplenishLife && Time.time >= ReplenishLifeTime && _Health < MaxHealth) {
				_Health += ReplenishRate * Time.deltaTime;
				if (_Health >= MaxHealth)
					_Health = MaxHealth;
			}
		}

		public virtual void ResetCharacter () {
			if (Health < 0f)
				Health = MaxHealth;
		}

		protected virtual void OnEnable () {
			IdleEvent?.Post (gameObject);
			IsVisible = true;
		}

		protected bool _IsVisible = true;
		public bool IsVisible { get { return _IsVisible && isActiveAndEnabled; } set { _IsVisible = value; } }

		public virtual void TakeDamage (float damage, CGVPlayerCharacter fromPlayer, RaycastHit hit, ICGVWeapon weapon) {
			Debug.LogFormat ("{0} took {1} damage from {2}", this, damage, fromPlayer);
			TriggerHitEvent (damage, fromPlayer, hit, weapon);

			if (MaxHealth > 0f && Health > 0f) {
				if (damage >= Health) {
					Health = 0f;
					Die (fromPlayer);
				}
				else {
					Health -= damage;
					if (ReplenishLife) {
						ReplenishLifeTime = Time.time + ReplenishDelay;
					}
				}
			}
		}

		public void TakeDamage (float damage, CGVPlayerCharacter fromPlayer, ICGVWeapon weapon) {
			TakeDamage (damage, fromPlayer, new RaycastHit (), weapon);
		}

		protected virtual void TriggerHitEvent (float damage, CGVPlayerCharacter fromPlayer, RaycastHit hit, ICGVWeapon weapon) {
			//Debug.Log ("TriggerHitEvent");
			if (weapon != null && hit.collider != null)
				CGVSingleShotSoundEvent.PlaySingleShotOnTransform (HitEvent, hit.collider.transform, hit.point - hit.collider.transform.position);
			else
				CGVSingleShotSoundEvent.PlaySingleShotAtLocation (HitEvent, transform.position);

			//_HitEvent?.Post (gameObject);
		}

		protected virtual bool Die (CGVPlayerCharacter player) {
			if (Health < 0f)
				return false;
			Health = -1f;
			IsVisible = false;
			State = TargetState.Dead;
			//OnTargetStateChanged?.Invoke (this, TargetState.Died);
			OnGotKilled?.Invoke (this, player);
			DieEvent?.Post (gameObject);
			return true;
		}
	}
}
