﻿using UnityEngine;
using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text;

#pragma warning disable 649
namespace CGV {
	[Serializable]
	public class CGVTargetSequence {
		[Serializable]
		public class Target {
			[XmlAttribute("id")] public int id;
			[XmlAttribute("delay")] public int delay;
			[XmlAttribute("duration")] public int duration;
			[XmlAttribute("wait_for_target_spawn_id")] public int wait_for_target_spawn_id;
			[XmlAttribute("wait_for_target_destroyed_id")] public int wait_for_target_destroyed_id;

			[XmlAttribute("scale")] public float scale;
			[XmlAttribute("radius")] public float radius;
			[XmlAttribute("azimuth")] public float azimuth;
			[XmlAttribute("elevation")] public float elevation;

			[XmlAttribute("is_moving")] public bool is_moving;
			[XmlAttribute("is_looping")] public bool is_looping;

			[XmlAttribute("prefab")] public string prefab;
			[XmlAttribute("epicenter")] public string epicenter;

			[SerializeField]
			[XmlArray(ElementName = "positions", IsNullable = true)]
			[XmlArrayItem("position")]
			public List<TargetPosition> positions;

			[XmlAttribute("orientation_type")]
			public OrientationType orientation_type;

			public override string ToString() {
				StringBuilder sb = new StringBuilder();

				sb.AppendLine("target " + id);

				sb.AppendLine((positions == null ? 0 : positions.Count).ToString());

				return sb.ToString();
			}
		}

		[Serializable]
		public class TargetPosition {
			[XmlAttribute("waiting_time")] public int waiting_time;

			[XmlAttribute("distance")] public float distance;
			[XmlAttribute("speed_to")] public float speed_to;
			[XmlAttribute("azimuth")] public float azimuth;
			[XmlAttribute("elevation")] public float elevation;

			[XmlAttribute("initial")] public bool initial;
			[XmlAttribute("move_towards_player")] public bool move_towards_player;

			[XmlAttribute("new_epicenter")] public string new_epicenter;
		}

		[Serializable]
		public enum TriggerType {
			DelayFromStart,
			DelayFromTrigger,
			DelayFromPreviousSequence
		}

		[Serializable]
		public enum OrientationType {
			PlayerOrientation,
			EpicenterOrientation,
			OtherTarget
		}

		[Serializable]
		public class Trigger {

			[XmlAttribute("trigger")]
			public TriggerType Type;
		}

		[XmlAttribute("id")]
		public int id;

		[XmlElement(ElementName = "player_teleport_to", IsNullable = true)]
		public string playerTeleportLocation;

		[SerializeField]
		[XmlElement("trigger")]
		public Trigger trigger = new Trigger();

		[SerializeField]
		[XmlArray("targets")]
		[XmlArrayItem("target")]
		public List<Target> targets = new List<Target>();

		public override string ToString() {
			StringBuilder sb = new StringBuilder();

			sb.AppendLine(id.ToString() + " : " + trigger.Type);

			foreach (var t in targets) {
				sb.AppendLine(t.ToString());
			}

			return sb.ToString();
		}
	}
}
