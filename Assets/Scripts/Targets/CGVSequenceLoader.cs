﻿using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

#pragma warning disable 649
namespace CGV {
	public class CGVSequenceLoader : MonoBehaviour {
		[SerializeField]
		private AK.Wwise.AcousticTexture tex;

		private static readonly string xmlFolder = "Sequences";
		private static string _xmlPath;
		private static string xmlPath {
			get {
				if (string.IsNullOrEmpty(_xmlPath)) {
					string path = Application.dataPath;
#if UNITY_EDITOR
					path = Directory.GetParent(path).FullName;
#endif
					_xmlPath = Path.Combine(path, xmlFolder);
				}
				return _xmlPath;
			}
		}

		[SerializeField]
		[ContextMenuItem("Open Folder", "OpenFolder")]
		private string xmlName;

		[SerializeField]
		private TextAsset defaultXML;

		[SerializeField]
		[ContextMenuItem("Save", "SaveSequence")]
		[ContextMenuItem("Load", "LoadSequence")]
		public CGVTargetSequenceContainer sequences;

		private void SaveSequence() {
			Save(sequences, xmlName);
		}

		private void LoadSequence() {
			sequences = Load(xmlName);
		}

		private void OpenFolder() {
#if UNITY_EDITOR
			CreateXMLPath();
			EditorUtility.RevealInFinder(xmlPath);
#endif
		}

		private static void CreateXMLPath() {
			if (!Directory.Exists(xmlPath)) {
				Directory.CreateDirectory(xmlPath);
			}
		}

		public static bool CreateDefaultXML(string xmlName, TextAsset defaultXML) {
			if (string.IsNullOrEmpty(xmlName))
				return false;

			CreateXMLPath();

			bool res;
			string path = Path.Combine(xmlPath, xmlName);
			if (res = !File.Exists(path)) {
				StreamWriter streamWriter = File.CreateText(path);
				streamWriter.Write(defaultXML.text);
				streamWriter.Flush();
				streamWriter.Close();
			}
			return res;
		}

		private static string CheckXMLName(string name) {
			if (!name.EndsWith(".xml", System.StringComparison.InvariantCultureIgnoreCase)) {
				name += ".xml";
			}
			return name;
		}

		public static CGVTargetSequenceContainer Load(string xmlName) {
			if (string.IsNullOrEmpty(xmlName))
				return null;

			CreateXMLPath();

			string path = Path.Combine(xmlPath, CheckXMLName(xmlName));
			if (!File.Exists(path)) {
				return null;
			}

			CGVTargetSequenceContainer sequences = null;

			using (StreamReader streamReader = File.OpenText(path)) {
				XmlSerializer serializer = new XmlSerializer(typeof(CGVTargetSequenceContainer));

				string content = streamReader.ReadToEnd();
				Debug.Log(content);
				StringReader reader = new StringReader(content);

				sequences = serializer.Deserialize(reader) as CGVTargetSequenceContainer;
			}

			return sequences;
		}

		public static bool Save(CGVTargetSequenceContainer sequences, string xmlName) {
			if (sequences == null)
				return false;
			if (string.IsNullOrEmpty(xmlName))
				return false;

			CreateXMLPath();

			string path = Path.Combine(xmlPath, CheckXMLName(xmlName));

			using (FileStream fileStream = File.OpenWrite(path)) { // Should create as well
				XmlSerializer serializer = new XmlSerializer(typeof(CGVTargetSequenceContainer));

				serializer.Serialize(fileStream, sequences);
			}

			return true;
		}
		//*/
	}
}
