﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CGV.Equipment;

#pragma warning disable 649
namespace CGV.Player {
	public class CGVTrainingTarget : CGVTarget {

		[Header ("Training Target")]
		[NaughtyAttributes.ReorderableList]
		[SerializeField] protected List<CGVTrainingTarget> NextTargets;

		[NaughtyAttributes.ShowIf ("HasNextTargets")]
		[NaughtyAttributes.SpecialNegativeValue ("Infinite", false)]
		[SerializeField] protected float NextTargetLifetime = -1f;

		[NaughtyAttributes.ShowIf ("HasMultipleTargets")]
		[NaughtyAttributes.SpecialNegativeValue ("No random", false)]
		[SerializeField] protected int RandomTargetsPulled = -1;

		[Header ("WWise")]
		[SerializeField] protected AK.Wwise.Event SpawnEvent;
		//[SerializeField] protected AK.Wwise.Switch TextureSwitch;

		private IEnumerator GrowOldCoroutine;

		private bool HasNextTargets () {
			return NextTargets.Count > 0;
		}

		private bool HasMultipleTargets () {
			return NextTargets.Count > 1;
		}

		protected override void Awake () {
			base.Awake ();
			//TextureSwitch?.SetValue (gameObject);
			for (int i = NextTargets.Count - 1; i >= 0; --i) {
				if (NextTargets[i] == this || NextTargets[i] == null)
					NextTargets.RemoveAt (i);
			}
			Name = "Target " + (Mathf.Abs (gameObject.GetInstanceID ()) % 256);
		}

		protected override void OnEnable () {
			base.OnEnable ();
			SpawnEvent?.Post (gameObject);
			Health = MaxHealth;
			IsVisible = true;
			NotifyPlayers ();
			CGVPlayerCharacter.OnCharacterPossessed += NotifyPlayers;
		}

		private void OnDisable () {
			CGVPlayerCharacter.OnCharacterPossessed -= NotifyPlayers;
		}

		protected void NotifyPlayers (CGVPlayerCharacter character = null, int teamID = 0) {
			CGVPlayerSight.OnSoundEmitted?.Invoke (this, CGVPlayerSight.DetectionRadius.AlwaysHeard, CGVPlayerSight.InfiniteDelay);
		}

		public override void TakeDamage (float damage, CGVPlayerCharacter fromPlayer, RaycastHit hit, ICGVWeapon weapon) {
			TriggerHitEvent (damage, fromPlayer, hit, weapon);
			Die (fromPlayer);
		}

		protected override bool Die (CGVPlayerCharacter player) {
			KillAndSpawn (true);
			return true;
		}

		public void KillAndSpawn (bool spawnNextTargets) {
			if (GrowOldCoroutine != null) {
				StopCoroutine (GrowOldCoroutine);
				GrowOldCoroutine = null;
			}
			if (spawnNextTargets) {
				if (RandomTargetsPulled <= 0 || RandomTargetsPulled >= NextTargets.Count) {
					foreach (CGVTrainingTarget target in NextTargets) {
						SpawnOther (target);
					}
				}
				else {
					List<CGVTrainingTarget> newList = new List<CGVTrainingTarget> (NextTargets);
					for (int i = 0; i < RandomTargetsPulled; ++i) {
						int randomIndex = Random.Range (0, newList.Count);
						SpawnOther (newList[randomIndex]);
						newList.RemoveAt (randomIndex);
					}
				}
			}
			base.Die (null);
			gameObject.SetActive (false);
		}

		protected void SpawnOther (CGVTrainingTarget target) {
			if (target == this || target == null || target.isActiveAndEnabled)
				return;
			target.gameObject.SetActive (true);
			if (NextTargetLifetime > 0f)
				target.GrowOld (NextTargetLifetime);
		}

		private void GrowOld (float lifetime) {
			StartCoroutine (GrowOldCoroutine = GrowOldAndDie (lifetime));
		}

		private IEnumerator GrowOldAndDie (float lifetime) {
			yield return new WaitForSeconds (lifetime);
			GrowOldCoroutine = null;
			Die (null);
		}
	}
}
