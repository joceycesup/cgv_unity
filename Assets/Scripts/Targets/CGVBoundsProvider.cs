﻿using UnityEngine;

namespace CGV.Player {
	public class CGVBoundsProvider : MonoBehaviour {

		private Renderer[] renderers;
		private Collider[] colliders;

		void OnEnable () {
			Refresh ();
		}

		public void Refresh () {
			renderers = transform.GetComponentsInChildren<Renderer> ();
			colliders = transform.GetComponentsInChildren<Collider> ();
		}

#if CGV_DEBUG
		void OnDrawGizmosSelected () {
			if (!Application.isPlaying)
				return;
			Bounds renderBounds = GetRenderBounds ();
			Gizmos.color = Color.magenta;
			Gizmos.DrawWireCube (renderBounds.center, renderBounds.extents * 2);//*
			Bounds colliderBounds = GetColliderBounds ();
			Gizmos.color = Color.cyan;
			Gizmos.DrawWireCube (colliderBounds.center, colliderBounds.extents * 2);//*/
		}
#endif

		public Bounds GetRenderBounds () {
			Bounds bounds = new Bounds ();

			if (renderers.Length > 0) {
				//Find first enabled renderer to start encapsulate from it
				foreach (Renderer renderer in renderers) {
					if (renderer.enabled) {
						bounds = renderer.bounds;
						break;
					}
				}

				//Encapsulate for all renderers
				foreach (Renderer renderer in renderers) {
					if (renderer.enabled) {
						bounds.Encapsulate (renderer.bounds);
					}
				}
			}

			return bounds;
		}

		public Bounds GetColliderBounds () {
			Bounds bounds = new Bounds ();

			if (colliders.Length > 0) {
				//Find first enabled collider to start encapsulate from it
				foreach (Collider collider in colliders) {
					if (collider.enabled) {
						bounds = collider.bounds;
						break;
					}
				}

				//Encapsulate for all colliders
				foreach (Collider collider in colliders) {
					if (collider.enabled) {
						bounds.Encapsulate (collider.bounds);
					}
				}
			}

			return bounds;
		}
	}
}
