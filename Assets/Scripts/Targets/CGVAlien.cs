﻿using System.Collections;
using UnityEngine;

using JuneCodes;

#pragma warning disable 649
namespace CGV.Player {
	public class CGVAlien : CGVTarget {

		//private static CGVObjectPool<CGVAlien> Pool;
		private static JCTypePool<CGVAlien> Pool;

		[SerializeField]
		private float DieDelay = 1f;

		[Header ("Wwise Events")]
		[SerializeField] protected AK.Wwise.Event FrightenedEvent;

		bool IsFrightened = false;

		protected override void OnEnable () {
			base.OnEnable ();
			gameObject.GetComponentInChildren<Renderer> ().material.color = Color.white;
			IsVisible = true;
			NotifyPlayers ();
			CGVPlayerCharacter.OnCharacterPossessed += NotifyPlayers;
		}

		private void OnDisable () {
			CGVPlayerCharacter.OnCharacterPossessed -= NotifyPlayers;
		}

		protected void NotifyPlayers (CGVPlayerCharacter character = null, int teamID = 0) {
			CGVPlayerSight.OnSoundEmitted?.Invoke (this, CGVPlayerSight.DetectionRadius.AlwaysHeard, CGVPlayerSight.InfiniteDelay);
		}

		protected override void Update () {
			base.Update ();
			if (AkSpatialAudioListener.TheSpatialAudioListener == null)
				return;
			if (!IsFrightened && Vector3.Distance (AkSpatialAudioListener.TheSpatialAudioListener.transform.position, transform.position) < 10f) {
				FrightenedEvent?.Post (gameObject);
				CGVPlayerSight.OnSoundEmitted?.Invoke (this, CGVPlayerSight.DetectionRadius.Level3, CGVPlayerSight.InfiniteDelay);
				IsFrightened = true;
			}
		}

		protected override bool Die (CGVPlayerCharacter player) {
			bool res = base.Die (player);
			if (res) {
				StartCoroutine (DieWithDelay (DieDelay));
				gameObject.GetComponentInChildren<Renderer> ().material.color = Color.red;
			}
			return res;
		}

		private IEnumerator DieWithDelay (float delay) {
			yield return new WaitForSeconds (delay);
			if (Pool == null)
				Pool = JCTypePool<CGVAlien>.GetPool ();
			//Pool = CGVObjectPool<CGVAlien>.GetPool ();
			if (Pool != null)
				Pool.Store (this);
		}
	}
}
