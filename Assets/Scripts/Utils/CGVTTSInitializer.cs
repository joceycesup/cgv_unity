﻿using UnityEngine;

namespace CGV {
	public class CGVTTSInitializer : MonoBehaviour {

		[SerializeField]
		private CGVTTSSettings Settings;

		void Start () {
			UAP_AccessibilityManager.EnableAccessibility (true);
			if (!string.IsNullOrEmpty (Settings.Language))
				UAP_AccessibilityManager.SetLanguage (Settings.Language);
			UAP_AccessibilityManager.SetSpeechRate (Settings.SpeechRate);
			//Debug.Log (UAP_AccessibilityManager.UseWindowsTTS ());
		}
	}
}
