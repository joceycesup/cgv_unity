﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGV {
	public abstract class CGVClonableScriptableObject<T> : ScriptableObject where T : CGVClonableScriptableObject<T> {

		public virtual bool Clone { get { return true; } }

		public bool IsCloned { get; private set; } = false;

		public T GetClone () {
			if (!Clone)
				return (T)this;
			T res = (T)Instantiate (this);
			res.InitClone ();
			res.IsCloned = true;
			return res;
		}

		protected abstract void InitClone ();
	}
}
