﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;

#pragma warning disable 649
namespace CGV {
	public class CGVLog : MonoBehaviour {
		private class LogWriter {
			public string name { get; private set; }
			private StreamWriter writer;
			private List<string> parameters;

			public LogWriter(string subFolder, string filename) {
				writer = new StreamWriter("Logs/" + subFolder + "/" + filename + ".csv", true);
				writer.AutoFlush = true;
			}
		}

		private const string commaSeparator = ";";

		private Dictionary<string, CGVLog> Logs;

		void Start() {

		}

		void Update() {

		}
	}
}
