﻿using UnityEngine;

namespace CGV {
	public class CGVDebugUtils {

		public static readonly Vector3[] _DodecahedronVertices;
		public static readonly Vector2Int[] _DodecahedronEdges;

		public static readonly Vector3[] _TruncatedIcosahedronVertices;
		public static readonly Vector2Int[] _TruncatedIcosahedronEdges = new Vector2Int[90];

		static CGVDebugUtils () {
			float goldenRatio = (1 + Mathf.Sqrt (5)) / 2;
			int i = 0;

			float a = 0.5f;
			float b = 0.5f * 1f / goldenRatio;
			float c = 0.5f * (2f - goldenRatio);

			_DodecahedronVertices = new Vector3[] {
				new Vector3 (c, 0, a),
				new Vector3 (-c, 0, a),
				new Vector3 (-b, b, b),
				new Vector3 (0, a, c),
				new Vector3 (b, b, b),
				new Vector3 (b, -b, b),
				new Vector3 (0, -a, c),
				new Vector3 (-b, -b, b),
				new Vector3 (c, 0, -a),
				new Vector3 (-c, 0, -a),
				new Vector3 (-b, -b, -b),
				new Vector3 (0, -a, -c),
				new Vector3 (b, -b, -b),
				new Vector3 (b, b, -b),
				new Vector3 (0, a, -c),
				new Vector3 (-b, b, -b),
				new Vector3 (a, c, 0),
				new Vector3 (-a, c, 0),
				new Vector3 (-a, -c, 0),
				new Vector3 (a, -c, 0)
			};
			for (i = 0; i < _DodecahedronVertices.Length; ++i) {
				_DodecahedronVertices[i] = _DodecahedronVertices[i].normalized;
			}

			_DodecahedronEdges = new Vector2Int[] {
				new Vector2Int(0,1),
				new Vector2Int(0,4),
				new Vector2Int(0,5),
				new Vector2Int(1,2),
				new Vector2Int(1,7),
				new Vector2Int(2,3),
				new Vector2Int(2,17),
				new Vector2Int(3,4),
				new Vector2Int(3,14),
				new Vector2Int(4,16),
				new Vector2Int(5,6),
				new Vector2Int(5,19),
				new Vector2Int(6,7),
				new Vector2Int(6,11),
				new Vector2Int(7,18),
				new Vector2Int(8,9),
				new Vector2Int(8,12),
				new Vector2Int(8,13),
				new Vector2Int(9,10),
				new Vector2Int(9,15),
				new Vector2Int(10,11),
				new Vector2Int(10,18),
				new Vector2Int(11,12),
				new Vector2Int(12,19),
				new Vector2Int(13,14),
				new Vector2Int(13,16),
				new Vector2Int(14,15),
				new Vector2Int(15,17),
				new Vector2Int(16,19),
				new Vector2Int(17,18)
			};

			Vector3 vA = new Vector3 (0f, 1f, 3f * goldenRatio);
			Vector3 vB = new Vector3 (1f, 2f + goldenRatio, 2f * goldenRatio);
			Vector3 vC = new Vector3 (goldenRatio, 2f, goldenRatio * goldenRatio * goldenRatio);
			_TruncatedIcosahedronVertices = new Vector3[60];
			ShiftPermutations (new Vector3 (+vA.x, +vA.y, +vA.z), ref _TruncatedIcosahedronVertices, 0);
			ShiftPermutations (new Vector3 (+vA.x, +vA.y, -vA.z), ref _TruncatedIcosahedronVertices, 3);
			ShiftPermutations (new Vector3 (+vA.x, -vA.y, +vA.z), ref _TruncatedIcosahedronVertices, 6);
			ShiftPermutations (new Vector3 (+vA.x, -vA.y, -vA.z), ref _TruncatedIcosahedronVertices, 9);

			ShiftPermutations (new Vector3 (+vB.x, +vB.y, +vB.z), ref _TruncatedIcosahedronVertices, 12);
			ShiftPermutations (new Vector3 (+vB.x, +vB.y, -vB.z), ref _TruncatedIcosahedronVertices, 15);
			ShiftPermutations (new Vector3 (+vB.x, -vB.y, +vB.z), ref _TruncatedIcosahedronVertices, 18);
			ShiftPermutations (new Vector3 (+vB.x, -vB.y, -vB.z), ref _TruncatedIcosahedronVertices, 21);
			ShiftPermutations (new Vector3 (-vB.x, +vB.y, +vB.z), ref _TruncatedIcosahedronVertices, 24);
			ShiftPermutations (new Vector3 (-vB.x, +vB.y, -vB.z), ref _TruncatedIcosahedronVertices, 27);
			ShiftPermutations (new Vector3 (-vB.x, -vB.y, +vB.z), ref _TruncatedIcosahedronVertices, 30);
			ShiftPermutations (new Vector3 (-vB.x, -vB.y, -vB.z), ref _TruncatedIcosahedronVertices, 33);

			ShiftPermutations (new Vector3 (+vC.x, +vC.y, +vC.z), ref _TruncatedIcosahedronVertices, 36);
			ShiftPermutations (new Vector3 (+vC.x, +vC.y, -vC.z), ref _TruncatedIcosahedronVertices, 39);
			ShiftPermutations (new Vector3 (+vC.x, -vC.y, +vC.z), ref _TruncatedIcosahedronVertices, 42);
			ShiftPermutations (new Vector3 (+vC.x, -vC.y, -vC.z), ref _TruncatedIcosahedronVertices, 45);
			ShiftPermutations (new Vector3 (-vC.x, +vC.y, +vC.z), ref _TruncatedIcosahedronVertices, 48);
			ShiftPermutations (new Vector3 (-vC.x, +vC.y, -vC.z), ref _TruncatedIcosahedronVertices, 51);
			ShiftPermutations (new Vector3 (-vC.x, -vC.y, +vC.z), ref _TruncatedIcosahedronVertices, 54);
			ShiftPermutations (new Vector3 (-vC.x, -vC.y, -vC.z), ref _TruncatedIcosahedronVertices, 57);
			for (i = 0; i < _TruncatedIcosahedronVertices.Length; ++i) {
				_TruncatedIcosahedronVertices[i] = _TruncatedIcosahedronVertices[i].normalized;
			}

			_TruncatedIcosahedronEdges[0] = new Vector2Int (12, 37);
			_TruncatedIcosahedronEdges[1] = new Vector2Int (13, 38);
			_TruncatedIcosahedronEdges[2] = new Vector2Int (14, 36);
			_TruncatedIcosahedronEdges[3] = new Vector2Int (15, 49);
			_TruncatedIcosahedronEdges[4] = new Vector2Int (16, 50);
			_TruncatedIcosahedronEdges[5] = new Vector2Int (17, 48);
			_TruncatedIcosahedronEdges[6] = new Vector2Int (18, 40);
			_TruncatedIcosahedronEdges[7] = new Vector2Int (19, 41);
			_TruncatedIcosahedronEdges[8] = new Vector2Int (20, 39);
			_TruncatedIcosahedronEdges[9] = new Vector2Int (21, 52);
			_TruncatedIcosahedronEdges[10] = new Vector2Int (22, 53);
			_TruncatedIcosahedronEdges[11] = new Vector2Int (23, 51);
			_TruncatedIcosahedronEdges[12] = new Vector2Int (24, 43);
			_TruncatedIcosahedronEdges[13] = new Vector2Int (25, 44);
			_TruncatedIcosahedronEdges[14] = new Vector2Int (26, 42);
			_TruncatedIcosahedronEdges[15] = new Vector2Int (27, 55);
			_TruncatedIcosahedronEdges[16] = new Vector2Int (28, 56);
			_TruncatedIcosahedronEdges[17] = new Vector2Int (29, 54);
			_TruncatedIcosahedronEdges[18] = new Vector2Int (30, 46);
			_TruncatedIcosahedronEdges[19] = new Vector2Int (31, 47);
			_TruncatedIcosahedronEdges[20] = new Vector2Int (32, 45);
			_TruncatedIcosahedronEdges[21] = new Vector2Int (33, 58);
			_TruncatedIcosahedronEdges[22] = new Vector2Int (34, 59);
			_TruncatedIcosahedronEdges[23] = new Vector2Int (35, 57);
			for (i = 0; i < 6; ++i) {
				_TruncatedIcosahedronEdges[i + 24] = new Vector2Int (i, i + 6);
				_TruncatedIcosahedronEdges[i + 30] = new Vector2Int (i, i + 36);
				_TruncatedIcosahedronEdges[i + 36] = new Vector2Int (i + 6, i + 42);
				_TruncatedIcosahedronEdges[i + 42] = new Vector2Int (i, i + 48);
				_TruncatedIcosahedronEdges[i + 48] = new Vector2Int (i + 6, i + 54);
			}
			for (i = 0; i < 12; ++i) {
				_TruncatedIcosahedronEdges[i + 54] = new Vector2Int (i + 12, i + 24);
				_TruncatedIcosahedronEdges[i + 66] = new Vector2Int (i + 12, i + 36);
				_TruncatedIcosahedronEdges[i + 78] = new Vector2Int (i + 24, i + 48);
			}
		}

		private static void ShiftPermutations (Vector3 vector, ref Vector3[] dst, int index) {
			dst[index] = vector;
			dst[index + 1] = new Vector3 (vector.y, vector.z, vector.x);
			dst[index + 2] = new Vector3 (vector.z, vector.x, vector.y);
		}

		private static Vector3[] CubePoint = new Vector3[8];

		public static void DrawCube (Vector3 position, float size, bool drawCenter, Color color, float duration = 0.0f, bool depthTest = true) {
			float[] axes = { position.x + size, position.x - size, position.y + size, position.y - size, position.z + size, position.z - size };

			CubePoint[0] = new Vector3 (axes[0], axes[2], axes[4]);
			CubePoint[1] = new Vector3 (axes[0], axes[3], axes[4]);
			CubePoint[2] = new Vector3 (axes[0], axes[2], axes[5]);
			CubePoint[3] = new Vector3 (axes[0], axes[3], axes[5]);
			CubePoint[4] = new Vector3 (axes[1], axes[2], axes[4]);
			CubePoint[5] = new Vector3 (axes[1], axes[3], axes[4]);
			CubePoint[6] = new Vector3 (axes[1], axes[2], axes[5]);
			CubePoint[7] = new Vector3 (axes[1], axes[3], axes[5]);

			Debug.DrawLine (CubePoint[0], CubePoint[4], color, duration, depthTest);
			Debug.DrawLine (CubePoint[1], CubePoint[5], color, duration, depthTest);
			Debug.DrawLine (CubePoint[2], CubePoint[6], color, duration, depthTest);
			Debug.DrawLine (CubePoint[3], CubePoint[7], color, duration, depthTest);

			Debug.DrawLine (CubePoint[0], CubePoint[1], color, duration, depthTest);
			Debug.DrawLine (CubePoint[2], CubePoint[3], color, duration, depthTest);
			Debug.DrawLine (CubePoint[4], CubePoint[5], color, duration, depthTest);
			Debug.DrawLine (CubePoint[6], CubePoint[7], color, duration, depthTest);

			Debug.DrawLine (CubePoint[0], CubePoint[2], color, duration, depthTest);
			Debug.DrawLine (CubePoint[1], CubePoint[3], color, duration, depthTest);
			Debug.DrawLine (CubePoint[4], CubePoint[6], color, duration, depthTest);
			Debug.DrawLine (CubePoint[5], CubePoint[7], color, duration, depthTest);

			if (drawCenter) {
				Debug.DrawLine (CubePoint[0], CubePoint[7], color, duration, depthTest);
				Debug.DrawLine (CubePoint[1], CubePoint[6], color, duration, depthTest);
				Debug.DrawLine (CubePoint[2], CubePoint[5], color, duration, depthTest);
				Debug.DrawLine (CubePoint[3], CubePoint[4], color, duration, depthTest);
			}
		}

		public static void DrawEmpty (Vector3 position, float size, Color color, float duration = 0.0f, bool depthTest = true) {
			Debug.DrawLine (position + Vector3.right * size, position - Vector3.right * size, color, duration, depthTest);
			Debug.DrawLine (position + Vector3.up * size, position - Vector3.up * size, color, duration, depthTest);
			Debug.DrawLine (position + Vector3.forward * size, position - Vector3.forward * size, color, duration, depthTest);
		}

		public static void DrawCube (Vector3 position, float size, Color color, float duration = 0.0f, bool depthTest = true) {
			DrawCube (position, size, false, color, duration, depthTest);
		}

		public static void DrawCube (Vector3 position, bool drawCenter, Color color, float duration = 0.0f, bool depthTest = true) {
			DrawCube (position, 1f, drawCenter, color, duration, depthTest);
		}

		public static void DrawCube (Vector3 position, Color color, float duration = 0.0f, bool depthTest = true) {
			DrawCube (position, 1f, false, color, duration, depthTest);
		}

		public static void DrawCircle (Vector3 position, Vector3 up, float radius, Color color, int segments = 8, float duration = 0.0f, bool depthTest = true) {
			Vector3 p1, p2;

			Vector3 corner = new Vector3 (radius, 0f, 0f);
			Quaternion rotation = Quaternion.LookRotation (up);
			p1 = position + rotation * corner;
			for (int i = 1; i <= segments; ++i) {
				p2 = position + rotation * Quaternion.Euler (0f, 0f, i * (360f / segments)) * corner;

				Debug.DrawLine (p1, p2, color, duration, depthTest);

				p1 = p2;
			}
		}

		public static void DrawDodecahedron (Vector3 position, float radius, Color color, float duration = 0.0f, bool depthTest = true) {
			for (int i = 0; i < _DodecahedronEdges.Length; ++i) {
				Debug.DrawLine (position + _DodecahedronVertices[_DodecahedronEdges[i].x] * radius, position + _DodecahedronVertices[_DodecahedronEdges[i].y] * radius, color, duration, depthTest);
			}
		}

		public static void DrawIcosahedron (Vector3 position, float radius, Color color, float duration = 0.0f, bool depthTest = true) {
			for (int i = 0; i < _TruncatedIcosahedronEdges.Length; ++i) {
				Debug.DrawLine (position + _TruncatedIcosahedronVertices[_TruncatedIcosahedronEdges[i].x] * radius, position + _TruncatedIcosahedronVertices[_TruncatedIcosahedronEdges[i].y] * radius, color, duration, depthTest);
			}
		}

		public static void DrawCone (Vector3 position, Vector3 direction, float angle, float distance, Color color, int segments = 8, int capSegments = 3, float duration = 0.0f, bool depthTest = true) {
			if (angle <= 0f || distance <= 0f || segments < 2)
				return;
			direction = direction.normalized;
			Quaternion rotation = Quaternion.LookRotation (direction);
			Vector3[] corners = new Vector3[1 + capSegments];
			Vector3 capPos = new Vector3 (0f, 0f, distance);
			for (int i = 0; i < corners.Length; ++i) {
				corners[i] = Quaternion.Euler (0f, Mathf.Lerp (0f, angle / 2f, (corners.Length - i - 1) / ((float)corners.Length - 1)), 0f) * capPos;
			}
			corners[0] = Quaternion.Euler (0f, angle / 2f, 0f) * capPos;
			DrawCircle (position + direction * corners[0].z, direction, corners[0].x, color, segments, duration, depthTest);
			capPos = position + rotation * capPos;
			for (int i = 0; i < segments; ++i) {
				Quaternion currentRotation = rotation * Quaternion.Euler (0f, 0f, i * (360f / segments));
				Vector3 corner = position + currentRotation * corners[0];

				Debug.DrawLine (position, corner, color, duration, depthTest);

				Vector3 corner1 = corner;
				if (capSegments > 0) {
					Vector3 corner2 = corner1;
					for (int j = 1; j < corners.Length - 1; ++j) {
						corner2 = position + currentRotation * corners[j];
						Debug.DrawLine (corner1, corner2, color, duration, depthTest);
						corner1 = corner2;
					}
				}
				Debug.DrawLine (corner1, capPos, color, duration, depthTest);
			}
		}
	}
}
