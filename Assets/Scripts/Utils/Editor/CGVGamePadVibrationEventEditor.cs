﻿using UnityEditor;
using UnityEngine;

#pragma warning disable 649
namespace CGV.Editor {
	[CanEditMultipleObjects]
	[CustomEditor (typeof (CGVGamePadVibrationEvent))]
	public class CGVGamePadVibrationEventEditor : UnityEditor.Editor {
		private CGVGamePadVibrationEvent vibration;

		void OnEnable () {
			vibration = (CGVGamePadVibrationEvent)target;
		}

		public override void OnInspectorGUI () {
			EditorGUI.BeginChangeCheck ();

			SerializedProperty sM = serializedObject.FindProperty ("_SynchronizeMotors");
			EditorGUILayout.PropertyField (sM);

			EditorGUILayout.Space ();

			if (sM.boolValue) {
				SerializedProperty lM = serializedObject.FindProperty ("_LeftMotor");
				EditorGUILayout.PropertyField (lM, new GUIContent ("Motors"));
			}
			else {
				SerializedProperty lM = serializedObject.FindProperty ("_LeftMotor");
				EditorGUILayout.PropertyField (lM);
				SerializedProperty rM = serializedObject.FindProperty ("_RightMotor");
				EditorGUILayout.PropertyField (rM);
			}
			EditorGUILayout.Space ();

			SerializedProperty time = serializedObject.FindProperty ("_Time");
			EditorGUILayout.PropertyField (time);

			serializedObject.ApplyModifiedProperties ();
		}
	}
}
