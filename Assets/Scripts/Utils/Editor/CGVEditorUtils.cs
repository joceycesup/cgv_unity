﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CGV.Editor {
	public static class CGVEditorUtils {

		private static GUIStyle upDownArrowStyle;
		private static readonly RectOffset upDownArrowMargin;
		private static readonly Color upDownArrowTextColor;

		private static GUIStyle crossButtonStyle;

		static CGVEditorUtils () {
			// Up/Down Arrow
			upDownArrowStyle = new GUIStyle (GUI.skin.button);
			upDownArrowStyle.clipping = TextClipping.Overflow;
			upDownArrowMargin = upDownArrowStyle.margin;
			upDownArrowStyle.padding = new RectOffset ();

			upDownArrowStyle.normal.background = new Texture2D (1, 1);
			upDownArrowStyle.normal.background.SetPixel (0, 0, Color.clear);
			upDownArrowStyle.normal.background.Apply ();
			upDownArrowStyle.active.background = upDownArrowStyle.normal.background;//*/

			upDownArrowStyle.active.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black;

			upDownArrowTextColor = EditorGUIUtility.isProSkin ? upDownArrowStyle.normal.textColor : (Color)new Color32 (180, 180, 180, 255);
			if (!EditorGUIUtility.isProSkin) {
				upDownArrowStyle.normal.textColor = upDownArrowTextColor;
			}

			// Cross Button
			crossButtonStyle = new GUIStyle (GUI.skin.button);
			crossButtonStyle.clipping = TextClipping.Overflow;
		}

		public static int UpDown (bool disableUp = false, bool disableDown = false) {
			int res = 0;

			float buttonWidth = 14f;
			upDownArrowStyle.margin = new RectOffset (upDownArrowMargin.left, 0, upDownArrowMargin.top, upDownArrowMargin.bottom);
			upDownArrowStyle.alignment = TextAnchor.MiddleRight;

			EditorGUI.BeginDisabledGroup (disableUp);
			upDownArrowStyle.normal.textColor = disableUp ? Color.gray : upDownArrowTextColor;
			if (GUILayout.Button ("\u25B2", upDownArrowStyle, GUILayout.Width (buttonWidth))) {
				res = -1;
			}
			EditorGUI.EndDisabledGroup ();
			upDownArrowStyle.margin = new RectOffset (0, upDownArrowMargin.right, upDownArrowMargin.top, upDownArrowMargin.bottom);
			upDownArrowStyle.alignment = TextAnchor.MiddleLeft;
			EditorGUI.BeginDisabledGroup (disableDown);
			upDownArrowStyle.normal.textColor = disableDown ? Color.gray : upDownArrowTextColor;
			if (GUILayout.Button ("\u25BC", upDownArrowStyle, GUILayout.Width (buttonWidth))) {
				res = 1;
			}
			EditorGUI.EndDisabledGroup ();
			return res;
		}

		public static bool CrossButton () {
			GUI.backgroundColor = new Color (1.0f, 0.4f, 0.4f);
			bool res = GUILayout.Button ("X", crossButtonStyle, GUILayout.Width (EditorGUIUtility.singleLineHeight));
			GUI.backgroundColor = Color.white;
			return res;
		}

		public static GUIContent IconContent (string text, string icon, string tooltip = "") {
			GUIContent content;

			if (string.IsNullOrEmpty (icon)) {
				content = new GUIContent ();
			}
			else {
				content = EditorGUIUtility.IconContent (icon);
			}

			content.text = text;
			content.tooltip = tooltip;
			return content;
		}
	}
}
