﻿using System;
using System.IO;
using System.Collections;

using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

#pragma warning disable 649
namespace CGV.Editor {
	public class CGVScreenshotWindow : EditorWindow {

		[MenuItem ("Window/CGV/Screenshot Window")]
		static void Init () {
			CGVScreenshotWindow window = EditorWindow.GetWindow<CGVScreenshotWindow> ();
			window.titleContent = CGVEditorUtils.IconContent ("Screenshots", "Screenshot");

			window._DestinationFolder = EditorPrefs.GetString ("CGVScreenshotsDestination", Application.dataPath);
			window._Filename = EditorPrefs.GetString ("CGVScreenshotsFilename", "Screenshot");
			window._Size = new Vector2Int (EditorPrefs.GetInt ("CGVScreenshotsWidth", 256), EditorPrefs.GetInt ("CGVScreenshotsHeight", 256));
			window._Mode = EditorPrefs.GetBool ("CGVScreenshotsMode", true) ? ScreenshotMode.Camera : ScreenshotMode.SceneView;
			window._UseSceneName = EditorPrefs.GetBool ("CGVScreenshotsUseSceneName", true);
			window._AddDate = EditorPrefs.GetBool ("CGVScreenshotsAddDate", false);
			window._UseTransparency = EditorPrefs.GetBool ("CGVScreenshotsTransparency", false);

			window.Show ();
		}

		private string _DestinationFolder;// = Application.dataPath;
		private string _Filename;
		private string Filename { get { return _UseSceneName ? EditorSceneManager.GetActiveScene ().name : _Filename; } }
		private Vector2Int _Size = new Vector2Int (256, 256);
		private ScreenshotMode _Mode = ScreenshotMode.Camera;
		private Camera _GameCamera;
		private SceneView _SceneView;
		private Camera Camera { get { return (_Mode == ScreenshotMode.Camera ? _GameCamera : _SceneView?.camera); } }
		private bool _UseSceneName;
		private bool _AddDate;
		private bool _UseTransparency;

		private IEnumerator _ScreenshotCoroutine;

		private enum ScreenshotMode {
			Camera,
			SceneView
		}

		private void OnEnable () {
			EditorApplication.update += UpdateScreenshot;
		}

		private void OnDisable () {
			EditorApplication.update -= UpdateScreenshot;
		}

		void UpdateScreenshot () {
			if (_ScreenshotCoroutine != null)
				_ScreenshotCoroutine.MoveNext ();
		}

		private void SaveSettings () {
			//Debug.Log ("Saving screenshot settings");
			EditorPrefs.SetString ("CGVScreenshotsDestination", _DestinationFolder);
			EditorPrefs.SetString ("CGVScreenshotsFilename", _Filename);
			EditorPrefs.SetInt ("CGVScreenshotsWidth", _Size.x);
			EditorPrefs.SetInt ("CGVScreenshotsHeight", _Size.y);
			EditorPrefs.SetBool ("CGVScreenshotsMode", _Mode == ScreenshotMode.Camera);
			EditorPrefs.SetBool ("CGVScreenshotsUseSceneName", _UseSceneName);
			EditorPrefs.SetBool ("CGVScreenshotsAddDate", _AddDate);
			EditorPrefs.SetBool ("CGVScreenshotsTransparency", _UseTransparency);
		}

		void OnGUI () {
			EditorGUI.BeginChangeCheck ();

			EditorGUILayout.BeginHorizontal ();
			EditorGUILayout.PrefixLabel ("Size");
			_Size.x = Mathf.Max (EditorGUILayout.IntField (_Size.x), 1);
			GUILayout.Label ("/");
			_Size.y = Mathf.Max (EditorGUILayout.IntField (_Size.y), 1);
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.Space ();

			_Mode = (ScreenshotMode)EditorGUILayout.EnumPopup ("Mode", _Mode);
			if (_Mode == ScreenshotMode.Camera) {
				if (_GameCamera == null)
					_GameCamera = Camera.main;
				_GameCamera = EditorGUILayout.ObjectField ("", _GameCamera, typeof (Camera), true) as Camera;
			}
			else {
				if (_SceneView == null)
					_SceneView = SceneView.lastActiveSceneView;
				var sceneViews = SceneView.sceneViews.ToArray (typeof (SceneView));
				string[] sceneViewNames = new string[sceneViews.Length];
				int i = 0;
				int selected = -1;
				foreach (SceneView view in sceneViews) {
					if (_SceneView == view)
						selected = i;
					sceneViewNames[i] = string.Format ("Scene {0} ({1} - {2}{3})", i, (view.orthographic ? "Iso" : "Persp"), (view.in2DMode ? "2D" : "3D"), ((view == SceneView.lastActiveSceneView) ? ", last active" : ""));
					//GUILayout.Label (sceneViewNames[i]);
					++i;
				}
				selected = EditorGUILayout.Popup ("Scene View", selected, sceneViewNames);
				if (selected >= 0)
					_SceneView = sceneViews.GetValue (selected) as SceneView;
			}

			EditorGUILayout.Space ();

			_UseSceneName = EditorGUILayout.Toggle ("Use Scene Name", _UseSceneName);
			if (!_UseSceneName) {
				_Filename = GetSafeFilename (EditorGUILayout.TextField ("Filename", _Filename));
			}
			_AddDate = EditorGUILayout.Toggle ("Add Date to Name", _AddDate);

			_DestinationFolder = GetSafePath (EditorGUILayout.TextField ("Destination", _DestinationFolder));
			if (GUILayout.Button ("Select New Destination Folder")) {
				string path = EditorUtility.OpenFolderPanel ("Screenshot Destination", _DestinationFolder, "");
				if (IsSubFolder (path, Application.dataPath)) {
					path = MakeRelative (path, Application.dataPath);
					if (path.StartsWith ("Assets/"))
						path = path.Substring (7);
				}
				if (!string.IsNullOrEmpty (path))
					_DestinationFolder = path;
			}

			EditorGUILayout.Space ();

			_UseTransparency = EditorGUILayout.Toggle ("Transparent Background", _UseTransparency);

			EditorGUILayout.Space ();

			if (EditorGUI.EndChangeCheck ()) {
				SaveSettings ();
			}

			EditorGUI.BeginDisabledGroup (_ScreenshotCoroutine != null);
			if (GUILayout.Button ("Take Screenshot")) {
				_ScreenshotCoroutine = TakeScreenshot ();
			}
			EditorGUI.EndDisabledGroup ();
		}

		private IEnumerator TakeScreenshot () {
			string dir = _DestinationFolder + "/";
			if (!Path.IsPathRooted (_DestinationFolder))
				dir = Application.dataPath + "/" + dir;
			string filename = (_AddDate ? Filename + "_" + DateTime.Now.ToString ("ddMMyy_HHmmss") : Filename) + ".png";
			string path = dir + filename;
			//Debug.Log (path);

			Camera cam = Camera;

			// Create Render Texture with width and height.
			RenderTexture rt = new RenderTexture (_Size.x, _Size.y, 0, RenderTextureFormat.ARGB32);

			// Assign Render Texture to camera.
			cam.targetTexture = rt;

			// save current background settings of the camera
			CameraClearFlags clearFlags = cam.clearFlags;
			Color backgroundColor = cam.backgroundColor;

			// make the background transparent when enabled
			if (_UseTransparency) {
				cam.clearFlags = CameraClearFlags.SolidColor;
				cam.backgroundColor = new Color (); // alpha is zero
			}

			yield return null;

			// Render the camera's view to the Target Texture.
			cam.Render ();

			yield return null;

			// restore the camera's background settings if they were changed before rendering
			if (_UseTransparency) {
				cam.clearFlags = clearFlags;
				cam.backgroundColor = backgroundColor;
			}

			// Save the currently active Render Texture so we can override it.
			RenderTexture currentRT = RenderTexture.active;

			// ReadPixels reads from the active Render Texture.
			RenderTexture.active = cam.targetTexture;

			// Make a new texture and read the active Render Texture into it.
			Texture2D screenshot = new Texture2D (_Size.x, _Size.y, TextureFormat.ARGB32, false);
			screenshot.ReadPixels (new Rect (0, 0, _Size.x, _Size.y), 0, 0, false);

			// PNGs should be sRGB so convert to sRGB color space when rendering in linear.
			if (QualitySettings.activeColorSpace == ColorSpace.Linear) {
				Color[] pixels = screenshot.GetPixels ();
				for (int p = 0; p < pixels.Length; p++) {
					pixels[p] = pixels[p].gamma;
				}
				screenshot.SetPixels (pixels);
			}

			yield return null;

			// Apply the changes to the screenshot texture.
			screenshot.Apply (false);

			// Save the screnshot.
			Directory.CreateDirectory (dir);
			byte[] png = screenshot.EncodeToPNG ();
			File.WriteAllBytes (path, png);

			// Remove the reference to the Target Texture so our Render Texture is garbage collected.
			cam.targetTexture = null;

			// Restore the original active Render Texture.
			RenderTexture.active = currentRT;

			Debug.Log ("Screenshot saved to:\n" + path);

			Repaint ();

			_ScreenshotCoroutine = null;
		}

		public string GetSafePath (string path) {
			if (string.IsNullOrEmpty (path))
				return string.Empty;
			return string.Join ("_", path.Split (Path.GetInvalidPathChars ()));
		}

		public string GetSafeFilename (string filename) {
			if (string.IsNullOrEmpty (filename))
				return string.Empty;
			return string.Join ("_", filename.Split (Path.GetInvalidFileNameChars ()));
		}

		public static string MakeRelative (string filePath, string referencePath) {
			var fileUri = new Uri (filePath);
			var referenceUri = new Uri (referencePath);
			return referenceUri.MakeRelativeUri (fileUri).ToString ();
		}

		public static bool IsSubFolder (string childPath, string parentPath) {
			if (string.IsNullOrEmpty (childPath))
				return false;
			var childUri = new Uri (childPath);
			var parentUri = new Uri (parentPath);
			return (parentUri != childUri && parentUri.IsBaseOf (childUri));
		}
	}
}
