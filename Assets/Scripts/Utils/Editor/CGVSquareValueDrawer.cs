﻿using UnityEditor;
using UnityEngine;

#pragma warning disable 649
namespace CGV.Editor {
	[CustomPropertyDrawer(typeof(CGVSquareValueAttribute))]
	public class CGVSquareValueDrawer : PropertyDrawer {
		private bool showSqrValue = false;

		public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label) {
			EditorGUI.BeginProperty(position, label, prop);

			float value = Mathf.Sqrt(Mathf.Max(prop.floatValue, 0f));
			if (showSqrValue) {
				position.height /= 2f;
			}
			position.width -= EditorStyles.foldout.padding.left;
			value = EditorGUI.FloatField(position, label, value);
			value = value <= 0f ? 0f : value * value;

			Rect foldoutRect = new Rect(position.x + position.width + EditorStyles.foldout.padding.left, position.y, EditorStyles.foldout.padding.left, position.height);
			//EditorGUI.DrawRect(foldoutRect, Color.red);
			showSqrValue = EditorGUI.Foldout(foldoutRect, showSqrValue, "", true);

			if (showSqrValue) {
				position.y += position.height;
				//EditorGUI.LabelField(position, "Square value : " + value.ToString());
				value = EditorGUI.FloatField(position, "    Square value : ", value);
			}
			prop.floatValue = value;

			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
			return base.GetPropertyHeight(property, label) * (showSqrValue ? 2f : 1f);
		}
	}
}
