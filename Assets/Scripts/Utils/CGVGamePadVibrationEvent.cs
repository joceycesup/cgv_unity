﻿using UnityEngine;

#pragma warning disable 649
namespace CGV {
	[CreateAssetMenu(fileName = "GamePadVibration.asset", menuName = "CGV/GamePad Vibration")]
	public class CGVGamePadVibrationEvent : ScriptableObject {
		[SerializeField]
		private bool _SynchronizeMotors = true;
		public bool SynchronizeMotors { get { return _SynchronizeMotors; } }

		[SerializeField] private AnimationCurve _LeftMotor = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(0.5f, 1f), new Keyframe(1f, 0f) });
		//public AnimationCurve LeftMotor { get { return _LeftMotor; } }
		[SerializeField] private AnimationCurve _RightMotor = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0f), new Keyframe(0.5f, 1f), new Keyframe(1f, 0f) });
		//public AnimationCurve RightMotor { get { return _RightMotor; } }

		public float LeftMotor(float t) {
			return _LeftMotor.Evaluate(t);
		}

		public float RightMotor(float t) {
			return (SynchronizeMotors ? _LeftMotor : _RightMotor).Evaluate(t);
		}

		[SerializeField]
		[Range(0f, 5f)]
		private float _Time = .5f;
		public float Time { get { return _Time; } }
	}
}
