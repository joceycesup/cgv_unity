﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CGV {
	[CreateAssetMenu (fileName = "TTSSettings.asset", menuName = "CGV/Sound/TextToSpeech Settings")]
	public class CGVTTSSettings : ScriptableObject {
		public string Language = "English";
		[Range (1, 100)]
		public int SpeechRate = 1;
	}
}
