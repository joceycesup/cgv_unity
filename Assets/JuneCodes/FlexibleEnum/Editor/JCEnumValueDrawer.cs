﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEditor;

using JuneCodes.Editor;

namespace JuneCodes.FlexibleEnum.Editor {
	[CustomPropertyDrawer (typeof (JCEnumValue), true)]
	public class JCEnumValueDrawer : PropertyDrawer {

		private float LineHeight = EditorGUIUtility.singleLineHeight;

		private UnityEngine.Object TargetObject;
		private MethodInfo CanChangeValueNameMI = null;

		internal static string CurrentInvalidName = string.Empty;

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {//*
			if (CanChangeValueNameMI == null) {
				TargetObject = property.serializedObject.targetObject;
				CanChangeValueNameMI = TargetObject.GetType ().GetMethod ("CanChangeValueName", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
			}

			EditorGUI.BeginProperty (position, label, property);

			var idRect = new Rect (position.x, position.y, Mathf.Min (60f, position.width / 2f), LineHeight);
			idRect.x += position.width - idRect.width;
			var nameRect = new Rect (position.x, position.y, position.width - idRect.width, LineHeight);

			EditorGUI.BeginChangeCheck ();
			SerializedProperty nameProp = property.FindPropertyRelative ("_Name");
			SerializedProperty idProp = property.FindPropertyRelative ("_Id");

			//TypeAttribute = fieldInfo.GetAttribute<JCEnumTypeAttribute> ();
			/*
			int controlId = JCEditorUtility.GetLastControlId () + 1;

			if (EditorGUIUtility.editingTextField) {
				if (controlId == EditorGUIUtility.keyboardControl) {
					//GUI.color = Color.cyan;
					TextEditor te = typeof (EditorGUI).GetField ("activeEditor", BindingFlags.Static | BindingFlags.NonPublic).GetValue (null) as TextEditor;
					if (te != null) {
						//JCEditorUtility.FilterInput (JCEnumKey.AuthorizedCharacters);
						//Debug.Log ("is editing "+property.displayName);
						string tmpFormatedName = JCEnumKey.FormatName (te.text);
						if (!NameIsValid (tmpFormatedName, idProp.intValue)) {
							GUI.color = JCEditorUtility.ErrorColor;
							CurrentInvalidName = tmpFormatedName;
						}
						else {
							CurrentInvalidName = string.Empty;
						}
						te.text = JCEnumKey.FormatName (te.text, false);
						//te.text = te.text.TrimStart (' ', '!');
					}
				}
				else if (nameProp.stringValue.Equals (CurrentInvalidName)) {
					GUI.color = JCEditorUtility.WarningColor;
				}
			}
			else if (!NameIsValid (nameProp.stringValue, idProp.intValue)) {
				GUI.color = JCEditorUtility.ErrorColor;
				CurrentInvalidName = string.Empty;
				//ErrorProp = NameIsValid (nameProp.stringValue) ? null : property;
			}

			//if (ReferenceEquals (property, ErrorProp)) GUI.color = JCEditorUtility.ErrorColor;
			//else if (TypeAttribute != null) GUI.color = Color.cyan;
			string newName = EditorGUI.DelayedTextField (nameRect, GUIContent.none, nameProp.stringValue);
			GUI.color = Color.white;/*/
			if (nameProp.stringValue.Equals (CurrentInvalidName)) {
				GUI.color = JCEditorUtility.WarningColor;
			}
			string newName = JCGUI.FilteredTextField (nameRect, GUIContent.none, nameProp.stringValue, JCEnumKey.AuthorizedCharacters, out TextEditor textEditor, (editor) => {
				string tmpFormatedName = JCEnumKey.FormatName (editor.text);
				if (!NameIsValid (tmpFormatedName, idProp.intValue)) {
					GUI.color = JCEditorUtility.ErrorColor;
					CurrentInvalidName = tmpFormatedName;
				}
				else {
					CurrentInvalidName = string.Empty;
				}
			}, true);
			GUI.color = Color.white;
			//*/

			if (!newName.Equals (nameProp.stringValue))
				Debug.Log ("Trying to change value " + newName);
			if (GUI.changed) {
				newName = newName.Trim ();
				if (NameIsValid (newName, idProp.intValue)) {
					nameProp.stringValue = newName;
					Debug.Log ("Changing value " + newName);
				}
			}

			var indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;
			EditorGUI.LabelField (idRect, " ID: " + property.FindPropertyRelative ("_Id").intValue);
			EditorGUI.indentLevel = indent;

			EditorGUI.EndProperty ();
			/*
			if (ReferenceEquals (property, ErrorProp)) {
				EditorGUI.HelpBox (new Rect (position.x, position.y + LineHeight, position.width, LineHeight), "This key is invalid!", MessageType.Error);
				position.y += LineHeight;
				position.height -= LineHeight;
			}//*/

			EditorGUI.PropertyField (position, property, GUIContent.none, true);
		}

		private bool NameIsValid (string name, int id) {
			return ((bool) CanChangeValueNameMI.Invoke (TargetObject, new object[] { id, name }));
		}

		public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
			LineHeight = base.GetPropertyHeight (property, label);

			float added = property.FindPropertyRelative ("_Id").intValue;
			//if (TypeAttribute != null)
			//added += 10f;

			return EditorGUI.GetPropertyHeight (property);
		}
	}
}
