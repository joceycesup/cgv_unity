﻿using System;
using UnityEditor;
using UnityEngine;

using JuneCodes.Editor;
using JetBrains.Annotations;

#pragma warning disable 649
namespace JuneCodes.FlexibleEnum.Editor {
	public static class JCEnumGUI {
		public static int EnumKeyField (Rect position, GUIContent label, int currentKeyId, [NotNull] Type type, bool allowNullValue = false, MessageType defaultState = MessageType.None) {
			return JCEnumTypeDrawer.EnumKeyField (position, label, currentKeyId, type, allowNullValue, defaultState);
		}

		public static int EnumKeyField (GUIContent label, int currentKeyId, [NotNull] Type type, bool allowNullValue = false, MessageType defaultState = MessageType.None) {
			Rect position = EditorGUILayout.GetControlRect (true, EditorGUIUtility.singleLineHeight);
			return JCEnumTypeDrawer.EnumKeyField (position, label, currentKeyId, type, allowNullValue, defaultState);
		}
	}
}
