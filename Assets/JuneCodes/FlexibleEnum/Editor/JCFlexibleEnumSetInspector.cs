﻿using System.Reflection;
using UnityEngine;
using UnityEditor;

using JuneCodes.Editor;

namespace JuneCodes.FlexibleEnum.Editor {
	[CustomEditor (typeof (JCFlexibleEnumSet<,,>), true)]
	public class JCFlexibleEnumSetInspector : UnityEditor.Editor {

		private MethodInfo CanAddValueMI;
		private MethodInfo AddValueMI;

		SerializedProperty DefaultKeyProp;
		SerializedProperty IdCounterProp;
		SerializedProperty ContainerProp;
		SerializedProperty ValuesProp;

		JCReorderableList ValuesList;

		string CurrentValueName = string.Empty;
		bool CanAddValue = false;

		void OnEnable () {
			CanAddValueMI = target.GetType ().GetMethod ("CanAddValue", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
			AddValueMI = target.GetType ().GetMethod ("AddValue", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.FlattenHierarchy);

			DefaultKeyProp = serializedObject.FindProperty ("_DefaultKey");
			IdCounterProp = serializedObject.FindProperty ("IdCounter");
			ContainerProp = serializedObject.FindProperty ("_Container");
			ValuesProp = ContainerProp.FindPropertyRelative ("_Values");

			ValuesList = new JCReorderableList (ValuesProp.serializedObject, ValuesProp, true, true, false, true);
			ValuesList.drawHeaderCallback += (Rect rect) => {
				EditorGUI.LabelField (ValuesList.HeaderRect (rect), string.Format ("Next ID: {0}", IdCounterProp.intValue), new GUIStyle (EditorStyles.label) { alignment = TextAnchor.MiddleRight });
			};
		}

		public override void OnInspectorGUI () {
			//DrawDefaultInspector ();

			serializedObject.Update ();

			EditorGUILayout.PropertyField (DefaultKeyProp);

			JCGUILayout.Separator ();

			ValuesList.DoLayoutList ();

			EditorGUILayout.BeginHorizontal ();

			if (!CanAddValue) {
				if (!EditorGUIUtility.editingTextField) {
					if (!string.IsNullOrWhiteSpace (CurrentValueName))
						GUI.color = JCEditorUtility.ErrorColor;
					JCEnumValueDrawer.CurrentInvalidName = CurrentValueName;
				}
				else
					GUI.color = JCEditorUtility.ErrorColor;
			}
			CurrentValueName = JCGUILayout.FilteredTextField (GUIContent.none, CurrentValueName, JCEnumKey.AuthorizedCharacters, out TextEditor te, (editor) => {
				string editedText = JCEnumKey.FormatName (editor.text);
				if (!editedText.Equals (CurrentValueName)) {
					//Debug.Log (editedText);
					CanAddValue = (bool) CanAddValueMI.Invoke (target, new object[] { editedText });
					if (CanAddValue) {
						JCEnumValueDrawer.CurrentInvalidName = string.Empty;
					}
				}
				else if (!CanAddValue) {
					JCEnumValueDrawer.CurrentInvalidName = editedText;
				}
			}, true);
			GUI.color = Color.white;
			//*/
			EditorGUI.BeginDisabledGroup (!CanAddValue);
			if (GUILayout.Button ("Add Value")) {
				if (te != null) { // if the TextField has focus, CurrentValueName will not have its current value
					CurrentValueName = te.text;
				}
				bool res = (bool) AddValueMI?.Invoke (target, new object[] { CurrentValueName });
				//Debug.Log (res ? "new value succesfully added" : "failed to add value");
				if (res) {
					EditorGUIUtility.editingTextField = false;
					CurrentValueName = string.Empty;
					CanAddValue = false;
					serializedObject.ApplyModifiedProperties ();
					JCFlexibleEnumSetAccessor.RefreshSet (target.GetType ());
				}
			}
			EditorGUI.EndDisabledGroup ();
			EditorGUILayout.EndHorizontal ();
		}
	}
}
