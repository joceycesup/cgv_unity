﻿using System;
using UnityEditor;
using UnityEngine;

using JuneCodes.Editor;
using JetBrains.Annotations;

#pragma warning disable 649
namespace JuneCodes.FlexibleEnum.Editor {
	[CustomPropertyDrawer (typeof (JCEnumTypeAttribute))]
	public class JCEnumTypeDrawer : PropertyDrawer {

		public override void OnGUI (Rect position, SerializedProperty prop, GUIContent label) {
			EditorGUI.BeginProperty (position, label, prop);

			JCEnumTypeAttribute eta = (JCEnumTypeAttribute)attribute;

			Type type = eta.Type;
			if (type == null) {
				type = prop.GetSerializedPropertyRootObject ().GetType ();

				if (type == null) {
					EditorGUI.HelpBox (position, "The property " + prop.name + " has an invalid attribute JCFlexibleEnumAttribute!", MessageType.Warning);
					EditorGUI.EndProperty ();
				}
			}

			JCFlexibleEnumSetAccessor.RefreshSet (type);
			JCEnumValueContainer<JCEnumValue> set = JCFlexibleEnumSetAccessor.GetSet (type);

			JCEnumKey key = null;

			if (prop.propertyType == SerializedPropertyType.Generic) {
				//key = (JCEnumKey) fieldInfo.GetValue (this.fieldInfo.);
				key = prop.GetValue<JCEnumKey> ();
			}

			if (ReferenceEquals (null, set)) {
				EditorGUI.HelpBox (position, "The type " + eta.Type + " does not seem to be registered", MessageType.Error);
			}
			else if (set.Count <= 0) {
				EditorGUI.HelpBox (position, "The type " + eta.Type + " has no values yet", MessageType.Warning);
			}
			else if (ReferenceEquals (null, key)) {
				EditorGUI.HelpBox (position, "The field " + prop.propertyPath + " is not of the type JCEnumKey", MessageType.Error);
			}
			else {
				int objectNewId = -1;

				string[] displayedOptions = new string[set.Count + (eta.AllowNullValue ? 1 : 0)];
				int[] optionValues = new int[displayedOptions.Length];

				int i = 0;
				if (eta.AllowNullValue) {
					displayedOptions[0] = "--- None ---";
					optionValues[0] = -1;
					i++;
				}

				int idIndex = -1;
				foreach (JCEnumValue enumValue in set) {
					displayedOptions[i] = enumValue.Name;
					optionValues[i] = enumValue.Id;
					if (enumValue.Matches (key))
						idIndex = i;
					i++;
				}

				bool valueChanged = false;

				MessageType popupState = MessageType.None;
				if (eta.AllowNullValue) {
					if (idIndex < 0) {
						idIndex = 0;
						popupState = MessageType.Warning;
					}
					valueChanged = Popup (position, label, key, out objectNewId, idIndex, displayedOptions, optionValues, popupState);
				}
				else {
					if (idIndex < 0) {
						popupState = MessageType.Error;
					}
					valueChanged = Popup (position, label, key, out objectNewId, idIndex, displayedOptions, optionValues, popupState);
				}

				if (valueChanged) {
					prop.FindPropertyRelative ("_Id").intValue = objectNewId;
				}
			}

			EditorGUI.EndProperty ();
		}

		private static bool Popup (Rect position, GUIContent label, int currentKeyId, out int newId, int index, string[] displayedOptions, int[] optionValues, MessageType popupState = MessageType.None) {
			bool valueChanged = false;

			position = EditorGUI.PrefixLabel (position, label);

			if (popupState != MessageType.None) {
				if (popupState == MessageType.Error) {
					GUI.color = JCEditorUtility.ErrorColor;
					EditorGUI.HelpBox (new Rect (position.x + position.width - 60f, position.y, 60f, position.height), "Invalid", MessageType.Error);
					position.width -= 60f;
				}
				else
					GUI.color = JCEditorUtility.WarningColor;
			}
			int newIndex = EditorGUI.Popup (position, index, displayedOptions);
			GUI.color = Color.white;
			if (newIndex >= 0 && newIndex != index) {
				newId = optionValues[newIndex];
				valueChanged = true;
			}
			else {
				newId = currentKeyId;
			}
			return valueChanged;
		}

		private static bool Popup (Rect position, GUIContent label, JCEnumKey currentKey, out int newId, int index, string[] displayedOptions, int[] optionValues, MessageType popupState = MessageType.None) {
			return Popup (position, label, currentKey.Id, out newId, index, displayedOptions, optionValues, popupState);
		}

		internal static int EnumKeyField (Rect position, GUIContent label, int currentKeyId, [NotNull] Type type, bool allowNullValue, MessageType overridePopupState) {
			JCFlexibleEnumSetAccessor.RefreshSet (type);
			JCEnumValueContainer<JCEnumValue> set = JCFlexibleEnumSetAccessor.GetSet (type);

			int objectNewId = currentKeyId;
			if (ReferenceEquals (null, set)) {
				EditorGUILayout.HelpBox ("The type " + type + " does not seem to be registered", MessageType.Error);
			}
			else if (set.Count <= 0) {
				EditorGUILayout.HelpBox ("The type " + type + " has no values yet", MessageType.Warning);
			}
			else {
				string[] displayedOptions = new string[set.Count + (allowNullValue ? 1 : 0)];
				int[] optionValues = new int[displayedOptions.Length];

				int i = 0;
				if (allowNullValue) {
					displayedOptions[0] = "--- None ---";
					optionValues[0] = -1;
					i++;
				}

				int idIndex = -1;
				foreach (JCEnumValue enumValue in set) {
					displayedOptions[i] = enumValue.Name;
					optionValues[i] = enumValue.Id;
					if (enumValue.Id == currentKeyId)
						idIndex = i;
					i++;
				}

				//position.height = EditorGUIUtility.singleLineHeight;

				MessageType popupState = overridePopupState == MessageType.Info ? MessageType.None : overridePopupState;
				if (allowNullValue) {
					if (idIndex < 0) {
						idIndex = 0;
						popupState = MessageType.Warning;
					}
					Popup (position, label, currentKeyId, out objectNewId, idIndex, displayedOptions, optionValues, popupState);
				}
				else {
					if (idIndex < 0) {
						popupState = MessageType.Error;
					}
					Popup (position, label, currentKeyId, out objectNewId, idIndex, displayedOptions, optionValues, popupState);
				}
			}
			return objectNewId;
		}
	}
}
