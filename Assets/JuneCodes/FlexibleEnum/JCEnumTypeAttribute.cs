﻿using System;
using UnityEngine;

#pragma warning disable 649
namespace JuneCodes.FlexibleEnum {
	public class JCEnumTypeAttribute : PropertyAttribute {
		public readonly Type Type;
		public readonly bool AllowNullValue;

		public JCEnumTypeAttribute (Type type, bool allowNullValue = true) {
			this.Type = type;
			this.AllowNullValue = allowNullValue;
		}
	}
}
