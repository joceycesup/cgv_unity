﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;

#pragma warning disable 649
namespace JuneCodes.FlexibleEnum {

	[Serializable]
	public sealed class JCEnumKey {
		public static readonly int InvalidId = -1;
		public static readonly int InvalidIndex = -2;
		public static readonly string AuthorizedCharacters = @"a-zA-Z\s\d";
		[SerializeField, HideInInspector] private int _Id;
		public int Id { get { return _Id; } private set { _Id = value; } }

		private int _Index = InvalidId;
		public int Index {
			get {
#if UNITY_EDITOR
				if (!IdIsValid (_Id))
					return InvalidIndex;
#endif
				return _Index;
			}
		}

		public static bool NameIsValid (string name) {
			if (string.IsNullOrWhiteSpace (name))
				return false;
			if (!Regex.IsMatch (name, "[" + AuthorizedCharacters + "]+"))
				return false;
			return true;
		}

		public static string FormatName (string name, bool trim = true) {
			string res = Regex.Replace (name, "[^" + AuthorizedCharacters + "]+", "", RegexOptions.IgnoreCase);
			if (trim) res = res.Trim ();
			return res;
		}

		public static bool IdIsValid (int id) {
			return id >= 0;
		}

		public bool IsValid () {
			return IdIsValid (_Id);
		}

		public static bool operator == (JCEnumKey k1, JCEnumKey k2) {
			if (ReferenceEquals (k1, null) != ReferenceEquals (k2, null)) return false;
			if (ReferenceEquals (k1, null) && ReferenceEquals (k2, null)) return true;
			return k1._Id == k2._Id;
		}

		public static bool operator == (JCEnumKey k1, int id) {
			return k1._Id == id;
		}

		public static bool operator != (JCEnumKey k1, JCEnumKey k2) {
			if (ReferenceEquals (k1, null) != ReferenceEquals (k2, null)) return true;
			if (ReferenceEquals (k1, null) && ReferenceEquals (k2, null)) return false;
			return k1._Id != k2._Id;
		}

		public static bool operator != (JCEnumKey k1, int id) {
			return k1._Id != id;
		}

		public bool Matches (JCEnumValue value) {
			if (ReferenceEquals (null, value)) return false;
			return value.Id == _Id;
		}

		public bool Equals (int otherId) {
			return otherId == _Id;
		}

		public bool Equals (JCEnumKey other) {
			if (ReferenceEquals (null, other)) return false;
			if (ReferenceEquals (this, other)) return true;
			return other._Id == _Id;
		}

		public override bool Equals (object obj) {
			if (ReferenceEquals (null, obj)) return false;
			if (ReferenceEquals (this, obj)) return true;
			// class is sealed, checking inheritance is unnecessary
			//if (!typeof (JCEnumKey).IsAssignableFrom (obj.GetType ())) return false;
			if (obj.GetType () != typeof (JCEnumKey)) return false;
			//return Equals ((JCEnumKey) obj);
			return ((JCEnumKey)obj)._Id == _Id;
		}

		public override int GetHashCode () {
			return _Id;
		}
	}

	public class JCEnumValue {
		[SerializeField, HideInInspector] protected string _Name;
		public string Name { get { return _Name; } protected internal set { _Name = value; } }

		[SerializeField, HideInInspector] private int _Id;
		public int Id { get { return _Id; } internal set { _Id = value; } }

		public JCEnumValue () {
		}

		public bool Matches (JCEnumKey key) {
			if (ReferenceEquals (null, key)) return false;
			return key.Id == _Id;
		}

		public bool Equals (JCEnumValue other) {
			if (ReferenceEquals (null, other)) return false;
			if (ReferenceEquals (this, other)) return true;
			return other._Id == _Id;
		}

		public override bool Equals (object obj) {
			if (ReferenceEquals (null, obj)) return false;
			if (ReferenceEquals (this, obj)) return true;
			// class is sealed, checking inheritance is unnecessary
			//if (!typeof (JCEnumKey).IsAssignableFrom (obj.GetType ())) return false;
			if (obj.GetType () != typeof (JCEnumKey)) return false;
			//return Equals ((JCEnumKey) obj);
			return ((JCEnumValue)obj)._Id == _Id;
		}

		public override int GetHashCode () {
			return _Id;
		}
	}

	[Serializable]
	public class JCEnumValueContainer<V> : IEnumerable<V>, ISerializationCallbackReceiver
		where V : JCEnumValue, new() {

		[SerializeField]
		private List<V> _Values;
		protected List<V> Values {
			get {
				if (_Values == null) {
					_Values = new List<V> ();
				}
				return _Values;
			}
		}
		protected Dictionary<int, V> IdDictionary;
		protected Dictionary<string, V> NameDictionary;

		public int Count {
			get { return Values.Count; }
		}

		public V this[JCEnumKey key] {
			get {
				V res = null;
				if (key != null)
					IdDictionary.TryGetValue (key.Id, out res);
				return res;
			}
		}

		public V this[string key] {
			get {
				V res = null;
				if (!string.IsNullOrEmpty (key))
					NameDictionary.TryGetValue (key, out res);
				return res;
			}
		}

		public V this[int id] {
			get {
				V res = null;
				IdDictionary.TryGetValue (id, out res);
				return res;
			}
		}

		internal bool AddValue (int id, string name) {
			if (!CanAddValue (id, name))
				return false;
			V newValue = new V ();
			newValue.Name = name;
			newValue.Id = id;
			_Values.Add (newValue);
			IdDictionary[id] = newValue;
			NameDictionary[name] = newValue;
			return true;
		}

		internal bool CanAddValue (int id, string name) {
			if (ContainsId (id))
				return false;
			if (!JCEnumKey.NameIsValid (name))
				return false;
			if (NameDictionary.ContainsKey (name))
				return false;
			return true;
		}

		internal bool CanChangeValueName (int id, string name) {
			if (!ContainsId (id))
				return false;
			if (!JCEnumKey.NameIsValid (name))
				return false;
			if (ContainsName (name, id))
				return false;
			return true;
		}

		public bool ContainsName (string name, int ignoredID = -1) {
			if (!JCEnumKey.NameIsValid (name))
				return false;
			if (NameDictionary.TryGetValue (name, out V value)) {
				if (value.Id != ignoredID)
					return true;
			}
			//return NameDictionary.ContainsKey (name);
			return false;
		}

		public bool ContainsId (int id) {
#if UNITY_EDITOR
			if (IdDictionary == null) {
				FillDictionary ();
			}
#endif
			return IdDictionary.ContainsKey (id);
		}

		protected void FillDictionary () {
			if (Values != null) {
				if (IdDictionary == null)
					IdDictionary = new Dictionary<int, V> (Values.Count);
				else
					IdDictionary.Clear ();
				if (NameDictionary == null)
					NameDictionary = new Dictionary<string, V> (Values.Count);
				else
					NameDictionary.Clear ();
				//Debug.Log ("Filling dictionary");
				for (int i = 0; i < Values.Count; ++i) {
					IdDictionary[Values[i].Id] = Values[i];
					NameDictionary[Values[i].Name] = Values[i];
				}
			}
		}

		IEnumerator<V> IEnumerable<V>.GetEnumerator () {
			return Values.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator () {
			return Values.GetEnumerator ();
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize () {
			FillDictionary ();
		}

		void ISerializationCallbackReceiver.OnBeforeSerialize () {
			// We just want to keep the List<V> serialized, Dictionary
			// should always reflect the List's state
			/*
			if (IdDictionary == null || IdDictionary.Count == 0) {
				//Dictionary is empty, erase data
				Values = null;
			}
			else {
				//Initialize arrays
				int count = IdDictionary.Count;
				Values = new List<V> (count);

				var e = IdDictionary.GetEnumerator ();
				while (e.MoveNext ()) {
					//Set the respective data from the dictionary
					Values.Add (e.Current.Value);
				}
			}//*/
		}
	}

	[Serializable]
	public class JCEnumValueContainer : JCEnumValueContainer<JCEnumValue> {

		internal void SetValues (IEnumerable<JCEnumValue> values) {
			Values.Clear ();
			Values.AddRange (values);
			FillDictionary ();
		}
	}

	[Serializable]
	public abstract class JCFlexibleEnumSet<T, C, V> : JCSingletonScriptableObject<T>
		where T : JCFlexibleEnumSet<T, C, V>
		where C : JCEnumValueContainer<V>
		where V : JCEnumValue, new() {

		[SerializeField, JCEnumType (null, false)]
		protected JCEnumKey _DefaultKey;
		public ref readonly JCEnumKey DefaultKey { get { return ref _DefaultKey; } }
		public V DefaultValue {
			get { return Container[_DefaultKey]; }
		}

		[SerializeField]
		protected C _Container;
		protected static C Container { get { return instance._Container; } }

		[SerializeField, HideInInspector]
		private int IdCounter = 0;

		public static V GetValue (JCEnumKey key, bool allowNullResult = false) {
			V res = Container[key];
			if (res == null && !allowNullResult)
				res = instance.DefaultValue;
			return res;
		}

		public static V GetValue (string key, bool allowNullResult = false) {
			V res = Container[key];
			if (res == null && !allowNullResult)
				res = instance.DefaultValue;
			return res;
		}

		public static V GetValue (int id, bool allowNullResult = false) {
			V res = Container[id];
			if (res == null && !allowNullResult)
				res = instance.DefaultValue;
			return res;
		}

		internal bool AddValue (string key) {
			bool res = Container.AddValue (IdCounter, key);
			if (res) {
				IdCounter++;
			}
			return res;
		}

		public bool CanAddValue (string name) {
			return Container.CanAddValue (IdCounter, name);
		}

		public bool CanChangeValueName (int id, string newName) {
			return Container.CanChangeValueName (id, newName);
		}

		public bool ContainsName (string name, int ignoredID = -1) {
			return Container.ContainsName (name, ignoredID);
		}

		protected static void InitInstance () {
			instance.InitSingleton ();
			JCEnumValueContainer d = new JCEnumValueContainer ();
			List<JCEnumValue> values = new List<JCEnumValue> (Container.Count);
			foreach (JCEnumValue value in Container) {
				values.Add (value);
			}
			d.SetValues (values);
			JCFlexibleEnumSetAccessor.RegisterSet (typeof (T), d);
		}
	}

	public sealed class JCFlexibleEnumSetAccessor {
		private static readonly Type FlexibleEnumSetBaseType = typeof (JCFlexibleEnumSet<,,>);

		private static Dictionary<Type, JCEnumValueContainer<JCEnumValue>> _RegisteredSets;
		private static Dictionary<Type, JCEnumValueContainer<JCEnumValue>> RegisteredSets {
			get {
				if (_RegisteredSets == null) {
					_RegisteredSets = new Dictionary<Type, JCEnumValueContainer<JCEnumValue>> ();
				}
				return _RegisteredSets;
			}
		}

		internal static void RefreshSets () {
			Type[] types = Assembly.GetExecutingAssembly ().GetTypes ();
			IEnumerable<Type> subclasses = types.Where (t => IsSubclassOfRawGeneric (FlexibleEnumSetBaseType, t) && t != FlexibleEnumSetBaseType);
			foreach (Type type in subclasses) {
				RegisterSet (type);
			}//*/
		}
		public static void RefreshSet (Type setType) {
			if (IsSubclassOfRawGeneric (FlexibleEnumSetBaseType, setType) && setType != FlexibleEnumSetBaseType) {
				RegisterSet (setType);
			}//*/
		}

		private static void RegisterSet (Type type) {
			if (type == typeof (JCFlexibleEnumSetAccessor))
				return;
			if (GetSet (type) == null) {
				MethodInfo mi = type.GetMethod ("InitInstance", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
				mi?.Invoke (null, null);
				/*
				PropertyInfo pi = type.GetProperty("IndexedValues", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);

				RegisteredSets[type] = pi.GetValue(null) as JCNamedPropertyDictionary<JCNamedInt>;//*/
			}
		}
		//*
		internal static void RegisterSet (Type type, JCEnumValueContainer<JCEnumValue> container) {
			if (type == typeof (JCFlexibleEnumSetAccessor))
				return;
			if (GetSet (type) == null) {
				RegisteredSets[type] = container;
			}
		}//*/

		static bool IsSubclassOfRawGeneric (Type generic, Type toCheck) {
			while (toCheck != null && toCheck != typeof (object)) {
				var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition () : toCheck;
				if (generic == cur) {
					return true;
				}
				toCheck = toCheck.BaseType;
			}
			return false;
		}

		public static JCEnumValueContainer<JCEnumValue> GetSet (Type t) {
			JCEnumValueContainer<JCEnumValue> set = null;
			if (!RegisteredSets.ContainsKey (t)) {
			}
			RegisteredSets.TryGetValue (t, out set);
			return set;
		}
	}
}
