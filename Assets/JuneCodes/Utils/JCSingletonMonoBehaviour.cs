﻿using System;
using UnityEngine;

#pragma warning disable 649
namespace JuneCodes {
	public class JCSingletonMonoBehaviour<T> : MonoBehaviour where T : JCSingletonMonoBehaviour<T> {

		public static bool CreateSingletonIfNull {
			get {
				JCCreateSingletonIfNullAttribute attribute = (JCCreateSingletonIfNullAttribute)Attribute.GetCustomAttribute (
					typeof (T),
					typeof (JCCreateSingletonIfNullAttribute));
				return attribute != null;
			}
		}

		protected static T _instance;
		public static T instance {
			get {
				if (!_instance) {
					instance = FindObjectOfType (typeof (T)) as T;

					if (!_instance) {
						if (!JCApplicationUtils.Quitting) {
							if (CreateSingletonIfNull)
								instance = new GameObject (typeof (T).ToString ()).AddComponent<T> ();
							else
								Debug.LogError ("There needs to be one active " + typeof (T) + " script on a GameObject in your scene.");
						}
					}
				}

				return _instance;
			}
			protected set {
				if (value == null)
					return;
				if (_instance == null) {
					_instance = value;
					if (Application.isPlaying && value.Persistent)
						DontDestroyOnLoad (_instance.gameObject);
				}
				else {
					if (value.KeepOldest) {
						Destroy (value.gameObject);
					}
					else {
						Destroy (_instance.gameObject);
						_instance = value;
					}
				}
			}
		}

		protected virtual bool KeepOldest { get { return true; } }
		protected virtual bool Persistent { get { return true; } }
	}
}

