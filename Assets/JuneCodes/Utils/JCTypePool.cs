﻿using System;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 649
namespace JuneCodes {
	public abstract class JCTypePoolAccessor {
		protected class JCTypePoolContainer : MonoBehaviour {
			private JCTypePoolAccessor Pool;
			private Type PoolType;

			public static JCTypePoolContainer CreatePoolContainer (GameObject parent, JCTypePoolAccessor pool, Type poolType) {
				JCTypePoolContainer container = parent.AddComponent<JCTypePoolContainer> ();
				container.Pool = pool;
				container.PoolType = poolType;
				container.InitContainer ();
				return container;
			}

			private void InitContainer () {
				DontDestroyOnLoad (gameObject);
				Pool.InitPool ();
			}

			private void OnDestroy () {
				Pool.DestroyPool ();
			}

#if UNITY_EDITOR
			private void OnApplicationQuit () {
				//Pools = null;
			}
#endif
		}

		protected abstract void InitPool ();

		protected abstract void DestroyPool ();
	}

	public class JCTypePool<T> : JCTypePoolAccessor where T : MonoBehaviour {
		private static Dictionary<Type, JCTypePool<T>> Pools;

		/* Since generic type MonoBehaviours cannot be added to GameObjects,
		 * you need to call this function GetPool like so after declaring
		 * the class inheriting JCObjectPool
		 * 
		protected class MiscItemPool : JCObjectPool<MiscItem> { }
		public void InstanciatePool () {
			SlotsPool = MiscItemPool.GetPool (g => { return g.AddComponent<MiscItemPool> (); });
		}
		//*/
		public static JCTypePool<T> GetPool (int increment = -1) {
			JCTypePool<T> pool = null;
			if (Pools != null)
				Pools.TryGetValue (typeof (T), out pool);
			//Debug.Log ("Getting ObjectPool for type " + typeof (T).ToString () + " : " + (pool == null ? "null" : pool.Count.ToString ()));

			if (pool == null) {
				GameObject poolGO = new GameObject ("Pool_" + typeof (T).ToString ());
				poolGO.SetActive (false);
				pool = new JCTypePool<T> ();
				pool.PoolContainer = JCTypePoolContainer.CreatePoolContainer (poolGO, pool, typeof (T));
				//pool = addPoolComponent.Invoke(poolGO);
				if (increment > 0) {
					pool._Increment = increment;
				}
			}

			return pool;
		}

		JCTypePoolContainer PoolContainer;

		Stack<T> ObjectsStack;
		//Queue<T> ObjectsQueue;
		int ObjectCounter = 0;

		[SerializeField]
		private GameObject _Blueprint;
		public GameObject Blueprint {
			get {
				if (_Blueprint == null) {
					_Blueprint = JCRegisteredTypePools.instance.GetPoolPrefab (typeof (T));
				}
				return _Blueprint;
			}
			set {
				if (value != _Blueprint) {
					Count = 0; // we'll need to reassign the pool whatever the value is
					if (value != null && value.GetComponent<T> () != null) {
						_Blueprint = value;
						Count = _Increment;
					}
					else {
						_Blueprint = null;
					}
				}
			}
		}

		public int Increment {
			get { return _Increment; }
			set {
				if (value < 1)
					_Increment = 1;
				else
					_Increment = value;
			}
		}
		private int _Increment = 10;

		public int Count {
			get { return ObjectsStack.Count; }
			//get { return ObjectsQueue.Count; }
			private set {
				SetCount (value);
			}
		}

		protected override void InitPool () {
			if (Pools == null)
				Pools = new Dictionary<Type, JCTypePool<T>> ();
			Pools.Add (typeof (T), this);
			//Pools[typeof (T)] = this;
			ObjectsStack = new Stack<T> (_Increment);
			//ObjectsQueue = new Queue<T> (_Increment);
		}

		protected override void DestroyPool () {
			//if (!PersistentInfo.applicationQuit) Pools.Remove(typeof(T));
		}

		public T Extract (Transform newParent = null) {
			if (Blueprint == null)
				return null;
			T o = null;
			if (Count > 0) {
				o = ObjectsStack.Pop ();
				//o = ObjectsQueue.Dequeue ();
			}
			else {
				SetCount (_Increment);
				o = ObjectsStack.Pop ();
				//o = ObjectsQueue.Dequeue ();
			}
			if (o != null) {
				o.transform.SetParent (newParent);
			}
			//Debug.Log("Extract : " + Count);
			return o;
		}

		public void Store (T o) {
			//if (PersistentInfo.applicationQuit) return;
			o.transform.SetParent (PoolContainer.transform);
			o.gameObject.SetActive (false);
			ObjectsStack.Push (o);
			//ObjectsQueue.Enqueue (o);
			//Debug.Log("Store : " + Count);
		}

		private T CreateObject () {
			if (Blueprint == null)
				return null;
			T o = GameObject.Instantiate (_Blueprint, PoolContainer.transform).GetComponent<T> ();
			o.gameObject.name = _Blueprint.name + "_" + ++ObjectCounter;
			return o;
		}

		private void SetCount (int c) {
			if (c < 0) {
				c = 0;
			}//*
			if (c < ObjectsStack.Count) {
				for (int i = ObjectsStack.Count; i > c; --i) {
					T o = ObjectsStack.Pop ();
					GameObject.Destroy (o.gameObject);
				}
			}
			else if (c > ObjectsStack.Count) {
				if (_Blueprint != null) {
					for (int i = ObjectsStack.Count; i < c; ++i)
						Store (CreateObject ());
				}
			}/*/
			if (c < ObjectsQueue.Count) {
				for (int i = ObjectsQueue.Count; i > c; --i) {
					T o = ObjectsQueue.Dequeue ();
					GameObject.Destroy (o.gameObject);
				}
			}
			else if (c > ObjectsQueue.Count) {
				if (_Blueprint != null) {
					for (int i = ObjectsQueue.Count; i < c; ++i)
						Store (CreateObject ());
				}
			}//*/
		}
	}
}

