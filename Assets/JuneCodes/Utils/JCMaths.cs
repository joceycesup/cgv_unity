﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace JuneCodes.Maths {

	[Serializable]
	public struct Range : IComparable<Range> {
		public float x;
		public float y;

		public float center { get { return (x + y) / 2f; } }
		public float gap { get { return y - x; } }

		public Range (float x, float y) {
			if (y < x) {
				this.x = y;
				this.y = x;
			}
			else {
				this.x = x;
				this.y = y;
			}
		}

		public bool IsNaN () {
			return float.IsNaN (x) || float.IsNaN (y);
		}

		public static bool Union (Range a, Range b, out Range union) {
			if (b.IsNaN () || a.IsNaN ()) {
				union = Range.NaN;
				return false;
			}
			if (a.x > b.y || b.x > a.y) {
				union = Range.NaN;
				return false;
			}
			union = new Range (Mathf.Min (a.x, b.x), Mathf.Max (a.y, b.y));
			return true;
		}

		public static bool Intersection (Range a, Range b, out Range intersection) {
			if (b.IsNaN () || a.IsNaN ()) {
				intersection = Range.NaN;
				return false;
			}
			if (a.x >= b.y || b.x >= a.y) {
				intersection = Range.NaN;
				return false;
			}
			intersection = new Range (Mathf.Max (a.x, b.x), Mathf.Min (a.y, b.y));
			return true;
		}

		public static bool Substract (Range a, Range b, out Range left, out Range right) {
			left = Range.NaN;
			right = Range.NaN;
			if (b.IsNaN () || a.IsNaN ())
				return false;
			if (a.x >= b.y || b.x >= a.y)
				return false;
			if (b.x <= a.x && b.y >= a.y)
				return true;

			if (b.x > a.x) {
				if (b.y < a.y)
					right = new Range (b.y, a.y);
				left = new Range (a.x, b.x);
				return true;
			}
			else {
				left = new Range (b.y, a.y);
				return true;
			}
		}

		public int CompareTo (Range other) {
			if (x < other.x)
				return -1;
			if (y > other.y)
				return 1;

			return 0;
		}

		public static implicit operator Vector2 (Range r) {
			return new Vector2 (r.x, r.y);
		}

		public static implicit operator Range (Vector2 v) {
			return new Range (v.x, v.y);
		}

		public override string ToString () {
			return string.Format ("Range : [{0};{1}]", x, y);
		}

		static readonly Range zeroRange = new Range (0f, 0f);
		public static Range zero { get { return zeroRange; } }
		static readonly Range NaNRange = new Range (float.NaN, float.NaN);
		public static Range NaN { get { return NaNRange; } }

		public struct RangeSet : IEnumerable<Range> {
			private List<Range> Ranges;

			public RangeSet (params Range[] ranges) {
				Ranges = new List<Range> (ranges.Length);
				for (int i = 0; i < ranges.Length - 1; ++i) {
					for (int j = i + 1; j < ranges.Length; ++j) {
						if (Range.Union (ranges[i], ranges[j], out Range union)) {
							ranges[i] = Range.NaN;
							ranges[j] = union;
							break;
						}
					}
				}
				for (int i = 0; i < ranges.Length; ++i) {
					if (!ranges[i].IsNaN ())
						Ranges.Add (ranges[i]);
				}
				Ranges.Sort ();
			}

			public float min {
				get { return IsEmpty () ? float.NaN : Ranges[0].x; }
				set {
					if (IsEmpty ())
						return;
					Ranges[0] = new Range (value, Ranges[0].y);
				}
			}
			public float max {
				get { return IsEmpty () ? float.NaN : Ranges[Ranges.Count - 1].y; }
				set {
					if (IsEmpty ())
						return;
					Ranges[Ranges.Count - 1] = new Range (Ranges[Ranges.Count - 1].x, value);
				}
			}

			public bool IsEmpty () {
				return Ranges == null || Ranges.Count <= 0;
			}

			public void Add (Range range) {
				if (range.IsNaN ())
					return;
				bool foundUnion = false;
				for (int i = 0; i < Ranges.Count; ++i) {
					if (Range.Union (Ranges[i], range, out Range union)) {
						Ranges[i] = union;
						foundUnion = true;
						break;
					}
				}
				if (!foundUnion)
					Ranges.Add (range);
			}

			public void Substract (Range range) {
				if (range.IsNaN ())
					return;
				for (int i = 0; i < Ranges.Count; ++i) {
					if (Range.Substract (Ranges[i], range, out Range left, out Range right)) {
						Ranges[i] = left;
						if (!right.IsNaN ()) {
							if (i >= Ranges.Count - 1)
								Ranges.Add (right);
							else
								Ranges.Insert (i + 1, right);
							break;
						}
					}
				}
				RemoveNaN ();
			}

			private void RemoveNaN () {
				for (int i = Ranges.Count - 1; i >= 0; --i) {
					if (Ranges[i].IsNaN ())
						Ranges.RemoveAt (i);
				}
			}

			public IEnumerator<Range> GetEnumerator () {
				return ((IEnumerable<Range>)Ranges).GetEnumerator ();
			}

			IEnumerator IEnumerable.GetEnumerator () {
				return ((IEnumerable<Range>)Ranges).GetEnumerator ();
			}

			public override string ToString () {
				string res = base.ToString ();
				foreach (Range range in Ranges) {
					res += '\n' + range.ToString ();
				}
				return res;
			}
		}
	}

	[Serializable]
	public struct AngleInterval {
		[SerializeField] [Range (-180f, 180f)] private float _Start;
		[SerializeField] [Range (-180f, 180f)] private float _End;
		[HideInInspector]
		[SerializeField] private float _DeltaAngle;
		[HideInInspector]
		[SerializeField] private bool _OverlapsWrapArea;
		public float Start { get { return _Start; } private set { _Start = value; } }
		public float End { get { return _End; } private set { _End = value; } }
		public float DeltaAngle { get { return _DeltaAngle; } private set { _DeltaAngle = value; } }
		public bool OverlapsWrapArea { get { return _OverlapsWrapArea; } private set { _OverlapsWrapArea = value; } }

		/// <summary>
		/// Start to End should be clockwise
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		public AngleInterval (float start, float end) {
			_Start = JCMaths.SignedNormalize (start);
			_End = JCMaths.SignedNormalize (end);

			_OverlapsWrapArea = (_End < _Start);

			if (_Start <= _End)
				_DeltaAngle = _End - _Start;
			else
				_DeltaAngle = 360f + _End - _Start;
		}

		public bool Contains (AngleInterval interval) {
			if (OverlapsWrapArea) {
				if (interval.OverlapsWrapArea)
					return (interval.Start >= Start || interval.Start <= End) && (interval.End <= End || interval.End >= Start);
				else
					return interval.End <= End ^ interval.Start >= Start;
			}
			else {
				if (interval.OverlapsWrapArea)
					return false;
				else
					return interval.Start >= Start && interval.End <= End;
			}
		}

		public bool Contains (float angle) {
			angle = JCMaths.SignedNormalize (angle);
			if (OverlapsWrapArea)
				return angle >= Start ^ angle <= End;
			else
				return angle >= Start && angle <= End;
		}


		public bool Intersects (AngleInterval interval) {
			if (OverlapsWrapArea) {
				if (interval.OverlapsWrapArea)
					return true;
				else
					return interval.Start <= End || interval.End >= Start;
			}
			else {
				if (interval.OverlapsWrapArea)
					return Start <= interval.End || End >= interval.Start;
				else
					return !(interval.Start > End || Start > interval.End);
			}
		}

		public float LerpAngle (float t) {
			float res = Start + DeltaAngle * t;
			if (OverlapsWrapArea)
				return res > 180f ? res - 360f : res;
			else
				return res;
		}

		public override bool Equals (object obj) {
			if (!(obj is AngleInterval)) {
				return false;
			}

			var interval = (AngleInterval)obj;
			return this == interval;
		}

		public override int GetHashCode () {
			var hashCode = 1704514873;
			hashCode = hashCode * -1521134295 + _Start.GetHashCode ();
			hashCode = hashCode * -1521134295 + _End.GetHashCode ();
			return hashCode;
		}

		public static AngleInterval operator ~ (AngleInterval interval) {
			return new AngleInterval (interval.End, interval.Start);
		}

		public static bool operator == (AngleInterval i1, AngleInterval i2) {
			return i1.Start == i2.Start && i1.End == i2.End;
		}

		public static bool operator != (AngleInterval i1, AngleInterval i2) {
			return i1.Start != i2.Start || i1.End != i2.End;
		}

		public void DrawGizmosAngleInterval (Transform transform, int iterations, float length = 1f, bool drawRadii = true) {
			Vector3 start = JCMaths.PointFromAngle (Start, transform, length);
			Vector3 end = JCMaths.PointFromAngle (End, transform, length);
			Gizmos.DrawLine (transform.position, start);
			Gizmos.DrawLine (transform.position, end);
			if (iterations <= 0)
				return;
			Vector3 from = start;
			for (int i = 1; i <= iterations; ++i) {
				Vector3 to = JCMaths.PointFromAngle (LerpAngle (((float)i) / (iterations + 1)), transform, length);
				Gizmos.DrawLine (from, to);
				if (drawRadii)
					Gizmos.DrawLine (transform.position, to);
				from = to;
			}
			Gizmos.DrawLine (from, end);
		}
	}

	public static class JCMaths {
		public static Bounds AsBounds (this Rect rect) {
			return new Bounds (rect.center, rect.size);
		}

		public static Rect AsRect (this Bounds bounds) {
			return new Rect (bounds.min, bounds.size);
		}

		public static bool Contains (this Rect rect, Rect other) {
			return rect.Contains (other.min) && rect.Contains (other.max);
		}

		public static float HorizontalDistanceToSegment (Vector3 start, Vector3 end, Vector3 pos, out float closest) {
			return DistanceToSegment (start.Horizontal (), end.Horizontal (), pos.Horizontal (), out closest);
		}

		public static float DistanceToSegment (Vector2 start, Vector2 end, Vector2 pos, out float closestT) {
			Vector2 v = end - start;
			Vector2 w = pos - start;

			float c1 = Vector2.Dot (w, v);
			if (c1 <= 0) {  // before start
				closestT = 0f;
				return Vector2.Distance (pos, start);
			}
			float c2 = Vector2.Dot (v, v);
			if (c2 <= c1) {  // after end
				closestT = 1f;
				return Vector2.Distance (pos, end);
			}

			closestT = c1 / c2;

			Vector2 p = start + closestT * v;
			return Vector2.Distance (pos, p);
		}

		public static float DistanceToSegment (Vector3 start, Vector3 end, Vector3 pos, out float closestT) {
			Vector3 v = end - start;
			Vector3 w = pos - start;

			float c1 = Vector3.Dot (w, v);
			if (c1 <= 0) {  // before start
				closestT = 0f;
				return Vector3.Distance (pos, start);
			}
			float c2 = Vector3.Dot (v, v);
			if (c2 <= c1) {  // after end
				closestT = 1f;
				return Vector3.Distance (pos, end);
			}

			closestT = c1 / c2;

			Vector3 p = start + closestT * v;
			return Vector3.Distance (pos, p);
		}

		public static Vector2 Horizontal (this Vector3 pos) {
			return new Vector2 (pos.x, pos.z);
		}

		#region Angles
		public static Vector3 PointFromAngle (float angle, Transform transform, float length = 1f) {
			return transform.position + Quaternion.AngleAxis (angle, transform.up) * transform.forward * length;
		}

		public static float HorizontalAngle (Vector3 forward, Vector3 direction) {
			forward.y = direction.y = 0.0f;
			return Vector3.Angle (forward, direction);
		}

		public static float SignedHorizontalAngle (Vector3 forward, Vector3 direction) {
			forward.y = direction.y = 0.0f;
			return Vector3.SignedAngle (forward, direction, Vector3.up);
		}

		public static float WrapAngle (float angle) {
			return angle >= 0f ? angle : 360f + angle;
		}

		public static float Normalize (float angle) {
			angle = angle % 360f;
			if (angle < 0)
				angle += 360;
			return angle;
		}

		public static float SignedNormalize (float angle) {
			angle = (angle + 180f) % 360f;
			if (angle < 0)
				angle += 360;
			return angle - 180f;
		}

		public static float VerticalAngle (Vector3 forward, Vector3 direction) {
			return Vector3.Angle (Vector3.up, forward) - Vector3.Angle (Vector3.up, direction);
		}

		public static float DeltaAngle (float a1, float a2) {
			return 180f - Mathf.Abs (Mathf.Abs (a1 - a2) - 180f);
		}
		#endregion Angles
	}
}
