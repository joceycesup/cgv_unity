﻿using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 649
namespace JuneCodes {
	[JCCreateSingletonIfNull]
	public class JCPrefabPool : JCSingletonMonoBehaviour<JCPrefabPool> {

		protected class PrefabPool {
			private GameObject _Prefab;
			public GameObject Prefab {
				get { return _Prefab; }
				set {
					if (value == null || _Prefab != null)
						return;
					_Prefab = value;
					PrefabID = _Prefab.GetInstanceID ();
				}
			}
			public int PrefabID { get; private set; }
			private int _Increment = 16;
			public int Increment {
				get { return _Increment; }
				set {
					if (value < 1)
						_Increment = 1;
					else
						_Increment = value;
				}
			}
			private Stack<GameObject> _Instances;
			private int _ObjectCounter = 0;

			public int Count {
				get { return _Instances.Count; }
				private set {
					SetCount (value);
				}
			}

			public PrefabPool (GameObject prefab, int increment = 16) {
				Prefab = prefab;
				Increment = increment;
				_Instances = new Stack<GameObject> (Increment);
				SetCount (Increment);
			}

			public void Destroy () {
				foreach (GameObject go in _Instances) {
					GameObject.Destroy (go);
				}
				_Instances.Clear ();
			}

			public void SetCount (int c) {
				if (c < 0) {
					c = 0;
				}
				if (c < _Instances.Count) {
					for (int i = _Instances.Count; i > c; --i) {
						GameObject.Destroy (_Instances.Pop ());
					}
				}
				else if (c > _Instances.Count) {
					for (int i = _Instances.Count; i < c; ++i)
						_Instances.Push (CreateObject ());
				}
			}

			private GameObject CreateObject () {
				GameObject res = Instantiate (Prefab);
				res.name = Prefab.name + "_" + ++_ObjectCounter;
				res.transform.SetParent (JCPrefabPool.instance.transform);
				res.SetActive (false);
				return res;
			}

			public GameObject ExtractInstance () {
				if (_Instances.Count <= 0) {
					SetCount (Increment);
				}
				return _Instances.Pop ();
			}

			public void StoreInstance (GameObject prefabInstance) {
				_Instances.Push (prefabInstance);
				if (prefabInstance.activeInHierarchy)
					prefabInstance.SetActive (false);
				prefabInstance.transform.SetParent (JCPrefabPool.instance.transform);
				//prefabInstance.transform.parent = JCPrefabPool.instance.transform;
			}
		}

		private static Dictionary<int, PrefabPool> Pools = new Dictionary<int, PrefabPool> ();

		public void CreatePool (GameObject prefab, int poolSize) {
			if (prefab == null)
				return;

			int prefabKey = prefab.GetInstanceID ();
			if (!Pools.ContainsKey (prefabKey)) {
				Pools.Add (prefabKey, new PrefabPool (prefab, poolSize));
			}
		}

		public static void DestroyPool (GameObject prefab) {
			if (prefab == null)
				return;

			DestroyPool (prefab.GetInstanceID ());
		}

		public static void DestroyPool (int prefabID) {
			if (JCApplicationUtils.Quitting)
				return;

			if (Pools.ContainsKey (prefabID)) {
				PrefabPool pool = Pools[prefabID];
				pool.Destroy ();
				Pools.Remove (prefabID);
			}
		}

		public static GameObject Extract (GameObject prefab, Transform newParent = null) {
			return Extract (prefab, -1, newParent);
		}

		public static GameObject Extract (GameObject prefab, int poolSize, Transform newParent = null) {
			if (prefab == null)
				return null;
			if (JCApplicationUtils.Quitting)
				return null;

			int prefabKey = prefab.GetInstanceID ();
			GameObject res = null;
			if (Pools.ContainsKey (prefabKey)) {
				PrefabPool pool = Pools[prefabKey];
				res = pool.ExtractInstance ();
			}
			else {
				PrefabPool pool = poolSize <= 0 ? new PrefabPool (prefab) : new PrefabPool (prefab, poolSize);
				Pools.Add (prefabKey, pool);
				res = pool.ExtractInstance ();
			}

			if (res != null) {
				res.transform.SetParent (newParent);
			}
			return res;
		}

		public static GameObject Extract (int prefabID, Transform newParent = null) {
			if (JCApplicationUtils.Quitting)
				return null;

			if (!Pools.ContainsKey (prefabID)) {
				return null;
			}
			GameObject res = Pools[prefabID].ExtractInstance ();

			if (res != null) {
				res.transform.SetParent (newParent);
			}
			return res;
		}

		public static bool Store (GameObject prefab, GameObject prefabInstance) {
			if (prefab == null || prefabInstance == null)
				return false;
			if (JCApplicationUtils.Quitting)
				return false;

			int prefabKey = prefab.GetInstanceID ();
			if (Pools.ContainsKey (prefabKey)) {
				Pools[prefabKey].StoreInstance (prefabInstance);
			}
			else {
				PrefabPool pool = new PrefabPool (prefab);
				Pools.Add (prefabKey, pool);
				pool.StoreInstance (prefabInstance);
			}
			return true;
		}

		public static bool Store (int prefabID, GameObject prefabInstance) {
			if (prefabInstance == null)
				return false;
			if (JCApplicationUtils.Quitting)
				return false;

			if (!Pools.ContainsKey (prefabID))
				return false;
			Pools[prefabID].StoreInstance (prefabInstance);
			return true;
		}
	}
}
