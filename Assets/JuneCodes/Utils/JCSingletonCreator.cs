﻿using System;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;

namespace JuneCodes {
	public class JCSingletonCreator {

		[RuntimeInitializeOnLoadMethod]
		static void CreateSingleton () {
			Assembly attrAssembly = typeof (JCCreateSingletonOnStartAttribute).Assembly;
			AssemblyName attrAN = attrAssembly.GetName ();
			foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies ()) {
				bool assemblyContainsReference = false;
				if (!(assemblyContainsReference = (attrAssembly == assembly))) {
					foreach (var an in assembly.GetReferencedAssemblies ()) {
						if (AssemblyName.ReferenceMatchesDefinition (an, attrAN)) {
							assemblyContainsReference = true;
							break;
						}
					}
				}
				if (assemblyContainsReference) {
					foreach (var type in assembly.GetTypes ()) {
						if (Attribute.GetCustomAttribute (type, typeof (JCCreateSingletonOnStartAttribute)) != (Attribute)null) {
							PropertyInfo propertyInfo = type.GetProperty ("instance", BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
							if (propertyInfo?.GetValue (null, null) == null)
								Debug.LogError ("Could not create singleton for type " + type.ToString ());
						}
					}
				}
			}
		}
	}
}
