﻿using UnityEngine;

namespace JuneCodes {
	public class JCApplicationUtils {

		public static bool Quitting { get; protected set; } = false;

		[RuntimeInitializeOnLoadMethod]
		static void RunOnStart () {
			Quitting = false;
			Application.quitting += () => { Quitting = true; };
		}

		public static void ExitGame () {
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit ();
#endif
		}
	}
}
