﻿using System;
using UnityEngine;

#pragma warning disable 649
namespace JuneCodes {
	public class JCSingletonScriptableObject<T> : ScriptableObject where T : JCSingletonScriptableObject<T> {
		/* Inheriting classes should have this attribute :
		[CreateAssetMenu(fileName = "FileName.asset", menuName = "Menu/Menu Item")]
		//*/
		public static string ResourceName {
			get {
				CreateAssetMenuAttribute attribute = (CreateAssetMenuAttribute)Attribute.GetCustomAttribute (
					typeof (T),
					typeof (CreateAssetMenuAttribute));
				if (attribute == null)
					Debug.LogError ("The class " + typeof (T) + " does not have an attribute CreateAssetMenuAttribute");
				return attribute.fileName;
			}
		}

		protected static T _instance;
		public static T instance {
			get {
				if (_instance == null) {
					string resourceName = ResourceName;
					int i = resourceName.IndexOf (".");
					if (i > 0)
						resourceName = resourceName.Substring (0, i);
					instance = Resources.Load<T> (resourceName);
					if (_instance == null) {
						Debug.LogError ("The class " + typeof (T) + " does not have an asset file with the default fileName.\nMaybe the asset file was put in a subfolder.");
					}
				}
				if (_instance != null && !_instance.IsInitialized ())
					_instance.InitSingleton ();
				return _instance;
			}
			protected set {
				_instance = value;
			}
		}

		private void Awake () {
			if (_instance == null || !_instance.KeepOldest)
				_instance = this as T;
		}

		public void SetAsInstance() {
			instance = this as T;
		}

		protected virtual bool KeepOldest => true;

		protected virtual bool IsInitialized () { return true; }

		protected virtual void InitSingleton () { }
	}
}
