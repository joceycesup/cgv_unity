﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 649
namespace JuneCodes {
	[CreateAssetMenu (fileName = "RegisteredTypePoolObjects.asset", menuName = "JuneCodes/RegisteredTypePoolObjects")]
	public class JCRegisteredTypePools : JCSingletonScriptableObject<JCRegisteredTypePools>, IEnumerable<GameObject> {

		[SerializeField]
		private List<MonoBehaviour> Prefabs;
		private Dictionary<Type, GameObject> _Dictionary;
		private Dictionary<Type, GameObject> Dictionary {
			get {
				RefreshDictionary ();
				return _Dictionary;
			}
		}

		public void RefreshDictionary () {
			if (Prefabs == null) {
				Prefabs = new List<MonoBehaviour> ();
			}
			if (_Dictionary == null) {
				_Dictionary = new Dictionary<Type, GameObject> (Prefabs.Count);
			}
			if (_Dictionary.Count != Prefabs.Count) {
				_Dictionary.Clear ();
				foreach (MonoBehaviour v in Prefabs) {
					if (v == null) {
						Debug.LogWarning (this.name + " contains null elements!");
						continue;
					}
					if (_Dictionary.ContainsKey (v.GetType ())) {
						Debug.LogWarning (this.name + " has multiple values for the type " + v.GetType ());
						continue;
					}
					_Dictionary.Add (v.GetType (), v.gameObject);
					//Debug.Log("Registering type " + v.GetType());
				}
			}
		}

		public GameObject GetPoolPrefab (Type type) {
			GameObject res;
			if (!Dictionary.TryGetValue (type, out res)) {
				Debug.LogError ("The prefab for type " + type + " has not yet been registered!");
			}
			return res;
		}

		public IEnumerator<GameObject> GetEnumerator () {
			return Dictionary.Values.GetEnumerator ();
		}

		IEnumerator IEnumerable.GetEnumerator () {
			return Dictionary.Values.GetEnumerator ();
		}

		public bool TypeIsRegistered (Type t) {
			return Dictionary.ContainsKey (t);
		}
	}
}
