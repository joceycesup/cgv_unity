﻿using System;
using System.Reflection;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace JuneCodes.Editor {
	public static class JCEditorUtility {

		public static Action EditorSceneChanged;

		private static Scene _CurrentScene;

		static JCEditorUtility () {
			// Even if update is not the most elegant. Using hierarchyWindowChanged for CPU sake will not work in all cases, because when hierarchyWindowChanged is called, Time's values might be all higher than current values. Why? Because current values are set at the first frame. If you keep reloading the same scene, this case happens.
			EditorApplication.update += JCEditorUtility.DetectChangeScene;
		}

		private static void DetectChangeScene () {
			if (Application.isPlaying)
				return;
			if (_CurrentScene != EditorSceneManager.GetActiveScene ()) {
				_CurrentScene = EditorSceneManager.GetActiveScene ();

				EditorSceneChanged?.Invoke ();
			}
		}

		public static readonly Color ErrorColor = new Color (1.0f, 0.4f, 0.4f);
		public static readonly Color WarningColor = new Color (.9f, 0.9f, 0.2f);

		private static GUIStyle _ToggleButtonStyleNormal = null;
		private static GUIStyle ToggleButtonStyleNormal {
			get {
				if (_ToggleButtonStyleNormal == null)
					_ToggleButtonStyleNormal = "Button";
				return _ToggleButtonStyleNormal;
			}
		}
		private static GUIStyle _ToggleButtonStyleToggled = null;
		private static GUIStyle ToggleButtonStyleToggled {
			get {
				if (_ToggleButtonStyleToggled == null) {
					_ToggleButtonStyleToggled = new GUIStyle (ToggleButtonStyleNormal);
					_ToggleButtonStyleToggled.normal.background = ToggleButtonStyleToggled.active.background;
				}
				return _ToggleButtonStyleToggled;
			}
		}

		public static GUIStyle ToggleButtonStyle (bool toggled) {
			return toggled ? ToggleButtonStyleToggled : ToggleButtonStyleNormal;
		}

		public static GUIContent IconContent (string text, string icon, string tooltip = "") {
			GUIContent content;

			if (string.IsNullOrEmpty (icon)) {
				content = new GUIContent ();
			}
			else {
				content = EditorGUIUtility.IconContent (icon);
			}

			content.text = text;
			content.tooltip = tooltip;
			return content;
		}

		public static void EditorPrefsSetColor (string key, Color value, bool setAlpha = false) {
			EditorPrefs.SetFloat (key + "_R", value.r);
			EditorPrefs.SetFloat (key + "_G", value.g);
			EditorPrefs.SetFloat (key + "_B", value.b);
			if (setAlpha)
				EditorPrefs.SetFloat (key + "_A", value.a);
		}

		public static Color EditorPrefsGetColor (string key, Color defaultValue) {
			if (string.IsNullOrWhiteSpace (key))
				return defaultValue;
			return new Color (
				EditorPrefs.GetFloat (key + "_R", defaultValue.r),
				EditorPrefs.GetFloat (key + "_G", defaultValue.g),
				EditorPrefs.GetFloat (key + "_B", defaultValue.b),
				EditorPrefs.GetFloat (key + "_A", defaultValue.a));
		}

		/// <summary>
		/// Return last control ID setted in GUI
		/// </summary>
		/// <returns>Last control ID setted</returns>
		public static int GetLastControlId () {
			FieldInfo getLastControlId = typeof (EditorGUIUtility).GetField ("s_LastControlID", BindingFlags.Static | BindingFlags.NonPublic);
			if (getLastControlId != null)
				return (int)getLastControlId.GetValue (null);
			return 0;
		}

		public static Rect GetFieldRect (GUIStyle style, GUIContent content) {
			Rect res = EditorGUILayout.GetControlRect (true, style.CalcHeight (content, EditorGUIUtility.currentViewWidth));
			res.x += EditorGUIUtility.labelWidth;
			res.width -= EditorGUIUtility.labelWidth;
			return res;
		}
	}
}
