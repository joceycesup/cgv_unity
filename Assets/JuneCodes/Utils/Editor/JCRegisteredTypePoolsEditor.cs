﻿using System;
using UnityEngine;
using UnityEditor;

namespace JuneCodes.Editor {
	[CustomEditor (typeof (JCRegisteredTypePools))]
	public class JCRegisteredTypePoolsEditor : UnityEditor.Editor {

		JCRegisteredTypePools Pools;

		GameObject CurrentGO;
		int CurrentComponentIndex;
		Type CurrentComponentType;
		Component[] GOComponents;
		string[] DisplayedOptions = new string[0];
		bool TypeAlreadyRegistered = false;

		SerializedProperty RegisteredPrefabListProp;

		JCReorderableList ValuesList;

		private void OnEnable () {
			Pools = (JCRegisteredTypePools) target;

			RegisteredPrefabListProp = serializedObject.FindProperty ("Prefabs");

			SetOptions (null);

			ValuesList = new JCReorderableList (RegisteredPrefabListProp.serializedObject, RegisteredPrefabListProp, true, true, false, true);//*
			ValuesList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
				rect = ValuesList.ElementInitializer (rect, index, out SerializedProperty element);
				if (element == null)
					return;

				if (GUI.Button (new Rect (rect.x, rect.y, 40f, EditorGUIUtility.singleLineHeight), "Edit")) {
					MonoBehaviour mb = element.objectReferenceValue as MonoBehaviour;
					SetOptions (mb.gameObject, mb.GetType ());
					ValuesList.index = index;
					ValuesList.onRemoveCallback (ValuesList);
					Pools.RefreshDictionary ();
				}
				else {
					GUIContent typeLabel = new GUIContent (element.objectReferenceValue.GetType ().Name);
					Vector2 textSize = GUI.skin.label.CalcSize (typeLabel);

					EditorGUI.BeginDisabledGroup (true);
					EditorGUI.ObjectField (new Rect (rect.x + 42f, rect.y, rect.width - 42f - textSize.x, EditorGUIUtility.singleLineHeight), element.objectReferenceValue, typeof (MonoBehaviour), false);
					EditorGUI.EndDisabledGroup ();

					EditorGUI.LabelField (new Rect (rect.xMax - textSize.x, rect.y, textSize.x, rect.height), typeLabel);
				}
			};//*/
		}

		public override void OnInspectorGUI () {
			serializedObject.Update ();

			ValuesList.DoLayoutList ();

			/*
			for (int i = 0; i < RegisteredPrefabListProp.arraySize; ++i) {
				EditorGUILayout.BeginHorizontal ();

				SerializedProperty p = RegisteredPrefabListProp.GetArrayElementAtIndex (i);

				bool deleteElement = JCGUILayout.CrossButton ();
				if (deleteElement) {
					p.objectReferenceValue = null;
					RegisteredPrefabListProp.DeleteArrayElementAtIndex (i);
					Pools.RefreshDictionary ();
				}

				int ud = JCGUILayout.UpDown (i <= 0, i >= RegisteredPrefabListProp.arraySize - 1);
				if (ud != 0)
					RegisteredPrefabListProp.SwapArrayElements (i, i + ud);

				if (GUILayout.Button ("Edit", GUILayout.Width (40f))) {
					deleteElement = true;
					MonoBehaviour mb = p.objectReferenceValue as MonoBehaviour;
					SetOptions (mb.gameObject, mb.GetType ());
					p.objectReferenceValue = null;
					RegisteredPrefabListProp.DeleteArrayElementAtIndex (i);
					Pools.RefreshDictionary ();
				}
				if (!deleteElement) {
					EditorGUI.BeginDisabledGroup (true);
					EditorGUILayout.ObjectField (p.objectReferenceValue, typeof (MonoBehaviour), false);
					EditorGUI.EndDisabledGroup ();

					EditorGUILayout.LabelField (p.objectReferenceValue.GetType ().Name);
				}

				EditorGUILayout.EndHorizontal ();
			}//*/

			EditorGUILayout.Space ();

			GUILayout.BeginVertical (GUI.skin.box);

			EditorGUILayout.BeginHorizontal ();
			GameObject go = (GameObject) EditorGUILayout.ObjectField ("Prefab", CurrentGO, typeof (GameObject), false);
			EditorGUI.BeginDisabledGroup (go == null);
			if (JCGUILayout.CrossButton ()) {
				go = null;
			}
			EditorGUI.EndDisabledGroup ();

			EditorGUILayout.EndHorizontal ();

			if (go != CurrentGO) {
				SetOptions (go, CurrentComponentType);
			}

			EditorGUI.BeginDisabledGroup (CurrentGO == null || DisplayedOptions.Length <= 0);
			{
				int index = EditorGUILayout.Popup ("Type", CurrentComponentIndex, DisplayedOptions);
				if (index != CurrentComponentIndex) {
					CurrentComponentIndex = index;
					CurrentComponentType = GOComponents[index].GetType ();
					//Debug.Log ("Selected type : " + DisplayedOptions[CurrentComponentIndex]);
					TypeAlreadyRegistered = Pools.TypeIsRegistered (CurrentComponentType);
				}

				EditorGUILayout.Space ();

				if (TypeAlreadyRegistered) {
					EditorGUILayout.HelpBox (DisplayedOptions[CurrentComponentIndex] + " is already registered!", MessageType.Warning);
				}

				EditorGUI.BeginDisabledGroup (TypeAlreadyRegistered || CurrentComponentIndex < 0);
				if (GUILayout.Button ("Register New Type")) {
					int newElementIndex = RegisteredPrefabListProp.arraySize;
					RegisteredPrefabListProp.InsertArrayElementAtIndex (newElementIndex);
					RegisteredPrefabListProp.GetArrayElementAtIndex (newElementIndex).objectReferenceValue = GOComponents[CurrentComponentIndex] as MonoBehaviour;

					CurrentComponentIndex = -1;
					CurrentComponentType = null;

					SetOptions (null);

					Pools.RefreshDictionary ();
				}
				EditorGUI.EndDisabledGroup ();
			}
			EditorGUI.EndDisabledGroup ();

			GUILayout.EndVertical ();

			serializedObject.ApplyModifiedProperties ();
		}

		private void SetOptions (GameObject go, Type findIndexOf = null) {
			CurrentGO = go;
			CurrentComponentIndex = -1;
			TypeAlreadyRegistered = false;

			if (CurrentGO == null) {
				GOComponents = new Component[0];
				DisplayedOptions = new string[0];
			}
			else {
				GOComponents = CurrentGO.GetComponents (typeof (MonoBehaviour));
				DisplayedOptions = new string[GOComponents.Length];

				for (int i = 0; i < GOComponents.Length; ++i) {
					if (GOComponents[i].GetType () == findIndexOf) {
						CurrentComponentIndex = i;
					}
					DisplayedOptions[i] = GOComponents[i].GetType ().Name;
				}
			}
		}
	}
}
