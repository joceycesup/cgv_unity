﻿/*
 Written by: Lucas Antunes (aka ItsaMeTuni), lucasba8@gmail.com
 In: 2/15/2018
 The only thing that you cannot do with this script is sell it by itself without substantially modifying it.
 */

using System;
using UnityEditor;
using UnityEngine;

#pragma warning disable 649
namespace JuneCodes.Editor {
	[CustomPropertyDrawer (typeof (JCEnumFlagsAttribute))]
	public class JCEnumFlagsDrawer : PropertyDrawer {
		bool foldoutOpen = false;

		object theEnum;
		Array enumValues;
		Type enumUnderlyingType;

		object allValue;
		string allValueBinary;
		string[] enumNames;
		string[] enumValuesBinary;

		private static readonly float BoxSeparatorHeight = 5f;

		public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
			if (foldoutOpen)
				return EditorGUIUtility.singleLineHeight * (Enum.GetValues (fieldInfo.FieldType).Length + 2) + BoxSeparatorHeight;
			else
				return EditorGUIUtility.singleLineHeight;
		}

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
			theEnum = fieldInfo.GetValue (property.serializedObject.targetObject);
			Type enumType = theEnum.GetType ();
			enumUnderlyingType = Enum.GetUnderlyingType (enumType);

			//We need to convert the enum to its underlying type, if we don't it will be boxed
			//into an object later and then we would need to unbox it like (UnderlyingType)(EnumType)theEnum.
			//If we do this here we can just do (UnderlyingType)theEnum later (plus we can visualize the value of theEnum in VS when debugging)
			theEnum = Convert.ChangeType (theEnum, enumUnderlyingType);

			EditorGUI.BeginProperty (position, label, property);

			Rect newPos = new Rect (position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
			Enum targetEnum = (Enum) fieldInfo.GetValue (property.serializedObject.targetObject);
			Enum enumNew = EditorGUI.EnumFlagsField (newPos, label, targetEnum);
			if (!targetEnum.Equals (enumNew)) {
				theEnum = Convert.ChangeType (enumNew, enumUnderlyingType);
				//Debug.Log ("Modified enum in native flagsField");
			}
			foldoutOpen = EditorGUI.Foldout (newPos, foldoutOpen, "");

			if (foldoutOpen) {
				if (enumValues == null || enumValues.Length <= 0) {
					enumValues = Enum.GetValues (enumType);
					enumNames = Enum.GetNames (fieldInfo.FieldType);
					allValue = Convert.ChangeType (0, enumUnderlyingType);
					for (int i = 0; i < enumValues.Length; i++) {
						allValue = DoOrOperator (allValue, GetEnumValue (i), enumUnderlyingType);
					}
					allValueBinary = IntToBinaryString ((int) allValue);
					enumValuesBinary = new string[enumValues.Length];
					for (int i = 0; i < enumValues.Length; i++) {
						enumValuesBinary[i] = IntToBinaryString ((int) GetEnumValue (i), (int) allValue, allValueBinary.Length);
					}
				}

				GUIStyle alignRightLabelStyle = new GUIStyle (GUI.skin.label) {
					alignment = TextAnchor.MiddleRight
				};

				bool tmpMixedValue = !theEnum.Equals (Convert.ChangeType (0, enumUnderlyingType));
				bool tmpValue = tmpMixedValue;
				if (tmpMixedValue)
					tmpMixedValue = !theEnum.Equals (Convert.ChangeType (allValue, enumUnderlyingType));
				EditorGUI.showMixedValue = tmpMixedValue;
				newPos.y += EditorGUIUtility.singleLineHeight;
				EditorGUI.LabelField (newPos, IntToBinaryString ((int) theEnum, (int) allValue, allValueBinary.Length), alignRightLabelStyle);
				bool newValue = EditorGUI.ToggleLeft (newPos, "", tmpValue && !tmpMixedValue);
				if (newValue == tmpMixedValue) {
					theEnum = Convert.ChangeType (0, enumUnderlyingType);
					//Debug.Log ("Checked to none");
				}
				else if (newValue && !tmpValue) {
					theEnum = Convert.ChangeType (allValue, enumUnderlyingType);
					//Debug.Log ("Checked to all");
				}
				EditorGUI.showMixedValue = false;

				newPos.y += EditorGUIUtility.singleLineHeight;
				JCGUI.Separator (new Rect (newPos.x, newPos.y, newPos.width, BoxSeparatorHeight));
				newPos.y += BoxSeparatorHeight;

				//Draw the list
				for (int i = 0; i < enumNames.Length; i++) {
					bool set = IsSet (i);
					EditorGUI.BeginDisabledGroup (!set);
					EditorGUI.LabelField (newPos, enumValuesBinary[i], alignRightLabelStyle);
					EditorGUI.EndDisabledGroup ();
					bool tmp = EditorGUI.ToggleLeft (newPos, enumNames[i], set);

					if (set != tmp) {
						ToggleIndex (i, tmp);
					}
					newPos.y += EditorGUIUtility.singleLineHeight;
				}
			}

			fieldInfo.SetValue (property.serializedObject.targetObject, theEnum);
			property.serializedObject.ApplyModifiedProperties ();
		}//*/

		/// <summary>
		/// Get the value of an enum element at the specified index (i.e. at the index of the name of the element in the names array)
		/// </summary>
		object GetEnumValue (int _index) {
			return Convert.ChangeType (enumValues.GetValue (_index), enumUnderlyingType);
		}

		/// <summary>
		/// Sets or unsets a bit in theEnum based on the index of the enum element (i.e. the index of the element in the names array)
		/// </summary>
		/// <param name="_set">If true the flag will be set, if false the flag will be unset.</param>
		void ToggleIndex (int _index, bool _set) {
			if (_set) {
				if (IsNoneElement (_index)) {
					theEnum = Convert.ChangeType (0, enumUnderlyingType);
				}

				//enum = enum | val
				theEnum = DoOrOperator (theEnum, GetEnumValue (_index), enumUnderlyingType);
			}
			else {
				if (IsNoneElement (_index) || IsAllElement (_index)) {
					return;
				}

				object val = GetEnumValue (_index);
				object notVal = DoNotOperator (val, enumUnderlyingType);

				//enum = enum & ~val
				theEnum = DoAndOperator (theEnum, notVal, enumUnderlyingType);
			}

		}

		/// <summary>
		/// Checks if a bit flag is set at the provided index of the enum element (i.e. the index of the element in the names array)
		/// </summary>
		bool IsSet (int _index) {
			object val = DoAndOperator (theEnum, GetEnumValue (_index), enumUnderlyingType);

			//We handle All and None elements differently, since they're "special"
			if (IsAllElement (_index)) {
				//If all other bits visible to the user (elements) are set, the "All" element checkbox has to be checked
				//We don't do a simple AND operation because there might be missing bits.
				//e.g. An enum with 6 elements including the "All" element. If we set all bits visible except the "All" bit,
				//two bits might be unset. Since we want the "All" element checkbox to be checked when all other elements are set
				//we have to make sure those two extra bits are also set.
				bool allSet = true;
				for (int i = 0; i < Enum.GetNames (fieldInfo.FieldType).Length; i++) {
					if (i != _index && !IsNoneElement (i) && !IsSet (i)) {
						allSet = false;
						break;
					}
				}

				//Make sure all bits are set if all "visible bits" are set
				if (allSet) {
					theEnum = DoNotOperator (Convert.ChangeType (0, enumUnderlyingType), enumUnderlyingType);
				}

				return allSet;
			}
			else if (IsNoneElement (_index)) {
				//Just check the "None" element checkbox our enum's value is 0
				return Convert.ChangeType (theEnum, enumUnderlyingType).Equals (Convert.ChangeType (0, enumUnderlyingType));
			}

			return val.Equals (GetEnumValue (_index));//!val.Equals(Convert.ChangeType(0, enumUnderlyingType));
		}

		/// <summary>
		/// Call the bitwise OR operator (|) on _lhs and _rhs given their types.
		/// Will basically return _lhs | _rhs
		/// </summary>
		/// <param name="_lhs">Left-hand side of the operation.</param>
		/// <param name="_rhs">Right-hand side of the operation.</param>
		/// <param name="_type">Type of the objects.</param>
		/// <returns>Result of the operation</returns>
		static object DoOrOperator (object _lhs, object _rhs, Type _type) {
			if (_type == typeof (int)) {
				return ((int) _lhs) | ((int) _rhs);
			}
			else if (_type == typeof (uint)) {
				return ((uint) _lhs) | ((uint) _rhs);
			}
			else if (_type == typeof (short)) {
				//ushort and short don't have bitwise operators, it is automatically converted to an int, so we convert it back
				return unchecked((short) ((short) _lhs | (short) _rhs));
			}
			else if (_type == typeof (ushort)) {
				//ushort and short don't have bitwise operators, it is automatically converted to an int, so we convert it back
				return unchecked((ushort) ((ushort) _lhs | (ushort) _rhs));
			}
			else if (_type == typeof (long)) {
				return ((long) _lhs) | ((long) _rhs);
			}
			else if (_type == typeof (ulong)) {
				return ((ulong) _lhs) | ((ulong) _rhs);
			}
			else if (_type == typeof (byte)) {
				//byte and sbyte don't have bitwise operators, it is automatically converted to an int, so we convert it back
				return unchecked((byte) ((byte) _lhs | (byte) _rhs));
			}
			else if (_type == typeof (sbyte)) {
				//byte and sbyte don't have bitwise operators, it is automatically converted to an int, so we convert it back
				return unchecked((sbyte) ((sbyte) _lhs | (sbyte) _rhs));
			}
			else {
				throw new System.ArgumentException ("Type " + _type.FullName + " not supported.");
			}
		}

		/// <summary>
		/// Call the bitwise AND operator (&) on _lhs and _rhs given their types.
		/// Will basically return _lhs & _rhs
		/// </summary>
		/// <param name="_lhs">Left-hand side of the operation.</param>
		/// <param name="_rhs">Right-hand side of the operation.</param>
		/// <param name="_type">Type of the objects.</param>
		/// <returns>Result of the operation</returns>
		static object DoAndOperator (object _lhs, object _rhs, Type _type) {
			if (_type == typeof (int)) {
				return ((int) _lhs) & ((int) _rhs);
			}
			else if (_type == typeof (uint)) {
				return ((uint) _lhs) & ((uint) _rhs);
			}
			else if (_type == typeof (short)) {
				//ushort and short don't have bitwise operators, it is automatically converted to an int, so we convert it back
				return unchecked((short) ((short) _lhs & (short) _rhs));
			}
			else if (_type == typeof (ushort)) {
				//ushort and short don't have bitwise operators, it is automatically converted to an int, so we convert it back
				return unchecked((ushort) ((ushort) _lhs & (ushort) _rhs));
			}
			else if (_type == typeof (long)) {
				return ((long) _lhs) & ((long) _rhs);
			}
			else if (_type == typeof (ulong)) {
				return ((ulong) _lhs) & ((ulong) _rhs);
			}
			else if (_type == typeof (byte)) {
				return unchecked((byte) ((byte) _lhs & (byte) _rhs));
			}
			else if (_type == typeof (sbyte)) {
				//byte and sbyte don't have bitwise operators, it is automatically converted to an int, so we convert it back
				return unchecked((sbyte) ((sbyte) _lhs & (sbyte) _rhs));
			}
			else {
				throw new System.ArgumentException ("Type " + _type.FullName + " not supported.");
			}
		}

		/// <summary>
		/// Call the bitwise NOT operator (~) on _lhs given its type.
		/// Will basically return ~_lhs
		/// </summary>
		/// <param name="_lhs">Left-hand side of the operation.</param>
		/// <param name="_type">Type of the object.</param>
		/// <returns>Result of the operation</returns>
		static object DoNotOperator (object _lhs, Type _type) {
			if (_type == typeof (int)) {
				return ~(int) _lhs;
			}
			else if (_type == typeof (uint)) {
				return ~(uint) _lhs;
			}
			else if (_type == typeof (short)) {
				//ushort and short don't have bitwise operators, it is automatically converted to an int, so we convert it back
				return unchecked((short) ~(short) _lhs);
			}
			else if (_type == typeof (ushort)) {
				//ushort and short don't have bitwise operators, it is automatically converted to an int, so we convert it back
				return unchecked((ushort) ~(ushort) _lhs);
			}
			else if (_type == typeof (long)) {
				return ~(long) _lhs;
			}
			else if (_type == typeof (ulong)) {
				return ~(ulong) _lhs;
			}
			else if (_type == typeof (byte)) {
				//byte and sbyte don't have bitwise operators, it is automatically converted to an int, so we convert it back
				return (byte) ~(byte) _lhs;
			}
			else if (_type == typeof (sbyte)) {
				//byte and sbyte don't have bitwise operators, it is automatically converted to an int, so we convert it back
				return unchecked((sbyte) ~(sbyte) _lhs);
			}
			else {
				throw new System.ArgumentException ("Type " + _type.FullName + " not supported.");
			}
		}

		/// <summary>
		/// Check if the element of specified index is a "None" element (all bits unset, value = 0).
		/// </summary>
		/// <param name="_index">Index of the element.</param>
		/// <returns>If the element has all bits unset or not.</returns>
		bool IsNoneElement (int _index) {
			return GetEnumValue (_index).Equals (Convert.ChangeType (0, enumUnderlyingType));
		}

		/// <summary>
		/// Check if the element of specified index is an "All" element (all bits set, value = ~0).
		/// </summary>
		/// <param name="_index">Index of the element.</param>
		/// <returns>If the element has all bits set or not.</returns>
		bool IsAllElement (int _index) {
			object elemVal = GetEnumValue (_index);
			return elemVal.Equals (DoNotOperator (Convert.ChangeType (0, enumUnderlyingType), enumUnderlyingType));
		}

		static string IntToBinaryString (int number, int padLength = -1) {
			var binary = string.Empty;
			while (number > 0) {
				binary = (number & 1) + binary;
				number = number >> 1;
			}
			if (padLength > 0)
				binary = binary.PadLeft (padLength, '0');
			return binary;
		}

		static string IntToBinaryString (int number, int maskValue, int totalLength) {
			char[] binary = new char[totalLength];
			int mask = 1 << (totalLength - 1);
			for (int i = 0; i < totalLength; ++i) {
				binary[i] = (maskValue & mask) != 0 ? ((number & mask) != 0 ? '1' : '0') : 'X';
				mask >>= 1;
			}
			return new string (binary);
		}
	}
}
