﻿using System.Collections;
using System;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace JuneCodes.Editor {
	public class JCReorderableList : ReorderableList {

		bool AllFolded = true;
		bool AllOpen = false;
		bool CanExpand = false;

		public JCReorderableList (SerializedObject serializedObject, SerializedProperty elements) :
			this (serializedObject, elements, true, true, true, true) {
		}

		public JCReorderableList (SerializedObject serializedObject, SerializedProperty elements, bool draggable, bool displayHeader, bool displayAddButton, bool displayRemoveButton) :
			base (serializedObject, elements, draggable, displayHeader, displayAddButton, displayRemoveButton) {
			drawHeaderCallback = (Rect rect) => {
				EditorGUI.LabelField (rect, string.Format ("{0}: {1}", elements.displayName, elements.arraySize), EditorStyles.label);
				if (CanExpand) {
					EditorGUI.BeginDisabledGroup (AllFolded);
					if (GUI.Button (new Rect (rect.x + rect.width - rect.height * 2f, rect.y, rect.height, rect.height), GUIContent.none, new GUIStyle (EditorStyles.foldout) {
						normal = EditorStyles.foldout.onNormal,
						active = EditorStyles.foldout.onActive,
						focused = EditorStyles.foldout.onActive,
						hover = EditorStyles.foldout.onHover
					})) {
						elements.ExpandArrayElements (false);
					}
					EditorGUI.EndDisabledGroup ();
					EditorGUI.BeginDisabledGroup (AllOpen);
					if (GUI.Button (new Rect (rect.x + rect.width - rect.height, rect.y, rect.height, rect.height), GUIContent.none, EditorStyles.foldout)) {
						elements.ExpandArrayElements (true);
					}
					EditorGUI.EndDisabledGroup ();
				}
			};
			drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
				EditorGUI.BeginChangeCheck ();
				rect = ElementInitializer (rect, index, out SerializedProperty element);
				EditorGUI.PropertyField (rect, element, true);
				if (EditorGUI.EndChangeCheck ()) {
					//Debug.LogWarning (element);
					serializedObject.ApplyModifiedProperties ();
					AssetDatabase.SaveAssets ();
				}
			};//*
			onRemoveCallback += (ReorderableList list) => {
				SerializedProperty element = elements.GetArrayElementAtIndex (index);
				if (element.propertyType == SerializedPropertyType.ObjectReference && element.objectReferenceValue != null)
					element.objectReferenceValue = null;
				elements.DeleteArrayElementAtIndex (list.index);
				list.serializedProperty.serializedObject.ApplyModifiedProperties ();
				//Debug.Log ("Remove " + list.index);
			};//*/
			elementHeightCallback = (int index) => {
				if (index >= elements.arraySize)
					return 0f;
				SerializedProperty element = elements.GetArrayElementAtIndex (index);
				return EditorGUI.GetPropertyHeight (element, true) + 4f;
			};
		}

		public Rect HeaderRect (Rect rect) {
			return new Rect (rect.x, rect.y, rect.width - rect.height * 2f, rect.height);
		}

		public Rect ElementInitializer (Rect rect, int index, out SerializedProperty element) {
			if (index >= serializedProperty.arraySize) {
				element = null;
				return rect;
			}
			element = serializedProperty.GetArrayElementAtIndex (index);

			if (element.hasVisibleChildren) {
				CanExpand = true;
				if (CanExpand) {
					if (index == 0) {
						AllFolded = true;
						AllOpen = true;
					}
					AllOpen &= element.isExpanded;
					AllFolded &= !element.isExpanded;
				}
			}

			rect.y += 2f;
			if (element.hasVisibleChildren) { // prevents the foldout arrow from conflicting with the reorderable item handle
				rect.x += 10f;
				rect.width -= 10f;
			}
			return rect;
		}
	}
}
