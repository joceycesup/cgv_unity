﻿using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;

namespace JuneCodes.Editor {
	public static class JCGUI {

		public static void Separator (Rect position) {
			Separator (position, GUIContent.none);
		}

		public static void Separator (Rect position, string label) {
			Separator (position, new GUIContent (label));
		}

		public static void Separator (Rect position, GUIContent label) {
			if (label == null || string.IsNullOrEmpty (label.text)) {
				position.y += position.height / 2f;
				position.height = 1f;
				GUI.Box (position, GUIContent.none);
			}
			else {
				Vector2 textSize = GUI.skin.label.CalcSize (label);
				float separatorWidth = (position.width - textSize.x) / 2f - 5f;
				position.y += position.height / 2f;

				textSize.y = Mathf.Max (textSize.y, position.height);

				GUI.Box (new Rect (position.xMin, position.yMin, separatorWidth, 1f), GUIContent.none);
				GUI.Label (new Rect (position.xMin + separatorWidth + 5f, position.y - textSize.y / 2f, textSize.x, textSize.y), label);
				GUI.Box (new Rect (position.xMin + separatorWidth + 10f + textSize.x, position.yMin, separatorWidth, 1f), GUIContent.none);
			}
		}

		public delegate void FilteredTextCallbackDelegate (TextEditor editor);

		public static string FilteredTextField (Rect position, GUIContent label, string text, string authorizedCharacters, out TextEditor textEditor, bool delayed = false) {
			return FilteredTextField (position, label, text, authorizedCharacters, out textEditor, null, delayed);
		}

		public static string FilteredTextField (Rect position, GUIContent label, string text, string authorizedCharacters, out TextEditor textEditor, FilteredTextCallbackDelegate callback, bool delayed = false) {
			string controlName = "JC_FTE_" + (JCEditorUtility.GetLastControlId () + 1);
			GUI.SetNextControlName (controlName);
			FilteredText (authorizedCharacters, out textEditor, controlName, callback);
			return delayed ? EditorGUI.DelayedTextField (position, label, text) : EditorGUI.TextField (position, label, text);
		}

		internal static void FilteredText (string authorizedCharacters, out TextEditor textEditor, string controlName, FilteredTextCallbackDelegate callback) {
			textEditor = null;
			if (EditorGUIUtility.editingTextField) {
				//Debug.Log (controlName + " : " + GUI.GetNameOfFocusedControl ());
				if (controlName.Equals (GUI.GetNameOfFocusedControl ())) {
					textEditor = typeof (EditorGUI).GetField ("activeEditor", BindingFlags.Static | BindingFlags.NonPublic).GetValue (null) as TextEditor;
					if (textEditor != null) {
						//Debug.Log ("Is editing " + text);
						textEditor.text = Regex.Replace (textEditor.text, "[^" + authorizedCharacters + "]+", "", RegexOptions.IgnoreCase);
						callback?.Invoke (textEditor);
					}
				}
			}
		}

		public static void SpritePreview (Rect position, Sprite sprite, bool drawBorder = false) {
			if (sprite == null) {
				if (!drawBorder && position.width > position.height) {
					Vector2 posCenter = position.center;
					position.width = position.height;
					position.center = posCenter;
				}
				GUI.Box (position, new GUIContent ("No Sprite"), new GUIStyle (GUI.skin.textField) { alignment = TextAnchor.MiddleCenter, wordWrap = true });
				return;
			}

			Vector2 fullSize = new Vector2 (sprite.texture.width, sprite.texture.height);
			Vector2 size = new Vector2 (sprite.textureRect.width, sprite.textureRect.height);

			Rect coords = sprite.textureRect;
			coords.x /= fullSize.x;
			coords.width /= fullSize.x;
			coords.y /= fullSize.y;
			coords.height /= fullSize.y;

			if (drawBorder) {
				GUI.Box (position, GUIContent.none, GUI.skin.textField);
				RemoveOffset (ref position, GUI.skin.textField.border);
			}

			float minRatio = Mathf.Min (position.width / size.x, position.height / size.y);

			Vector2 center = position.center;
			position.width = size.x * minRatio;
			position.height = size.y * minRatio;
			position.center = center;

			GUI.DrawTextureWithTexCoords (position, sprite.texture, coords);
		}

		private static void RemoveOffset (ref Rect rect, RectOffset offset) {
			rect.x += offset.left;
			rect.y += offset.top;
			rect.width -= (offset.left + offset.right);
			rect.height -= (offset.top + offset.bottom);
		}
	}

	public static class JCGUILayout {

		private static GUIStyle UpDownArrowStyle;
		private static readonly RectOffset UpDownArrowMargin;
		private static readonly Color UpDownArrowTextColor;

		private static GUIStyle CrossButtonStyle;

		static JCGUILayout () {
			// Up/Down Arrow
			UpDownArrowStyle = new GUIStyle (GUI.skin.button);
			UpDownArrowStyle.clipping = TextClipping.Overflow;
			UpDownArrowMargin = UpDownArrowStyle.margin;
			UpDownArrowStyle.padding = new RectOffset ();

			UpDownArrowStyle.normal.background = new Texture2D (1, 1);
			UpDownArrowStyle.normal.background.SetPixel (0, 0, Color.clear);
			UpDownArrowStyle.normal.background.Apply ();
			UpDownArrowStyle.active.background = UpDownArrowStyle.normal.background;//*/

			UpDownArrowStyle.active.textColor = EditorGUIUtility.isProSkin ? Color.white : Color.black;

			UpDownArrowTextColor = EditorGUIUtility.isProSkin ? UpDownArrowStyle.normal.textColor : (Color)new Color32 (180, 180, 180, 255);
			if (!EditorGUIUtility.isProSkin) {
				UpDownArrowStyle.normal.textColor = UpDownArrowTextColor;
			}

			// Cross Button
			CrossButtonStyle = new GUIStyle (GUI.skin.button);
			CrossButtonStyle.clipping = TextClipping.Overflow;
		}

		public static int UpDown (bool disableUp = false, bool disableDown = false) {
			int res = 0;

			float buttonWidth = 14f;
			UpDownArrowStyle.margin = new RectOffset (UpDownArrowMargin.left, 0, UpDownArrowMargin.top, UpDownArrowMargin.bottom);
			UpDownArrowStyle.alignment = TextAnchor.MiddleRight;

			EditorGUI.BeginDisabledGroup (disableUp);
			UpDownArrowStyle.normal.textColor = disableUp ? Color.gray : UpDownArrowTextColor;
			if (GUILayout.Button ("\u25B2", UpDownArrowStyle, GUILayout.Width (buttonWidth))) {
				res = -1;
			}
			EditorGUI.EndDisabledGroup ();
			UpDownArrowStyle.margin = new RectOffset (0, UpDownArrowMargin.right, UpDownArrowMargin.top, UpDownArrowMargin.bottom);
			UpDownArrowStyle.alignment = TextAnchor.MiddleLeft;
			EditorGUI.BeginDisabledGroup (disableDown);
			UpDownArrowStyle.normal.textColor = disableDown ? Color.gray : UpDownArrowTextColor;
			if (GUILayout.Button ("\u25BC", UpDownArrowStyle, GUILayout.Width (buttonWidth))) {
				res = 1;
			}
			EditorGUI.EndDisabledGroup ();
			return res;
		}

		public static bool CrossButton () {
			GUI.backgroundColor = JCEditorUtility.ErrorColor;
			bool res = GUILayout.Button ("X", CrossButtonStyle, GUILayout.Width (EditorGUIUtility.singleLineHeight), GUILayout.Height (EditorGUIUtility.singleLineHeight));
			GUI.backgroundColor = Color.white;
			return res;
		}

		public static void Separator () {
			Separator (GUIContent.none);
		}

		public static void Separator (string label) {
			Separator (new GUIContent (label));
		}

		public static void Separator (GUIContent label) {
			GUILayout.Label (GUIContent.none);
			JCGUI.Separator (GUILayoutUtility.GetLastRect (), label);
		}

		public static string FilteredTextField (GUIContent label, string text, string authorizedCharacters, out TextEditor textEditor, bool delayed = false) {
			return FilteredTextField (label, text, authorizedCharacters, out textEditor, null, delayed);
		}

		public static string FilteredTextField (GUIContent label, string text, string authorizedCharacters, out TextEditor textEditor, JCGUI.FilteredTextCallbackDelegate callback, bool delayed = false) {
			string controlName = "JC_FTE_" + (JCEditorUtility.GetLastControlId () + 1);
			GUI.SetNextControlName (controlName);
			JCGUI.FilteredText (authorizedCharacters, out textEditor, controlName, callback);
			return delayed ? EditorGUILayout.DelayedTextField (label, text) : EditorGUILayout.TextField (label, text);
		}

		public static void SpritePreview (Sprite sprite, bool drawBorder = false, bool displayIfNull = true, float height = 64f) {
			if (sprite == null && !displayIfNull)
				return;
			JCGUI.SpritePreview (EditorGUILayout.GetControlRect (false, height + (drawBorder ? GUI.skin.textField.border.vertical : 0f)), sprite, drawBorder);
		}

		public static void SpritePreview (GUIContent label, Sprite sprite, bool drawBorder = false, bool displayIfNull = true, float height = 64f) {
			if (sprite == null && !displayIfNull)
				return;
			Rect previewRect = EditorGUILayout.GetControlRect (false, height + (drawBorder ? GUI.skin.textField.border.vertical : 0f));
			GUI.Label (previewRect, label, new GUIStyle (GUI.skin.label) { alignment = TextAnchor.MiddleLeft });
			previewRect.x += EditorGUIUtility.labelWidth;
			previewRect.width -= EditorGUIUtility.labelWidth;
			JCGUI.SpritePreview (previewRect, sprite, drawBorder);
		}

		public static void ScriptField (SerializedObject serializedObject) {
			GUI.enabled = false;
			SerializedProperty prop = serializedObject.FindProperty ("m_Script");
			EditorGUILayout.PropertyField (prop, true, new GUILayoutOption[0]);
			GUI.enabled = true;
		}
	}
}
