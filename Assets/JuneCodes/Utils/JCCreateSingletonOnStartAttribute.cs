﻿using System;

namespace JuneCodes {
	[AttributeUsage (AttributeTargets.Class, AllowMultiple = false)]
	public class JCCreateSingletonOnStartAttribute : JCCreateSingletonIfNullAttribute {
	}
}
