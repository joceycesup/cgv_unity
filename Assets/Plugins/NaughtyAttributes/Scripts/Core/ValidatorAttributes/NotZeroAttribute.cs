using System;

namespace NaughtyAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class NotZeroAttribute : ValidatorAttribute
    {
		public float DefaultValue { get; private set; } = 0;

        public NotZeroAttribute ()
        {
            this.DefaultValue = 0f;
        }

        public NotZeroAttribute (float minValue)
        {
            this.DefaultValue = minValue;
        }

        public NotZeroAttribute (int minValue)
        {
            this.DefaultValue = minValue;
        }
    }
}
