using System;

namespace NaughtyAttributes
{
    public enum ComparisonOperator {
        LessThan,
		LessThanOrEqualTo,
		EqualTo,
		NotEqualTo,
		GreaterThan,
		GreaterThanOrEqualTo,
	}
}
