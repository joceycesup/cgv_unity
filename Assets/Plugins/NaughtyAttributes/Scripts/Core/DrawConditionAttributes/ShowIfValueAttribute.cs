﻿using System;

namespace NaughtyAttributes {
	[AttributeUsage (AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
	public class ShowIfValueAttribute : DrawConditionAttribute {

		public string[] Conditions { get; private set; }
		public ComparisonOperator ComparisonOperator { get; private set; }
		public float Value { get; protected set; }
		public bool Reversed { get; protected set; }

		public ShowIfValueAttribute (ComparisonOperator comparisonOperator, float value, string condition) {
			ComparisonOperator = comparisonOperator;
			Value = value;
			Conditions = new string[1] { condition };
		}

		public ShowIfValueAttribute (ComparisonOperator comparisonOperator, float value, params string[] conditions) {
			ComparisonOperator = comparisonOperator;
			Value = value;
			Conditions = conditions;
		}

		public ShowIfValueAttribute (ComparisonOperator comparisonOperator, int value, string condition) {
			ComparisonOperator = comparisonOperator;
			Value = value;
			Conditions = new string[1] { condition };
		}

		public ShowIfValueAttribute (ComparisonOperator comparisonOperator, int value, params string[] conditions) {
			ComparisonOperator = comparisonOperator;
			Value = value;
			Conditions = conditions;
		}
	}
}
