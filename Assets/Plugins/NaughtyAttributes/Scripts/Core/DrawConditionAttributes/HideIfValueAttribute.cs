﻿using System;

namespace NaughtyAttributes {
	[AttributeUsage (AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
	public class HideIfValueAttribute : ShowIfValueAttribute {

		public HideIfValueAttribute (ComparisonOperator comparisonOperator, float value, string condition)
			: base (comparisonOperator, value, condition) {
			Reversed = true;
		}

		public HideIfValueAttribute (ComparisonOperator comparisonOperator, float value, params string[] conditions)
			: base (comparisonOperator, value, conditions) {
			Reversed = true;
		}

		public HideIfValueAttribute (ComparisonOperator comparisonOperator, int value, string condition)
			: base (comparisonOperator, value, condition) {
			Reversed = true;
		}

		public HideIfValueAttribute (ComparisonOperator comparisonOperator, int value, params string[] conditions)
			: base (comparisonOperator, value, conditions) {
			Reversed = true;
		}
	}
}
