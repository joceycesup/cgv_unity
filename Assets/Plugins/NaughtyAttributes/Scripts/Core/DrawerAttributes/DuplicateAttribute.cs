﻿using System;

namespace NaughtyAttributes {
	[AttributeUsage (AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
	public class DuplicateAttribute : DrawerAttribute {
		public string Duplicate { get; private set; }

		public DuplicateAttribute (string duplicate) {
			Duplicate = duplicate;
		}
	}
}
