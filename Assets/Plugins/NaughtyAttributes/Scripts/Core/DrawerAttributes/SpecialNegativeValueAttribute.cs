﻿using System;

namespace NaughtyAttributes {
	[AttributeUsage (AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
	public class SpecialNegativeValueAttribute : DrawerAttribute {
		public string NegativeValueDescription { get; private set; }
		public string PositiveValueDescription { get; private set; }
		public bool AllowZero { get; private set; }

		public SpecialNegativeValueAttribute (string negativeValueDescription, bool allowZero = true) {
			this.NegativeValueDescription = negativeValueDescription;
			this.PositiveValueDescription = "Positive Value";
			this.AllowZero = allowZero;
		}

		public SpecialNegativeValueAttribute (string negativeValueDescription, string positiveValueDescription, bool allowZero = true) {
			this.NegativeValueDescription = negativeValueDescription;
			this.PositiveValueDescription = positiveValueDescription;
			this.AllowZero = allowZero;
		}
	}
}
