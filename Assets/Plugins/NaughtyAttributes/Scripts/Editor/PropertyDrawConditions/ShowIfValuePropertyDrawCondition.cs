﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;

namespace NaughtyAttributes.Editor {
	[PropertyDrawCondition (typeof (ShowIfValueAttribute))]
	public class ShowIfValuePropertyDrawCondition : PropertyDrawCondition {

		public override bool CanDrawProperty (SerializedProperty property) {
			ShowIfValueAttribute showIfAttribute = PropertyUtility.GetAttribute<ShowIfValueAttribute> (property);
			UnityEngine.Object target = PropertyUtility.GetTargetObject (property);

			List<float> conditionValues = new List<float> ();
			foreach (var condition in showIfAttribute.Conditions) {
				FieldInfo conditionField = ReflectionUtility.GetField (target, condition);
				if (conditionField != null) {
					if (conditionField.FieldType == typeof (int))
						conditionValues.Add ((int)conditionField.GetValue (target));
					else if (conditionField.FieldType == typeof (float))
						conditionValues.Add ((float)conditionField.GetValue (target));
				}
			}

			if (conditionValues.Count > 0) {

				Func<float, float, bool> ComparisonFunc = null;
				switch (showIfAttribute.ComparisonOperator) {
					case ComparisonOperator.LessThan:
						ComparisonFunc = LessThan;
						break;
					case ComparisonOperator.LessThanOrEqualTo:
						ComparisonFunc = LessThanOrEqualTo;
						break;
					case ComparisonOperator.EqualTo:
						ComparisonFunc = EqualTo;
						break;
					case ComparisonOperator.NotEqualTo:
						ComparisonFunc = NotEqualTo;
						break;
					case ComparisonOperator.GreaterThan:
						ComparisonFunc = GreaterThan;
						break;
					case ComparisonOperator.GreaterThanOrEqualTo:
						ComparisonFunc = GreaterThanOrEqualTo;
						break;
				}

				bool draw = false;
				foreach (var value in conditionValues) {
					draw = draw || ComparisonFunc (value, showIfAttribute.Value);
				}

				if (showIfAttribute.Reversed) {
					draw = !draw;
				}

				return draw;
			}
			else {
				string warning = showIfAttribute.GetType ().Name + " needs a valid boolean condition field or method name to work";
				EditorDrawUtility.DrawHelpBox (warning, MessageType.Warning, context: target);

				return true;
			}
		}

		private static bool LessThan (float v1, float v2) { return v1 < v2; }
		private static bool LessThanOrEqualTo (float v1, float v2) { return v1 <= v2; }
		private static bool EqualTo (float v1, float v2) { return v1 == v2; }
		private static bool NotEqualTo (float v1, float v2) { return v1 != v2; }
		private static bool GreaterThan (float v1, float v2) { return v1 > v2; }
		private static bool GreaterThanOrEqualTo (float v1, float v2) { return v1 >= v2; }
	}
}
