﻿using UnityEditor;

namespace NaughtyAttributes.Editor {
	[PropertyDrawCondition (typeof (HideIfValueAttribute))]
	public class HideIfValuePropertyDrawCondition : ShowIfValuePropertyDrawCondition {
	}
}
