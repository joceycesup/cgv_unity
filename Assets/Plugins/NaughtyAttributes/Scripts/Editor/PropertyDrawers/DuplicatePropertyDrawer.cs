﻿using System.Reflection;
using UnityEditor;

namespace NaughtyAttributes.Editor {
	[PropertyDrawer (typeof (DuplicateAttribute))]
	public class DuplicatePropertyDrawer : PropertyDrawer {

		public override void DrawProperty (SerializedProperty property) {
			EditorDrawUtility.DrawHeader (property);

			DuplicateAttribute duplicateAttribute = PropertyUtility.GetAttribute<DuplicateAttribute> (property);

			SerializedProperty duplicateProperty = property.serializedObject.FindProperty (duplicateAttribute.Duplicate);
			if (duplicateProperty == null || !property.type.Equals (duplicateProperty.type)) {
				string warning = "Duplicate attribute needs to be of the same type as this field";
				EditorDrawUtility.DrawHelpBox (warning, MessageType.Warning, context: PropertyUtility.GetTargetObject (property));

				EditorDrawUtility.DrawPropertyField (property);
				return;
			}

			PropertyUtility.SetPropertyValueFromOther (duplicateProperty, property);

			EditorDrawUtility.DrawPropertyField (property);

			PropertyUtility.SetPropertyValueFromOther (property, duplicateProperty);
		}
	}
}
