using UnityEngine;
using UnityEditor;

namespace NaughtyAttributes.Editor {
	[PropertyDrawer (typeof (SteppedVectorAttribute))]
	public class SteppedVectorPropertyDrawer : PropertyDrawer {
		public override void DrawProperty (SerializedProperty property) {
			EditorDrawUtility.DrawHeader (property);

			SteppedVectorAttribute steppedVectorAttribute = PropertyUtility.GetAttribute<SteppedVectorAttribute> (property);

			bool intType = (property.propertyType == SerializedPropertyType.Vector2Int || property.propertyType == SerializedPropertyType.Vector3Int);
			bool vectorType = (property.propertyType == SerializedPropertyType.Vector2 || property.propertyType == SerializedPropertyType.Vector3) || intType;
			bool vector3Type = (property.propertyType == SerializedPropertyType.Vector3 || property.propertyType == SerializedPropertyType.Vector3Int);

			if (vectorType) {
				Rect controlRect = EditorGUILayout.GetControlRect ();
				float labelWidth = EditorGUIUtility.labelWidth;
				float numberFieldWidth = EditorGUIUtility.fieldWidth;
				float sliderWidth = controlRect.width - labelWidth - (vector3Type ? 3f : 1f) * numberFieldWidth;
				float fieldPadding = 2.5f;
				float maxFieldWidth = 20f + numberFieldWidth;

				Rect labelRect = new Rect (
					controlRect.x,
					controlRect.y,
					labelWidth,
					controlRect.height);

				Rect yFieldRect = new Rect (
					controlRect.x + labelWidth + sliderWidth + numberFieldWidth * (vector3Type ? 1f : 0f) - (vector3Type ? fieldPadding : 0f),
					controlRect.y,
					numberFieldWidth,
					controlRect.height);

				Rect sliderRect = new Rect (
					controlRect.x + labelWidth + fieldPadding,
					controlRect.y,
					sliderWidth - (vector3Type ? 4f : 2f) * fieldPadding,
					controlRect.height);

				Rect maxFieldRect = new Rect (
					controlRect.x + labelWidth + numberFieldWidth + sliderWidth,
					controlRect.y,
					numberFieldWidth,
					controlRect.height);

				// Draw the label
				EditorGUI.LabelField (labelRect, property.displayName);

				// Draw the slider
				EditorGUI.BeginChangeCheck ();

				Vector3 sliderValue = Vector3.zero;
				switch (property.propertyType) {
					case SerializedPropertyType.Vector2:
						sliderValue = property.vector2Value;
						break;
					case SerializedPropertyType.Vector3:
						sliderValue = property.vector3Value;
						break;
					case SerializedPropertyType.Vector2Int:
						sliderValue = (Vector2)property.vector2IntValue;
						break;
					case SerializedPropertyType.Vector3Int:
						sliderValue = property.vector3IntValue;
						break;
				}

				if (vector3Type) {
					Rect xFieldRect = new Rect (
						controlRect.x + labelWidth + sliderWidth - 2f * fieldPadding,
						controlRect.y,
						numberFieldWidth,
						controlRect.height);

					Rect zFieldRect = new Rect (
						controlRect.x + labelWidth + sliderWidth + 2f * numberFieldWidth,
						controlRect.y,
						numberFieldWidth,
						controlRect.height);

					EditorGUI.MinMaxSlider (sliderRect, ref sliderValue.x, ref sliderValue.y, 0f, sliderValue.z);//*

					sliderValue.x = intType ? EditorGUI.IntField (xFieldRect, (int)sliderValue.x) : EditorGUI.FloatField (xFieldRect, sliderValue.x);
					sliderValue.x = Mathf.Clamp (sliderValue.x, 0f, sliderValue.y);

					sliderValue.y = intType ? EditorGUI.IntField (yFieldRect, (int)sliderValue.y) : EditorGUI.FloatField (yFieldRect, sliderValue.y);
					sliderValue.y = Mathf.Clamp (sliderValue.y, sliderValue.x, sliderValue.z);

					sliderValue.z = intType ? EditorGUI.IntField (zFieldRect, (int)sliderValue.z) : EditorGUI.FloatField (zFieldRect, sliderValue.z);
					sliderValue.z = Mathf.Max (sliderValue.z, sliderValue.y);//*/
				}
				else {
					sliderValue.x = EditorGUI.Slider (sliderRect, sliderValue.x, 0f, sliderValue.y);

					//sliderValue.x = intType ? EditorGUI.IntField (xFieldRect, (int)sliderValue.x) : EditorGUI.FloatField (xFieldRect, sliderValue.x);
					//sliderValue.x = Mathf.Clamp (sliderValue.x, 0f, sliderValue.y);

					sliderValue.y = intType ? EditorGUI.IntField (yFieldRect, (int)sliderValue.y) : EditorGUI.FloatField (yFieldRect, sliderValue.y);
					sliderValue.y = Mathf.Max (sliderValue.y, sliderValue.x);
				}

				if (EditorGUI.EndChangeCheck ()) {
					switch (property.propertyType) {
						case SerializedPropertyType.Vector2:
							property.vector2Value = sliderValue;
							break;
						case SerializedPropertyType.Vector3:
							property.vector3Value = sliderValue;
							break;
						case SerializedPropertyType.Vector2Int:
							property.vector2IntValue = new Vector2Int ((int)sliderValue.x, (int)sliderValue.y);
							break;
						case SerializedPropertyType.Vector3Int:
							property.vector3IntValue = new Vector3Int ((int)sliderValue.x, (int)sliderValue.y, 0);
							break;
					}
				}
			}
			else {
				string warning = steppedVectorAttribute.GetType ().Name + " can be used only on Vector2 fields";
				EditorDrawUtility.DrawHelpBox (warning, MessageType.Warning, context: PropertyUtility.GetTargetObject (property));

				EditorDrawUtility.DrawPropertyField (property);
			}
		}
	}
}
