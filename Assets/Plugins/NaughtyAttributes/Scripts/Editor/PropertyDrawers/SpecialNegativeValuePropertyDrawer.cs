using UnityEngine;
using UnityEditor;

namespace NaughtyAttributes.Editor {
	[PropertyDrawer (typeof (SpecialNegativeValueAttribute))]
	public class SpecialNegativeValuePropertyDrawer : PropertyDrawer {

		struct ContextMenuData {
			public SerializedProperty CurrentProperty;
			public bool IsSpecialValue;
			public bool IsInt;

			public ContextMenuData (SerializedProperty property, bool isSpecialValue, bool isInt) {
				CurrentProperty = property;
				IsSpecialValue = isSpecialValue;
				IsInt = isInt;
			}
		}

		private ContextMenuData MenuData;

		private static readonly float OptionsButtonWidth = 20f;

		public override void DrawProperty (SerializedProperty property) {
			EditorDrawUtility.DrawHeader (property);

			SpecialNegativeValueAttribute snvAttribute = PropertyUtility.GetAttribute<SpecialNegativeValueAttribute> (property);

			if (property.propertyType == SerializedPropertyType.Integer) {
				bool isSpecialValue = property.intValue < 0;

				float labelWidth = EditorGUIUtility.labelWidth;
				EditorGUIUtility.labelWidth += OptionsButtonWidth;

				Rect fieldRect = EditorGUILayout.GetControlRect (false);
				Rect optionRect = fieldRect;
				optionRect.width = OptionsButtonWidth;
				optionRect.x += labelWidth;

				EditorGUIUtility.AddCursorRect (optionRect, MouseCursor.Arrow);
				if (GUI.Button (optionRect, EditorGUIUtility.IconContent ("LookDevPaneOption"), GUI.skin.label)) {
					MenuData = new ContextMenuData (property, isSpecialValue, true);

					int normalValue = Mathf.Abs (property.intValue);
					int specialValue = -normalValue;

					GenericMenu menu = new GenericMenu ();

					menu.AddItem (new GUIContent (snvAttribute.NegativeValueDescription), isSpecialValue, ToggleSpecial, true);
					menu.AddItem (new GUIContent (snvAttribute.PositiveValueDescription), !isSpecialValue, ToggleSpecial, false);

					menu.ShowAsContext ();
				}

				if (isSpecialValue) {
					GUI.Label (new Rect (fieldRect.position, new Vector2 (EditorGUIUtility.labelWidth, fieldRect.height)), property.displayName);
					EditorGUI.BeginDisabledGroup (true);
					EditorGUI.TextField (new Rect (new Vector2 (fieldRect.x + EditorGUIUtility.labelWidth, fieldRect.y), fieldRect.size), snvAttribute.NegativeValueDescription);
					EditorGUI.EndDisabledGroup ();
				}
				else {
					int newValue = EditorGUI.IntField (fieldRect, property.displayName, property.intValue);
					if (newValue <= 0) {
						newValue = snvAttribute.AllowZero ? 0 : 1;
					}
					property.intValue = newValue;
				}
				EditorGUIUtility.labelWidth = labelWidth;
			}
			else if (property.propertyType == SerializedPropertyType.Float) {
				bool isSpecialValue = property.floatValue < 0f;

				float labelWidth = EditorGUIUtility.labelWidth;
				EditorGUIUtility.labelWidth += OptionsButtonWidth;

				Rect fieldRect = EditorGUILayout.GetControlRect (false);
				Rect optionRect = fieldRect;
				optionRect.width = OptionsButtonWidth;
				optionRect.x += labelWidth;

				EditorGUIUtility.AddCursorRect (optionRect, MouseCursor.Arrow);
				if (GUI.Button (optionRect, EditorGUIUtility.IconContent ("LookDevPaneOption"), GUI.skin.label)) {
					MenuData = new ContextMenuData (property, isSpecialValue, false);

					float normalValue = Mathf.Abs (property.floatValue);
					float specialValue = -normalValue;

					GenericMenu menu = new GenericMenu ();

					menu.AddItem (new GUIContent (snvAttribute.NegativeValueDescription), isSpecialValue, ToggleSpecial, true);
					menu.AddItem (new GUIContent (snvAttribute.PositiveValueDescription), !isSpecialValue, ToggleSpecial, false);

					menu.ShowAsContext ();
				}

				if (isSpecialValue) {
					GUI.Label (new Rect (fieldRect.position, new Vector2 (EditorGUIUtility.labelWidth, fieldRect.height)), property.displayName);
					EditorGUI.BeginDisabledGroup (true);
					EditorGUI.TextField (new Rect (new Vector2 (fieldRect.x + EditorGUIUtility.labelWidth, fieldRect.y), fieldRect.size), snvAttribute.NegativeValueDescription);
					EditorGUI.EndDisabledGroup ();
				}
				else {
					float newValue = EditorGUI.FloatField (fieldRect, property.displayName, property.floatValue);
					if (newValue <= 0f) {
						newValue = snvAttribute.AllowZero ? 0f : float.Epsilon;
					}
					property.floatValue = newValue;
				}
				EditorGUIUtility.labelWidth = labelWidth;
			}
			else {
				string warning = snvAttribute.GetType ().Name + " can be used only on int or float fields";
				EditorDrawUtility.DrawHelpBox (warning, MessageType.Warning, context: PropertyUtility.GetTargetObject (property));

				EditorDrawUtility.DrawPropertyField (property);
			}
		}

		private void ToggleSpecial (object isSpecialValue) {
			if (MenuData.IsSpecialValue != (bool)isSpecialValue) {
				if (MenuData.IsInt) {
					if (MenuData.CurrentProperty.intValue == 0)
						MenuData.CurrentProperty.intValue = -1;
					else
						MenuData.CurrentProperty.intValue = -MenuData.CurrentProperty.intValue;
				}
				else {
					if (MenuData.CurrentProperty.floatValue == 0f)
						MenuData.CurrentProperty.floatValue = -float.Epsilon;
					else
						MenuData.CurrentProperty.floatValue = -MenuData.CurrentProperty.floatValue;
				}
				MenuData.CurrentProperty.serializedObject.ApplyModifiedProperties ();
			}
		}
	}
}
