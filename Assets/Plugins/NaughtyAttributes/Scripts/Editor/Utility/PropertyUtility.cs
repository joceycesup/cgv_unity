using UnityEditor;
using System;
using System.Reflection;

namespace NaughtyAttributes.Editor
{
    public static class PropertyUtility
    {
        public static T GetAttribute<T>(SerializedProperty property) where T : Attribute
        {
            T[] attributes = GetAttributes<T>(property);
            return attributes.Length > 0 ? attributes[0] : null;
        }

        public static T[] GetAttributes<T>(SerializedProperty property) where T : Attribute
        {
            FieldInfo fieldInfo = ReflectionUtility.GetField(GetTargetObject(property), property.name);

            return (T[])fieldInfo.GetCustomAttributes(typeof(T), true);
        }

        public static UnityEngine.Object GetTargetObject(SerializedProperty property)
        {
            return property.serializedObject.targetObject;
		}

		public static void SetPropertyValueFromOther (SerializedProperty sourceProp, SerializedProperty destProp) {
			switch (sourceProp.propertyType) {
				case SerializedPropertyType.Integer:
					if (destProp.intValue != sourceProp.intValue)
						destProp.intValue = sourceProp.intValue;
					break;
				case SerializedPropertyType.Boolean:
					if (destProp.boolValue != sourceProp.boolValue)
						destProp.boolValue = sourceProp.boolValue;
					break;
				case SerializedPropertyType.Float:
					if (destProp.floatValue != sourceProp.floatValue)
						destProp.floatValue = sourceProp.floatValue;
					break;
				case SerializedPropertyType.String:
					if (destProp.stringValue != sourceProp.stringValue)
						destProp.stringValue = sourceProp.stringValue;
					break;
				case SerializedPropertyType.Color:
					if (destProp.colorValue != sourceProp.colorValue)
						destProp.colorValue = sourceProp.colorValue;
					break;
				case SerializedPropertyType.ObjectReference:
					if (destProp.objectReferenceValue != sourceProp.objectReferenceValue)
						destProp.objectReferenceValue = sourceProp.objectReferenceValue;
					break;/*
				case SerializedPropertyType.LayerMask:
					if (destProp.lay != sourceProp.boolValue)
						destProp.boolValue = sourceProp.boolValue;
					break;//*/
				case SerializedPropertyType.Enum:
					if (destProp.enumValueIndex != sourceProp.enumValueIndex)
						destProp.enumValueIndex = sourceProp.enumValueIndex;
					break;
				case SerializedPropertyType.Vector2:
					if (destProp.vector2Value != sourceProp.vector2Value)
						destProp.vector2Value = sourceProp.vector2Value;
					break;
				case SerializedPropertyType.Vector3:
					if (destProp.vector3Value != sourceProp.vector3Value)
						destProp.vector3Value = sourceProp.vector3Value;
					break;
				case SerializedPropertyType.Vector4:
					if (destProp.vector4Value != sourceProp.vector4Value)
						destProp.vector4Value = sourceProp.vector4Value;
					break;
				case SerializedPropertyType.Rect:
					if (destProp.rectValue != sourceProp.rectValue)
						destProp.rectValue = sourceProp.rectValue;
					break;
				case SerializedPropertyType.ArraySize:
					if (destProp.arraySize != sourceProp.arraySize)
						destProp.arraySize = sourceProp.arraySize;
					break;/*
				case SerializedPropertyType.Character:
					if (destProp.enumValueIndex != sourceProp.enumValueIndex)
						destProp.enumValueIndex = sourceProp.enumValueIndex;
					break;//*/
				case SerializedPropertyType.AnimationCurve:
					if (destProp.animationCurveValue != sourceProp.animationCurveValue)
						destProp.animationCurveValue = sourceProp.animationCurveValue;
					break;
				case SerializedPropertyType.Bounds:
					if (destProp.boundsValue != sourceProp.boundsValue)
						destProp.boundsValue = sourceProp.boundsValue;
					break;/*
				case SerializedPropertyType.Gradient:
					if (destProp.gr != sourceProp.vector4Value)
						destProp.vector4Value = sourceProp.vector4Value;
					break;//*/
				case SerializedPropertyType.Quaternion:
					if (destProp.quaternionValue != sourceProp.quaternionValue)
						destProp.quaternionValue = sourceProp.quaternionValue;
					break;
				case SerializedPropertyType.ExposedReference:
					if (destProp.exposedReferenceValue != sourceProp.exposedReferenceValue)
						destProp.exposedReferenceValue = sourceProp.exposedReferenceValue;
					break;/*
				case SerializedPropertyType.FixedBufferSize:
					if (destProp.fixedBufferSize != sourceProp.fixedBufferSize)
						destProp.fixedBufferSize = sourceProp.fixedBufferSize;
					break;//*/
				case SerializedPropertyType.Vector2Int:
					if (destProp.vector2IntValue != sourceProp.vector2IntValue)
						destProp.vector2IntValue = sourceProp.vector2IntValue;
					break;
				case SerializedPropertyType.Vector3Int:
					if (destProp.vector3IntValue != sourceProp.vector3IntValue)
						destProp.vector3IntValue = sourceProp.vector3IntValue;
					break;
				case SerializedPropertyType.RectInt:
					if (destProp.rectIntValue.Equals (sourceProp.rectIntValue))
						destProp.rectIntValue = sourceProp.rectIntValue;
					break;
				case SerializedPropertyType.BoundsInt:
					if (destProp.boundsIntValue != sourceProp.boundsIntValue)
						destProp.boundsIntValue = sourceProp.boundsIntValue;
					break;
			}
		}
	}
}
