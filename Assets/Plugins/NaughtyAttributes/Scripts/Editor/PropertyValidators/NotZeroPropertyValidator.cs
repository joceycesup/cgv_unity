using UnityEditor;

namespace NaughtyAttributes.Editor
{
    [PropertyValidator(typeof(NotZeroAttribute))]
    public class NotZeroPropertyValidator : PropertyValidator
    {
        public override void ValidateProperty(SerializedProperty property)
        {
            NotZeroAttribute notZeroAttribute = PropertyUtility.GetAttribute<NotZeroAttribute> (property);

            if (property.propertyType == SerializedPropertyType.Integer)
            {
                if ((int) property.intValue == 0) {
					if (notZeroAttribute.DefaultValue == 0) {
						string errorMessage = property.name + " must not equal 0";
						EditorDrawUtility.DrawHelpBox (errorMessage, MessageType.Error, context: PropertyUtility.GetTargetObject (property));
					}
					else {
						property.intValue = (int)notZeroAttribute.DefaultValue;
					}
				}
			}
            else if (property.propertyType == SerializedPropertyType.Float)
            {
				if (property.floatValue == 0f) {
					if (notZeroAttribute.DefaultValue == 0) {
						string errorMessage = property.name + " must not equal 0";
						EditorDrawUtility.DrawHelpBox (errorMessage, MessageType.Error, context: PropertyUtility.GetTargetObject (property));
					}
					else {
						property.floatValue = notZeroAttribute.DefaultValue;
					}
				}
			}
            else
            {
                string warning = notZeroAttribute.GetType().Name + " can be used only on int or float fields";
                EditorDrawUtility.DrawHelpBox(warning, MessageType.Warning, context: PropertyUtility.GetTargetObject(property));
            }
        }
    }
}
