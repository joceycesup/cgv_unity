/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID FOCUS_ON_TARGET_METHODE_000 = 1074370695U;
        static const AkUniqueID FOCUS_ON_TARGET_METHODE_001 = 1074370694U;
        static const AkUniqueID FOCUS_ON_TARGET_METHODE_002 = 1074370693U;
        static const AkUniqueID FOCUS_ON_TARGET_METHODE_003 = 1074370692U;
        static const AkUniqueID FOCUS_ON_TARGET_METHODE_004 = 1074370691U;
        static const AkUniqueID FOCUS_ON_TARGET_METHODE_005 = 1074370690U;
        static const AkUniqueID INNIT_LVL_01 = 1708677172U;
        static const AkUniqueID PLAY_AMB_CRIQUETS_LP = 3553076424U;
        static const AkUniqueID PLAY_AMB_OBJECT_BIRD_02_LP = 2882639062U;
        static const AkUniqueID PLAY_AMB_OBJECT_BIRDS_LP = 141953592U;
        static const AkUniqueID PLAY_AMB_OBJECT_WATER_LITTLE_LP_48K = 4131918732U;
        static const AkUniqueID PLAY_AMB_QUAI_LAPWATER_LP_48K = 987805345U;
        static const AkUniqueID PLAY_AMB_WATER_CANAUX_LP_48K = 1427940498U;
        static const AkUniqueID PLAY_AMB_WIND_TREES_LP = 3679710760U;
        static const AkUniqueID PLAY_BALISE_CHECK_DESTINATION = 61851316U;
        static const AkUniqueID PLAY_BALISE_IDLE = 3594429829U;
        static const AkUniqueID PLAY_BOUSSOLE_PITCH = 783989207U;
        static const AkUniqueID PLAY_BULLET_IMPACT_WALL_BRICK_DEBRIS_RND = 3199277275U;
        static const AkUniqueID PLAY_CIBLES_ACQUISE_RND = 2765664947U;
        static const AkUniqueID PLAY_DOG_PANT_RND = 2686240817U;
        static const AkUniqueID PLAY_DOG_WHINE_RND = 1843217929U;
        static const AkUniqueID PLAY_DRIPS = 4008983246U;
        static const AkUniqueID PLAY_FIRE = 3015324718U;
        static const AkUniqueID PLAY_FOCUS_METHODE_000 = 3489520674U;
        static const AkUniqueID PLAY_FOCUS_METHODE_001 = 3489520675U;
        static const AkUniqueID PLAY_FOCUS_METHODE_002 = 3489520672U;
        static const AkUniqueID PLAY_FOCUS_METHODE_003 = 3489520673U;
        static const AkUniqueID PLAY_FOCUS_METHODE_004 = 3489520678U;
        static const AkUniqueID PLAY_FOCUS_METHODE_005 = 3489520679U;
        static const AkUniqueID PLAY_HORLOGE = 1086738306U;
        static const AkUniqueID PLAY_LANCER_BALLE = 1164255670U;
        static const AkUniqueID PLAY_LVL2_AMB_BORDSRHONES_01_LP = 2825191648U;
        static const AkUniqueID PLAY_MENU_RADIAL_APPARITION = 526322005U;
        static const AkUniqueID PLAY_MENU_RADIAL_DISPARITION = 1243798086U;
        static const AkUniqueID PLAY_MUSIQUE_01 = 1432149431U;
        static const AkUniqueID PLAY_OLD_TV = 2410688630U;
        static const AkUniqueID PLAY_PLAYER_FOOTSTEPS_GRASS_STOP = 3323024045U;
        static const AkUniqueID PLAY_PLAYER_FOOTSTEPS_L_GRASS = 1276009619U;
        static const AkUniqueID PLAY_PLAYER_FOOTSTEPS_R_GRASS = 756392941U;
        static const AkUniqueID PLAY_SFX_AIM_OFF_TARGET_400HZ = 3908951774U;
        static const AkUniqueID PLAY_SFX_ARROW_IMPACT = 3388828742U;
        static const AkUniqueID PLAY_SFX_CIBLE_FALSE_OS = 1841624762U;
        static const AkUniqueID PLAY_SFX_CIBLE_TRUE_OS = 1385886693U;
        static const AkUniqueID PLAY_UI_CAMERA_CENTRE_OS = 1542794221U;
        static const AkUniqueID PLAY_UI_EST_OS = 2167445182U;
        static const AkUniqueID PLAY_UI_NORD_OS = 2731661985U;
        static const AkUniqueID PLAY_UI_OUEST_OS = 1356778802U;
        static const AkUniqueID PLAY_UI_SUD_OS = 1737743610U;
        static const AkUniqueID PLAY_VENTILO = 1694881383U;
        static const AkUniqueID PLAY_VOICE_RND = 880563941U;
        static const AkUniqueID PLAY_WEAPON_BOW_HANDLE_OS = 789729481U;
        static const AkUniqueID PLAY_WEAPON_BOW_PICKUP_OS = 948551671U;
        static const AkUniqueID PLAY_WEAPON_BOW_SHOT = 664134054U;
        static const AkUniqueID PLAY_WEAPON_GUN_HANDLE_OS = 2827320839U;
        static const AkUniqueID PLAY_WEAPON_GUN_PICKUP_OS = 3366531289U;
        static const AkUniqueID PLAY_WEAPON_GUN_RELOAD_OS = 3563108840U;
        static const AkUniqueID PLAY_WEAPON_GUN_SHOT = 3998561240U;
        static const AkUniqueID PLAY_WEAPON_MACHINEGUN_HANDLE = 3625500915U;
        static const AkUniqueID PLAY_WEAPON_MACHINEGUN_PICKUP = 2746665293U;
        static const AkUniqueID PLAY_WEAPON_MACHINEGUN_RELOAD = 3576845610U;
        static const AkUniqueID PLAY_WEAPON_MACHINEGUN_SHOT = 3361432925U;
        static const AkUniqueID PLAY_WEAPON_SHOTGUN_HANDLE = 1810191178U;
        static const AkUniqueID PLAY_WEAPON_SHOTGUN_PICKUP = 228785484U;
        static const AkUniqueID PLAY_WEAPON_SHOTGUN_RELOAD = 1775225375U;
        static const AkUniqueID PLAY_WEAPON_SHOTGUN_SHOT = 1325848824U;
        static const AkUniqueID PLAY_WOOD_CREAK = 425210220U;
        static const AkUniqueID STOP_BOUSSOLE_PITCH = 2988824753U;
        static const AkUniqueID STOP_FOCUS = 1887433856U;
        static const AkUniqueID STOP_WEAPON_GUN_RELOAD_OS = 2088624630U;
        static const AkUniqueID STOP_WEAPON_MACHINEGUN_RELOAD_OS = 3956258985U;
        static const AkUniqueID STOP_WEAPON_SHOTGUN_RELOAD_RND = 616750244U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace FOCUS
        {
            static const AkUniqueID GROUP = 249970651U;

            namespace STATE
            {
                static const AkUniqueID FOCUS_OFF = 1398623469U;
                static const AkUniqueID FOCUS_ON = 2911257633U;
            } // namespace STATE
        } // namespace FOCUS

    } // namespace STATES

    namespace SWITCHES
    {
        namespace AMMOCOUNT
        {
            static const AkUniqueID GROUP = 3342192252U;

            namespace SWITCH
            {
                static const AkUniqueID LASTAMMO = 345463335U;
                static const AkUniqueID OTHER = 2376466361U;
            } // namespace SWITCH
        } // namespace AMMOCOUNT

        namespace FORWARD_FACTOR
        {
            static const AkUniqueID GROUP = 1203397190U;

            namespace SWITCH
            {
                static const AkUniqueID AVANCER = 3804645229U;
                static const AkUniqueID IDLE = 1874288895U;
                static const AkUniqueID RECULER = 2236320491U;
            } // namespace SWITCH
        } // namespace FORWARD_FACTOR

        namespace GUN_SUPRESSED
        {
            static const AkUniqueID GROUP = 3213967530U;

            namespace SWITCH
            {
                static const AkUniqueID WITH = 1704837181U;
                static const AkUniqueID WITHOUT = 190872515U;
            } // namespace SWITCH
        } // namespace GUN_SUPRESSED

        namespace TEXTURES
        {
            static const AkUniqueID GROUP = 1276632493U;

            namespace SWITCH
            {
                static const AkUniqueID COBBLESTONE = 2383988601U;
                static const AkUniqueID GRASS = 4248645337U;
                static const AkUniqueID GRAVEL = 2185786256U;
                static const AkUniqueID LEAVES = 582824249U;
                static const AkUniqueID WATER = 2654748154U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace TEXTURES

        namespace WAVEFORM_COMPLEX
        {
            static const AkUniqueID GROUP = 1652042229U;

            namespace SWITCH
            {
                static const AkUniqueID SAWTOOTH_SINE = 4197299954U;
                static const AkUniqueID SINE_SQUARE = 1392791608U;
                static const AkUniqueID SQUARE_TRIANGULAR = 1684075876U;
                static const AkUniqueID TRIANGULAR_SAWTOOTH = 1600898006U;
            } // namespace SWITCH
        } // namespace WAVEFORM_COMPLEX

        namespace WAVEFORM_SIMPLE
        {
            static const AkUniqueID GROUP = 1227597005U;

            namespace SWITCH
            {
                static const AkUniqueID SAWTOOTH = 2095597218U;
                static const AkUniqueID SINE = 805220766U;
                static const AkUniqueID SQUARE = 1818333208U;
                static const AkUniqueID TRIANGULAR = 1094837866U;
            } // namespace SWITCH
        } // namespace WAVEFORM_SIMPLE

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID GP_AMMOCOUNT = 786303554U;
        static const AkUniqueID GP_CIBLEACQUISE_DIST = 84253788U;
        static const AkUniqueID GP_DIRECTION = 277020254U;
        static const AkUniqueID GP_DISTFOCUS_ABS_X = 3334051485U;
        static const AkUniqueID GP_DISTFOCUS_ABS_Y = 3334051484U;
        static const AkUniqueID GP_DISTFOCUS_SOMME_ABST_XY = 2563077794U;
        static const AkUniqueID GP_DISTFOCUS_Y = 966032737U;
        static const AkUniqueID GP_EARCONS_BOUSSOLE = 2137197471U;
        static const AkUniqueID GP_FILTRE_BACK = 1985747083U;
        static const AkUniqueID GP_FILTRE_HAUTEUR = 1312461920U;
        static const AkUniqueID GP_FORWARD_FACTOR = 3040381600U;
        static const AkUniqueID GP_VITESSE_MURS = 3665553784U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID PLAYER = 1069431850U;
        static const AkUniqueID PROTOTYPE_TARGET_V1 = 1451696657U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID AMB = 1117531639U;
        static const AkUniqueID BOUSSOLE_EARCONS = 3110890749U;
        static const AkUniqueID BOUSSOLE_LP = 3377547538U;
        static const AkUniqueID ENTITY = 3023338776U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_MUSIC_BUS = 48433064U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID UI = 1551306167U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID EFFECT_01 = 2288170604U;
        static const AkUniqueID REFLECT = 243379636U;
        static const AkUniqueID REFLECT_01 = 760079770U;
        static const AkUniqueID REFLECT_02 = 760079769U;
        static const AkUniqueID RVRB = 2157448753U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
