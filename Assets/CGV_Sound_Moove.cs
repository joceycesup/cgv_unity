﻿using UnityEngine;
using UnityEngine.AI;

public class CGV_Sound_Moove : MonoBehaviour {
	public Transform[] waypoints;
	public AK.Wwise.Event name_event;
	public float speed_min;
	public float speed_max;
	protected float speed;
	protected NavMeshAgent agent;
	protected int to_go;

	void Start () {
		speed = speed_min;
		this.agent = GetComponent<NavMeshAgent> ();
	}

	void FixedUpdate () {
		this.move ();
	}

	void move () {
		Transform currentWaypoint;
		float way;

		if (waypoints.Length <= 0 || waypoints[to_go] == null)
			return;
		currentWaypoint = waypoints[to_go];
		way = Vector3.Distance (this.transform.position, currentWaypoint.position);
		this.transform.position = Vector3.Lerp (transform.position, currentWaypoint.position, (speed * Time.fixedDeltaTime) / way);
		if (way < 1.0f)
			++to_go;
		if (to_go == waypoints.Length)
			to_go = 0;
	}

	private void OnTriggerEnter (Collider other) {
		if (other.gameObject.CompareTag ("Player") || other.gameObject.CompareTag ("Ball")) {
			Debug.Log ("You Touched " + this.name + " ! You Monster !");
			speed = speed_max;
			name_event.Post (this.gameObject, (uint)AkCallbackType.AK_EndOfEvent, callback_function);
		}
	}

	private void callback_function (object in_cookie, AkCallbackType in_type, AkCallbackInfo in_info) {
		speed = speed_min;
	}
}
